# SmartPy and SmartPy.io

SmartPy and SmartPy.io are an Intuitive and Effective Smart Contract Language and Platform for Tezos.

## Links

### SmartPy.io

[SmartPy.io](https://SmartPy.io)

### Source Code

Gitlab: https://gitlab.com/SmartPy/SmartPy

### On social media

Twitter: https://twitter.com/SmartPy_io

Medium: https://smartpy-io.medium.com

Telegram: https://t.me/SmartPy_io

## Installation

### Install dependencies

```sh
# Tezos submodule requires rust compiler 1.44.0
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup toolchain install 1.44.0
rustup override set 1.44.0
```

### Clone the SmartPy repository

You can clone the SmartPy repo by running:

```
git clone --recurse-submodules https://gitlab.com/SmartPy/SmartPy
```

If you forgot to specify `--recurse-submodules`, you can fix this with:

```
git submodule init
git submodule update
```

### Install Environment and Dependencies

There are two alternative ways to obtain SmartPy's dependencies: via Nix or by hand
(`naked`).

You can use one way now and switch to the other way at a later time by simply calling the same command.

#### Via Nix

If you haven't done so yet, install Nix: https://nixos.org/nix/download.html.

You can then run

```
env/nix/init
```

#### By hand

You first need to install the following packages with your favourite tool adapted to your setup: opam, npm,
python3, asciidoctor, and fswatch (optional).

You can then run

```
env/naked/init
```

### Use the SmartPy Environment

We can enter the adapted environment by either running a new shell

```
./envsh
```

or by launching commands with

```
./with_env <command>
```

When using `make`, `./with_env` is optional, but will be slightly faster in the `nix` environment.

## First Build

To build for the first time:

```
./with_env make full
```

## Regular Build

To build afterwards:

```
./with_env make
```

## Getting Started

Install all dependencies:

```sh
$ make frontend_install
```

To run the in-browser version locally, you can type:

It will be available at http://localhost:3000
```sh
$ make frontend_start
```

Build frontend for production:

The production bundle will be at [frontend/build](frontend/build)
```sh
$ make frontend_build
```

Run frontend tests:

This will detect any file changes inside [src](src) and re-run the affected tests automatically.
```sh
$ make frontend_test
```

### The frontend currently is composed by the following technologies:

| Tech        | Desc.                           |
| :---------- | :------------------------------ |
| ReactJS     | As web component framework      |
| Typescript  | Adds types on top of javascript |
| React Redux | As State Container              |
| Material UI | As UI framework                 |
| Axios       | As HTTP client                  |
| Jest        | As testing framework            |

#
### Front-end base structure

```bash
frontend/public
├── static
│   ├── static
│   │   ├── python  # Python Scripts
│   │   ├── js      # Javascript Scripts (Brython, js_of_ocaml, ...)
│   │   └── img
│   └── dev # Symbolic link from site/dev
└── utils

frontend/src
├── constants # Constant values should be stored here
├── features  # Features should be placed here (e.g. Pages[Home, Editor])
│   └── <feature>
│       ├── elements    # Icons, Buttons, ...
│       ├── containers  # Views Logic
│       ├── views       # Feature structure (A feature can have multiple views)
│       ├── components  # Composed elements (e.g. Menus)
│       ├── utils       # Util functions
│       └── ...
├── polyfills   # Polyfills should be placed here.
├── services    # Services (logger, toaster, ...)
├── store       # Redux Logic
├── __tests__   # Tests should be placed here
├── typings     # Type definitions for untyped modules should be placed here
└── utils       # Util functions should be placed here or in features/<feature>/utils
```


To get started with the command line version of SmartPy you can type

```
./with_env smartpy-cli/SmartPy.sh --help
```

In the case of a naked environment, this should also work:

```
smartpy-cli/SmartPy.sh --help
```

## Testing, Development and Contributions

Full tests can be launched by typing

```
./with_env make test
```

A slightly less thorough but much quicker target is

```
./with_env make test-quick
```

When iterating on a template, we can launch an even quicker

```
./with_env make test-quick-incremental
```

Updating switch-state at `env/{naked,nix}`

```sh
# Install the dependencies you need
opam install ...

# Update switch-state
cp _opam/.opam-switch/switch-state env/naked/switch.export
cp _opam/.opam-switch/switch-state env/nix/switch.export
```

Merge requests are welcome! Please launch tests before submitting.

## License

Please check our [license](LICENSE). For https://gitlab.com/SmartPy/SmartPy, we use MIT.

## Tools

We acknowledge and appreciate using tools from third parties that all come with their respective licenses

### Some Tools in [ext/](ext)

ace-builds
brython
eztz
flextesa
jquery
jquery-ui-themes-1.12.1
pako
tezos

### Other Tools

We also integrate directly: [conseilJS](https://github.com/Cryptonomic/ConseilJS), [pace](https://github.hubspot.com/pace/docs/welcome/), [@taquito/michel-codec](https://github.com/ecadlabs/taquito/tree/master/packages/taquito-michel-codec).
