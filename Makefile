# Copyright 2019-2020 Smart Chain Arena LLC.

SOURCES := $(wildcard ./smartML/**/*.ml) $(wildcard ./smartML/**/*.mli)

build: .phony
	@$(MAKE) -s _build/local-tezos-binaries.ok
	@$(MAKE) -s _build/zcash.ok
	@$(MAKE) -s _build/smartML.ok

all: .phony
	@$(MAKE) -s build
	@$(MAKE) -s manual
	@$(MAKE) test-common
	@git status --short
	@$(MAKE) test-mockup-only-warn

full: .phony test manual doc

clean: .phony
	rm -rf _build

test: .phony test-common test-mockup
	git status --short

test-quick: .phony test-common test-mockup-only-warn
	git status --short

test-common: .phony \
  test-scenario \
  test-scenario-michel \
  test-compile \
  test-unit

touch_ok=@mkdir -p $(@D); touch $@

########################################################################
# SmartML

DUNE_TARGETS := \
  smartML/web_js/smartmljs.bc.js \
  smartML/test/main.exe \
  smartML/smartpyc/smartpyc_js.bc.js \
  smartML/smartpyc/smartpyc_unix.exe

_build/smartML.ok: $(SOURCES) $(wildcard ./smartML/**/dune) $(wildcard ./smartML/**/.)
	dune build $(DUNE_TARGETS)
	smartML/smartpyc/stitch_smartpyc_js
	ocamlformat --check $(SOURCES)
	$(touch_ok)


########################################################################
# Documentation

doc: _build/doc.ok .phony

_build/doc.ok:
	dune build @doc
	@echo -e '\n\n\n'
	@echo -e 'SmartML doc:\n  open _build/default/_doc/_html/smartml/index.html'
	@echo -e 'SmartPy doc:\n  open frontend/public/reference.html'

manual: _build/manual.ok .phony

_build/manual.ok: doc/*.md doc/*.css private/build_completion.py _build/frontend/deps.ok
	@mkdir -p frontend/build
	@echo "asciidoctor ..."
	@asciidoctor doc/reference.md      -a toc=left -a linkcss -a stylesheet=reference.css --destination-dir frontend/public -d book
	@asciidoctor doc/faq.md            -a toc=left -a linkcss --destination-dir frontend/public
	@asciidoctor doc/editor_help.md    -a toc=left -a linkcss --destination-dir frontend/public
	@asciidoctor doc/releases.md       -a toc=left -a linkcss --destination-dir frontend/public
	@python3 private/build_completion.py
	@./wrapper --silent _build/frontend/build.log.txt npm run --prefix frontend prettier:write 'build/completers.ts'
	@cp frontend/build/completers.ts frontend/src/features/editor/constants/completers.ts
	$(touch_ok)


########################################################################
# Frontend and its tests

frontend-deps: .phony _build/frontend/deps.ok

_build/frontend/deps.ok: frontend/package.json
	./wrapper --silent _build/frontend/deps.log.txt npm install --prefix frontend
	$(touch_ok)

frontend-build: _build/frontend/build.ok

_build/frontend/build.ok: .phony _build/frontend/deps.ok frontend/package.json _build/smartML.ok $(wildcard frontend/src/**/*.js) $(wildcard frontend/src/**/*.ts*) _build/manual.ok
	./wrapper --silent _build/frontend/check.log.txt npm run prettier:check --prefix frontend
	./wrapper --silent _build/frontend/build.log.txt npm run build --prefix frontend
	$(touch_ok)

frontend-test: .phony _build/frontend/test.ok

_build/frontend/test.ok: _build/frontend/deps.ok frontend/package.json _build/smartML.ok $(wildcard frontend/src/**/*.js) $(wildcard frontend/src/**/*.ts*)
	npm run test --prefix frontend
	$(touch_ok)

frontend-start: .phony _build/frontend/deps.ok
	npm run start --prefix frontend

frontend-ci-test: .phony _build/frontend/deps.ok _build/frontend/build.ok
	./wrapper --silent _build/frontend/ci-test.log.txt npm run ci-test --prefix frontend

frontend-fmt: .phony
	npm run prettier:fix --prefix frontend


########################################################################
# SmartML tests

WANTED=$(basename $(notdir $(filter-out $< .phony, $^)))
ACTUAL=$(notdir $(wildcard $</*))
remove_unused=@cd $<; rm -rfv $(filter-out $(WANTED), $(ACTUAL))

TEMPLATES := $(wildcard python/templates/*.py)

FLAG_HTML=$(if $(filter $(addprefix python/templates/, welcome.py stateChannels.py),$<), --html,)

FLAG_NATIVE=$(if $(filter $(addprefix python/templates/, FA1.2.py	\
  FA2.py bakingSwap.py bls12381.py \
  testHashFunctions.py bls12_381.py int_bls12_381_fr.py			\
  neg_bls12_381_g1.py neg_bls12_381_g2.py neg_bls12_381_fr.py		\
  add_bls12_381_g1.py add_bls12_381_g2.py add_bls12_381_fr.py		\
  mul_bls12_381_g1_fr.py mul_bls12_381_g2_fr.py				\
  mul_bls12_381_fr_fr.py mul_bls12_381_int_fr.py			\
  pairing_check_bls12_381.py bls12_381_conv.py voting_power.py		\
  test_pack.py sapling2.py stateChannels.py testCheckSignature.py),$<),, --native)

FLAG_DECOMPILE=$(if $(filter $(addprefix python/templates/, FA1.2.py	\
  FA2.py atomicSwap.py bakingSwap.py collatz.py escrow.py fifo.py	\
  game_of_life.py lambdas.py minikitties.py multisig.py nim.py		\
  nimLift.py oracle.py send_back.py shuffle.py stringManipulations.py	\
  syntax.py testCheckSignature.py testDiv.py testEmpty.py testFor.py	\
  testHashFunctions.py testSend.py testTimestamp.py testVariant.py	\
  test_import.py tictactoeFactory.py worldCalculator.py fibonacci.py	\
  check_dfs.py), $<), --no-decompile, --decompile)

FLAG_PROTOCOL=$(if $(filter $(addprefix python/templates/, test_pack.py test_baker_hash.py), $<), --protocol florence, --protocol edo)

TEST_SCENARIO := $(addprefix _build/test/scenario/,		\
  $(filter-out __init__.ok, $(TEMPLATES:python/templates/%.py=%.ok)))

_build/test/scenario/%.ok: python/templates/%.py _build/smartML.ok
	@./wrapper _build/test/scenario/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario/$* --purge $(FLAG_HTML) $(FLAG_NATIVE)
	$(touch_ok)

test-scenario: test_baselines/scenario $(TEST_SCENARIO) .phony
	$(remove_unused)


TEST_SCENARIO_MICHEL := $(addprefix _build/test/scenario_michel/,	\
  $(filter-out __init__.ok, $(TEMPLATES:python/templates/%.py=%.ok)))

_build/test/scenario_michel/%.ok: python/templates/%.py _build/smartML.ok
	@./wrapper _build/test/scenario_michel/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_michel/$* --simplify-via-michel --purge $(FLAG_NATIVE)
	$(touch_ok)

test-scenario-michel: test_baselines/scenario_michel $(TEST_SCENARIO_MICHEL) .phony
	$(remove_unused)


TEST_MOCKUP := $(addprefix test_baselines/mockup/, $(filter-out	\
  __init__.tsv test_expression_compilation.tsv, $(TEMPLATES:python/templates/%.py=%.tsv)))

test_baselines/mockup/%.tsv: python/templates/%.py _build/local-tezos-binaries.ok _build/zcash.ok _build/smartML.ok
	@rm -rf _build/test/mockup/$*/ test_baselines/mockup/$*.tsv
	@./wrapper --silent _build/test/mockup/$*.log.txt smartpy-cli/SmartPy.sh test-mockup $< _build/test/mockup/$* --purge $(FLAG_PROTOCOL)
	@cat _build/test/mockup/$*/mockup/*/result.tsv \
          > test_baselines/mockup/$*.tsv

test-mockup: $(TEST_MOCKUP) .phony

test-mockup-only-warn:
	@for i in $(TEST_MOCKUP); do \
	  test -f $$i || (echo $$i does not exist; false) ; \
	done

test-mockup-only-new:
	$(MAKE) $(filter-out $(wildcard test_baselines/mockup/*.tsv), $(TEST_MOCKUP))


TEST_COMPILE := $(addprefix _build/test/compile/, $(filter-out	\
  __init__.ok, $(TEMPLATES:python/templates/%.py=%.ok)))

_build/test/compile/%.ok: python/templates/%.py _build/smartML.ok
	@./wrapper _build/test/compile/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile/$* --purge $(FLAG_PROTOCOL) $(FLAG_DECOMPILE) $(FLAG_NATIVE)
	$(touch_ok)

test-compile: test_baselines/compile $(TEST_COMPILE) .phony
	$(remove_unused)


TEST_UNIT=_build/test_unit.ok

$(TEST_UNIT): _build/smartML.ok
	@./wrapper --silent _build/test/test_unit.log.txt dune build @smartML/test/runtest
	$(touch_ok)

test-unit: .phony $(TEST_UNIT)


########################################################################
# Local Tezos build

LOCAL_TEZOS_TARGETS=\
  src/bin_sandbox/main.exe \
  src/bin_client/main_client.exe \
  src/bin_client/main_admin.exe \
  src/bin_node/main.exe

local-tezos: .phony _build/local-tezos.ok

export RUSTUP_TOOLCHAIN=1.44.0

_build/local-tezos.ok: ext/tezos/tezos-master/*
	rustup toolchain install $$RUSTUP_TOOLCHAIN --profile minimal
	cd ext/tezos-current; make build-deps
	cd ext/tezos-current; eval $$(opam env) && make
	cd ext/tezos-current; eval $$(opam env) && dune build src/bin_sandbox/main.exe
	$(touch_ok)

_build/local-tezos-binaries.ok: _build/local-tezos.ok
	@mkdir -p _build/tezos-bin/
	cp -f ext/tezos-current/_build/default/src/bin_sandbox/main.exe _build/tezos-bin/tezos-sandbox
	cp -f ext/tezos-current/_build/default/src/bin_client/main_client.exe _build/tezos-bin/tezos-client
	cp -f ext/tezos-current/_build/default/src/bin_client/main_admin.exe _build/tezos-bin/tezos-admin-client
	cp -f ext/tezos-current/_build/default/src/bin_node/main.exe _build/tezos-bin/tezos-node
	printf '#!/bin/sh\nexit 42\n' > _build/tezos-bin/tezos-baker-alpha
	printf '#!/bin/sh\nexit 42\n' > _build/tezos-bin/tezos-endorser-alpha
	printf '#!/bin/sh\nexit 42\n' > _build/tezos-bin/tezos-accuser-alpha
	@chmod +x _build/tezos-bin/tezos-baker-alpha
	@chmod +x _build/tezos-bin/tezos-endorser-alpha
	@chmod +x _build/tezos-bin/tezos-accuser-alpha
	$(touch_ok)

_build/zcash.ok:
	mkdir -p ~/.zcash-params
	cd ~/.zcash-params; \
	for f in sapling-output.params sapling-spend.params sprout-groth16.params; do \
	  [ -e $$f ] || curl -OL https://download.z.cash/downloads/$$f; \
	done
	@mkdir -p $(@D); touch $@


########################################################################
# Miscellaneous phony targets

fmt_check: .phony
	ocamlformat --check $(SOURCES)

fmt_fix: .phony
	ocamlformat --inplace $(SOURCES)

www: .phony
	(sleep 1; open http://localhost:$(PORT)) &
	cd frontend/build && python3 -m http.server $(PORT)

open-index: .phony
	open _build/default/_doc/_html/smartml/index.html

open-manual: .phony manual
	open frontend/public/reference.html


########################################################################
# Makefile and environment plumbing

.phony:

.PHONY: .phony


ifeq (,$(wildcard env/current))
  $(error Please run './env/nix/init' or './env/naked/init' before running make)
endif

ifndef SMARTPY_ENV
  ifeq ($(shell readlink env/current),nix/)
    $(warning Initializing a new nix shell for each command. Run 'make' under './envsh' or directly as './with_env make' for better performance.)
  endif
  SHELL=./envsh
endif

MAKEFLAGS += --warn-undefined-variables --no-builtin-rules --output-sync

# Disable further implicit rules, resulting in a slight speed-up:
.SUFFIXES:
