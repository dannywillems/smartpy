import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(counter = 0, onEven = sp.contract_address(Contract0), onOdd = sp.contract_address(Contract1))

  @sp.entry_point
  def reset(self, params):
    self.data.counter = 0

  @sp.entry_point
  def run(self, params):
    sp.if params > 1:
      self.data.counter += 1
      sp.if (params % 2) == 0:
        sp.transfer(sp.record(k = sp.self_entry_point('run'), x = params), sp.tez(0), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), self.data.onEven).open_some())
      sp.else:
        sp.transfer(sp.record(k = sp.self_entry_point('run'), x = params), sp.tez(0), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), self.data.onOdd).open_some())