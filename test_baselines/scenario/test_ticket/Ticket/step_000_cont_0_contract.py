import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = sp.none, y = sp.none)

  @sp.entry_point
  def auto_call(self, params):
    ticket_10 = sp.local("ticket_10", sp.ticket(1, 43))
    sp.transfer(ticket_10.value, sp.tez(0), sp.self_entry_point('run'))

  @sp.entry_point
  def run(self, params):
    sp.set_type(params, sp.TTicket(sp.TInt))
    ticket_15_data, ticket_15_copy = sp.match_tuple(sp.read_ticket_raw(params), "ticket_15_data", "ticket_15_copy")
    ticket_15_ticketer, ticket_15_content, ticket_15_amount = sp.match_tuple(ticket_15_data, "ticket_15_ticketer", "ticket_15_content", "ticket_15_amount")
    ticket_16 = sp.local("ticket_16", sp.ticket('abc', 42))
    self.data.y = sp.some(ticket_16.value)
    ticket1_17, ticket2_17 = sp.match_tuple(sp.split_ticket_raw(ticket_15_copy, (ticket_15_amount // 3, sp.as_nat(ticket_15_amount - (ticket_15_amount // 3)))).open_some(), "ticket1_17", "ticket2_17")
    self.data.x = sp.some(sp.join_tickets_raw((ticket2_17, ticket1_17)).open_some())

  @sp.entry_point
  def run2(self, params):
    sp.set_type(params, sp.TRecord(t = sp.TTicket(sp.TInt), x = sp.TInt).layout(("t", "x")))
    x_23, t_23 = sp.match_record(params, "x", "t")
    sp.verify(x_23 == 42)
    ticket_25_data, ticket_25_copy = sp.match_tuple(sp.read_ticket_raw(t_23), "ticket_25_data", "ticket_25_copy")
    ticket_25_ticketer, ticket_25_content, ticket_25_amount = sp.match_tuple(ticket_25_data, "ticket_25_ticketer", "ticket_25_content", "ticket_25_amount")
    ticket_26 = sp.local("ticket_26", sp.ticket('abc', 42))
    self.data.y = sp.some(ticket_26.value)
    ticket1_27, ticket2_27 = sp.match_tuple(sp.split_ticket_raw(ticket_25_copy, (ticket_25_amount // 3, sp.as_nat(ticket_25_amount - (ticket_25_amount // 3)))).open_some(), "ticket1_27", "ticket2_27")
    self.data.x = sp.some(sp.join_tickets_raw((ticket2_27, ticket1_27)).open_some())