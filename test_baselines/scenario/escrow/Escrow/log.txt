Comment...
 h1: Escrow
Creating contract
 -> (Pair (Pair (Pair 0 0) (Pair "tz0FakeBob" "1970-01-01T00:02:03Z")) (Pair (Pair 4000000 50000000) (Pair 0x62326268874163bc6a181ee8a3bd24feb063c4 "tz0FakeAlice")))
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_storage.tz 1
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_storage.json 16
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_storage.py 1
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_types.py 7
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_contract.tz 275
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_contract.json 305
 => test_baselines/scenario/escrow/Escrow/step_001_cont_0_contract.py 34
 => test_baselines/scenario/escrow/Escrow/step_002_cont_0_params.py 1
 => test_baselines/scenario/escrow/Escrow/step_002_cont_0_params.tz 1
 => test_baselines/scenario/escrow/Escrow/step_002_cont_0_params.json 1
Executing addBalanceOwner(sp.record())...
 -> (Pair (Pair (Pair 0 50000000) (Pair "tz0FakeBob" "1970-01-01T00:02:03Z")) (Pair (Pair 4000000 50000000) (Pair 0x62326268874163bc6a181ee8a3bd24feb063c4 "tz0FakeAlice")))
 => test_baselines/scenario/escrow/Escrow/step_003_cont_0_params.py 1
 => test_baselines/scenario/escrow/Escrow/step_003_cont_0_params.tz 1
 => test_baselines/scenario/escrow/Escrow/step_003_cont_0_params.json 1
Executing addBalanceCounterparty(sp.record())...
 -> (Pair (Pair (Pair 4000000 50000000) (Pair "tz0FakeBob" "1970-01-01T00:02:03Z")) (Pair (Pair 4000000 50000000) (Pair 0x62326268874163bc6a181ee8a3bd24feb063c4 "tz0FakeAlice")))
Comment...
 h3: Erronous secret
 => test_baselines/scenario/escrow/Escrow/step_005_cont_0_params.py 1
 => test_baselines/scenario/escrow/Escrow/step_005_cont_0_params.tz 1
 => test_baselines/scenario/escrow/Escrow/step_005_cont_0_params.json 1
Executing claimCounterparty(sp.record(secret = sp.bytes('0x01223343')))...
 -> --- Expected failure in transaction --- WrongCondition in line 37: self.data.hashedSecret == sp.blake2b(params.secret)
Comment...
 h3: Correct secret
 => test_baselines/scenario/escrow/Escrow/step_007_cont_0_params.py 1
 => test_baselines/scenario/escrow/Escrow/step_007_cont_0_params.tz 1
 => test_baselines/scenario/escrow/Escrow/step_007_cont_0_params.json 1
Executing claimCounterparty(sp.record(secret = sp.bytes('0x01223344')))...
 -> (Pair (Pair (Pair 0 0) (Pair "tz0FakeBob" "1970-01-01T00:02:03Z")) (Pair (Pair 4000000 50000000) (Pair 0x62326268874163bc6a181ee8a3bd24feb063c4 "tz0FakeAlice")))
