import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x1 = sp.record(a = 0, b = 1, c = True, d = 'abc'), x2 = sp.record(a = 0, b = 1, c = True, d = 'abc'), x3 = sp.record(a = 0, b = 1, c = True, d = 'abc'), x4 = sp.record(a = 0, b = 1, c = True, d = 'abc'), y = (0, 1, True, 'abc'), z = sp.record(a = 0, b = 1, c = True, d = sp.record(e = 1, f = 'x')))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(self.data.x2, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))))
    sp.set_type(self.data.x3, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(((("a", "b"), "c"), "d")))
    sp.set_type(self.data.x4, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout((("a", "b"), ("c", "d"))))
    with sp.match_record(self.data.x1, fields = ["d", "a", "c", "b"]) as d_21, a_21, c_21, b_21:
      sp.verify((abs(b_21.value)) == (a_21.value + 1))
      d_21.value = 'xyz'
      sp.result(sp.record(a = a_21.value, b = b_21.value, c = c_21.value, d = d_21.value))
    with sp.match_record(self.data.x2, fields = ["d", "a", "c", "b"]) as d_26, a_26, c_26, b_26:
      sp.verify((abs(b_26.value)) == (a_26.value + 1))
      d_26.value = 'xyz'
      sp.result(sp.record(a = a_26.value, b = b_26.value, c = c_26.value, d = d_26.value))
    with sp.match_record(self.data.x3, fields = ["d", "a", "c", "b"]) as d_31, a_31, c_31, b_31:
      sp.verify((abs(b_31.value)) == (a_31.value + 1))
      d_31.value = 'xyz'
      sp.result(sp.record(a = a_31.value, b = b_31.value, c = c_31.value, d = d_31.value))
    with sp.match_record(self.data.x4, fields = ["d", "a", "c", "b"]) as d_36, a_36, c_36, b_36:
      sp.verify((abs(b_36.value)) == (a_36.value + 1))
      d_36.value = 'xyz'
      sp.result(sp.record(a = a_36.value, b = b_36.value, c = c_36.value, d = d_36.value))

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data.x1, fields = ["b", "a", "c"]) as b_44, a_44, c_44:
      sp.verify((abs(b_44.value)) == (a_44.value + 1))
      sp.result(sp.record(a = a_44.value, b = b_44.value, c = c_44.value, d = 'xyz'))

  @sp.entry_point
  def ep3(self, params):
    with sp.match_tuple(self.data.y, "a", "b", "c", "d") as a, b, c, d:
      sp.verify((abs(b.value)) == (a.value + 1))
      d.value = 'xyz'
      sp.result((a.value, b.value, c.value, d.value))

  @sp.entry_point
  def ep4(self, params):
    with sp.match_record(self.data.x1, fields = ["d", "a"]) as d_59, a_59:
      sp.result(self.data.x1)

  @sp.entry_point
  def ep5(self, params):
    with sp.match_record(self.data.x1, fields = ["d", "a", "c", "b"]) as d_74, a_74, c_74, b_74:
      sp.send(params, sp.tez(0))
      d_74.value = 'xyz'
      sp.result(sp.record(a = a_74.value, b = b_74.value, c = c_74.value, d = d_74.value))
    self.data.x1.a += 5

  @sp.entry_point
  def ep6(self, params):
    with sp.match_record(self.data.z, fields = ["a", "b", "c", "d"]) as a_83, b_83, c_83, d_83:
      with sp.match_record(d_83.value, fields = ["e", "f"]) as e_85, f_85:
        b_83.value = 100
        e_85.value = 2
        f_85.value = 'y'
        sp.result(sp.record(e = e_85.value, f = f_85.value))
      sp.result(sp.record(a = a_83.value, b = b_83.value, c = c_83.value, d = d_83.value))

  @sp.entry_point
  def ep7(self, params):
    with sp.match_record(self.data.z.d, fields = ["e", "f"]) as e_95, f_95:
      e_95.value = 3
      f_95.value = 'z'
      sp.result(sp.record(e = e_95.value, f = f_95.value))