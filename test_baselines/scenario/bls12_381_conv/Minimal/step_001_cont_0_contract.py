import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(fr = sp.none)

  @sp.entry_point
  def entry_point_1(self, params):
    self.data.fr = sp.some(sp.mul(sp.fst(sp.ediv(params, sp.mutez(1)).open_some()), sp.bls12_381_fr('0x01')))