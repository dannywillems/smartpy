import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(h = lambda(sp.TLambda(sp.TRecord(a = sp.TSaplingState(12), b = sp.TSaplingTransaction(12)).layout(("a", "b")), sp.TOption(sp.TPair(sp.TInt, sp.TSaplingState(12))))), x = 12, y = sp.none, z = lambda(sp.TLambda(sp.TIntOrNat, sp.TPair(sp.TIntOrNat, sp.TSaplingState(8)))))

  @sp.entry_point
  def entry_point_1(self, params):
    sp.set_type(params.s, sp.TSaplingState(15))
    self.data.y = sp.some(sp.sapling_verify_update(params.s, params.t))