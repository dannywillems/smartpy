import smartpy as sp

class Delegating(sp.Contract):
    @sp.entry_point
    def delegate(self, baker):
        sp.set_delegate(baker)

@sp.add_test(name = "Delegating Pre Florence")
def preFlorenceTest():
    preFlorenceScenario = sp.test_scenario()
    preFlorenceScenario.h1("PreFlorence")
    preFlorenceScenario.add_flag("protocol", "edo")
    pre = Delegating()
    preFlorenceScenario += pre
    preFlorenceScenario += pre.delegate(sp.some(sp.key_hash("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4")))

@sp.add_test(name = "Delegating Post Florence")
def postFlorenceTest():
    postFlorenceScenario = sp.test_scenario()
    postFlorenceScenario.h1("PostFlorence")
    postFlorenceScenario.add_flag("protocol", "florence")
    post = Delegating()
    postFlorenceScenario += post
    postFlorenceScenario += post.delegate(sp.some(sp.baker_hash("SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv")))

sp.add_compilation_target("set_delegate_pre_florence", Delegating(), flags = [("protocol", "edo")])
sp.add_compilation_target("set_delegate_post_florence", Delegating(), flags = [("protocol", "florence")])
