import smartpy as sp

class Mul__Bls12_381_g1_fr(sp.Contract):
    def __init__(self):
        self.init(g1 = sp.none)

    """
    MUL: Multiply a curve point or field element by a scalar field element. Fr
    elements can be built from naturals by multiplying by the unit of Fr using PUSH bls12_381_fr 1; MUL. Note
    that the multiplication will be computed using the natural modulo the order
    of Fr.

    :: bls12_381_g1 : bls12_381_fr : 'S -> bls12_381_g1 : 'S
    """
    @sp.entry_point
    def mul(self, pair):
        self.data.g1 = sp.some(sp.mul(sp.fst(pair), sp.snd(pair)))

@sp.add_test(name = "Mul__Bls12_381_g1_fr")
def test():
    c1 = Mul__Bls12_381_g1_fr();

    scenario = sp.test_scenario()
    scenario += c1

    scenario += c1.mul(
        sp.pair(
            sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"),
            sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
        )
    );
