import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(fr = sp.none)

  @sp.entry_point
  def negate(self, params):
    sp.set_type(params.fr, sp.TBls12_381_fr)
    self.data.fr = sp.some(- params.fr)