import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(g2 = sp.none)

  @sp.entry_point
  def add(self, params):
    self.data.g2 = sp.some(sp.fst(params) + sp.snd(params))