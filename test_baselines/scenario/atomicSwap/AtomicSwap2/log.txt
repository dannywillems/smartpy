Comment...
 h1: Atomic Swap
Comment...
 h1: c1
Creating contract
 -> (Pair (Pair "tz0FakeRobert" "1970-01-01T00:00:50Z") (Pair 0x6232622a57b4adcf1de7bee13349df8a9f256b (Pair 12 "tz0FakeAlice")))
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_storage.tz 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_storage.json 7
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_storage.py 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_types.py 7
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_contract.tz 217
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_contract.json 229
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_002_cont_0_contract.py 28
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_003_cont_0_params.py 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_003_cont_0_params.tz 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_003_cont_0_params.json 1
Executing knownSecret(sp.record(secret = sp.bytes('0x12345678aa')))...
 -> --- Expected failure in transaction --- WrongCondition in line 36: self.data.hashedSecret == sp.blake2b(params.secret)
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_004_cont_0_params.py 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_004_cont_0_params.tz 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_004_cont_0_params.json 1
Executing knownSecret(sp.record(secret = sp.bytes('0x12345678aabb')))...
 -> (Pair (Pair "tz0FakeRobert" "1970-01-01T00:00:50Z") (Pair 0x6232622a57b4adcf1de7bee13349df8a9f256b (Pair 0 "tz0FakeAlice")))
Comment...
 h1: c2
Creating contract
 -> (Pair (Pair "tz0FakeAlice" "1970-01-01T00:00:50Z") (Pair 0x6232622a57b4adcf1de7bee13349df8a9f256b (Pair 20 "tz0FakeRobert")))
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_storage.tz 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_storage.json 7
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_sizes.csv 2
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_storage.py 1
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_types.py 7
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_contract.tz 217
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_contract.json 229
 => test_baselines/scenario/atomicSwap/AtomicSwap2/step_006_cont_1_contract.py 28
Comment...
 h2: C2.export()
Comment...
 p: (storage (record 7 (counterparty (reduce (attr (account_of_seed "Alice" 54) "address" 54) 54)) (epoch (literal (timestamp 50) 63)) (hashedSecret (hashCrypto "BLAKE2B" (literal (bytes "0x12345678aabb") 59) 59)) (notional (literal (mutez 20) 63)) (owner (reduce (attr (account_of_seed "Robert" 55) "address" 55) 55)))
storage_type (())
messages ((allSigned True ((verify (neq (attr (data) "notional" 14) (literal (mutez 0) 14) 14) 14) (verify (eq (attr (data) "owner" 22) (sender) 15) 15) (set (operations 23) (cons (transfer (unit) (attr (data) "notional" 14) (openVariant (contract "" "unit" (attr (data) "counterparty" 23) 23) "Some" "None" 23) 23) (operations 23) 23) 23) (set (attr (data) "notional" 14) (literal (mutez 0) 18) 18))) (cancelSwap True ((verify (neq (attr (data) "notional" 14) (literal (mutez 0) 14) 14) 14) (verify (eq (attr (data) "owner" 22) (sender) 15) 15) (verify (lt (attr (data) "epoch" 29) (now) 29) 29) (set (operations 30) (cons (transfer (unit) (attr (data) "notional" 14) (openVariant (contract "" "unit" (attr (data) "owner" 22) 30) "Some" "None" 30) 30) (operations 30) 30) 30) (set (attr (data) "notional" 14) (literal (mutez 0) 18) 18))) (knownSecret True ((verify (neq (attr (data) "notional" 14) (literal (mutez 0) 14) 14) 14) (verify (eq (attr (data) "counterparty" 23) (sender) 15) 15) (verify (eq (attr (data) "hashedSecret" 36) (hashCrypto "BLAKE2B" (attr (params 34) "secret" 36) 36) 36) 36) (set (operations 37) (cons (transfer (unit) (attr (data) "notional" 14) (openVariant (contract "" "unit" (attr (data) "counterparty" 23) 37) "Some" "None" 37) 37) (operations 37) 37) 37) (set (attr (data) "notional" 14) (literal (mutez 0) 18) 18))))
flags ()
globals ()
views ()
entry_points_layout ()
initial_metadata ()
balance ())
