import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, nbMoves = sp.TInt, nextPlayer = sp.TInt, winner = sp.TInt).layout((("deck", "draw"), ("nbMoves", ("nextPlayer", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))
    sp.failwith('[Error: to_cmd: let [s22; s23; s24] =
          match Eq(Compare
                     ( (Getn 6)(Pair
                                  ( Pair(__storage.deck, __storage.draw)
                                  , Pair
                                      ( __storage.nbMoves
                                      , Pair
                                          ( __storage.nextPlayer
                                          , __storage.winner
                                          )
                                      )
                                  ))
                     , 0
                     )) with
          | True _ ->
              [ Not(__storage.draw)
              ; __parameter
              ; Pair
                  ( Pair(__storage.deck, __storage.draw)
                  , Pair
                      ( __storage.nbMoves
                      , Pair(__storage.nextPlayer, __storage.winner)
                      )
                  )
              ]
          | False _ ->
              [ False
              ; __parameter
              ; Pair
                  ( Pair(__storage.deck, __storage.draw)
                  , Pair
                      ( __storage.nbMoves
                      , Pair(__storage.nextPlayer, __storage.winner)
                      )
                  )
              ]
          end
        match s22 with
        | True _ ->
            let [s43; s44; s45] =
              match Ge(Compare(Car(s23), 0)) with
              | True _ -> [ Gt(Compare(3, Car(s23))); s23; s24 ]
              | False _ -> [ False; s23; s24 ]
              end
            match s43 with
            | True _ ->
                let [s64; s65; s66] =
                  match Ge(Compare((Getn 3)(s44), 0)) with
                  | True _ -> [ Gt(Compare(3, (Getn 3)(s44))); s44; s45 ]
                  | False _ -> [ False; s44; s45 ]
                  end
                match s64 with
                | True _ ->
                    match Eq(Compare((Getn 4)(s65), (Getn 5)(s66))) with
                    | True _ ->
                        match Get(Car(s65), Car(Car(s66))) with
                        | Some s104 ->
                            match Get((Getn 3)(s65), s104) with
                            | Some s116 ->
                                match Eq(Compare(s116, 0)) with
                                | True _ ->
                                    match Get(Car(s65), Car(Car(s66))) with
                                    | Some s148 ->
                                        let x191 =
                                          Pair
                                            ( Add(1, Car(Cdr(s66)))
                                            , Cdr(Cdr(s66))
                                            )
                                        let x190 =
                                          Pair
                                            ( Update(Car(s65), Some_(Update((
                                          Getn 3)(s65), Some_((Getn 4)(s65)), s148)), Car(Car(s66)))
                                            , Cdr(Car(s66))
                                            )
                                        let x223 =
                                          Pair
                                            ( x190
                                            , Pair
                                                ( Car(x191)
                                                , Pair
                                                    ( Sub
                                                        ( 3
                                                        , (Getn 5)(Pair
                                                                    ( x190
                                                                    , x191
                                                                    ))
                                                        )
                                                    , Cdr(Cdr(x191))
                                                    )
                                                )
                                            )
                                        match Get(Car(s65), Car(Car(x223))) with
                                        | Some s237 ->
                                            match Get(0, s237) with
                                            | Some s245 ->
                                                let [s312; s313; s314] =
                                                  match Neq(Compare(s245, 0)) with
                                                  | True _ ->
                                                      match Get
                                                              ( Car(s65)
                                                              , Car(Car(x223))
                                                              ) with
                                                      | Some s272 ->
                                                          match Get(1, s272) with
                                                          | Some s279 ->
                                                              match Get
                                                                    ( Car(s65)
                                                                    , Car(Car(x223))
                                                                    ) with
                                                              | Some s296 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , s296
                                                                    ) with
                                                                  | Some s304 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s304
                                                                    , s279
                                                                    ))
                                                                    ; s65
                                                                    ; x223
                                                                    ]
                                                                  | None _ ->
                                                                    Failwith(31)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(23)
                                                              end
                                                          | None _ ->
                                                              Failwith(31)
                                                          end
                                                      | None _ ->
                                                          Failwith(23)
                                                      end
                                                  | False _ ->
                                                      [ False; s65; x223 ]
                                                  end
                                                let [s374; s375; s376] =
                                                  match s312 with
                                                  | True _ ->
                                                      match Get
                                                              ( Car(s313)
                                                              , Car(Car(s314))
                                                              ) with
                                                      | Some s334 ->
                                                          match Get(2, s334) with
                                                          | Some s341 ->
                                                              match Get
                                                                    ( Car(s313)
                                                                    , Car(Car(s314))
                                                                    ) with
                                                              | Some s358 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , s358
                                                                    ) with
                                                                  | Some s366 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s366
                                                                    , s341
                                                                    ))
                                                                    ; s313
                                                                    ; s314
                                                                    ]
                                                                  | None _ ->
                                                                    Failwith(31)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(23)
                                                              end
                                                          | None _ ->
                                                              Failwith(31)
                                                          end
                                                      | None _ ->
                                                          Failwith(23)
                                                      end
                                                  | False _ ->
                                                      [ False; s313; s314 ]
                                                  end
                                                let [s435; s436] =
                                                  match s374 with
                                                  | True _ ->
                                                      match Get
                                                              ( Car(s375)
                                                              , Car(Car(s376))
                                                              ) with
                                                      | Some s408 ->
                                                          match Get(0, s408) with
                                                          | Some s417 ->
                                                              [ s375
                                                              ; Pair
                                                                  ( Car(s376)
                                                                  , Pair
                                                                    ( Car(Cdr(s376))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s376)))
                                                                    , s417
                                                                    )
                                                                    )
                                                                  )
                                                              ]
                                                          | None _ ->
                                                              Failwith(32)
                                                          end
                                                      | None _ ->
                                                          Failwith(23)
                                                      end
                                                  | False _ -> [ s375; s376 ]
                                                  end
                                                match Get(0, Car(Car(s436))) with
                                                | Some s446 ->
                                                    match Get
                                                            ( (Getn 3)(s435)
                                                            , s446
                                                            ) with
                                                    | Some s458 ->
                                                        let [s525; s526; s527
                                                              ] =
                                                          match Neq(Compare
                                                                    ( s458
                                                                    , 0
                                                                    )) with
                                                          | True _ ->
                                                              match Get
                                                                    ( 1
                                                                    , Car(Car(s436))
                                                                    ) with
                                                              | Some s478 ->
                                                                  match 
                                                                  Get
                                                                    ( (
                                                                  Getn 3)(s435)
                                                                    , s478
                                                                    ) with
                                                                  | Some s492 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s436))
                                                                    ) with
                                                                    | Some s505 ->
                                                                    match 
                                                                    Get
                                                                    ( (
                                                                    Getn 3)(s435)
                                                                    , s505
                                                                    ) with
                                                                    | Some s517 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s517
                                                                    , s492
                                                                    ))
                                                                    ; s435
                                                                    ; s436
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(24)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | False _ ->
                                                              [ False
                                                              ; s435
                                                              ; s436
                                                              ]
                                                          end
                                                        let [s587; s588; s589
                                                              ] =
                                                          match s525 with
                                                          | True _ ->
                                                              match Get
                                                                    ( 2
                                                                    , Car(Car(s527))
                                                                    ) with
                                                              | Some s540 ->
                                                                  match 
                                                                  Get
                                                                    ( (
                                                                  Getn 3)(s526)
                                                                    , s540
                                                                    ) with
                                                                  | Some s554 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s527))
                                                                    ) with
                                                                    | Some s567 ->
                                                                    match 
                                                                    Get
                                                                    ( (
                                                                    Getn 3)(s526)
                                                                    , s567
                                                                    ) with
                                                                    | Some s579 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s579
                                                                    , s554
                                                                    ))
                                                                    ; s526
                                                                    ; s527
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(24)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(24)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | False _ ->
                                                              [ False
                                                              ; s526
                                                              ; s527
                                                              ]
                                                          end
                                                        let s645 =
                                                          match s587 with
                                                          | True _ ->
                                                              match Get
                                                                    ( 0
                                                                    , Car(Car(s589))
                                                                    ) with
                                                              | Some s616 ->
                                                                  match 
                                                                  Get
                                                                    ( (
                                                                  Getn 3)(s588)
                                                                    , s616
                                                                    ) with
                                                                  | Some s630 ->
                                                                    Pair
                                                                    ( Car(s589)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s589))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s589)))
                                                                    , s630
                                                                    )
                                                                    )
                                                                    )
                                                                  | None _ ->
                                                                    Failwith(24)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | False _ -> s589
                                                          end
                                                        match Get
                                                                ( 0
                                                                , Car(Car(s645))
                                                                ) with
                                                        | Some s655 ->
                                                            match Get(0, s655) with
                                                            | Some s662 ->
                                                                let [s711;
                                                                    s712] =
                                                                  match Neq(
                                                                  Compare
                                                                    ( s662
                                                                    , 0
                                                                    )) with
                                                                  | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(s645))
                                                                    ) with
                                                                    | Some s676 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s676
                                                                    ) with
                                                                    | Some s682 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s645))
                                                                    ) with
                                                                    | Some s697 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s697
                                                                    ) with
                                                                    | Some s704 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s704
                                                                    , s682
                                                                    ))
                                                                    ; s645
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | False _ ->
                                                                    [ False
                                                                    ; s645
                                                                    ]
                                                                  end
                                                                let [s755;
                                                                    s756] =
                                                                  match s711 with
                                                                  | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(s712))
                                                                    ) with
                                                                    | Some s720 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s720
                                                                    ) with
                                                                    | Some s726 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s712))
                                                                    ) with
                                                                    | Some s741 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s741
                                                                    ) with
                                                                    | Some s748 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s748
                                                                    , s726
                                                                    ))
                                                                    ; s712
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | False _ ->
                                                                    [ False
                                                                    ; s712
                                                                    ]
                                                                  end
                                                                let s799 =
                                                                  match s755 with
                                                                  | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s756))
                                                                    ) with
                                                                    | Some s776 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s776
                                                                    ) with
                                                                    | Some s784 ->
                                                                    Pair
                                                                    ( Car(s756)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s756))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s756)))
                                                                    , s784
                                                                    )
                                                                    )
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | False _ ->
                                                                    s756
                                                                  end
                                                                match 
                                                                Get
                                                                  ( 0
                                                                  , Car(Car(s799))
                                                                  ) with
                                                                | Some s809 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s809
                                                                    ) with
                                                                    | Some s816 ->
                                                                    let 
                                                                    [s865;
                                                                    s866] =
                                                                    match Neq(
                                                                    Compare
                                                                    ( s816
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(s799))
                                                                    ) with
                                                                    | Some s830 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s830
                                                                    ) with
                                                                    | Some s836 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s799))
                                                                    ) with
                                                                    | Some s851 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s851
                                                                    ) with
                                                                    | Some s858 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s858
                                                                    , s836
                                                                    ))
                                                                    ; s799
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s799
                                                                    ]
                                                                    end
                                                                    let 
                                                                    [s909;
                                                                    s910] =
                                                                    match s865 with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(s866))
                                                                    ) with
                                                                    | Some s874 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s874
                                                                    ) with
                                                                    | Some s880 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s866))
                                                                    ) with
                                                                    | Some s895 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s895
                                                                    ) with
                                                                    | Some s902 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s902
                                                                    , s880
                                                                    ))
                                                                    ; s866
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s866
                                                                    ]
                                                                    end
                                                                    let s953 =
                                                                    match s909 with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s910))
                                                                    ) with
                                                                    | Some s930 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s930
                                                                    ) with
                                                                    | Some s938 ->
                                                                    Pair
                                                                    ( Car(s910)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s910))
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(Cdr(s910)))
                                                                    , s938
                                                                    )
                                                                    )
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | False _ ->
                                                                    s910
                                                                    end
                                                                    let 
                                                                    [s967;
                                                                    s968] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( 9
                                                                    , (
                                                                    Getn 3)(s953)
                                                                    )) with
                                                                    | True _ ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 0
                                                                    , (
                                                                    Getn 6)(s953)
                                                                    ))
                                                                    ; s953
                                                                    ]
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s953
                                                                    ]
                                                                    end
                                                                    match s967 with
                                                                    | True _ ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s968))
                                                                    , True
                                                                    )
                                                                    , Cdr(s968)
                                                                    )
                                                                    )
                                                                    | False _ ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , s968
                                                                    )
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                | None _ ->
                                                                    Failwith(26)
                                                                end
                                                            | None _ ->
                                                                Failwith(25)
                                                            end
                                                        | None _ ->
                                                            Failwith(25)
                                                        end
                                                    | None _ -> Failwith(24)
                                                    end
                                                | None _ -> Failwith(24)
                                                end
                                            | None _ -> Failwith(31)
                                            end
                                        | None _ -> Failwith(23)
                                        end
                                    | None _ -> Failwith(20)
                                    end
                                | False _ ->
                                    Failwith("WrongCondition: self.data.deck[params.i][params.j] == 0")
                                end
                            | None _ -> Failwith(19)
                            end
                        | None _ -> Failwith(19)
                        end
                    | False _ ->
                        Failwith("WrongCondition: params.move == self.data.nextPlayer")
                    end
                | False _ ->
                    Failwith("WrongCondition: (params.j >= 0) & (params.j < 3)")
                end
            | False _ ->
                Failwith("WrongCondition: (params.i >= 0) & (params.i < 3)")
            end
        | False _ ->
            Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")
        end]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
