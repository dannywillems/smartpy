import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)

  @sp.entry_point
  def divide(self, params):
    sp.set_type(params, sp.TNat)
    sp.verify(5 < params, message = 'WrongCondition: params.divisor > 5')
    with (sp.ediv(self.data, params)).match_cases() as arg:
      with arg.match('Some') as s25:
        self.data = sp.fst(s25)
      with arg.match('None') as _x1:
        sp.failwith(20)


  @sp.entry_point
  def double(self, params):
    sp.set_type(params, sp.TUnit)
    self.data *= 2

  @sp.entry_point
  def replace(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
