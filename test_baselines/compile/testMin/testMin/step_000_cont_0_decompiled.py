import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TInt)

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TUnit)
    sp.failwith('[Error: to_cmd: let [] = (Drop 1)([ l2 ])
        match True with
        | True _ -> 0
        | False _ -> 0
        end]')

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TUnit)
    sp.failwith('[Error: to_cmd: let [] = (Drop 1)([ r3 ])
        match True with
        | True _ -> 0
        | False _ -> 0
        end]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
