import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(s = sp.TString, value = sp.TNat).layout(("s", "value")))

  @sp.entry_point
  def add(self, params):
    sp.set_type(params, sp.TUnit)
    self.data.value = 31

  @sp.entry_point
  def concat1(self, params):
    sp.set_type(params, sp.TUnit)
    sp.failwith('[Error: to_cmd: ["a"; "b"; "c"]#list(string)]')

  @sp.entry_point
  def concat2(self, params):
    sp.set_type(params, sp.TUnit)
    self.data.s = '[Error: TODO prim2: Concat2]'

  @sp.entry_point
  def seq(self, params):
    sp.set_type(params, sp.TUnit)
    self.data.value = 277729

  @sp.entry_point
  def seq2(self, params):
    sp.set_type(params, sp.TUnit)
    self.data.value = 262144

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
