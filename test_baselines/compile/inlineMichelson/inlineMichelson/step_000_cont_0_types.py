import smartpy as sp

tstorage = sp.TRecord(s = sp.TString, value = sp.TNat).layout(("s", "value"))
tparameter = sp.TVariant(add = sp.TUnit, concat1 = sp.TUnit, concat2 = sp.TUnit, seq = sp.TUnit, seq2 = sp.TUnit).layout((("add", "concat1"), ("concat2", ("seq", "seq2"))))
tglobals = { }
tviews = { }
