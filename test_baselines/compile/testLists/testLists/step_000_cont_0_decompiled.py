import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TOption(sp.TPair(sp.TPair(sp.TPair(sp.TList(sp.TInt), sp.TList(sp.TInt)), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TBool))), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TBool))), sp.TList(sp.TString)))), sp.TPair(sp.TPair(sp.TList(sp.TString), sp.TList(sp.TPair(sp.TString, sp.TBool))), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TBool)), sp.TPair(sp.TList(sp.TInt), sp.TList(sp.TInt)))))), b = sp.TInt, c = sp.TString, d = sp.TInt, e = sp.TString, f = sp.TList(sp.TInt), g = sp.TList(sp.TInt), head = sp.TString, tail = sp.TList(sp.TString)).layout(((("a", "b"), ("c", "d")), (("e", "f"), ("g", ("head", "tail"))))))

  @sp.entry_point
  def test(self, params):
    sp.set_type(params, sp.TPair(sp.TList(sp.TInt), sp.TPair(sp.TMap(sp.TString, sp.TPair(sp.TString, sp.TBool)), sp.TSet(sp.TInt))))
    sp.failwith('[Error: to_cmd: let [r246; r249; r250] =
          iter [ (Getn 4)(l3)
               ; Nil<int>
               ; Pair
                   ( Pair(__storage.e, __storage.f)
                   , Pair(__storage.g, Pair(__storage.head, __storage.tail))
                   )
               ; l3
               ]
          step s239; s240; s243; s244 ->
            [ Cons(s239, s240); s243; s244 ]
        let [r268; r269; r272; r273] =
          iter [ (Getn 4)(r250); Nil<int>; r246; r249; r250 ]
          step s260; s261; s262; s265; s266 ->
            [ Cons(s260, s261); s262; s265; s266 ]
        let [r285; r286; r289; r290] =
          iter [ r268; Nil<int>; r269; r272; r273 ]
          step s277; s278; s279; s282; s283 ->
            [ Cons(s277, s278); s279; s282; s283 ]
        let [r310; r311; r314; r315] =
          iter [ (Getn 3)(r290)
               ; Nil<{ string ; < True : unit | False : unit > }>
               ; Pair<s,sr>(r285, r286)
               ; r289
               ; r290
               ]
          step s301; s302; s303; s306; s307 ->
            [ Cons(Cdr(s301), s302); s303; s306; s307 ]
        let [r335; r336; r339; r340] =
          iter [ (Getn 3)(r315)
               ; Nil<{ string ; < True : unit | False : unit > }>
               ; Pair<mvr,_>(r310, r311)
               ; r314
               ; r315
               ]
          step s326; s327; s328; s331; s332 ->
            [ Cons(Cdr(s326), s327); s328; s331; s332 ]
        let [r352; r353; r356; r357] =
          iter [ r335
               ; Nil<{ string ; < True : unit | False : unit > }>
               ; r336
               ; r339
               ; r340
               ]
          step s344; s345; s346; s349; s350 ->
            [ Cons(s344, s345); s346; s349; s350 ]
        let [r378; r379; r380; r383; r384] =
          iter [ (Getn 3)(r357); Nil<string>; r352; r353; r356; r357 ]
          step s368; s369; s370; s371; s374; s375 ->
            [ Cons(Car(s368), s369); s370; s371; s374; s375 ]
        let [r405; r406; r409; r410] =
          iter [ (Getn 3)(r384)
               ; Nil<string>
               ; Pair(Pair<mkr,mv>(r378, r379), r380)
               ; r383
               ; r384
               ]
          step s396; s397; s398; s401; s402 ->
            [ Cons(Car(s396), s397); s398; s401; s402 ]
        let [r422; r423; r426; r427] =
          iter [ r405; Nil<string>; r406; r409; r410 ]
          step s414; s415; s416; s419; s420 ->
            [ Cons(s414, s415); s416; s419; s420 ]
        let [r447; r448; r449; r452; r453] =
          iter [ (Getn 3)(r427)
               ; Nil<{ key : string ; value : { string ; < True : unit | False : unit > } }>
               ; r422
               ; r423
               ; r426
               ; r427
               ]
          step s438; s439; s440; s441; s444; s445 ->
            [ Cons(s438, s439); s440; s441; s444; s445 ]
        let [r474; r475; r476; r479; r480] =
          iter [ (Getn 3)(r453)
               ; Nil<{ key : string ; value : { string ; < True : unit | False : unit > } }>
               ; Pair<mir,mk>(r447, r448)
               ; r449
               ; r452
               ; r453
               ]
          step s465; s466; s467; s468; s471; s472 ->
            [ Cons(s465, s466); s467; s468; s471; s472 ]
        let [r493; r494; r495; r498; r499] =
          iter [ r474
               ; Nil<{ key : string ; value : { string ; < True : unit | False : unit > } }>
               ; r475
               ; r476
               ; r479
               ; r480
               ]
          step s484; s485; s486; s487; s490; s491 ->
            [ Cons(s484, s485); s486; s487; s490; s491 ]
        let [r521; r522; r523; r526; r527] =
          iter [ Car(r499)
               ; Nil<int>
               ; Pair<mi,_>(r493, r494)
               ; r495
               ; r498
               ; r499
               ]
          step s512; s513; s514; s515; s518; s519 ->
            [ Cons(s512, s513); s514; s515; s518; s519 ]
        let [r564; r565; r567; r568] =
          iter [ Car(r527)
               ; 0
               ; Some_(Pair(Pair(Pair<l,lr>(Car(r527), r521), r522), r523))
               ; r526
               ; r527
               ]
          step s557; s558; s559; s561; s562 ->
            [ Add(s557, s558); s559; s561; s562 ]
        let [r597; r599; r600; r601] =
          iter [ (Getn 3)(r568); Nil<string>; Pair(r565, r564); r567; r568 ]
          step s589; s590; s592; s593; s594 ->
            [ Cons(Car(s589), s590); s592; s593; s594 ]
        let [r612; r614; r615; r616] =
          iter [ r597; Nil<string>; r599; r600; r601 ]
          step s605; s606; s608; s609; s610 ->
            [ Cons(s605, s606); s608; s609; s610 ]
        let [r648; r649; r650; r651; r652; r653] =
          iter [ (Getn 4)(r616)
               ; Nil<int>
               ; 0
               ; Concat1(r612)
               ; r614
               ; r615
               ; r616
               ]
          step s640; s641; s642; s643; s644; s645; s646 ->
            [ Cons(s640, s641); s642; s643; s644; s645; s646 ]
        let [r661; r662; r663; r664; r665] =
          iter [ r648; r649; r650; r651; r652; r653 ]
          step s654; s655; s656; s657; s658; s659 ->
            [ Add(s654, s655); s656; s657; s658; s659 ]
        let [_; r730] =
          iter [ (Getn 3)(r665)
               ; r665
               ; Pair
                   ( Pair(r663, Pair(r662, r661))
                   , Pair(Pair("", Cdr(Car(r664))), Cdr(r664))
                   )
               ]
          step s691; s692; s693 ->
            let [s727; s728] =
              match Cdr(Cdr(s691)) with
              | True _ ->
                  [ s692
                  ; Pair
                      ( Car(s693)
                      , Pair
                          ( Pair
                              ( Concat2(Car(Car(Cdr(s693))), Car(Cdr(s691)))
                              , Cdr(Car(Cdr(s693)))
                              )
                          , Cdr(Cdr(s693))
                          )
                      )
                  ]
              | False _ -> [ s692; s693 ]
              end
            [ s727; s728 ]
        let [_; r794] =
          loop [ True; 0; r730 ]
          step s734; s736 ->
            let x787 = Add(1, s734)
            [ Gt(Compare(5, x787))
            ; x787
            ; Pair
                ( Car(s736)
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(s736)))
                        , Cons(Mul(s734, s734), Cdr(Car(Cdr(s736))))
                        )
                    , Cdr(Cdr(s736))
                    )
                )
            ]
        let [_; r832; r833; r834; r835] =
          loop [ True
               ; 1
               ; Nil<int>
               ; Cdr(Cdr(Cdr(r794)))
               ; Car(Cdr(r794))
               ; Car(r794)
               ]
          step s809; s810; s811; _; _ ->
            let x826 = Add(1, s809)
            [ Gt(Compare(12, x826))
            ; x826
            ; Cons(s809, s810)
            ; s811
            ; Car(Cdr(r794))
            ; Car(r794)
            ]
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [r849; r850; r851; r852] =
                                  iter [ r832; Nil<int>; r833; r834; r835 ]
                                  step s843; s844; s845; s846; s847 ->
                                    [ Cons(s843, s844); s845; s846; s847 ]
                                Pair(r852, Pair(r851, Pair(r849, r850))))
          )]')

  @sp.entry_point
  def test_match(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    sp.failwith('[Error: to_cmd: record_of_tree(..., let [s864] =
                              if l5 is x135 :: xs136
                              then
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair(__storage.g, Pair(x135, xs136))
                                        )
                                    )
                                ]
                              else
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair
                                            ( __storage.g
                                            , Pair("abc", __storage.tail)
                                            )
                                        )
                                    )
                                ]
                            s864)]')

  @sp.entry_point
  def test_match2(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    sp.failwith('[Error: to_cmd: record_of_tree(..., let [s863] =
                              if r6 is x9 :: xs10
                              then
                                if xs10 is x43 :: xs44
                                then
                                  [ Pair
                                      ( Pair
                                          ( Pair(__storage.a, __storage.b)
                                          , Pair(__storage.c, __storage.d)
                                          )
                                      , Pair
                                          ( Pair(__storage.e, __storage.f)
                                          , Pair
                                              ( __storage.g
                                              , Pair(Concat2(x9, x43), xs44)
                                              )
                                          )
                                      )
                                  ]
                                else
                                  [ Pair
                                      ( Pair
                                          ( Pair(__storage.a, __storage.b)
                                          , Pair(__storage.c, __storage.d)
                                          )
                                      , Pair
                                          ( Pair(__storage.e, __storage.f)
                                          , Pair
                                              ( __storage.g
                                              , Pair
                                                  ( __storage.head
                                                  , __storage.tail
                                                  )
                                              )
                                          )
                                      )
                                  ]
                              else
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair
                                            ( __storage.g
                                            , Pair("abc", __storage.tail)
                                            )
                                        )
                                    )
                                ]
                            s863)]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
