import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TOption(sp.TAddress))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TList(sp.TInt)).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l3:
        with l3.match_cases() as arg:
          with arg.match('Left') as l103:
            sp.failwith('[Error: to_cmd: let [] = (Drop 2)([ l103; __storage ])
        let [x184; x185] =
          create_contract(parameter :  { x : int ; y : nat }
                          storage   :  { a : int ; b : nat }
                          ps186 ->
                          let x199 =
                            Pair
                              ( Add(Car(Car(ps186)), Car(Cdr(ps186)))
                              , Cdr(Cdr(ps186))
                              )
                          Pair
                            ( Nil<operation>
                            , Pair(Car(x199), Add(Cdr(Car(ps186)), Cdr(x199)))
                            ), None<key_hash>, 0#mutez, Pair<a,b>(12, 15#nat))
[ Cons(x184, Nil<operation>); Some_(x185) ]]')
          with arg.match('Right') as _x10:
            sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat }
                        storage   :  { a : int ; b : nat }
                        ps111 ->
                        let x124 =
                          Pair
                            ( Add(Car(Car(ps111)), Car(Cdr(ps111)))
                            , Cdr(Cdr(ps111))
                            )
                        Pair
                          ( Nil<operation>
                          , Pair(Car(x124), Add(Cdr(Car(ps111)), Cdr(x124)))
                          ), None<key_hash>, 2000000#mutez, Pair<a,b>(12, 15#nat))]')

      with arg.match('Right') as r4:
        with r4.value.match_cases() as arg:
          with arg.match('Left') as l5:
            sp.failwith('[Error: to_cmd: let [] = (Drop 2)([ l5; __storage ])
        let [x57; x58] =
          create_contract(parameter :  { x : int ; y : nat }
                          storage   :  { a : int ; b : nat }
                          ps59 ->
                          let x72 =
                            Pair
                              ( Add(Car(Car(ps59)), Car(Cdr(ps59)))
                              , Cdr(Cdr(ps59))
                              )
                          Pair
                            ( Nil<operation>
                            , Pair(Car(x72), Add(Cdr(Car(ps59)), Cdr(x72)))
                            ), None<key_hash>, 0#mutez, Pair<a,b>(12, 15#nat))
[ Cons(x57, Nil<operation>); Some_(x58) ]]')
          with arg.match('Right') as r6:
            sp.failwith('[Error: to_cmd: iter [ r6; Nil<operation>; __storage ]
        step s10; s11; s12 ->
          let [x19; _] =
            create_contract(parameter :  { x : int ; y : nat }
                            storage   :  { a : int ; b : nat }
                            ps21 ->
                            let x34 =
                              Pair
                                ( Add(Car(Car(ps21)), Car(Cdr(ps21)))
                                , Cdr(Cdr(ps21))
                                )
                            Pair
                              ( Nil<operation>
                              , Pair(Car(x34), Add(Cdr(Car(ps21)), Cdr(x34)))
                              ), None<key_hash>, 0#mutez, Pair<a,b>(s10, 15#nat))
[ Cons(x19, s11); s12 ]]')



@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
