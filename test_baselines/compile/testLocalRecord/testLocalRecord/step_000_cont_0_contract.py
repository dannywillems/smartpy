import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init()

  @sp.entry_point
  def ep(self, params):
    x = sp.local("x", sp.record(a = 1, b = 2))
    x.value.a = 15

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TNat))