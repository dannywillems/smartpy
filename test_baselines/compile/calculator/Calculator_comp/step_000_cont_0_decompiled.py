import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)

  @sp.entry_point
  def add(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TNat))
    self.data = sp.fst(params) + sp.snd(params)

  @sp.entry_point
  def factorial(self, params):
    sp.set_type(params, sp.TNat)
    x194 = sp.local("x194", 1 + params)
    c11 = sp.local("c11", x194.value > 1)
    y9 = sp.local("y9", 1)
    r234 = sp.local("r234", 1)
    sp.while c11.value:
      x223 = sp.local("x223", 1 + y9.value)
      c11.value = x194.value > x223.value
      y9.value = x223.value
      r234.value = y9.value * r234.value
    self.data = r234.value

  @sp.entry_point
  def log2(self, params):
    sp.set_type(params, sp.TNat)
    c31 = sp.local("c31", 1 < params)
    y29 = sp.local("y29", params)
    r182 = sp.local("r182", 0)
    sp.while c31.value:
      with (sp.ediv(y29.value, 2)).match_cases() as arg:
        with arg.match('Some') as s169:
          c31.value = 1 < sp.fst(s169)
          y29.value = sp.fst(s169)
          r182.value += 1
        with arg.match('None') as _x33:
          sp.failwith(42)

    self.data = r182.value

  @sp.entry_point
  def multiply(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TNat))
    self.data = sp.fst(params) * sp.snd(params)

  @sp.entry_point
  def square(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params * params

  @sp.entry_point
  def squareRoot(self, params):
    sp.set_type(params, sp.TNat)
    sp.verify(params >= 0, message = 'WrongCondition: params >= 0')
    c66 = sp.local("c66", (params * params) > params)
    r85 = sp.local("r85", params)
    r86 = sp.local("r86", params)
    sp.while c66.value:
      with (sp.ediv(params, r85.value)).match_cases() as arg:
        with arg.match('Some') as s51:
          with (sp.ediv(sp.fst(s51) + r85.value, 2)).match_cases() as arg:
            with arg.match('Some') as s60:
              c66.value = (sp.fst(s60) * sp.fst(s60)) > params
              r85.value = sp.fst(s60)
              r86.value = params
            with arg.match('None') as _x94:
              sp.failwith(26)

        with arg.match('None') as _x88:
          sp.failwith(26)

    s127 = sp.local("s127", sp.fst(sp.eif((r85.value * r85.value) <= r86.value, (r86.value < ((r85.value + 1) * (1 + r85.value)), r85.value), (False, r85.value))))
    s128 = sp.local("s128", sp.snd(sp.eif((r85.value * r85.value) <= r86.value, (r86.value < ((r85.value + 1) * (1 + r85.value)), r85.value), (False, r85.value))))
    sp.verify(s127.value, message = 'WrongCondition: ((y.value * y.value) <= params) & (params < ((y.value + 1) * (y.value + 1)))')
    self.data = s128.value

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
