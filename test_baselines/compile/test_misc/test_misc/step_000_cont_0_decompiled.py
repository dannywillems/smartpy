import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TPair(sp.TInt, sp.TInt))

  @sp.entry_point
  def ep1(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = (sp.snd(self.data), sp.fst(self.data))

  @sp.entry_point
  def ep2(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = (sp.snd(self.data), sp.fst(self.data))

  @sp.entry_point
  def ep3(self, params):
    sp.set_type(params, sp.TUnit)
    sp.snd(self.data).set(sp.fst(self.data))

  @sp.entry_point
  def ep4(self, params):
    sp.set_type(params, sp.TUnit)
    sp.fst(self.data).set(sp.snd(self.data))

  @sp.entry_point
  def ep5(self, params):
    sp.set_type(params, sp.TNat)
    with (sp.ediv(params, 2)).match_cases() as arg:
      with arg.match('Some') as s28:
        sp.failwith('[Error: to_cmd: record_of_tree(..., let [] = (Drop 3)([ __storage; s28; l7 ])
                            Pair(1, 1))]')
      with arg.match('None') as _x25:
        sp.failwith('[Error: to_cmd: record_of_tree(..., let [] = (Drop 2)([ l7; __storage ])
                            Pair(0, 0))]')


  @sp.entry_point
  def ep6(self, params):
    sp.set_type(params, sp.TNat)
    with (sp.ediv(params, 2)).match_cases() as arg:
      with arg.match('Some') as s15:
        sp.failwith('[Error: to_cmd: record_of_tree(..., let [] = (Drop 3)([ __storage; s15; r8 ])
                            Pair(1, 1))]')
      with arg.match('None') as _x33:
        sp.failwith('[Error: to_cmd: record_of_tree(..., let [] = (Drop 2)([ r8; __storage ])
                            Pair(0, 0))]')


@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
