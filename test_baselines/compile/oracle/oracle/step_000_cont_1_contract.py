import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(active = True, admin = sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), min_amount = 0, min_timeout_minutes = 5, next_id = 0, requests = {}, reverse_requests = {}, token = sp.contract_address(Contract0))

  @sp.entry_point
  def cancel_request(self, params):
    compute_105 = sp.local("compute_105", sp.record(client = sp.sender, client_request_id = params))
    request_id = sp.local("request_id", self.data.reverse_requests[compute_105.value])
    request = sp.local("request", self.data.requests[request_id.value])
    sp.verify(request.value.timeout <= sp.now, message = 'OracleCannotCancelBeforeTimeout')
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = request.value.client, token_id = 0, amount = request.value.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.requests[request_id.value]
    del self.data.reverse_requests[compute_105.value]

  @sp.entry_point
  def create_request(self, params):
    sp.verify(sp.sender == self.data.token, message = 'OracleSenderIsNotTokenContract')
    sp.verify(self.data.active, message = 'OracleInactive')
    sp.verify(self.data.min_amount <= params.params.amount, message = 'OracleAmountBelowMinAmount')
    sp.verify(sp.add_seconds(sp.now, self.data.min_timeout_minutes * 60) <= params.params.timeout, message = 'OracleTimeoutBelowMinTimeout')
    compute_72 = sp.local("compute_72", sp.record(client = params.client, client_request_id = params.params.client_request_id))
    sp.verify(~ (self.data.reverse_requests.contains(compute_72.value)), message = 'OracleRequestKeyAlreadyKnown')
    self.data.reverse_requests[compute_72.value] = self.data.next_id
    self.data.requests[self.data.next_id] = sp.record(amount = params.params.amount, client = params.client, client_request_id = params.params.client_request_id, job_id = params.params.job_id, parameters = params.params.parameters, target = params.params.target, timeout = params.params.timeout)
    self.data.next_id += 1

  @sp.entry_point
  def fulfill_request(self, params):
    sp.verify(self.data.admin == sp.sender, message = 'OracleSenderIsNotAdmin')
    request = sp.local("request", self.data.requests[params.request_id])
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = self.data.admin, token_id = 0, amount = request.value.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    sp.transfer(sp.record(client_request_id = request.value.client_request_id, result = params.result), sp.tez(0), sp.contract(sp.TRecord(client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))).layout(("client_request_id", "result")), request.value.target).open_some(message = 'OracleWrongTypeForClientTarget'))
    del self.data.requests[params.request_id]
    del self.data.reverse_requests[sp.record(client = request.value.client, client_request_id = request.value.client_request_id)]

  @sp.entry_point
  def setup(self, params):
    sp.verify(self.data.admin == sp.sender, message = 'OracleSenderIsNotAdmin')
    self.data.active = params.active
    self.data.min_timeout_minutes = params.min_timeout_minutes
    self.data.min_amount = params.min_amount