import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, kings = sp.TMap(sp.TInt, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TBool))), nextPlayer = sp.TInt, previousPawnMove = sp.TOption(sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))), winner = sp.TInt).layout((("check", ("deck", "draw")), (("kings", "nextPlayer"), ("previousPawnMove", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))
    sp.failwith('[Error: to_cmd: let [s23; s24; s25] =
          match Eq(Compare
                     ( (Getn 6)(Pair
                                  ( Pair
                                      ( __storage.check
                                      , Pair(__storage.deck, __storage.draw)
                                      )
                                  , Pair
                                      ( Pair
                                          ( __storage.kings
                                          , __storage.nextPlayer
                                          )
                                      , Pair
                                          ( __storage.previousPawnMove
                                          , __storage.winner
                                          )
                                      )
                                  ))
                     , 0
                     )) with
          | True _ ->
              [ Not(__storage.draw)
              ; __parameter
              ; Pair
                  ( Pair
                      ( __storage.check
                      , Pair(__storage.deck, __storage.draw)
                      )
                  , Pair
                      ( Pair(__storage.kings, __storage.nextPlayer)
                      , Pair(__storage.previousPawnMove, __storage.winner)
                      )
                  )
              ]
          | False _ ->
              [ False
              ; __parameter
              ; Pair
                  ( Pair
                      ( __storage.check
                      , Pair(__storage.deck, __storage.draw)
                      )
                  , Pair
                      ( Pair(__storage.kings, __storage.nextPlayer)
                      , Pair(__storage.previousPawnMove, __storage.winner)
                      )
                  )
              ]
          end
        match s23 with
        | True _ ->
            let [s46; s47; s48] =
              match Ge(Compare(Car(Car(s24)), 0)) with
              | True _ -> [ Gt(Compare(8, Car(Car(s24)))); s24; s25 ]
              | False _ -> [ False; s24; s25 ]
              end
            match s46 with
            | True _ ->
                let [s69; s70; s71] =
                  match Ge(Compare(Cdr(Car(s47)), 0)) with
                  | True _ -> [ Gt(Compare(8, Cdr(Car(s47)))); s47; s48 ]
                  | False _ -> [ False; s47; s48 ]
                  end
                match s69 with
                | True _ ->
                    let [s90; s91; s92] =
                      match Ge(Compare((Getn 3)(s70), 0)) with
                      | True _ -> [ Gt(Compare(8, (Getn 3)(s70))); s70; s71 ]
                      | False _ -> [ False; s70; s71 ]
                      end
                    match s90 with
                    | True _ ->
                        let [s111; s112; s113] =
                          match Ge(Compare((Getn 4)(s91), 0)) with
                          | True _ ->
                              [ Gt(Compare(8, (Getn 4)(s91))); s91; s92 ]
                          | False _ -> [ False; s91; s92 ]
                          end
                        match s111 with
                        | True _ ->
                            match Get(Car(Car(s112)), Car(Cdr(Car(s113)))) with
                            | Some s144 ->
                                match Get(Cdr(Car(s112)), s144) with
                                | Some s159 ->
                                    match Eq(Compare
                                               ( Compare(s159, 0)
                                               , Cdr(Car(Cdr(s113)))
                                               )) with
                                    | True _ ->
                                        match Get
                                                ( (Getn 3)(s112)
                                                , Car(Cdr(Car(s113)))
                                                ) with
                                        | Some s198 ->
                                            match Get((Getn 4)(s112), s198) with
                                            | Some s212 ->
                                                match Neq(Compare
                                                            ( Compare(s212, 0)
                                                            , Cdr(Car(Cdr(s113)))
                                                            )) with
                                                | True _ ->
                                                    match Get
                                                            ( Car(Car(s112))
                                                            , Car(Cdr(Car(s113)))
                                                            ) with
                                                    | Some s240 ->
                                                        match Get
                                                                ( Cdr(Car(s112))
                                                                , s240
                                                                ) with
                                                        | Some s253 ->
                                                            let [s501; s502] =
                                                              match Eq(
                                                              Compare
                                                                ( Abs(s253)
                                                                , 1#nat
                                                                )) with
                                                              | True _ ->
                                                                  let 
                                                                  [s345;
                                                                    s346;
                                                                    s347] =
                                                                    match Neq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s112)
                                                                    , Cdr(Car(s112))
                                                                    )
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( (
                                                                    Getn 3)(s112)
                                                                    , Car(Cdr(Car(s113)))
                                                                    ) with
                                                                    | Some s321 ->
                                                                    match 
                                                                    Get
                                                                    ( (
                                                                    Getn 4)(s112)
                                                                    , s321
                                                                    ) with
                                                                    | Some s335 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 
                                                                    Compare
                                                                    ( s335
                                                                    , 0
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    ; s112
                                                                    ; s113
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(55)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(55)
                                                                    end
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s112
                                                                    ; s113
                                                                    ]
                                                                    end
                                                                  let 
                                                                  [s459; s460
                                                                    ] =
                                                                    match s345 with
                                                                    | True _ ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s346)
                                                                    , Cdr(Car(s346))
                                                                    ))
                                                                    , 1#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    let 
                                                                    [s457;
                                                                    s458] =
                                                                    match (
                                                                    Getn 5)(s347) with
                                                                    | Some _ ->
                                                                    match (
                                                                    Getn 5)(s347) with
                                                                    | Some s390 ->
                                                                    match (
                                                                    Getn 5)(s347) with
                                                                    | Some s401 ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( Car(Car(s401))
                                                                    , (
                                                                    Getn 3)(s390)
                                                                    )) with
                                                                    | True _ ->
                                                                    match (
                                                                    Getn 5)(s347) with
                                                                    | Some s431 ->
                                                                    match (
                                                                    Getn 5)(s347) with
                                                                    | Some s444 ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( Cdr(Car(s444))
                                                                    , 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s431)
                                                                    , 
                                                                    Mul
                                                                    ( 2
                                                                    , Cdr(Car(Cdr(s347)))
                                                                    )
                                                                    )
                                                                    )) with
                                                                    | True _ ->
                                                                    [ s346
                                                                    ; s347
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: self.data.previousPawnMove.open_some().f.j == (self.data.previousPawnMove.open_some().t.j - (2 * self.data.nextPlayer))")
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    end
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: self.data.previousPawnMove.open_some().f.i == self.data.previousPawnMove.open_some().t.i")
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    end
                                                                    | None _ ->
                                                                    [ s346
                                                                    ; s347
                                                                    ]
                                                                    end
                                                                    [ s457
                                                                    ; s458
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: (abs(params.t.j - params.f.j)) == 1")
                                                                    end
                                                                    | False _ ->
                                                                    [ s346
                                                                    ; s347
                                                                    ]
                                                                    end
                                                                  [ s459
                                                                  ; Pair
                                                                    ( Car(s460)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s460))
                                                                    , 
                                                                    Pair
                                                                    ( Some_(
                                                                    Pair<f,t>
                                                                    ( Car(s459)
                                                                    , Cdr(s459)
                                                                    ))
                                                                    , Cdr(Cdr(Cdr(s460)))
                                                                    )
                                                                    )
                                                                    )
                                                                  ]
                                                              | False _ ->
                                                                  [ s112
                                                                  ; Pair
                                                                    ( Car(s113)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s113))
                                                                    , 
                                                                    Pair
                                                                    ( None<{ f : { i : int ; j : int } ; t : { i : int ; j : int } }>
                                                                    , Cdr(Cdr(Cdr(s113)))
                                                                    )
                                                                    )
                                                                    )
                                                                  ]
                                                              end
                                                            match Get
                                                                    ( Car(Car(s501))
                                                                    , Car(Cdr(Car(s502)))
                                                                    ) with
                                                            | Some s518 ->
                                                                match 
                                                                Get
                                                                  ( Cdr(Car(s501))
                                                                  , s518
                                                                  ) with
                                                                | Some s531 ->
                                                                    let 
                                                                    [s835;
                                                                    s836] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(s531)
                                                                    , 5#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    let 
                                                                    [s579;
                                                                    s580;
                                                                    s581] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s501)
                                                                    , Cdr(Car(s501))
                                                                    )
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    [ True
                                                                    ; s501
                                                                    ; s502
                                                                    ]
                                                                    | False _ ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s501)
                                                                    , Car(Car(s501))
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    ; s501
                                                                    ; s502
                                                                    ]
                                                                    end
                                                                    let 
                                                                    [s616;
                                                                    s617;
                                                                    s618] =
                                                                    match s579 with
                                                                    | True _ ->
                                                                    [ True
                                                                    ; s580
                                                                    ; s581
                                                                    ]
                                                                    | False _ ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s580)
                                                                    , Cdr(Car(s580))
                                                                    ))
                                                                    , Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s580)
                                                                    , Car(Car(s580))
                                                                    ))
                                                                    ))
                                                                    ; s580
                                                                    ; s581
                                                                    ]
                                                                    end
                                                                    match s616 with
                                                                    | True _ ->
                                                                    let x640 =
                                                                    Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s617)
                                                                    , Cdr(Car(s617))
                                                                    ))
                                                                    let x663 =
                                                                    Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s617)
                                                                    , Car(Car(s617))
                                                                    ))
                                                                    let 
                                                                    [s680;
                                                                    s681;
                                                                    s682;
                                                                    s683] =
                                                                    match Le(
                                                                    Compare
                                                                    ( x663
                                                                    , x640
                                                                    )) with
                                                                    | True _ ->
                                                                    [ x640
                                                                    ; 1
                                                                    ; s617
                                                                    ; s618
                                                                    ]
                                                                    | False _ ->
                                                                    [ x663
                                                                    ; 1
                                                                    ; s617
                                                                    ; s618
                                                                    ]
                                                                    end
                                                                    let x689 =
                                                                    Sub
                                                                    ( Int(s680)
                                                                    , s681
                                                                    )
                                                                    let 
                                                                    [_; r831;
                                                                    r832] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x689
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ; s682
                                                                    ; s683
                                                                    ]
                                                                    step s696; _; _ ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s682))
                                                                    , 
                                                                    Mul
                                                                    ( s696
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s682)
                                                                    , Car(Car(s682))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s683)))
                                                                    ) with
                                                                    | Some s753 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s682))
                                                                    , 
                                                                    Mul
                                                                    ( s696
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s682)
                                                                    , Cdr(Car(s682))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s753
                                                                    ) with
                                                                    | Some s804 ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( s804
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    let x821 =
                                                                    Add
                                                                    ( 1
                                                                    , s696
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x689
                                                                    , x821
                                                                    ))
                                                                    ; x821
                                                                    ; s682
                                                                    ; s683
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * (sp.sign(params.t.i - params.f.i)))][params.f.j + (k * (sp.sign(params.t.j - params.f.j)))] == 0")
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(70)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(70)
                                                                    end
                                                                    [ r831
                                                                    ; r832
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: (((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)) | ((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i)))")
                                                                    end
                                                                    | False _ ->
                                                                    [ s501
                                                                    ; s502
                                                                    ]
                                                                    end
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s835))
                                                                    , Car(Cdr(Car(s836)))
                                                                    ) with
                                                                    | Some s852 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s835))
                                                                    , s852
                                                                    ) with
                                                                    | Some s865 ->
                                                                    let 
                                                                    [s1114;
                                                                    s1115] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(s865)
                                                                    , 6#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    let 
                                                                    [s915;
                                                                    s916;
                                                                    s917] =
                                                                    match Le(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s835)
                                                                    , Cdr(Car(s835))
                                                                    ))
                                                                    , 1#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    [ Le(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s835)
                                                                    , Car(Car(s835))
                                                                    ))
                                                                    , 1#nat
                                                                    ))
                                                                    ; s835
                                                                    ; s836
                                                                    ]
                                                                    | False _ ->
                                                                    [ False
                                                                    ; s835
                                                                    ; s836
                                                                    ]
                                                                    end
                                                                    match s915 with
                                                                    | True _ ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s917)))
                                                                    , Car(Car(Cdr(s917)))
                                                                    ) with
                                                                    | Some s954 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s917)))
                                                                    , Update(Cdr(Car(Cdr(s917))), Some_(
                                                                    Pair
                                                                    ( Car(s954)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s954))
                                                                    , True
                                                                    )
                                                                    )), Car(Car(Cdr(s917))))
                                                                    ) with
                                                                    | Some s1015 ->
                                                                    let s1019 =
                                                                    Update(Cdr(Car(Cdr(s917))), Some_(
                                                                    Pair
                                                                    ( Car(s954)
                                                                    , 
                                                                    Pair
                                                                    ( Car(Cdr(s954))
                                                                    , True
                                                                    )
                                                                    )), Car(Car(Cdr(s917))))
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s917)))
                                                                    , Update(Cdr(Car(Cdr(s917))), Some_(
                                                                    Pair
                                                                    ( (
                                                                    Getn 3)(s916)
                                                                    , Cdr(s1015)
                                                                    )), s1019)
                                                                    ) with
                                                                    | Some s1075 ->
                                                                    [ s916
                                                                    ; 
                                                                    Pair
                                                                    ( Car(s917)
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Update(Cdr(Car(Cdr(s917))), Some_(
                                                                    Pair
                                                                    ( Car(s1075)
                                                                    , 
                                                                    Pair
                                                                    ( (
                                                                    Getn 4)(s916)
                                                                    , Cdr(Cdr(s1075))
                                                                    )
                                                                    )), Update(Cdr(Car(Cdr(s917))), Some_(
                                                                    Pair
                                                                    ( (
                                                                    Getn 3)(s916)
                                                                    , Cdr(s1015)
                                                                    )), s1019))
                                                                    , Cdr(Car(Cdr(s917)))
                                                                    )
                                                                    , Cdr(Cdr(s917))
                                                                    )
                                                                    )
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(76)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(75)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(74)
                                                                    end
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: ((abs(params.t.j - params.f.j)) <= 1) & ((abs(params.t.i - params.f.i)) <= 1)")
                                                                    end
                                                                    | False _ ->
                                                                    [ s835
                                                                    ; s836
                                                                    ]
                                                                    end
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1114))
                                                                    , Car(Cdr(Car(s1115)))
                                                                    ) with
                                                                    | Some s1131 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1114))
                                                                    , s1131
                                                                    ) with
                                                                    | Some s1144 ->
                                                                    let 
                                                                    [s1411;
                                                                    s1412] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(s1144)
                                                                    , 2#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    let 
                                                                    [s1192;
                                                                    s1193;
                                                                    s1194] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1114)
                                                                    , Cdr(Car(s1114))
                                                                    )
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    [ True
                                                                    ; s1114
                                                                    ; s1115
                                                                    ]
                                                                    | False _ ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1114)
                                                                    , Car(Car(s1114))
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    ; s1114
                                                                    ; s1115
                                                                    ]
                                                                    end
                                                                    match s1192 with
                                                                    | True _ ->
                                                                    let x1216 =
                                                                    Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1193)
                                                                    , Cdr(Car(s1193))
                                                                    ))
                                                                    let x1239 =
                                                                    Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1193)
                                                                    , Car(Car(s1193))
                                                                    ))
                                                                    let 
                                                                    [s1256;
                                                                    s1257;
                                                                    s1258;
                                                                    s1259] =
                                                                    match Le(
                                                                    Compare
                                                                    ( x1239
                                                                    , x1216
                                                                    )) with
                                                                    | True _ ->
                                                                    [ x1216
                                                                    ; 1
                                                                    ; s1193
                                                                    ; s1194
                                                                    ]
                                                                    | False _ ->
                                                                    [ x1239
                                                                    ; 1
                                                                    ; s1193
                                                                    ; s1194
                                                                    ]
                                                                    end
                                                                    let x1265 =
                                                                    Sub
                                                                    ( Int(s1256)
                                                                    , s1257
                                                                    )
                                                                    let 
                                                                    [_;
                                                                    r1407;
                                                                    r1408] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1265
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ; s1258
                                                                    ; s1259
                                                                    ]
                                                                    step s1272; _; _ ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s1258))
                                                                    , 
                                                                    Mul
                                                                    ( s1272
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1258)
                                                                    , Car(Car(s1258))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s1259)))
                                                                    ) with
                                                                    | Some s1329 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s1258))
                                                                    , 
                                                                    Mul
                                                                    ( s1272
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1258)
                                                                    , Cdr(Car(s1258))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s1329
                                                                    ) with
                                                                    | Some s1380 ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( s1380
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    let x1397 =
                                                                    Add
                                                                    ( 1
                                                                    , s1272
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1265
                                                                    , x1397
                                                                    ))
                                                                    ; x1397
                                                                    ; s1258
                                                                    ; s1259
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * (sp.sign(params.t.i - params.f.i)))][params.f.j + (k * (sp.sign(params.t.j - params.f.j)))] == 0")
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(82)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(82)
                                                                    end
                                                                    [ r1407
                                                                    ; r1408
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: ((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)")
                                                                    end
                                                                    | False _ ->
                                                                    [ s1114
                                                                    ; s1115
                                                                    ]
                                                                    end
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1411))
                                                                    , Car(Cdr(Car(s1412)))
                                                                    ) with
                                                                    | Some s1428 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1411))
                                                                    , s1428
                                                                    ) with
                                                                    | Some s1441 ->
                                                                    let 
                                                                    [s1699;
                                                                    s1700] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(s1441)
                                                                    , 4#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1411)
                                                                    , Cdr(Car(s1411))
                                                                    ))
                                                                    , Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1411)
                                                                    , Car(Car(s1411))
                                                                    ))
                                                                    )) with
                                                                    | True _ ->
                                                                    let x1504 =
                                                                    Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1411)
                                                                    , Cdr(Car(s1411))
                                                                    ))
                                                                    let x1527 =
                                                                    Abs(
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1411)
                                                                    , Car(Car(s1411))
                                                                    ))
                                                                    let 
                                                                    [s1544;
                                                                    s1545;
                                                                    s1546;
                                                                    s1547] =
                                                                    match Le(
                                                                    Compare
                                                                    ( x1527
                                                                    , x1504
                                                                    )) with
                                                                    | True _ ->
                                                                    [ x1504
                                                                    ; 1
                                                                    ; s1411
                                                                    ; s1412
                                                                    ]
                                                                    | False _ ->
                                                                    [ x1527
                                                                    ; 1
                                                                    ; s1411
                                                                    ; s1412
                                                                    ]
                                                                    end
                                                                    let x1553 =
                                                                    Sub
                                                                    ( Int(s1544)
                                                                    , s1545
                                                                    )
                                                                    let 
                                                                    [_;
                                                                    r1695;
                                                                    r1696] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1553
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ; s1546
                                                                    ; s1547
                                                                    ]
                                                                    step s1560; _; _ ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s1546))
                                                                    , 
                                                                    Mul
                                                                    ( s1560
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1546)
                                                                    , Car(Car(s1546))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s1547)))
                                                                    ) with
                                                                    | Some s1617 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s1546))
                                                                    , 
                                                                    Mul
                                                                    ( s1560
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1546)
                                                                    , Cdr(Car(s1546))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s1617
                                                                    ) with
                                                                    | Some s1668 ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( s1668
                                                                    , 0
                                                                    )) with
                                                                    | True _ ->
                                                                    let x1685 =
                                                                    Add
                                                                    ( 1
                                                                    , s1560
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1553
                                                                    , x1685
                                                                    ))
                                                                    ; x1685
                                                                    ; s1546
                                                                    ; s1547
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * (sp.sign(params.t.i - params.f.i)))][params.f.j + (k * (sp.sign(params.t.j - params.f.j)))] == 0")
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(88)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(88)
                                                                    end
                                                                    [ r1695
                                                                    ; r1696
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: (abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i))")
                                                                    end
                                                                    | False _ ->
                                                                    [ s1411
                                                                    ; s1412
                                                                    ]
                                                                    end
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1699))
                                                                    , Car(Cdr(Car(s1700)))
                                                                    ) with
                                                                    | Some s1716 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1699))
                                                                    , s1716
                                                                    ) with
                                                                    | Some s1729 ->
                                                                    let 
                                                                    [s1780;
                                                                    s1781] =
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(s1729)
                                                                    , 3#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    match Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Mul
                                                                    ( 
                                                                    Sub
                                                                    ( (
                                                                    Getn 4)(s1699)
                                                                    , Cdr(Car(s1699))
                                                                    )
                                                                    , 
                                                                    Sub
                                                                    ( (
                                                                    Getn 3)(s1699)
                                                                    , Car(Car(s1699))
                                                                    )
                                                                    ))
                                                                    , 2#nat
                                                                    )) with
                                                                    | True _ ->
                                                                    [ s1699
                                                                    ; s1700
                                                                    ]
                                                                    | False _ ->
                                                                    Failwith("WrongCondition: (abs((params.t.j - params.f.j) * (params.t.i - params.f.i))) == 2")
                                                                    end
                                                                    | False _ ->
                                                                    [ s1699
                                                                    ; s1700
                                                                    ]
                                                                    end
                                                                    let x1807 =
                                                                    (
                                                                    Getn 3)(s1780)
                                                                    match 
                                                                    Get
                                                                    ( x1807
                                                                    , Car(Cdr(Car(s1781)))
                                                                    ) with
                                                                    | Some s1813 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1780))
                                                                    , Car(Cdr(Car(s1781)))
                                                                    ) with
                                                                    | Some s1846 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1780))
                                                                    , s1846
                                                                    ) with
                                                                    | Some s1868 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1780))
                                                                    , Update(x1807, Some_(Update((
                                                                    Getn 4)(s1780), Some_(s1868), s1813)), Car(Cdr(Car(s1781))))
                                                                    ) with
                                                                    | Some s1924 ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1781))
                                                                    , 
                                                                    Pair
                                                                    ( Update(Car(Car(s1780)), Some_(Update(Cdr(Car(s1780)), Some_(0), s1924)), Update(x1807, Some_(Update((
                                                                    Getn 4)(s1780), Some_(s1868), s1813)), Car(Cdr(Car(s1781)))))
                                                                    , Cdr(Cdr(Car(s1781)))
                                                                    )
                                                                    )
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(Cdr(s1781)))
                                                                    , Neg(Cdr(Car(Cdr(s1781))))
                                                                    )
                                                                    , Cdr(Cdr(s1781))
                                                                    )
                                                                    )
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(92)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(89)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(89)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(83)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(83)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(77)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(77)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(71)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(71)
                                                                    end
                                                                | None _ ->
                                                                    Failwith(65)
                                                                end
                                                            | None _ ->
                                                                Failwith(65)
                                                            end
                                                        | None _ ->
                                                            Failwith(53)
                                                        end
                                                    | None _ -> Failwith(53)
                                                    end
                                                | False _ ->
                                                    Failwith("WrongCondition: (sp.sign(self.data.deck[params.t.i][params.t.j])) != self.data.nextPlayer")
                                                end
                                            | None _ -> Failwith(52)
                                            end
                                        | None _ -> Failwith(52)
                                        end
                                    | False _ ->
                                        Failwith("WrongCondition: (sp.sign(self.data.deck[params.f.i][params.f.j])) == self.data.nextPlayer")
                                    end
                                | None _ -> Failwith(51)
                                end
                            | None _ -> Failwith(51)
                            end
                        | False _ ->
                            Failwith("WrongCondition: (params.t.j >= 0) & (params.t.j < 8)")
                        end
                    | False _ ->
                        Failwith("WrongCondition: (params.t.i >= 0) & (params.t.i < 8)")
                    end
                | False _ ->
                    Failwith("WrongCondition: (params.f.j >= 0) & (params.f.j < 8)")
                end
            | False _ ->
                Failwith("WrongCondition: (params.f.i >= 0) & (params.f.i < 8)")
            end
        | False _ ->
            Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")
        end]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
