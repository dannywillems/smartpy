import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(check = False, deck = {0 : {0 : 2, 1 : 3, 2 : 4, 3 : 5, 4 : 6, 5 : 4, 6 : 3, 7 : 2}, 1 : {0 : 1, 1 : 1, 2 : 1, 3 : 1, 4 : 1, 5 : 1, 6 : 1, 7 : 1}, 2 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 3 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 4 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 5 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 6 : {0 : -1, 1 : -1, 2 : -1, 3 : -1, 4 : -1, 5 : -1, 6 : -1, 7 : -1}, 7 : {0 : -2, 1 : -3, 2 : -4, 3 : -5, 4 : -6, 5 : -4, 6 : -3, 7 : -2}}, draw = False, kings = {-1 : sp.record(i = 7, j = 4, moved = False), 1 : sp.record(i = 0, j = 4, moved = False)}, nextPlayer = 1, previousPawnMove = sp.none, winner = 0)

  @sp.entry_point
  def play(self, params):
    sp.verify((self.data.winner == 0) & (~ self.data.draw))
    sp.verify((params.f.i >= 0) & (params.f.i < 8))
    sp.verify((params.f.j >= 0) & (params.f.j < 8))
    sp.verify((params.t.i >= 0) & (params.t.i < 8))
    sp.verify((params.t.j >= 0) & (params.t.j < 8))
    sp.verify((sp.sign(self.data.deck[params.f.i][params.f.j])) == self.data.nextPlayer)
    sp.verify((sp.sign(self.data.deck[params.t.i][params.t.j])) != self.data.nextPlayer)
    sp.if (abs(self.data.deck[params.f.i][params.f.j])) == 1:
      sp.if ((params.t.j - params.f.j) != 0) & ((sp.sign(self.data.deck[params.t.i][params.t.j])) == 0):
        sp.verify((abs(params.t.j - params.f.j)) == 1)
        sp.if self.data.previousPawnMove.is_some():
          sp.verify(self.data.previousPawnMove.open_some().f.i == self.data.previousPawnMove.open_some().t.i)
          sp.verify(self.data.previousPawnMove.open_some().f.j == (self.data.previousPawnMove.open_some().t.j - (2 * self.data.nextPlayer)))
      self.data.previousPawnMove = sp.some(sp.record(f = params.f, t = params.t))
    sp.else:
      self.data.previousPawnMove = sp.none
    sp.if (abs(self.data.deck[params.f.i][params.f.j])) == 5:
      sp.verify((((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)) | ((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i))))
      sp.for k in sp.range(1, (sp.to_int(sp.max(abs(params.t.j - params.f.j), abs(params.t.i - params.f.i)))) - 1):
        sp.verify(self.data.deck[params.f.i + (k * (sp.sign(params.t.i - params.f.i)))][params.f.j + (k * (sp.sign(params.t.j - params.f.j)))] == 0)
    sp.if (abs(self.data.deck[params.f.i][params.f.j])) == 6:
      sp.verify(((abs(params.t.j - params.f.j)) <= 1) & ((abs(params.t.i - params.f.i)) <= 1))
      king = sp.local("king", self.data.nextPlayer)
      self.data.kings[self.data.nextPlayer].moved = True
      self.data.kings[self.data.nextPlayer].i = params.t.i
      self.data.kings[self.data.nextPlayer].j = params.t.j
    sp.if (abs(self.data.deck[params.f.i][params.f.j])) == 2:
      sp.verify(((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0))
      sp.for k in sp.range(1, (sp.to_int(sp.max(abs(params.t.j - params.f.j), abs(params.t.i - params.f.i)))) - 1):
        sp.verify(self.data.deck[params.f.i + (k * (sp.sign(params.t.i - params.f.i)))][params.f.j + (k * (sp.sign(params.t.j - params.f.j)))] == 0)
    sp.if (abs(self.data.deck[params.f.i][params.f.j])) == 4:
      sp.verify((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i)))
      sp.for k in sp.range(1, (sp.to_int(sp.max(abs(params.t.j - params.f.j), abs(params.t.i - params.f.i)))) - 1):
        sp.verify(self.data.deck[params.f.i + (k * (sp.sign(params.t.i - params.f.i)))][params.f.j + (k * (sp.sign(params.t.j - params.f.j)))] == 0)
    sp.if (abs(self.data.deck[params.f.i][params.f.j])) == 3:
      sp.verify((abs((params.t.j - params.f.j) * (params.t.i - params.f.i))) == 2)
    self.data.deck[params.t.i][params.t.j] = self.data.deck[params.f.i][params.f.j]
    self.data.deck[params.f.i][params.f.j] = 0
    self.data.nextPlayer = - self.data.nextPlayer