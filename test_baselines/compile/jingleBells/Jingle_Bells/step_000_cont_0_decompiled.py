import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(current = sp.TString, played = sp.TInt, rules = sp.TList(sp.TString), verse = sp.TInt).layout((("current", "played"), ("rules", "verse"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    sp.failwith('[Error: to_cmd: let [_; _; _; r176] =
          loop [ Gt(Compare(__parameter, 0))
               ; 0
               ; __parameter
               ; __parameter
               ; Pair
                   ( Pair(__storage.current, __storage.played)
                   , Pair(__storage.rules, __storage.verse)
                   )
               ]
          step s15; s16; s17; s18 ->
            let [s67; s68; s69; s70] =
              match Eq(Compare((Getn 4)(s18), 16)) with
              | True _ ->
                  [ s15
                  ; s16
                  ; s17
                  ; Pair
                      ( Pair("", Add(1, Cdr(Car(s18))))
                      , Pair(Car(Cdr(s18)), 0)
                      )
                  ]
              | False _ -> [ s15; s16; s17; s18 ]
              end
            let [s100; s101; s102; s103] =
              match Neq(Compare(Car(Car(s70)), "")) with
              | True _ ->
                  [ s67
                  ; s68
                  ; s69
                  ; Pair
                      ( Pair(Concat2(Car(Car(s70)), "\n"), Cdr(Car(s70)))
                      , Cdr(s70)
                      )
                  ]
              | False _ -> [ s67; s68; s69; s70 ]
              end
            match Get
                    ( (Getn 4)(s103)
                    , [0: "Dashing through the snow"; 1: "In a one-horse open sleigh"; 2: "O'er the fields we go"; 3: "Laughing all the way"; 4: "Bells on bob tail ring"; 5: "Making spirits bright"; 6: "What fun it is to ride and sing"; 7: "A sleighing song tonight!"; 8: "Jingle bells, jingle bells,"; 9: "Jingle all the way."; 10: "Oh! what fun it is to ride"; 11: "In a one-horse open sleigh."; 12: "Jingle bells, jingle bells,"; 13: "Jingle all the way;"; 14: "Oh! what fun it is to ride"; 15: "In a one-horse open sleigh."]#map(int,string)
                    ) with
            | Some s129 ->
                let x165 = Add(1, s100)
                [ Gt(Compare(s101, x165))
                ; x165
                ; s101
                ; s102
                ; Pair
                    ( Pair(Concat2(Car(Car(s103)), s129), Cdr(Car(s103)))
                    , Pair(Car(Cdr(s103)), Add(1, Cdr(Cdr(s103))))
                    )
                ]
            | None _ -> Failwith(48)
            end
        Pair(Nil<operation>, r176)]')

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
