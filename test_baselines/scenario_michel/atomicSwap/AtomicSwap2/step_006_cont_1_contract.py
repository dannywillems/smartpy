import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(counterparty = sp.address('tz0FakeAlice'), epoch = sp.timestamp(50), hashedSecret = sp.bytes('0x6232622a57b4adcf1de7bee13349df8a9f256b'), notional = sp.mutez(20), owner = sp.address('tz0FakeRobert'))

  @sp.entry_point
  def allSigned(self, params):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.owner == sp.sender)
    sp.send(self.data.counterparty, self.data.notional)
    self.data.notional = sp.tez(0)

  @sp.entry_point
  def cancelSwap(self, params):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.owner == sp.sender)
    sp.verify(self.data.epoch < sp.now)
    sp.send(self.data.owner, self.data.notional)
    self.data.notional = sp.tez(0)

  @sp.entry_point
  def knownSecret(self, params):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.counterparty == sp.sender)
    sp.verify(self.data.hashedSecret == sp.blake2b(params.secret))
    sp.send(self.data.counterparty, self.data.notional)
    self.data.notional = sp.tez(0)