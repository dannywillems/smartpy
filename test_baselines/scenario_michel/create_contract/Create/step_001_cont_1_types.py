import smartpy as sp

tstorage = sp.TRecord(x = sp.TOption(sp.TAddress)).layout("x")
tparameter = sp.TVariant(create1 = sp.TUnit, create2 = sp.TUnit, create3 = sp.TUnit, create4 = sp.TList(sp.TInt)).layout((("create1", "create2"), ("create3", "create4")))
tglobals = { }
tviews = { }
