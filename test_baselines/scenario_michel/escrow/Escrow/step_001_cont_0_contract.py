import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(balanceCounterparty = sp.tez(0), balanceOwner = sp.tez(0), counterparty = sp.address('tz0FakeBob'), epoch = sp.timestamp(123), fromCounterparty = sp.tez(4), fromOwner = sp.tez(50), hashedSecret = sp.bytes('0x62326268874163bc6a181ee8a3bd24feb063c4'), owner = sp.address('tz0FakeAlice'))

  @sp.entry_point
  def addBalanceCounterparty(self, params):
    sp.verify(self.data.balanceCounterparty == sp.tez(0))
    sp.verify(sp.amount == self.data.fromCounterparty)
    self.data.balanceCounterparty = self.data.fromCounterparty

  @sp.entry_point
  def addBalanceOwner(self, params):
    sp.verify(self.data.balanceOwner == sp.tez(0))
    sp.verify(sp.amount == self.data.fromOwner)
    self.data.balanceOwner = self.data.fromOwner

  @sp.entry_point
  def claimCounterparty(self, params):
    sp.verify(sp.now < self.data.epoch)
    sp.verify(self.data.hashedSecret == sp.blake2b(params.secret))
    sp.verify(sp.sender == self.data.counterparty)
    sp.send(self.data.counterparty, self.data.balanceOwner + self.data.balanceCounterparty)
    self.data.balanceOwner = sp.tez(0)
    self.data.balanceCounterparty = sp.tez(0)

  @sp.entry_point
  def claimOwner(self, params):
    sp.verify(self.data.epoch < sp.now)
    sp.verify(sp.sender == self.data.owner)
    sp.send(self.data.owner, self.data.balanceOwner + self.data.balanceCounterparty)
    self.data.balanceOwner = sp.tez(0)
    self.data.balanceCounterparty = sp.tez(0)