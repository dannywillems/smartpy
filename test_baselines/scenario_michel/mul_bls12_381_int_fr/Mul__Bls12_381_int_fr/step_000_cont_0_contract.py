import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(fr = sp.none)

  @sp.entry_point
  def mul_int_fr(self, params):
    match_pair_17_fst, match_pair_17_snd = sp.match_tuple(params, "match_pair_17_fst", "match_pair_17_snd")
    self.data.fr = sp.some(sp.mul(match_pair_17_fst, match_pair_17_snd))

  @sp.entry_point
  def mul_nat_fr(self, params):
    match_pair_30_fst, match_pair_30_snd = sp.match_tuple(params, "match_pair_30_fst", "match_pair_30_snd")
    self.data.fr = sp.some(sp.mul(match_pair_30_fst, match_pair_30_snd))