import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(b2b = sp.bytes('0x'), s256 = sp.bytes('0x'), s512 = sp.bytes('0x'), tz1 = sp.key_hash('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), v = sp.bytes('0x'))

  @sp.entry_point
  def new_key(self, params):
    self.data.tz1 = sp.hash_key(params)

  @sp.entry_point
  def new_value(self, params):
    self.data.v = params
    self.data.b2b = sp.blake2b(params)
    self.data.s256 = sp.sha256(params)
    self.data.s512 = sp.sha512(params)