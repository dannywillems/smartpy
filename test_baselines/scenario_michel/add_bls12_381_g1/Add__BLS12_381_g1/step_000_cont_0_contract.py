import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(g1 = sp.none)

  @sp.entry_point
  def add(self, params):
    self.data.g1 = sp.some(sp.fst(params) + sp.snd(params))