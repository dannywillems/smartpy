Comment...
 h1: FA2 Contract Name: FA2-debug-no_ops
Table Of Contents

 FA2 Contract Name: FA2-debug-no_ops
# Accounts
# Initial Minting
# Transfers Alice -> Bob
# More Token Types
## Multi-token Transfer Bob -> Alice
# Other Basic Permission Tests
## Bob cannot transfer Alice's tokens.
## Admin can transfer anything.
## Even Admin cannot transfer too much.
## Consumer Contract for Callback Calls.
# Balance-of.
# Operators
## This version was compiled with no operator support
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Robert")])...
 => sp.list([sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Robert', address = sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), public_key = sp.key('edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2'), public_key_hash = sp.key_hash('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), secret_key = sp.secret_key('edskRiaffUWqB9zgaEhuX6EmejbLzk2xcpSEXLv3G4cDfcbY75c71ASyGnFHXuaTAVMPt2bJLGGye1gm24oBmAc2k5VDHHo5Ua'))])
Creating contract
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 {})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {})))
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_storage.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_storage.json 13
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_storage.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_types.py 7
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_metadata.metadata_base.json 216
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_contract.tz 703
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_contract.json 812
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_004_cont_0_contract.py 68
Comment...
 h2: Initial Minting
Comment...
 p: The administrator mints 100 token-0's to Alice.
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_007_cont_0_params.json 17
Executing mint(sp.record(address = sp.reduce(sp.test_account("Alice").address), amount = 100, metadata = {'decimals' : sp.bytes('0x32'), 'name' : sp.bytes('0x54686520546f6b656e205a65726f'), 'symbol' : sp.bytes('0x544b30')}, token_id = 0))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 {Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 100})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100)})))
Comment...
 h2: Transfers Alice -> Bob
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_009_cont_0_params.json 9
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 10)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 10; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 90})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100)})))
Verifying sp.contract_data(0).ledger[(sp.set_type_expr(sp.reduce(sp.test_account("Alice").address), sp.TAddress), sp.set_type_expr(0, sp.TNat))].balance == 90...
 OK
Verifying sp.contract_data(0).ledger[(sp.set_type_expr(sp.reduce(sp.test_account("Robert").address), sp.TAddress), sp.set_type_expr(0, sp.TNat))].balance == 10...
 OK
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_012_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_012_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_012_cont_0_params.json 12
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 10), sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 11)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 1 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 31; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 69})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100)})))
Verifying sp.contract_data(0).ledger[(sp.set_type_expr(sp.reduce(sp.test_account("Alice").address), sp.TAddress), sp.set_type_expr(0, sp.TNat))].balance == 69...
 OK
Verifying sp.contract_data(0).ledger[(sp.set_type_expr(sp.reduce(sp.test_account("Robert").address), sp.TAddress), sp.set_type_expr(0, sp.TNat))].balance == 31...
 OK
Comment...
 h2: More Token Types
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_016_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_016_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_016_cont_0_params.json 17
Executing mint(sp.record(address = sp.reduce(sp.test_account("Robert").address), amount = 100, metadata = {'decimals' : sp.bytes('0x30'), 'name' : sp.bytes('0x546865205365636f6e6420546f6b656e'), 'symbol' : sp.bytes('0x544b31')}, token_id = 1))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 2 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 31; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1) 100; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 69})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100); Elt 1 (Pair {Elt "decimals" 0x30; Elt "name" 0x546865205365636f6e6420546f6b656e; Elt "symbol" 0x544b31} 100)})))
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_017_cont_0_params.json 17
Executing mint(sp.record(address = sp.reduce(sp.test_account("Robert").address), amount = 200, metadata = {'decimals' : sp.bytes('0x30'), 'name' : sp.bytes('0x54686520546f6b656e204e756d626572205468726565'), 'symbol' : sp.bytes('0x544b32')}, token_id = 2))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 31; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1) 100; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 2) 200; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 69})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100); Elt 1 (Pair {Elt "decimals" 0x30; Elt "name" 0x546865205365636f6e6420546f6b656e; Elt "symbol" 0x544b31} 100); Elt 2 (Pair {Elt "decimals" 0x30; Elt "name" 0x54686520546f6b656e204e756d626572205468726565; Elt "symbol" 0x544b32} 200)})))
Comment...
 h3: Multi-token Transfer Bob -> Alice
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_019_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_019_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_019_cont_0_params.json 19
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Robert").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Alice").address), token_id = 0, amount = 10), sp.record(to_ = sp.reduce(sp.test_account("Alice").address), token_id = 1, amount = 10)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Robert").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Alice").address), token_id = 2, amount = 10)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 21; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1) 90; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 2) 190; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 79; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 10; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 10})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100); Elt 1 (Pair {Elt "decimals" 0x30; Elt "name" 0x546865205365636f6e6420546f6b656e; Elt "symbol" 0x544b31} 100); Elt 2 (Pair {Elt "decimals" 0x30; Elt "name" 0x54686520546f6b656e204e756d626572205468726565; Elt "symbol" 0x544b32} 200)})))
Comment...
 h2: Other Basic Permission Tests
Comment...
 h3: Bob cannot transfer Alice's tokens.
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_022_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_022_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_022_cont_0_params.json 12
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 10), sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 1)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> --- Expected failure in transaction --- WrongCondition in line 401: (sp.sender == self.data.administrator) | (transfer.from_ == sp.sender) ['FA2_NOT_OWNER']
Comment...
 h3: Admin can transfer anything.
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_024_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_024_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_024_cont_0_params.json 19
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 10), sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 1, amount = 10)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Robert").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Alice").address), token_id = 0, amount = 11)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 20; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1) 100; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 2) 190; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 80; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 0; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 10})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100); Elt 1 (Pair {Elt "decimals" 0x30; Elt "name" 0x546865205365636f6e6420546f6b656e; Elt "symbol" 0x544b31} 100); Elt 2 (Pair {Elt "decimals" 0x30; Elt "name" 0x54686520546f6b656e204e756d626572205468726565; Elt "symbol" 0x544b32} 200)})))
Comment...
 h3: Even Admin cannot transfer too much.
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_026_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_026_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_026_cont_0_params.json 9
Executing transfer(sp.list([sp.set_type_expr(sp.record(from_ = sp.reduce(sp.test_account("Alice").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Robert").address), token_id = 0, amount = 1000)])), sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))]))...
 -> --- Expected failure in transaction --- WrongCondition in line 410: self.data.ledger[(sp.set_type_expr(transfer.from_, sp.TAddress), sp.set_type_expr(tx.token_id, sp.TNat))].balance >= tx.amount ['FA2_INSUFFICIENT_BALANCE']
Comment...
 h3: Consumer Contract for Callback Calls.
Creating contract
 -> (Pair 0 True)
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_storage.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_storage.json 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_storage.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_types.py 7
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_contract.tz 40
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_contract.json 66
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_028_cont_1_contract.py 16
Comment...
 p: Consumer virtual address: (literal (local-address (static_id 1 706)) 706)
Comment...
 h2: Balance-of.
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_031_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_031_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_031_cont_0_params.json 11
Executing balance_of(sp.record(requests = sp.list([sp.record(owner = sp.reduce(sp.test_account("Alice").address), token_id = 0), sp.record(owner = sp.reduce(sp.test_account("Alice").address), token_id = 1), sp.record(owner = sp.reduce(sp.test_account("Alice").address), token_id = 2)]), callback = sp.contract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))), sp.contract_address(Contract1), entry_point='receive_balances').open_some()))...
 -> (Pair (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 3 {Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0) 20; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 1) 100; Elt (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 2) 190; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 0) 80; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 1) 0; Elt (Pair "tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi" 2) 10})) (Pair (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x32; Elt "name" 0x54686520546f6b656e205a65726f; Elt "symbol" 0x544b30} 100); Elt 1 (Pair {Elt "decimals" 0x30; Elt "name" 0x546865205365636f6e6420546f6b656e; Elt "symbol" 0x544b31} 100); Elt 2 (Pair {Elt "decimals" 0x30; Elt "name" 0x54686520546f6b656e204e756d626572205468726565; Elt "symbol" 0x544b32} 200)})))
Executing receive_balances(sp.list([sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 0), balance = 80), sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 1), balance = 0), sp.record(request = sp.record(owner = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), token_id = 2), balance = 10)]))...
 -> (Pair 90 True)
Verifying sp.contract_data(1).last_sum == 90...
 OK
Comment...
 h2: Operators
Comment...
 h3: This version was compiled with no operator support
Comment...
 p: Calls should fail even for the administrator:
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_036_cont_0_params.py 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_036_cont_0_params.tz 1
 => test_baselines/scenario_michel/FA2/FA2-debug-no_ops/step_036_cont_0_params.json 1
Executing update_operators(sp.list([]))...
 -> --- Expected failure in transaction --- Failure: 'FA2_OPERATORS_UNSUPPORTED' 
 (line 490)
