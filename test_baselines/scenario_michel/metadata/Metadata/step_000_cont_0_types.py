import smartpy as sp

tstorage = sp.TRecord(metadata = sp.TBigMap(sp.TString, sp.TBytes), x = sp.TIntOrNat).layout(("metadata", "x"))
tparameter = sp.TVariant(change_metadata = sp.TBytes, incr = sp.TUnit).layout(("change_metadata", "incr"))
tglobals = { }
tviews = { "big_fail": ((), sp.TUnknown()), "big_fail2": ((), sp.TBool), "get_cst": ((), sp.TIntOrNat), "get_storage": ((), sp.TIntOrNat), "get_x": (sp.TIntOrNat, sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat)) }
