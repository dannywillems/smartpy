import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init()

  @sp.entry_point
  def ep1(self, params):
    match_pair_10_fst, match_pair_10_snd = sp.match_tuple(params, "match_pair_10_fst", "match_pair_10_snd")
    sp.verify(match_pair_10_fst == 'x')
    sp.verify(match_pair_10_snd == 2)

  @sp.entry_point
  def ep2(self, params):
    my_x, my_y, my_z = sp.match_tuple(params, "my_x", "my_y", "my_z")
    sp.verify(my_x == 'x')
    sp.verify(my_y == 2)
    sp.verify(my_z)

  @sp.entry_point
  def ep3(self, params):
    sp.set_type(params, sp.TRecord(x = sp.TString, y = sp.TInt, z = sp.TBool).layout(("x", ("y", "z"))))
    x_24, z_24 = sp.match_record(params, "x", "z")
    sp.verify(x_24 == 'x')
    sp.verify(z_24)

  @sp.entry_point
  def ep4(self, params):
    sp.set_type(params.x01, sp.TInt)
    sp.set_type(params.x02, sp.TKey)
    sp.set_type(params.x03, sp.TString)
    sp.set_type(params.x04, sp.TTimestamp)
    sp.set_type(params.x05, sp.TBytes)
    sp.set_type(params.x06, sp.TAddress)
    sp.set_type(params.x07, sp.TBool)
    sp.set_type(params.x08, sp.TKeyHash)
    sp.set_type(params.x09, sp.TSignature)
    sp.set_type(params.x10, sp.TMutez)
    x07_40, x03_40 = sp.match_record(params, "x07", "x03")
    sp.verify(x03_40 == 'x')
    sp.verify(x07_40)

  @sp.entry_point
  def ep5(self, params):
    a, b, c, d = sp.match_tuple(params, "a", "b", "c", "d")
    sp.verify(((a * b) + (c * d)) == 12)

  @sp.entry_point
  def ep6(self, params):
    a, b, c, d = sp.match_tuple(params, "a", "b", "c", "d")
    sp.set_type(c, sp.TInt)
    sp.verify(((a * b) + d) == 12)