import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(chainId = sp.none)

  @sp.entry_point
  def test(self, params):
    self.data.chainId = sp.some(sp.chain_id)