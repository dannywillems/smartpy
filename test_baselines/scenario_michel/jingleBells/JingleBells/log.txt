Comment...
 h1: Jingle Bells
Creating contract
 -> (Pair (Pair "" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 0))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_storage.json 7
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_contract.tz 109
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_contract.json 141
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_001_cont_0_contract.py 19
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_002_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_002_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_002_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 1))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_003_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 2))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_004_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 3))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_005_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 4))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_006_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 5))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_007_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring\nMaking spirits bright" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 6))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_008_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring\nMaking spirits bright\nWhat fun it is to ride and sing" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 7))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_009_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring\nMaking spirits bright\nWhat fun it is to ride and sing\nA sleighing song tonight!" 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 8))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_010_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_010_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_010_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring\nMaking spirits bright\nWhat fun it is to ride and sing\nA sleighing song tonight!\nJingle bells, jingle bells," 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 9))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_011_cont_0_params.json 1
Executing sing(sp.record(verses = 1))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring\nMaking spirits bright\nWhat fun it is to ride and sing\nA sleighing song tonight!\nJingle bells, jingle bells,\nJingle all the way." 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 10))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_012_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_012_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_012_cont_0_params.json 1
Executing sing(sp.record(verses = 6))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way\nBells on bob tail ring\nMaking spirits bright\nWhat fun it is to ride and sing\nA sleighing song tonight!\nJingle bells, jingle bells,\nJingle all the way.\nOh! what fun it is to ride\nIn a one-horse open sleigh.\nJingle bells, jingle bells,\nJingle all the way;\nOh! what fun it is to ride\nIn a one-horse open sleigh." 0) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 16))
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_013_cont_0_params.py 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_013_cont_0_params.tz 1
 => test_baselines/scenario_michel/jingleBells/JingleBells/step_013_cont_0_params.json 1
Executing sing(sp.record(verses = 100))...
 -> (Pair (Pair "Dashing through the snow\nIn a one-horse open sleigh\nO'er the fields we go\nLaughing all the way" 7) (Pair {"Please sing as much as you wish!"; "Happy Holidays from the SmartPy team!"} 4))
