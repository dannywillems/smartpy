import smartpy as sp

tstorage = sp.TRecord(bossPublicKey = sp.TKey, counter = sp.TIntOrNat, currentValue = sp.TString).layout(("bossPublicKey", ("counter", "currentValue")))
tparameter = sp.TVariant(setCurrentValue = sp.TRecord(newValue = sp.TString, userSignature = sp.TSignature).layout(("newValue", "userSignature"))).layout("setCurrentValue")
tglobals = { }
tviews = { }
