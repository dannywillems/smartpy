import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(fr = sp.none)

  @sp.entry_point
  def add(self, params):
    self.data.fr = sp.some(sp.fst(params) + sp.snd(params))