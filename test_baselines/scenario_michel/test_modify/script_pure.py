import smartpy as sp

t = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString)

class C1(sp.Contract):
    def __init__(self):
        self.add_flag("protocol", "edo")
        self.init(x1 = sp.record(a = sp.nat(0), b = sp.int(1), c = True, d = "abc")
                , x2 = sp.record(a = sp.nat(0), b = sp.int(1), c = True, d = "abc")
                , x3 = sp.record(a = sp.nat(0), b = sp.int(1), c = True, d = "abc")
                , x4 = sp.record(a = sp.nat(0), b = sp.int(1), c = True, d = "abc")
                , y = (sp.nat(0), sp.int(1), True, "abc")
                , z = sp.record(a = sp.nat(0), b = sp.int(1), c = True, d = sp.record(e = 1, f = "x"))
                )

    @sp.entry_point
    def ep1(self):
        sp.set_type(self.data.x2, t.layout(("a",("b",("c","d")))))
        sp.set_type(self.data.x3, t.layout(((("a","b"),"c"),"d")))
        sp.set_type(self.data.x4, t.layout((("a","b"),("c","d"))))
        with sp.modify_record(self.data.x1, "d", "a", "c", "b") as brython:
            (d,a,c,b) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))
        with sp.modify_record(self.data.x2, "d", "a", "c", "b") as brython:
            (d,a,c,b) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))
        with sp.modify_record(self.data.x3, "d", "a", "c", "b") as brython:
            (d,a,c,b) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))
        with sp.modify_record(self.data.x4, "d", "a", "c", "b") as brython:
            (d,a,c,b) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))

    @sp.entry_point
    def ep2(self):
        with sp.modify_record(self.data.x1, "b", "a", "c") as brython:
            (b,a,c) = brython
            sp.verify(abs(b) == a + 1)
            sp.result(sp.record(a = a, b = b, c = c, d = "xyz"))

    @sp.entry_point
    def ep3(self):
        with sp.modify_tuple(self.data.y, "a", "b", "c", "d") as brython:
            (a,b,c,d) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result((a,b,c,d))

    @sp.entry_point
    def ep4(self):
        with sp.modify_record(self.data.x1, "d", "a") as brython:
            (d,a) = brython
            sp.result(self.data.x1)

    @sp.entry_point
    def ep5(self):
        with sp.modify_record(self.data.x1, "d", "a", "c", "b") as brython:
            (d,a,c,b) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))
        self.data.x1.a += 5

    @sp.entry_point
    def ep5(self, alice):
        with sp.modify_record(self.data.x1, "d", "a", "c", "b") as brython:
            (d,a,c,b) = brython
            sp.send(alice, sp.tez(0))
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))
        self.data.x1.a += 5

    @sp.entry_point
    def ep6(self, alice):
        with sp.modify_record(self.data.z, "a", "b", "c", "d") as brython:
            (a,b,c,d) = brython
            with sp.modify_record(d, "e", "f") as brython:
                (e,f) = brython
                b.set(100)
                e.set(2)
                f.set("y")
                sp.result(sp.record(e = e, f = f))
            sp.result(sp.record(a = a, b = b, c = c, d = d))

    @sp.entry_point
    def ep7(self, alice):
        with sp.modify_record(self.data.z.d, "e", "f") as brython:
            (e,f) = brython
            e.set(3)
            f.set("z")
            sp.result(sp.record(e = e, f = f))

class C2(sp.Contract):
    def __init__(self):
        self.add_flag("protocol", "edo")
        self.init(a = sp.nat(0), b = sp.int(1), c = True, d = "abc")

    @sp.entry_point
    def ep1(self):
        with sp.modify_record(self.data, 'd', 'a', 'c', 'b') as brython:
            (d,a,c,b) = brython
            sp.verify(abs(b) == a + 1)
            d.set("xyz")
            sp.result(sp.record(a = a, b = b, c = c, d = d))

    @sp.entry_point
    def ep2(self):
        with sp.modify_record(self.data, 'd') as d:
            d.set("abc")
            sp.result(sp.record(a = sp.nat(0), b = sp.int(1), c = True, d = d))


class C3(sp.Contract):
    def __init__(self):
        self.add_flag("protocol", "edo")
        self.init(42)

    @sp.entry_point
    def ep1(self):
        with sp.modify(self.data, 'x') as x:
            sp.result(x + 1)

@sp.add_test(name = "Match")
def test():
    alice = sp.test_account("Alice")
    s = sp.test_scenario()
    c1 = C1()
    s += c1
    s += c1.ep1()
    s += c1.ep2()
    s += c1.ep3()
    s += c1.ep4()
    # s += c1.ep5(alice.address)
    s += c1.ep6()
    s += c1.ep7()

    s += C2()

    s += C3()
