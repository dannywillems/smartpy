import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(42)

  @sp.entry_point
  def ep1(self, params):
    with sp.modify(self.data, "x") as "x":
      sp.result(x.value + 1)