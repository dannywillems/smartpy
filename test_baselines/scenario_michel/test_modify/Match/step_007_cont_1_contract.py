import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = 0, b = 1, c = True, d = 'abc')

  @sp.entry_point
  def ep1(self, params):
    with sp.match_record(self.data, fields = ["d", "a", "c", "b"]) as d_108, a_108, c_108, b_108:
      sp.verify((abs(b_108.value)) == (a_108.value + 1))
      d_108.value = 'xyz'
      sp.result(sp.record(a = a_108.value, b = b_108.value, c = c_108.value, d = d_108.value))

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data, fields = ["d"]) as d_116:
      d_116.value = 'abc'
      sp.result(sp.record(a = 0, b = 1, c = True, d = d_116.value))