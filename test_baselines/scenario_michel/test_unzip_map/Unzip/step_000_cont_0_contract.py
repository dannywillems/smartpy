import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(m = {'abc' : sp.record(a = 10, b = 20)}, out = 'z')

  @sp.entry_point
  def ep(self, params):
    k = sp.local("k", 'abc')
    with sp.match_record(self.data.m[k.value], fields = ["a", "b"]) as a_10, b_10:
      k.value = 'xyz' + k.value
      sp.result(sp.record(a = a_10.value, b = b_10.value))
    self.data.out = k.value