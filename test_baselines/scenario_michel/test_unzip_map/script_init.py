import smartpy as sp

class C(sp.Contract):
    def __init__(self):
        self.init(m = {"abc": sp.record(a = 10, b = 20)}, out = "z")

    @sp.entry_point
    def ep(self, params):
       k = sp.local('k', "abc")
       with sp.modify_record(self.data.m[k.value], "a", "b") as (a,b):
          k.value.set("xyz" + k.value)
          sp.result(sp.record(a=a,b=b))
       self.data.out = k.value

@sp.add_test(name = "Unzip")
def test():
    c = C()
    s = sp.test_scenario()
    s += c
    s += c.ep()
