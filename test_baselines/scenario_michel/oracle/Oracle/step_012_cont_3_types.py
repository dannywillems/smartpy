import smartpy as sp

tstorage = sp.TRecord(admin = sp.TAddress, next_request_id = sp.TNat, oracle = sp.TAddress, token = sp.TAddress, waiting_xtzusd_id = sp.TOption(sp.TNat), xtzusd = sp.TInt, xtzusd_job_id = sp.TBytes).layout((("admin", ("next_request_id", "oracle")), (("token", "waiting_xtzusd_id"), ("xtzusd", "xtzusd_job_id"))))
tparameter = sp.TVariant(cancel_xtzusd = sp.TUnit, change_oracle = sp.TRecord(oracle = sp.TAddress, xtzusd_job_id = sp.TBytes).layout(("oracle", "xtzusd_job_id")), request_xtzusd = sp.TUnit, set_xtzusd = sp.TRecord(client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))).layout(("client_request_id", "result"))).layout((("cancel_xtzusd", "change_oracle"), ("request_xtzusd", "set_xtzusd")))
tglobals = { }
tviews = { }
