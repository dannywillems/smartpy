Comment...
 h1: Chainlink Oracles
Table Of Contents

 Chainlink Oracles
# Accounts
# Link Token
# Token Faucet
# Oracle
# Client1
# Client2
# Tokens
# Client1 sends a request that gets fulfilled
## A request
## Ledger
## Oracle consumes the request
# Client1 sends a request that gets cancelled
## A request
## Ledger
## Client1 cancels the request
# Client2 sends a request that gets fulfilled
## A request
## Ledger
## Oracle consumes the request
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Oracle1"), sp.test_account("Client1 admin"), sp.test_account("Client2 admin")])...
 => sp.list([sp.record(seed = 'Administrator', address = sp.address('tz0FakeAdministrator'), public_key = sp.key('edpkFakeAdministrator'), public_key_hash = sp.key_hash('tz0FakeAdministrator'), secret_key = sp.secret_key('edskFakeAdministrator')), sp.record(seed = 'Oracle1', address = sp.address('tz0FakeOracle1'), public_key = sp.key('edpkFakeOracle1'), public_key_hash = sp.key_hash('tz0FakeOracle1'), secret_key = sp.secret_key('edskFakeOracle1')), sp.record(seed = 'Client1 admin', address = sp.address('tz0FakeClient1 admin'), public_key = sp.key('edpkFakeClient1 admin'), public_key_hash = sp.key_hash('tz0FakeClient1 admin'), secret_key = sp.secret_key('edskFakeClient1 admin')), sp.record(seed = 'Client2 admin', address = sp.address('tz0FakeClient2 admin'), public_key = sp.key('edpkFakeClient2 admin'), public_key_hash = sp.key_hash('tz0FakeClient2 admin'), secret_key = sp.secret_key('edskFakeClient2 admin'))])
Comment...
 h2: Link Token
Creating contract
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 0 {})) (Pair (Pair {Elt "" 0x} {}) (Pair False {})))
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_storage.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_storage.json 10
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_storage.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_types.py 7
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_metadata.metadata_base.json 217
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_contract.tz 1055
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_contract.json 1269
 => test_baselines/scenario_michel/oracle/Oracle/step_005_cont_0_contract.py 96
 => test_baselines/scenario_michel/oracle/Oracle/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_006_cont_0_params.json 17
Executing mint(sp.record(address = sp.reduce(sp.test_account("Administrator").address), amount = 200, metadata = {'decimals' : sp.bytes('0x3138'), 'name' : sp.bytes('0x77726170706564204c494e4b'), 'symbol' : sp.bytes('0x774c494e4b')}, symbol = 'tzLINK', token_id = 0))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "tz0FakeAdministrator" 200})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Comment...
 h2: Token Faucet
Creating contract
 -> (Pair (Pair True "tz0FakeAdministrator") (Pair 10 "Contract_0"))
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_storage.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_storage.json 4
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_storage.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_types.py 7
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_contract.tz 74
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_contract.json 147
 => test_baselines/scenario_michel/oracle/Oracle/step_008_cont_1_contract.py 15
Comment...
 h2: Oracle
Creating contract
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 0 {}) (Pair {} "Contract_0")))
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_storage.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_storage.json 10
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_sizes.csv 2
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_storage.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_types.py 7
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_contract.tz 467
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_contract.json 788
 => test_baselines/scenario_michel/oracle/Oracle/step_010_cont_2_contract.py 43
Comment...
 h2: Client1
Creating contract
 -> (Pair (Pair "tz0FakeClient1 admin" (Pair 1 "Contract_2")) (Pair (Pair "Contract_0" None) (Pair 0 0x0001)))
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_storage.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_storage.json 10
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_sizes.csv 2
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_storage.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_types.py 7
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_contract.tz 301
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_contract.json 369
 => test_baselines/scenario_michel/oracle/Oracle/step_012_cont_3_contract.py 34
Comment...
 h2: Client2
Creating contract
 -> (Pair (Pair "tz0FakeClient2 admin" (Pair 1 "Contract_2")) (Pair (Pair "Contract_0" None) (Pair 0 0x0001)))
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_storage.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_storage.json 10
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_sizes.csv 2
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_storage.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_types.py 7
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_contract.tz 301
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_contract.json 369
 => test_baselines/scenario_michel/oracle/Oracle/step_014_cont_4_contract.py 34
Comment...
 h2: Tokens
 => test_baselines/scenario_michel/oracle/Oracle/step_016_cont_0_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_016_cont_0_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_016_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.reduce(sp.test_account("Administrator").address), txs = sp.list([sp.record(to_ = sp.contract_address(Contract1), token_id = 0, amount = 100)]))]))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 100; Elt "tz0FakeAdministrator" 100})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
 => test_baselines/scenario_michel/oracle/Oracle/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_017_cont_0_params.json 9
Executing transfer(sp.list([sp.record(from_ = sp.reduce(sp.test_account("Administrator").address), txs = sp.list([sp.record(to_ = sp.reduce(sp.test_account("Oracle1").address), token_id = 0, amount = 1)]))]))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 100; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 1})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
 => test_baselines/scenario_michel/oracle/Oracle/step_018_cont_1_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_018_cont_1_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_018_cont_1_params.json 1
Executing request_tokens(sp.set([sp.contract_address(Contract3), sp.contract_address(Contract4)]))...
 -> (Pair (Pair True "tz0FakeAdministrator") (Pair 10 "Contract_0"))
Executing transfer(sp.list([sp.record(from_ = sp.contract_address(Contract1), txs = sp.list([sp.record(to_ = sp.contract_address(Contract3), token_id = 0, amount = 10), sp.record(to_ = sp.contract_address(Contract4), token_id = 0, amount = 10)]))]))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_3" 10; Elt "Contract_4" 10; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 1})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Comment...
 h2: Client1 sends a request that gets fulfilled
Comment...
 h3: A request
 => test_baselines/scenario_michel/oracle/Oracle/step_021_cont_3_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_021_cont_3_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_021_cont_3_params.json 1
Executing request_xtzusd(sp.record())...
 -> (Pair (Pair "tz0FakeClient1 admin" (Pair 2 "Contract_2")) (Pair (Pair "Contract_0" (Some 1)) (Pair 0 0x0001)))
Executing proxy(sp.record(oracle = sp.contract_address(Contract2), params = sp.record(amount = 2, client_request_id = 1, job_id = sp.bytes('0x0001'), parameters = {}, target = sp.contract_address(Contract3%set_xtzusd), timeout = sp.timestamp(300))))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_2" 2; Elt "Contract_3" 8; Elt "Contract_4" 10; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 1})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Executing create_request(sp.record(client = sp.contract_address(Contract3), params = sp.record(amount = 2, client_request_id = 1, job_id = sp.bytes('0x0001'), parameters = {}, target = sp.contract_address(Contract3%set_xtzusd), timeout = sp.timestamp(300))))...
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 1 {Elt 0 (Pair (Pair 2 (Pair "Contract_3" 1)) (Pair (Pair 0x0001 {}) (Pair "Contract_3%set_xtzusd" "1970-01-01T00:05:00Z")))}) (Pair {Elt (Pair "Contract_3" 1) 0} "Contract_0")))
Comment...
 h3: Ledger
Computing sp.contract_data(0).ledger...
 => {sp.address('tz0FakeAdministrator') : sp.record(balance = 99), sp.address('tz0FakeOracle1') : sp.record(balance = 1), sp.contract_address(Contract1) : sp.record(balance = 80), sp.contract_address(Contract2) : sp.record(balance = 2), sp.contract_address(Contract3) : sp.record(balance = 8), sp.contract_address(Contract4) : sp.record(balance = 10)}
Comment...
 h3: Oracle consumes the request
Verifying sp.pack(sp.set_type_expr(sp.contract_data(2).requests[0].parameters, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))))) == sp.pack(sp.set_type_expr({}, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))))))...
 OK
 => test_baselines/scenario_michel/oracle/Oracle/step_026_cont_2_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_026_cont_2_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_026_cont_2_params.json 1
Executing fulfill_request(sp.record(request_id = 0, result = variant('int', 2500000)))...
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 1 {}) (Pair {} "Contract_0")))
Executing transfer(sp.list([sp.record(from_ = sp.contract_address(Contract2), txs = sp.list([sp.record(to_ = sp.address('tz0FakeOracle1'), token_id = 0, amount = 2)]))]))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_2" 0; Elt "Contract_3" 8; Elt "Contract_4" 10; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 3})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Executing set_xtzusd(sp.record(client_request_id = 1, result = variant('int', 2500000)))...
 -> (Pair (Pair "tz0FakeClient1 admin" (Pair 2 "Contract_2")) (Pair (Pair "Contract_0" None) (Pair 2500000 0x0001)))
Comment...
 h2: Client1 sends a request that gets cancelled
Comment...
 h3: A request
 => test_baselines/scenario_michel/oracle/Oracle/step_029_cont_3_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_029_cont_3_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_029_cont_3_params.json 1
Executing request_xtzusd(sp.record())...
 -> (Pair (Pair "tz0FakeClient1 admin" (Pair 3 "Contract_2")) (Pair (Pair "Contract_0" (Some 2)) (Pair 2500000 0x0001)))
Executing proxy(sp.record(oracle = sp.contract_address(Contract2), params = sp.record(amount = 2, client_request_id = 2, job_id = sp.bytes('0x0001'), parameters = {}, target = sp.contract_address(Contract3%set_xtzusd), timeout = sp.timestamp(300))))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_2" 2; Elt "Contract_3" 6; Elt "Contract_4" 10; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 3})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Executing create_request(sp.record(client = sp.contract_address(Contract3), params = sp.record(amount = 2, client_request_id = 2, job_id = sp.bytes('0x0001'), parameters = {}, target = sp.contract_address(Contract3%set_xtzusd), timeout = sp.timestamp(300))))...
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 2 {Elt 1 (Pair (Pair 2 (Pair "Contract_3" 2)) (Pair (Pair 0x0001 {}) (Pair "Contract_3%set_xtzusd" "1970-01-01T00:05:00Z")))}) (Pair {Elt (Pair "Contract_3" 2) 1} "Contract_0")))
Comment...
 h3: Ledger
Computing sp.contract_data(0).ledger...
 => {sp.address('tz0FakeAdministrator') : sp.record(balance = 99), sp.address('tz0FakeOracle1') : sp.record(balance = 3), sp.contract_address(Contract1) : sp.record(balance = 80), sp.contract_address(Contract2) : sp.record(balance = 2), sp.contract_address(Contract3) : sp.record(balance = 6), sp.contract_address(Contract4) : sp.record(balance = 10)}
Comment...
 h3: Client1 cancels the request
Verifying sp.pack(sp.set_type_expr(sp.contract_data(2).requests[1].parameters, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))))) == sp.pack(sp.set_type_expr({}, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))))))...
 OK
 => test_baselines/scenario_michel/oracle/Oracle/step_034_cont_3_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_034_cont_3_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_034_cont_3_params.json 1
Executing cancel_xtzusd(sp.record())...
 -> (Pair (Pair "tz0FakeClient1 admin" (Pair 3 "Contract_2")) (Pair (Pair "Contract_0" None) (Pair 2500000 0x0001)))
Executing cancel_request(2)...
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 2 {}) (Pair {} "Contract_0")))
Executing transfer(sp.list([sp.record(from_ = sp.contract_address(Contract2), txs = sp.list([sp.record(to_ = sp.contract_address(Contract3), token_id = 0, amount = 2)]))]))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_2" 0; Elt "Contract_3" 8; Elt "Contract_4" 10; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 3})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Comment...
 h2: Client2 sends a request that gets fulfilled
Comment...
 h3: A request
 => test_baselines/scenario_michel/oracle/Oracle/step_037_cont_4_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_037_cont_4_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_037_cont_4_params.json 1
Executing request_xtzusd(sp.record())...
 -> (Pair (Pair "tz0FakeClient2 admin" (Pair 2 "Contract_2")) (Pair (Pair "Contract_0" (Some 1)) (Pair 0 0x0001)))
Executing proxy(sp.record(oracle = sp.contract_address(Contract2), params = sp.record(amount = 2, client_request_id = 1, job_id = sp.bytes('0x0001'), parameters = {}, target = sp.contract_address(Contract4%set_xtzusd), timeout = sp.timestamp(300))))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_2" 2; Elt "Contract_3" 8; Elt "Contract_4" 8; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 3})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Executing create_request(sp.record(client = sp.contract_address(Contract4), params = sp.record(amount = 2, client_request_id = 1, job_id = sp.bytes('0x0001'), parameters = {}, target = sp.contract_address(Contract4%set_xtzusd), timeout = sp.timestamp(300))))...
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 3 {Elt 2 (Pair (Pair 2 (Pair "Contract_4" 1)) (Pair (Pair 0x0001 {}) (Pair "Contract_4%set_xtzusd" "1970-01-01T00:05:00Z")))}) (Pair {Elt (Pair "Contract_4" 1) 2} "Contract_0")))
Comment...
 h3: Ledger
Computing sp.contract_data(0).ledger...
 => {sp.address('tz0FakeAdministrator') : sp.record(balance = 99), sp.address('tz0FakeOracle1') : sp.record(balance = 3), sp.contract_address(Contract1) : sp.record(balance = 80), sp.contract_address(Contract2) : sp.record(balance = 2), sp.contract_address(Contract3) : sp.record(balance = 8), sp.contract_address(Contract4) : sp.record(balance = 8)}
Comment...
 h3: Oracle consumes the request
Verifying sp.pack(sp.set_type_expr(sp.contract_data(2).requests[2].parameters, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))))) == sp.pack(sp.set_type_expr({}, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))))))...
 OK
 => test_baselines/scenario_michel/oracle/Oracle/step_042_cont_2_params.py 1
 => test_baselines/scenario_michel/oracle/Oracle/step_042_cont_2_params.tz 1
 => test_baselines/scenario_michel/oracle/Oracle/step_042_cont_2_params.json 1
Executing fulfill_request(sp.record(request_id = 2, result = variant('int', 2500000)))...
 -> (Pair (Pair (Pair True "tz0FakeOracle1") (Pair 0 5)) (Pair (Pair 3 {}) (Pair {} "Contract_0")))
Executing transfer(sp.list([sp.record(from_ = sp.contract_address(Contract2), txs = sp.list([sp.record(to_ = sp.address('tz0FakeOracle1'), token_id = 0, amount = 2)]))]))...
 -> (Pair (Pair "tz0FakeAdministrator" (Pair 1 {Elt "Contract_1" 80; Elt "Contract_2" 0; Elt "Contract_3" 8; Elt "Contract_4" 8; Elt "tz0FakeAdministrator" 99; Elt "tz0FakeOracle1" 5})) (Pair (Pair {Elt "" 0x} {}) (Pair False {Elt 0 (Pair {Elt "decimals" 0x3138; Elt "name" 0x77726170706564204c494e4b; Elt "symbol" 0x774c494e4b} 200)})))
Executing set_xtzusd(sp.record(client_request_id = 1, result = variant('int', 2500000)))...
 -> (Pair (Pair "tz0FakeClient2 admin" (Pair 2 "Contract_2")) (Pair (Pair "Contract_0" None) (Pair 2500000 0x0001)))
