import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TTicket(sp.TString))

  @sp.entry_point
  def run(self, params):
    with sp.modify(self.data, "t") as "t":
      ticket_38_data, ticket_38_copy = sp.match_tuple(sp.read_ticket_raw(t.value), "ticket_38_data", "ticket_38_copy")
      ticket_38_ticketer, ticket_38_content, ticket_38_amount = sp.match_tuple(ticket_38_data, "ticket_38_ticketer", "ticket_38_content", "ticket_38_amount")
      sp.verify(ticket_38_content == 'abc')
      ticket1_40, ticket2_40 = sp.match_tuple(sp.split_ticket_raw(ticket_38_copy, (ticket_38_amount // 2, ticket_38_amount // 2)).open_some(), "ticket1_40", "ticket2_40")
      sp.result(sp.join_tickets_raw((ticket2_40, ticket1_40)).open_some())