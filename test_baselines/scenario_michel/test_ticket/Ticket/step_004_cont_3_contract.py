import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TTicket(sp.TInt))).layout("m"))

  @sp.entry_point
  def ep1(self, params):
    with sp.match_record(self.data, fields = ["m"]) as m_66:
      match_pair_67_fst, match_pair_67_snd = sp.match_tuple(sp.get_and_update(m_66.value, 42, sp.none), "match_pair_67_fst", "match_pair_67_snd")
      sp.result(sp.record(m = match_pair_67_snd))