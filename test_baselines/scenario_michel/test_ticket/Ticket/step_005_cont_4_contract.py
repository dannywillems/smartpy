import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TTicket(sp.TInt)), x = sp.TInt).layout(("m", "x")))

  @sp.entry_point
  def ep1(self, params):
    with sp.match_record(self.data, fields = ["m", "x"]) as m_77, x_77:
      match_pair_78_fst, match_pair_78_snd = sp.match_tuple(sp.get_and_update(m_77.value, 42, sp.none), "match_pair_78_fst", "match_pair_78_snd")
      sp.result(sp.record(m = match_pair_78_snd, x = x_77.value))

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data, fields = ["m", "x"]) as m_83, x_83:
      ticket_84 = sp.local("ticket_84", sp.ticket('a', 1))
      ticket_85 = sp.local("ticket_85", sp.ticket('b', 2))
      sp.transfer((ticket_84.value, ticket_85.value), sp.tez(0), params)
      sp.result(sp.record(m = m_83.value, x = x_83.value))