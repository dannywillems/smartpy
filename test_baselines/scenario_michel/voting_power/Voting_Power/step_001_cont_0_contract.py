import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = 0, b = 0)

  @sp.entry_point
  def validate(self, params):
    self.data.a = sp.voting_power(sp.hash_key(params))
    self.data.b = sp.total_voting_power