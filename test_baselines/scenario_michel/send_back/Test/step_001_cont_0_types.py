import smartpy as sp

tstorage = sp.TRecord(x = sp.TList(sp.TUnknown()), y = sp.TOption(sp.TUnknown())).layout(("x", "y"))
tparameter = sp.TVariant(bounce = sp.TUnit, bounce2 = sp.TUnit).layout(("bounce", "bounce2"))
tglobals = { }
tviews = { }
