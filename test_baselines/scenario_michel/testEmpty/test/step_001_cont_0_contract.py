import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(l1 = sp.list([]), l2 = sp.list(['c']), m1 = {}, m2 = {'a' : 'b'}, o1 = sp.none)

  @sp.entry_point
  def ep1(self, params):
    self.data.m1 = {'e' : 'f'}
    self.data.l1 = sp.list(['g'])
    self.data.o1 = sp.some('h')

  @sp.entry_point
  def ep2(self, params):
    self.data.m2 = {}
    self.data.l2 = sp.list([])