const Networks = {
    MAINNET: 'Mainnet',
    DELPHINET: 'Delphinet',
    EDONET: 'Edonet',
    EDO2NET: 'Edo2net',
    FLORENCENET: 'Florencenet',
    FLORENCENOBANET: 'FlorenceNoBAnet',
};

const EXPLORER = {
    MAINNET: {
        tzstats: 'https://tzstats.com',
        tzkt: 'https://tzkt.io',
        bcd: 'https://better-call.dev',
    },
    DELPHINET: {
        tzstats: 'https://delphi.tzstats.com',
        tzkt: 'https://delphi.tzkt.io',
        bcd: 'https://better-call.dev/delphinet',
    },
    EDONET: {
        tzstats: 'https://edo.tzstats.com',
        tzkt: 'https://edonet.tzkt.io',
        bcd: 'https://better-call.dev/edonet',
    },
    EDO2NET: {
        tzstats: 'https://edo.tzstats.com',
        tzkt: 'https://edonet.tzkt.io',
        bcd: 'https://better-call.dev/edonet',
    },
    FLORENCENET: {
        tzstats: '',
        tzkt: '',
        bcd: '',
    },
    FLORENCENOBANET: {
        tzstats: '',
        tzkt: '',
        bcd: '',
    },
};

const API = {
    MAINNET: {
        tzkt: 'https://api.tzkt.io/v1',
        tzstats: 'https://api.tzstats.com',
    },
    DELPHINET: {
        tzkt: 'https://api.delphi.tzkt.io/v1',
        tzstats: 'https://api.delphi.tzstats.com',
    },
    EDONET: {
        tzkt: 'https://edonet.smartpy.io/indexer',
        tzstats: 'https://api.edo.tzstats.com',
    },
    EDO2NET: {
        tzkt: 'https://edonet.smartpy.io/indexer',
        tzstats: 'https://api.edo.tzstats.com',
    },
    FLORENCENET: {
        tzkt: '',
        tzstats: '',
    },
    FLORENCENOBANET: {
        tzkt: '',
        tzstats: '',
    },
};

const smartPyNodes = {
    'https://mainnet.smartpy.io': Networks.MAINNET,
    'https://delphinet.smartpy.io': Networks.DELPHINET,
    'https://edonet.smartpy.io': Networks.EDONET,
    'https://florencenet.smartpy.io': Networks.FLORENCENET,
    'https://florencenobanet.smartpy.io': Networks.FLORENCENOBANET,
};

const gigaNodes = {
    'https://mainnet-tezos.giganode.io': Networks.MAINNET,
    'https://delphinet-tezos.giganode.io': Networks.DELPHINET,
    'https://edonet-tezos.giganode.io': Networks.EDONET,
    // From slack (zed)
    // testnet-tezos.giganode.io is supporting Carthage. Will be switched to Delphi testnet when we will have Edo test network launched.
};

const networkFilterByFeature = {
    origination: [],
    faucet: ['mainnet'],
};

/**
 * Filter network by feature
 *
 * @param {string} feature
 * @param {string} node
 *
 * @returns true to skip, false otherwise
 */
const shouldSkipNetwork = (feature, node) => {
    const featureFilter = networkFilterByFeature[feature];
    return featureFilter && featureFilter.some((filter) => node.toLowerCase().includes(filter));
};

/**
 *  GET Request
 */
const requestGET = (url) =>
    new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.timeout = 2000;
        req.onreadystatechange = () => {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    resolve(JSON.parse(req.response));
                } else {
                    reject(req.responseText);
                }
            }
        };
        req.open('GET', url, true);
        req.send();
    });

/**
 * Get the RPC network.
 * @param {string} rpcAddress - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET, DELPHINET, EDONET]
 */
const getRpcNetwork = async (rpcAddress) => {
    const {
        network_version: { chain_name },
    } = await requestGET(`${rpcAddress}/version`).catch(async (e) => {
        console.warn(e);
        return {
            network_version: await requestGET(`${rpcAddress}/network/version`),
        };
    });

    const network = chain_name.split('_')[1];
    if (!Object.keys(Networks).includes(network)) {
        console.error(`Unknown network: ${network}.`);
    }

    return network?.replace(Networks.EDO2NET, Networks.EDONET);
};
