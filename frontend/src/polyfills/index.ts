import './eztz';
import './sodium';

window.smartpyContext = window.smartpyContext || {};

// @TODO[low]: Rodrigo Quelhas - Find a way to disable babel transformation of big int exponentiation (**) to Math.pow
window.smartpyContext.Bls12 = (window as any).Bls12;
