import { Protocol, ProtocolHash, DefaultProtocol } from './protocol';

export enum Network {
    MAINNET = 'Mainnet',
    DELPHINET = 'Delphinet',
    EDONET = 'Edonet',
    FLORENCENET = 'Florencenet',
    FLORENCENOBANET = 'FlorenceNoBAnet',
}

export const NetworkProtocolHash = {
    [String(Network.MAINNET)]: ProtocolHash[DefaultProtocol],
    [String(Network.DELPHINET)]: ProtocolHash[Protocol.DELPHI],
    [String(Network.EDONET)]: ProtocolHash[Protocol.EDO],
    [String(Network.FLORENCENET)]: ProtocolHash[Protocol.FLORENCE],
    [String(Network.FLORENCENOBANET)]: ProtocolHash[Protocol.FLORENCENOBA],
};
