// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import 'jest-canvas-mock';
import crypto from 'crypto';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('jest-fetch-mock').enableMocks();

// Use real translations in testing
fetchMock.mockIf(/^\/static\/locales\/\w{2,2}[.]json$/, async (req) => {
    return {
        status: 200,
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        body: require('fs').readFileSync(`../public${req.url}`, 'r'),
    };
});

// Fix window.crypto in tests
Object.defineProperty(global.self, 'crypto', {
    value: {
        getRandomValues: (arr: any) => crypto.randomBytes(arr.length),
    },
});

// Configure the default unit test timeout
jest.setTimeout(Number(process.env.TEST_TIMEOUT) || 20000); // in milliseconds
