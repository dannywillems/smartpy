declare module 'SmartPyModels' {
    export type OriginationContract = {
        codeJson?: string;
        storageJson?: string;
    };
}

declare module 'OriginationTypes' {
    export type OriginationParameters = {
        name: string;
        source: string;
        balance: number;
        delegate: string;
        fee: number;
        gas_limit: number;
        storage_limit: number;
        code?: string;
        storage?: string;
    };
}
