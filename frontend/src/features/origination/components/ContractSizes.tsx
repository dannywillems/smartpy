import React from 'react';
import ContractSizes from '../../common/components/ContractSizes';
import { useOriginationContract } from '../selectors/contract';
import { getContractSizes } from '../util/michelson';

const OriginationSizes = () => {
    const contract = useOriginationContract();

    const sizes = React.useMemo(() => getContractSizes(contract.codeJson, contract.storageJson), [contract]);

    return sizes ? <ContractSizes codeSize={sizes.code} storageSize={sizes.storage} /> : null;
};

export default OriginationSizes;
