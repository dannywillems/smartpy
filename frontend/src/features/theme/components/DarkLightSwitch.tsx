import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { makeStyles, createStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Light from '@material-ui/icons/Brightness7';
import Dark from '@material-ui/icons/Brightness4';

import selectors from '../../../store/selectors';
import actions from '../../../store/root-action';

const useStyles = makeStyles(() =>
    createStyles({
        darkThemeToggle: {
            marginRight: 20,
        },
    }),
);

const DarkLightSwitch = () => {
    const isDarkMode = selectors.theme.useThemeMode();
    const dispatch = useDispatch();
    const classes = useStyles();

    return (
        <Tooltip
            title="Toggle Light/Dark Theme"
            aria-label="theme"
            placement="left"
            className={classes.darkThemeToggle}
        >
            <IconButton
                aria-label="theme"
                size="small"
                onClick={() => dispatch(actions.theme.setDarkMode(!isDarkMode))}
            >
                {isDarkMode ? <Dark /> : <Light />}
            </IconButton>
        </Tooltip>
    );
};

export default DarkLightSwitch;
