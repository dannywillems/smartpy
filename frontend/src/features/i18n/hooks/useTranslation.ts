import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation as useTranslationHook } from 'react-i18next';

import { RootState } from 'SmartPyTypes';

const useTranslation = (namespaces?: string[]) => {
    const language = useSelector((state: RootState) => state.i18n.language);
    const [t, i18n] = useTranslationHook(namespaces ? namespaces : []);

    useEffect(() => {
        if (language !== i18n.language) {
            i18n.changeLanguage(language);
        }
    }, [i18n, i18n.language, language]);

    return t;
};

export default useTranslation;
