import { createAction } from 'typesafe-actions';
import { IDEContract, IDESettings } from 'SmartPyModels';
import { IDENewcomerGuideSteps } from './types.d';

export enum Actions {
    UPDATE_IDE_CONTRACT = 'UPDATE_IDE_CONTRACT',
    ADD_IDE_CONTRACT = 'ADD_IDE_CONTRACT',
    REMOVE_IDE_CONTRACT = 'REMOVE_IDE_CONTRACT',
    SELECT_IDE_CONTRACT = 'SELECT_IDE_CONTRACT',
    // VOLATILE CONTRACT
    SET_VOLATILE_CONTRACT = 'SET_VOLATILE_CONTRACT',
    // SETTINGS
    UPDATE_IDE_SETTINGS = 'UPDATE_IDE_SETTINGS',
    // ERROR DIALOG
    SHOW_ERROR = 'SHOW_ERROR',
    HIDE_ERROR = 'HIDE_ERROR',
    // SHORTCUTS DIALOG
    SHOW_SHORTCUTS = 'SHOW_SHORTCUTS',
    HIDE_SHORTCUTS = 'HIDE_SHORTCUTS',
    // TEMPLATES
    TOGGLE_FAVORITE_TEMPLATE = 'TOGGLE_FAVORITE_TEMPLATE',
    LOAD_TEMPLATE = 'LOAD_TEMPLATE',
    LOAD_NEXT_TEMPLATE = 'LOAD_NEXT_TEMPLATE',
    LOAD_LAST_TEMPLATE = 'LOAD_LAST_TEMPLATE',
    // NEWCOMER
    UPDATE_NEWCOMER_GUIDE_STEP = 'UPDATE_NEWCOMER_GUIDE_STEP',
    TOGGLE_NEWCOMER_DIALOG = 'TOGGLE_NEWCOMER_DIALOG',
}

// CONTRACTS

export const updateContract = createAction(Actions.UPDATE_IDE_CONTRACT)<IDEContract>();
export const addContract = createAction(Actions.ADD_IDE_CONTRACT)<IDEContract, boolean>();
export const removeContract = createAction(Actions.REMOVE_IDE_CONTRACT)<string>();
export const selectContract = createAction(Actions.SELECT_IDE_CONTRACT)<string>();

// VOLATILE CONTRACT

export const setVolatileContract = createAction(Actions.SET_VOLATILE_CONTRACT)<string>();

// ERROR

export const showError = createAction(Actions.SHOW_ERROR, (error: string) => error)<string>();
export const hideError = createAction(Actions.HIDE_ERROR)<void>();

// Shortcuts

export const showShortcuts = createAction(Actions.SHOW_SHORTCUTS, (platform: string) => platform)<string>();
export const hideShortcuts = createAction(Actions.HIDE_SHORTCUTS)<void>();

// Templates

export const toggleFavoriteTemplate = createAction(Actions.TOGGLE_FAVORITE_TEMPLATE)<string>();
export const loadTemplate = createAction(Actions.LOAD_TEMPLATE)<string>();
export const loadLastTemplate = createAction(Actions.LOAD_LAST_TEMPLATE)<void>();
export const loadNextTemplate = createAction(Actions.LOAD_NEXT_TEMPLATE)<void>();

// SETTINGS

export const updateSettings = createAction(
    Actions.UPDATE_IDE_SETTINGS,
    (settings: IDESettings) => settings,
)<IDESettings>();

// NEWCOMER
export const toggleNewcomerDialog = createAction(Actions.TOGGLE_NEWCOMER_DIALOG)<boolean | void>();
export const updateNewcomerGuideStep = createAction(Actions.UPDATE_NEWCOMER_GUIDE_STEP)<IDENewcomerGuideSteps>();

const actions = {
    updateContract,
    addContract,
    removeContract,
    updateSettings,
    selectContract,
    showError,
    hideError,
    showShortcuts,
    hideShortcuts,
    loadTemplate,
    loadLastTemplate,
    loadNextTemplate,
    updateNewcomerGuideStep,
    toggleNewcomerDialog,
    toggleFavoriteTemplate,
    setVolatileContract,
};

export default actions;
