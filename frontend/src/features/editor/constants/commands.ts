export default {
    showShortcutsWin: {
        name: 'showShortcutsWin',
        description: 'Show Shortcuts',
        bindKey: {
            win: 'Shift-Ctrl-H',
            mac: '',
        },
    },
    showShortcutsMac: {
        name: 'showShortcutsMac',
        description: 'Show Shortcuts',
        bindKey: {
            win: '',
            mac: 'Shift-Cmd-H',
        },
    },
    loadLastTemplate: {
        name: 'loadLastTemplate',
        description: 'Load Last Template',
        bindKey: {
            win: 'Ctrl-Alt-R',
            mac: 'Shift-Cmd-R',
        },
    },
    loadNextTemplate: {
        name: 'loadNextTemplate',
        description: 'Load Next Template',
        bindKey: {
            win: 'Ctrl-Alt-N',
            mac: 'Shift-Cmd-N',
        },
    },
    runCode: {
        name: 'runCode',
        description: 'Run Code',
        bindKey: {
            win: 'Ctrl-Enter',
            mac: 'Cmd-Enter',
        },
    },
    runCodeWithoutTests: {
        name: 'runCodeWithoutTests',
        description: 'Run Code Without Tests',
        bindKey: {
            win: 'Shift-Ctrl-Enter',
            mac: 'Shift-Cmd-Enter',
        },
    },
} as { [key: string]: { name: string; description: string; bindKey: { win: string; mac: string } } };
