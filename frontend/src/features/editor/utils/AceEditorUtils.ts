import completers from '../constants/completers';
import { Ace } from 'ace-builds';

export const getCompleters = (aceCompleters: Ace.Completer[]) => ({
    getCompletions: (
        editor: Ace.Editor,
        session: Ace.EditSession,
        point: Ace.Point,
        prefix: string,
        callback: (a: unknown, b: Ace.Completion[]) => void,
    ) => {
        const getCompletions = (completions?: Ace.Completion[]) => {
            let finalCompletions = completions || [];

            editor.clearSelection();
            const posX = point.column - prefix.length - 3;
            let fullPrefix = prefix;
            const line = editor.getValue().split(/\r?\n/)[point.row];
            if (0 <= posX && line.substring(posX, posX + 3) === 'sp.') {
                fullPrefix = line.substring(posX, posX + prefix.length + 3);
            }
            prefix = fullPrefix;
            if (fullPrefix.startsWith('sp.')) {
                finalCompletions = completers;
            }

            callback(
                null,
                finalCompletions.filter((c) => c.caption?.startsWith(prefix)),
            );
        };

        // Get Ace completers
        if (Array.isArray(aceCompleters) && aceCompleters.length > 0) {
            aceCompleters[1].getCompletions(editor, session, point, prefix, (_, completions: Ace.Completion[]) => {
                getCompletions(completions);
            });
        } else {
            getCompletions();
        }
    },
});

const AceEditorUtils = {
    getCompleters,
};

export default AceEditorUtils;
