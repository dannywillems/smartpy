import pako from 'pako';
import httpRequester from '../../../services/httpRequester';

import toast from '../../../services/toast';
import { getAllTemplates } from '../constants/templates';
import { downloadFile, getFileContent } from '../../../utils/file';
import { getBase } from '../../../utils/url';

/**
 * Evaluate the code in the editor
 *
 * @param {boolean} withTests - When true, the evaluation will run with tests.
 */
export const evalRun = async (withTests: boolean) => {
    try {
        if (typeof window.evalRun == 'function') {
            window.evalRun(withTests);
        } else {
            toast.error('SmartPyIO is not loaded yet...');
        }
    } catch (error) {
        try {
            window.showTraceback(error, String(error));
        } catch (_error) {
            toast.error(String(error));
        }
    }
};

/**
 * Evaluate a specific test.
 *
 * @param {string} testName - Name of the Tests to evaluate.
 */
export const evalTest = async (testName: string) => {
    try {
        window.evalTest(testName);
    } catch (error) {
        window.showTraceback(error, String(error));
    }
};

export const getEmbeddedLink = (contract: string) => {
    const encoded = btoa(pako.deflate(contract, { to: 'string' }))
        .replace(/\+/g, '@')
        .replace(/\//g, '_')
        .replace(/=/g, '-');
    return `${window.location.origin}/ide?code=${encoded}`;
};

/**
 * Get template code.
 * @param {string} templateName - Template name.
 * @return {Promise<string | void>} A promise with the template code, or null if the template doesn't exist.
 */
export const getTemplateCode = async (templateName: string): Promise<string> => {
    const template = getAllTemplates()[templateName]?.fileName || templateName;

    return getFileContent(`${getBase()}/templates/${template}`);
};

export const downloadOutputPanel = async (
    contractName = 'output-panel',
    isDark: boolean,
    outputPanel: HTMLDivElement,
) => {
    const output = outputPanel.innerHTML.replace(/\/static/g, `${window.location.origin}/static`);
    let styles = await (await httpRequester.get(`${window.location.origin}/static/css/smart.css`)).data;
    styles += await (await httpRequester.get(`${window.location.origin}/static/css/typography.css`)).data;
    styles = styles.replace(/\/static/g, `${window.location.origin}/static`);
    const script = await (await httpRequester.get(`${window.location.origin}/static/js/smart.js`)).data;
    downloadFile(
        `${contractName}.html`,
        `<html>
            <head>
                <style>${styles}</style>
                <script>${script}</script>
            </head>
            <body>
                <div id="outputPanel">${output}</div>
            </body>
        </html>`,
    );
};
