import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { makeStyles, createStyles, Theme, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';

// React Ace IDE
import AceEditor from 'react-ace';
import 'ace-builds/src-min-noconflict/mode-python';
import 'ace-builds/src-min-noconflict/theme-chrome';
import 'ace-builds/src-min-noconflict/theme-monokai';
import 'ace-builds/src-min-noconflict/ext-language_tools';
import 'ace-builds/src-min-noconflict/ext-searchbox';

// State Management
import { IDESettings } from 'SmartPyModels';
import actions from '../actions';
import { useSelectedContract } from '../selectors';

// Local Components
import EditorToolBar from '../components/EditorToolBar';
import OutputPanel from '../../common/components/OutputPanel';
import ContractManagement from '../containers/ContractManagement';
// Local Constants
import commands from '../constants/commands';

// Local Utils
import { evalRun } from '../utils/EditorUtils';
import { evalTest } from '../utils/EditorUtils';
import { getCompleters } from '../utils/AceEditorUtils';
import { downloadOutputPanel } from '../utils/EditorUtils';
import toast from '../../../services/toast';
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = (stacked: boolean) =>
    makeStyles((theme: Theme) =>
        createStyles({
            root: {
                display: 'flex',
                flexDirection: 'column',
                flexGrow: 1,
            },
            editorSection: {
                display: 'flex',
                flexGrow: 1,
                borderTopWidth: 2,
                borderTopStyle: 'solid',
                borderTopColor: theme.palette.primary.light,
                // screen height - navBar - toolBar (percentage doesn't work here)
                height: 'calc(100vh - 80px - 60px)',
            },
            outputPanel: {
                background: theme.palette.background.paper,
                position: 'relative',
                height: stacked ? '40%' : '100%',
                padding: 15,
                overflowY: 'auto',
            },
            borderLeft: {
                borderLeftWidth: 2,
                borderLeftStyle: 'solid',
                borderLeftColor: theme.palette.primary.light,
            },
            borderTop: {
                borderTopWidth: 2,
                borderTopStyle: 'solid',
                borderTopColor: theme.palette.primary.light,
            },
            editor: {
                display: 'flex',
                flexDirection: 'column',
                height: stacked ? '60%' : '100%',
            },
            hidden: {
                display: 'none',
            },
        }),
    );

interface OwnProps {
    clearOutputs: () => void;
    editorRef: React.RefObject<AceEditor>;
    htmlOutput: { __html: string };
    settings: IDESettings;
    tests: string[];
    showError: (error: string) => void;
}

const EditorView: React.FC<OwnProps> = (props) => {
    const classes = useStyles(props.settings.layout === 'stacked')();
    const outputPanelRef = React.useRef((null as unknown) as HTMLDivElement);
    const theme = useTheme();
    const editorTheme = theme.palette.mode === 'dark' ? 'monokai' : 'chrome';
    const dispatch = useDispatch();
    const contract = useSelectedContract();
    const t = useTranslation();
    const { tests, clearOutputs, editorRef, htmlOutput, settings, showError } = props;
    const downMd = useMediaQuery((theme: Theme) => theme.breakpoints.down('xs'));
    const [compiling, setCompiling] = React.useState(false);

    const getGridSm = () => (settings.layout === 'side-by-side' ? 6 : 12);
    const getBorderClass = () =>
        settings.layout !== 'side-by-side' || downMd ? classes.borderTop : classes.borderLeft;

    const downloadOutput = () => {
        if (contract && htmlOutput.__html) {
            downloadOutputPanel(contract.name, theme.palette.mode === 'dark', outputPanelRef.current);
        } else {
            toast.error(t('ide.errors.outputEmpty'));
        }
    };

    const onInput = () => {
        if (editorRef.current) {
            const newCode = editorRef.current.editor.getValue();
            if (contract?.id && newCode !== contract?.code) {
                dispatch(
                    actions.updateContract({
                        ...contract,
                        code: newCode,
                    }),
                );
            } else if (!contract?.id) {
                dispatch(
                    actions.addContract(
                        {
                            code: newCode,
                            shared: false,
                        },
                        false,
                    ),
                );
            }
        }
    };

    const compileContract = async (withTests: boolean) => {
        setCompiling(true);
        try {
            await new Promise((r) => setTimeout(r, 500));
            await evalRun(withTests);
        } catch (error) {
            showError(error);
        }
        setCompiling(false);
    };

    const runEvalTest = async (testName: string) => {
        setCompiling(true);
        try {
            await evalTest(testName);
        } catch (error) {
            showError(error);
        }
        setCompiling(false);
    };

    const resizeEditor = React.useCallback(() => {
        editorRef.current?.editor.resize(true);
    }, [editorRef]);

    /**
     * This method returns the editor
     */
    const showEditor = () => {
        return (
            <Grid
                item
                xs={getGridSm()}
                className={`${classes.editor} ${settings.layout === 'output-only' ? classes.hidden : ''}`}
            >
                <ContractManagement onResize={resizeEditor} />
                <div style={{ display: 'flex', flexGrow: 1 }}>
                    <AceEditor
                        debounceChangePeriod={1000}
                        placeholder={contract?.id ? 'Place your code here...' : 'Start typing to initiate a contract.'}
                        ref={editorRef}
                        mode="python"
                        theme={editorTheme}
                        onLoad={(editor) => {
                            window.editor = editor;
                            editor.renderer.setScrollMargin(0, 30, 0, 0);
                        }}
                        setOptions={{
                            enableBasicAutocompletion: true,
                            enableLiveAutocompletion: false,
                            showLineNumbers: true,
                            tabSize: 4,
                            useWorker: true,
                            autoScrollEditorIntoView: true,
                            showPrintMargin: false,
                            useSoftTabs: true,
                            showGutter: true,
                            wrap: true,
                            highlightActiveLine: true,
                        }}
                        commands={[
                            {
                                ...commands['showShortcutsWin'],
                                exec: () => {
                                    try {
                                        dispatch(actions.showShortcuts('win'));
                                    } catch (error) {
                                        showError(error);
                                    }
                                },
                            },
                            {
                                ...commands['showShortcutsMac'],
                                exec: () => {
                                    try {
                                        dispatch(actions.showShortcuts('mac'));
                                    } catch (error) {
                                        showError(error);
                                    }
                                },
                            },
                            {
                                ...commands['loadLastTemplate'],
                                exec: () => {
                                    dispatch(actions.loadLastTemplate());
                                },
                            },
                            {
                                ...commands['loadNextTemplate'],
                                exec: () => {
                                    dispatch(actions.loadNextTemplate());
                                },
                            },
                            {
                                ...commands['runCode'],
                                exec: () => compileContract(true),
                            },
                            {
                                ...commands['runCodeWithoutTests'],
                                exec: async () => compileContract(false),
                            },
                        ]}
                        value={contract?.code || ''}
                        name="smartpy-editor"
                        editorProps={{ $blockScrolling: true }}
                        width="100%"
                        height="auto"
                        onInput={onInput}
                    />
                </div>
            </Grid>
        );
    };

    React.useEffect(() => {
        const editor = editorRef.current?.editor;
        if (editor) {
            editor.completers = [getCompleters(editor.completers)];
            resizeEditor();
        }
    }, [editorRef, resizeEditor, settings.layout]);

    return (
        <div className={classes.root}>
            <EditorToolBar
                tests={tests}
                clearOutputs={clearOutputs}
                downloadOutputPanel={downloadOutput}
                compileContract={compileContract}
                runEvalTest={runEvalTest}
            />
            <div className={classes.editorSection}>
                <Grid container justifyContent="center">
                    {showEditor()}
                    <Grid
                        item
                        xs={getGridSm()}
                        className={`${classes.outputPanel} ${getBorderClass()} ${
                            settings.layout === 'editor-only' ? classes.hidden : ''
                        }`}
                        ref={outputPanelRef}
                    >
                        <OutputPanel output={htmlOutput} isContractCompiling={compiling} />
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default EditorView;
