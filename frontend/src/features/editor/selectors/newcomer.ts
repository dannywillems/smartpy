import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';

export const useNewcomerMode = () =>
    useSelector<RootState, boolean>((state) => state.editor.settings.newcomersMode || false);

export const useNewcomerDialogOpen = () => useSelector((state: RootState) => state.editor.newcomerDialogOpen);

export const useNewcomerGuideStep = () => useSelector((state: RootState) => state.editor.newcomerGuideStep);

export const useFavoriteTemplates = () =>
    useSelector<RootState, string[]>((state) => state.editor?.favoriteTemplates || []);
