import React from 'react';

// Material UI
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import HomeHeaderMenu from './HomeHeaderMenu';
import Paper from '@material-ui/core/Paper';

import Logo from '../../common/elements/Logo';
import DarkLightSwitch from '../../theme/components/DarkLightSwitch';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            flexGrow: 1,
        },
        logoSection: {
            flexGrow: 1,
            display: 'flex',
            padding: 20,
        },
        logoParagraph: {
            margin: '-10px 0 0 40px',
            fontVariant: 'small-caps',
            fontFamily: 'Verdana, sans-serif',
            color: theme.palette.mode === 'dark' ? '#66aacc' : '#01608c',
        },
        logoLink: {
            textDecoration: 'none',
        },
        logo: {
            width: 230,
            [theme.breakpoints.down('sm')]: {
                width: 150,
            },
        },
        regular: {
            minHeight: 0,
        },
        paper: {
            background: theme.palette.background.default,
            display: 'flex',
            flexGrow: 1,
        },
    }),
);

const HomeHeader = () => {
    const classes = useStyles();

    return (
        <div className={classes.appBar}>
            <AppBar position="static" color="primary">
                <Paper square className={classes.paper} color="primary">
                    <Toolbar className={classes.paper} color="primary">
                        <div className={classes.logoSection}>
                            <div className={classes.logoLink}>
                                <Logo />
                                <p className={classes.logoParagraph}>by Smart Chain Arena</p>
                            </div>
                        </div>
                        <DarkLightSwitch />
                        <HomeHeaderMenu />
                    </Toolbar>
                </Paper>
            </AppBar>
            <Toolbar id="back-to-top-anchor" disableGutters className={classes.regular} />
        </div>
    );
};

export default HomeHeader;
