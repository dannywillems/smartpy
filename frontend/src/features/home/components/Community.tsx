import React from 'react';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            margin: 20,
        },
        card: {
            borderWidth: 2,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
        },
        media: {
            height: 140,
            backgroundSize: 200,
        },
        title: {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
            margin: 20,
        },
        description: {
            color: theme.palette.primary.light,
            margin: 20,
        },
    }),
);

const Community: React.FC = () => {
    const classes = useStyles();

    return (
        <div className={classes.root} id="community">
            <Typography variant="h4" className={classes.title}>
                In The Tezos Community
            </Typography>
            <Typography gutterBottom variant="h6" className={classes.description}>
                We are grateful to be part of the Tezos community and for the tremendous support received from TQ Tezos
                and the Tezos Foundation.
            </Typography>
            <Grid container spacing={2} justifyContent="center">
                <Grid item xs={12} sm={4} lg={3}>
                    <Card className={classes.card} raised>
                        <CardActionArea href="https://tqtezos.com">
                            <CardMedia
                                className={classes.media}
                                image={`${getBase()}/static/img/TQ_Tezos_Logo_Update-BF-outlined_TQ_Tezos_Logo_Color.svg`}
                                title="Tocqueville Group"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    TQ Tezos
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    TQ Tezos (Tocqueville Group) works to advance the Tezos ecosystem by creating open
                                    source software and other public goods, providing support to projects and companies
                                    building on Tezos, and connecting the global Tezos community.
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary" variant="outlined" href="https://tqtezos.com">
                                Read More
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={4} lg={3}>
                    <Card className={classes.card} raised>
                        <CardActionArea href="https://tezos.foundation">
                            <CardMedia
                                className={classes.media}
                                image={`${getBase()}/static/img/tf.png`}
                                title="Tezos Foundation"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    Tezos Foundation
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    The Tezos Foundation stands as part of the community in support of the Tezos
                                    protocol and ecosystem.
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary" variant="outlined" href="https://tezos.foundation">
                                Read More
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={4} lg={3}>
                    <Card className={classes.card} raised>
                        <CardActionArea href="https://tezos.com">
                            <CardMedia
                                className={classes.media}
                                image={`${getBase()}/static/img/tz.png`}
                                title="Tezos"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    Tezos
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Tezos is an open-source platform for assets and applications backed by a global
                                    community of validators, researchers, and builders.
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary" variant="outlined" href="https://tezos.com">
                                Read More
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
};

export default Community;
