import React from 'react';

// Material UI
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import CodeIcon from '@material-ui/icons/Code';

// Local Elements
import RouterFab from '../../navigation/elements/RouterFab';

// Local Styles
import useCommonStyles from '../../../hooks/useCommonStyles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        mainButton: {
            marginRight: 10,
        },
        innerIcon: {
            marginRight: theme.spacing(1),
        },
    }),
);

const menuButtons = [
    {
        component: Button,
        props: {
            href: '#introduction',
        } as any,
        text: 'Introduction',
    },
    {
        component: Button,
        props: {
            href: '#overview',
        } as any,
        text: 'Overview',
    },
    {
        component: Button,
        props: {
            href: '#articles',
        } as any,
        text: 'Articles & Guides',
    },
    {
        component: Button,
        props: {
            href: '#community',
        } as any,
        text: 'Tezos Community',
    },
    {
        component: Button,
        props: {
            href: '#contact',
        } as any,
        text: 'Contact',
    },
];

const wideMenuItems = menuButtons.map((item, index) => (
    <item.component key={index} {...item.props}>
        {item.text}
    </item.component>
));

const mobileMenuItems = menuButtons.map((item, index) => (
    <MenuItem key={index}>
        <item.component key={index} {...item.props} fullWidth>
            {item.text}
        </item.component>
    </MenuItem>
));

const HomeHeaderMenu = () => {
    const showWideMenu = useMediaQuery('(min-width:1300px)');
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const classes = {
        ...useStyles(),
        ...useCommonStyles(),
    };

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <React.Fragment>
            {showWideMenu ? (
                <React.Fragment>
                    <RouterFab color="primary" className={classes.mainButton} variant="extended" to="ide">
                        <CodeIcon className={classes.innerIcon} />
                        Online Editor
                    </RouterFab>
                    {wideMenuItems}
                </React.Fragment>
            ) : (
                <React.Fragment>
                    <IconButton aria-label="menu" aria-controls="menu" aria-haspopup="true" onClick={handleClick}>
                        <MenuIcon fontSize="large" />
                    </IconButton>
                    <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                        className={classes.noShadow}
                    >
                        <MenuItem>
                            <RouterFab color="primary" className={classes.noShadow} variant="extended" to="ide">
                                <CodeIcon className={classes.innerIcon} />
                                Online Editor
                            </RouterFab>
                        </MenuItem>
                        {mobileMenuItems}
                    </Menu>
                </React.Fragment>
            )}
        </React.Fragment>
    );
};

export default HomeHeaderMenu;
