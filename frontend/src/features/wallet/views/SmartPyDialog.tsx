import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
import DialogActions from '@material-ui/core/DialogActions';
import Button, { ButtonProps } from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import KeyIcon from '@material-ui/icons/VpnKey';
import { TransitionProps } from '@material-ui/core/transitions/transition';
import Slide from '@material-ui/core/Slide';

import useTranslation from '../../i18n/hooks/useTranslation';
import Logo from '../../common/elements/Logo';
import { clearAccountInfo } from '../actions';
import LedgerIcon from '../../common/elements/icons/Ledger';
import FaucetIcon from '../../common/elements/icons/Faucet';
import LedgerForm from '../components/LedgerForm';
import FaucetForm from '../components/FaucetForm';
import SecretKeyForm from '../components/SecretKeyForm';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        panel: {
            padding: theme.spacing(2, 0, 2, 0),
            overflowY: 'auto',
        },
        fullHeight: {
            height: '100%',
        },
        marginBottom: {
            marginBottom: 10,
        },
        hidden: {
            display: 'none',
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        input: {
            padding: '10px 12px 8px',
        },
        spacer: {
            margin: theme.spacing(2),
        },
        section: {
            padding: 20,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        centralSection: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            backgroundColor: theme.palette.background.paper,
            padding: 10,
        },
    }),
);

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className: string;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

/**
 * @summary Accessibility props (a11y)
 */
const a11yProps = (index: number) => {
    return {
        id: `tab-${index}`,
        'aria-controls': `tabpanel-${index}`,
    };
};

type OwnProps = ButtonProps;

const SmartPyDialog: React.FC<OwnProps> = ({ ...props }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [open, setOpen] = React.useState(false);
    const t = useTranslation();
    const dispatch = useDispatch();

    const handleOpen = () => {
        setOpen((isTrue) => {
            if (!isTrue) {
                // Reset Account Information
                dispatch(clearAccountInfo());
            }
            return !isTrue;
        });
    };

    const handleChange = (_: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setValue((state) => {
            if (state !== newValue) {
                dispatch(clearAccountInfo());
            }
            return newValue;
        });
    };

    return (
        <>
            <Button {...props} onClick={handleOpen}>
                <Logo />
            </Button>
            <Dialog maxWidth="md" fullWidth onClose={handleOpen} open={open} TransitionComponent={Transition}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        scrollButtons="auto"
                        aria-label="SmartPy Wallet Dialog"
                        variant="fullWidth"
                    >
                        <Tab {...a11yProps(0)} icon={<LedgerIcon />} label={t('Ledger')} />
                        <Tab {...a11yProps(1)} icon={<FaucetIcon />} label={t('Faucet')} />
                        <Tab {...a11yProps(2)} icon={<KeyIcon />} label={t('Secret Key')} />
                    </Tabs>
                </AppBar>

                <TabPanel value={value} className={classes.panel} index={0}>
                    <LedgerForm />
                </TabPanel>
                <TabPanel value={value} className={classes.panel} index={1}>
                    <FaucetForm />
                </TabPanel>
                <TabPanel value={value} className={classes.panel} index={2}>
                    <SecretKeyForm />
                </TabPanel>

                <Divider />
                <DialogActions>
                    <Button autoFocus color="primary" onClick={handleOpen}>
                        {t('common.close')}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default SmartPyDialog;
