import React from 'react';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import CancelIcon from '@material-ui/icons/Cancel';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions/transition';

// Utils
import { prettifyJsonString } from '../../../utils/json';
// Components
import CodeBlock from '../../common/components/CodeBlock';
// State Management
import useTranslation from '../../i18n/hooks/useTranslation';

import { OriginationInformation } from 'SmartPyWalletTypes';
import TezosWalletIcon from '../../common/elements/icons/TezosWallet';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(2, 0, 2, 0),
            overflowY: 'auto',
        },
        tabContent: {
            padding: 20,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        spacer: {
            margin: theme.spacing(2),
        },
        fullHeight: {
            height: '100%',
        },
        redButton: {
            backgroundColor: theme.palette.error.main,
        },
    }),
);

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="down" ref={ref} {...props} />;
});

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
    className: string;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && children}
        </div>
    );
}

interface OwnProps {
    originationInformation?: OriginationInformation;
    cancel: () => void;
    accept: () => void;
}

const OriginationInformationDialog: React.FC<OwnProps> = ({ originationInformation, cancel, accept }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const t = useTranslation();

    const handleChange = (_: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setValue(newValue);
    };

    const open = React.useMemo(() => !!originationInformation?.bytes, [originationInformation]);

    return (
        <Dialog maxWidth="md" fullWidth onClose={cancel} open={open} TransitionComponent={Transition}>
            <DialogTitle id="operation-information-title">{t('wallet.operation.title')}</DialogTitle>
            <DialogContent dividers>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="Operation Information Dialog"
                    >
                        <Tab label={t('wallet.operation.hashes')} key={0} />
                        <Tab label={t('wallet.operation.jsonEncodingNoScript')} key={1} />
                        <Tab label={t('wallet.operation.jsonEncoding')} key={2} />
                        <Tab label={t('wallet.operation.bytesEncoding')} key={3} />
                    </Tabs>
                </AppBar>

                <TabPanel value={value} index={0} className={classes.root}>
                    <Paper className={classes.tabContent}>
                        <TextField
                            variant="outlined"
                            disabled
                            fullWidth
                            label={t('wallet.operation.blake2BHash')}
                            value={originationInformation?.blake2bHash || ''}
                        />
                        <Divider className={classes.spacer} />
                        <TextField
                            variant="outlined"
                            disabled
                            fullWidth
                            label={t('wallet.operation.blockHash')}
                            value={originationInformation?.blockHash || ''}
                        />
                    </Paper>
                </TabPanel>
                <TabPanel value={value} index={1} className={classes.root}>
                    <CodeBlock
                        language="json"
                        showLineNumbers
                        text={
                            prettifyJsonString(
                                JSON.stringify({
                                    ...(originationInformation?.operationContents || {}),
                                    script: undefined,
                                }),
                                4,
                            ) || ''
                        }
                    />
                </TabPanel>
                <TabPanel value={value} index={2} className={classes.root}>
                    <CodeBlock
                        language="json"
                        showLineNumbers
                        text={
                            prettifyJsonString(JSON.stringify(originationInformation?.operationContents || {}), 4) || ''
                        }
                    />
                </TabPanel>
                <TabPanel value={value} index={3} className={classes.root}>
                    <Paper className={classes.tabContent}>
                        <TextField
                            variant="outlined"
                            disabled
                            fullWidth
                            multiline
                            minRows={15}
                            label={t('wallet.operation.bytesEncoding')}
                            value={originationInformation?.bytes || ''}
                        />
                    </Paper>
                </TabPanel>
            </DialogContent>
            <DialogActions>
                <Button onClick={cancel} variant="contained" className={classes.redButton} endIcon={<CancelIcon />}>
                    {t('common.cancel')}
                </Button>
                <Button autoFocus onClick={accept} variant="contained" endIcon={<TezosWalletIcon />}>
                    {t('common.accept')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default OriginationInformationDialog;
