import { NetworkType, PermissionScope, DAppClient } from '@airgap/beacon-sdk';

import info from '../../../constants/info';
import { AccountSource } from '../constants/sources';
import AbstractWallet from './AbstractWallet';
import BeaconSigner from './BeaconSigner';

class Beacon extends AbstractWallet {
    private client;

    constructor() {
        super(AccountSource.BEACON);
        const options = {
            name: info.name,
            iconUrl: `${window.location.origin}${process.env.PUBLIC_URL}/static/img/logo-only.png`,
            preferredNetwork: NetworkType.CUSTOM,
        };
        this.client = new DAppClient(options);
        this.signer = new BeaconSigner(this.client);
    }

    /**
     * @description Connect to the wallet.
     *
     * @param options Options passed to the wallet.
     *
     * @returns A promise that resolves to void;
     */
    public connect = async (network: { name: string; rpc: string }) => {
        // Reset config, we want users to always be able to select a wallet provider.
        this.client.clearActiveAccount();

        const permissionsOutput = await this.client.requestPermissions({
            network: { type: NetworkType.CUSTOM, name: network.name, rpcUrl: network.rpc },
            scopes: [PermissionScope.SIGN],
        });

        this.rpc = network.rpc;
        this.pkh = permissionsOutput.address;
    };
}

export default Beacon;
