import { DAppClient, SigningType } from '@airgap/beacon-sdk';
import bs58check from 'bs58check';

import { Signer } from './Signer';

class BeaconSigner implements Signer {
    private client;
    constructor(client: DAppClient) {
        this.client = client;
    }

    public sign = async (bytes: string) => {
        const result = await this.client.requestSignPayload({
            signingType: SigningType.RAW,
            payload: '03' + bytes, // 0x03 generic prefix
        });

        let sbytes = bs58check.decode(result.signature);
        if (result.signature.startsWith('edsig') || result.signature.startsWith('spsig1')) {
            sbytes = sbytes.slice(5).toString('hex');
        } else if (result.signature.startsWith('p2sig')) {
            sbytes = sbytes.slice(4).toString('hex');
        } else {
            sbytes = sbytes.slice(3).toString('hex');
        }

        return {
            bytes,
            sig: sbytes,
            prefixSig: result.signature,
            sbytes: `${bytes}${sbytes}`,
        };
    };

    public publicKey = async () => (await this.client.getActiveAccount())?.publicKey as string;

    public publicKeyHash = async () => (await this.client.getActiveAccount())?.address as string;

    secretKey(): Promise<string | undefined> {
        throw new Error('Method not implemented.');
    }

    public isReady = async () => await this.client.ready;
}

export default BeaconSigner;
