import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import ResetIcon from '@material-ui/icons/RotateLeft';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/core/Alert';

import useTranslation from '../../i18n/hooks/useTranslation';
import { clearAccountInfo, loadingAccountInfo, updateAccountInfo } from '../actions';
import { Curve } from '../services/smartpy/LedgerSigner';
import WalletServices from '../services';
import { getRpcNetwork } from '../../../utils/tezosRpc';
import { useNetworkInfo } from '../selectors/network';
import ledgerInfo from '../constants/ledger';
import AccountInfo from './AccountInfo';
import { usingSafari } from '../../../utils/browser';
import logger from '../../../services/logger';
import { AccountSource } from '../constants/sources';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 5,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
        },
        fullHeight: {
            height: '100%',
        },
        section: {
            padding: 20,
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        alert: {
            margin: theme.spacing(2, 0, 2, 0),
            padding: 20,
        },
    }),
);

enum FormField {
    CURVE = 'curve',
    DERIVATION_PATH = 'derivationPath',
}

const DEFAULT_FORM_VALUES = {
    [FormField.CURVE]: Curve.ED25519,
    [FormField.DERIVATION_PATH]: ledgerInfo.defaultDerivationPath,
};

const LedgerForm: React.FC = () => {
    const classes = useStyles();
    const [values, setValues] = React.useState(DEFAULT_FORM_VALUES);
    const t = useTranslation();
    const dispatch = useDispatch();
    const { rpc } = useNetworkInfo();

    const resetDerivationPath = () => {
        setValues((state) => ({ ...state, [FormField.DERIVATION_PATH]: ledgerInfo.defaultDerivationPath }));
    };

    const handleInputChange = (
        event: React.ChangeEvent<{
            name?: string | undefined;
            value: string;
        }>,
    ) => {
        const { name, value } = event.target;
        if (name) {
            setValues((state) => ({ ...state, [name]: value }));
        }
    };

    const onConnect = async () => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            const network = await getRpcNetwork(rpc);
            if (network) {
                await WalletServices[AccountSource.SMARTPY_LEDGER].connect(
                    { name: network.toLowerCase(), rpc },
                    { curve: values[FormField.CURVE], path: values[FormField.DERIVATION_PATH] },
                );
                const accountInformation = await WalletServices[AccountSource.SMARTPY_LEDGER].getInformation();
                dispatch(
                    updateAccountInfo({
                        ...accountInformation,
                        network,
                    }),
                );
            }
        } catch (e) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    };

    return (
        <div className={classes.root}>
            <Typography variant="overline">{t('wallet.ledgerForm.title')}</Typography>
            <Paper className={classes.section}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <FormControl variant="filled" fullWidth>
                            <InputLabel>{t('wallet.curve')}</InputLabel>
                            <Select
                                name={FormField.CURVE}
                                value={String(values[FormField.CURVE])}
                                onChange={handleInputChange}
                            >
                                <MenuItem value={Curve.ED25519}>
                                    <em>ed25519 (tz1)</em>
                                </MenuItem>
                                <MenuItem value={Curve.SECP256K1}>
                                    <em>secp256k1 (tz2)</em>
                                </MenuItem>
                                <MenuItem value={Curve.P256}>
                                    <em>p256 (tz3)</em>
                                </MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            name={FormField.DERIVATION_PATH}
                            fullWidth
                            variant="filled"
                            label={t('wallet.derivationPath')}
                            value={values[FormField.DERIVATION_PATH]}
                            onChange={handleInputChange}
                            InputProps={{
                                endAdornment: (
                                    <Tooltip
                                        title={t('wallet.resetDerivationPath') as string}
                                        aria-label="reset-derivation-path"
                                        placement="left"
                                    >
                                        <span>
                                            <IconButton
                                                disabled={
                                                    values[FormField.DERIVATION_PATH] ===
                                                    ledgerInfo.defaultDerivationPath
                                                }
                                                color="primary"
                                                onClick={resetDerivationPath}
                                            >
                                                <ResetIcon />
                                            </IconButton>
                                        </span>
                                    </Tooltip>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.fullHeight}
                            onClick={onConnect}
                            disabled={usingSafari()}
                        >
                            {t('Connect')}
                        </Button>
                    </Grid>
                </Grid>
            </Paper>

            {usingSafari() ? (
                <Alert severity="error" className={classes.alert}>
                    {t('wallet.ledgerForm.safariNotSupported')}
                </Alert>
            ) : (
                <AccountInfo />
            )}
        </div>
    );
};

export default LedgerForm;
