enum LedgerErrorCodes {
    NOT_DETECTED = 'wallet.ledger.not_detected',
    NOT_ABLE_TO_GET_ACCOUNT = 'wallet.ledger.not_able_to_get_account',
    NOT_ABLE_TO_REVEAL_ACCOUNT = 'wallet.ledger.not_able_to_reveal_account',
    NOT_READY = 'wallet.ledger.not_ready',
    NOT_ALLOWED = 'wallet.ledger.not_allowed',
    NOT_ABLE_TO_ORIGINATE_CONTRACT = 'wallet.ledger.not_able_to_originate_contract',
    NOT_ABLE_TO_GET_BALANCE = 'wallet.ledger.not_able_to_get_balance',
    NOT_ABLE_TO_PREPARE_ORIGINATION = 'wallet.ledger.not_able_to_prepare_origination',
}

export default LedgerErrorCodes;
