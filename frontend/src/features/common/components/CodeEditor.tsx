import React from 'react';

// React Ace IDE
import AceEditor from 'react-ace';
import 'ace-builds/src-min-noconflict/mode-json';
import 'ace-builds/src-min-noconflict/theme-chrome';
import 'ace-builds/src-min-noconflict/theme-monokai';
import ReactAce from 'react-ace/lib/ace';

// Material UI
import { makeStyles, createStyles, Theme, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            height: 300,
            boxShadow: 'inset 0 0 10px #000000',
        },
    }),
);

type OwnProps = {
    mode: string;
    value: string;
    onChange: (value: string) => void;
};

const CodeEditor: React.FC<OwnProps> = ({ onChange, value, mode }) => {
    const classes = useStyles();
    const theme = useTheme();
    const editorTheme = theme.palette.mode === 'dark' ? 'monokai' : 'chrome';
    const textAreaRef = React.useRef((null as unknown) as ReactAce);

    const onTextInput = () => {
        onChange(textAreaRef.current?.editor.getValue());
    };

    return (
        <AceEditor
            className={classes.paper}
            debounceChangePeriod={1000}
            mode={mode}
            ref={textAreaRef}
            theme={editorTheme}
            setOptions={{
                useWorker: false,
                showPrintMargin: false,
                showGutter: false,
                showLineNumbers: false,
                wrap: true,
            }}
            value={value}
            name="smartpy-editor"
            onInput={onTextInput}
            width="100%"
            height="300px"
        />
    );
};

export default CodeEditor;
