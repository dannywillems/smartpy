import React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';

// Local Services
import toast from '../../../services/toast';
import ipfs from '../../../services/ipfs';
import { generateRandomPassPhrase, AES } from '../../../services/crypto';

// Utils
import { copyToClipboard } from '../../../utils/clipboard';
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles(() =>
    createStyles({
        divider: {
            margin: 5,
        },
    }),
);

interface OwnProps {
    open: boolean;
    handleClose: () => void;
    title: string;
    content?: string;
    baseUrl: string;
}

const IpfsLinkShare: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const [permaLink, setPermalink] = React.useState<string>();
    const t = useTranslation();

    const { open, handleClose, content, baseUrl, title } = props;

    const generatePermaLink = async () => {
        if (content) {
            const passPhrase = generateRandomPassPhrase();
            const encrypted = AES.encrypt(content, passPhrase);
            const cid = await ipfs.uploadString(encrypted);
            setPermalink(`${baseUrl}?cid=${cid}&k=${passPhrase}`);
        }
    };

    const onCopy = () => {
        if (permaLink) {
            copyToClipboard(permaLink);
            toast.info(`Permalink was copied!`);
            handleClose();
            setPermalink(undefined);
        }
    };

    return (
        <Dialog
            fullWidth
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">{title}</DialogTitle>
            <DialogContent dividers>
                <Fade in={open}>
                    <div>
                        {permaLink ? (
                            <React.Fragment>
                                <TextField fullWidth disabled variant="outlined" value={permaLink} />
                                <Divider className={classes.divider} />
                                <Button variant="outlined" fullWidth onClick={onCopy}>
                                    {t('common.copy')}
                                </Button>
                            </React.Fragment>
                        ) : (
                            <Button variant="outlined" disabled={!content} fullWidth onClick={generatePermaLink}>
                                {t('common.generateLink')}
                            </Button>
                        )}
                    </div>
                </Fade>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default IpfsLinkShare;
