import React from 'react';

// Material UI
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// Material Icons
import SaveIcon from '@material-ui/icons/Save';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

interface OwnProps {
    open: boolean;
    handleClose: () => void;
    handleCreateContract: (name: string) => void;
}

const CreateContractDialog: React.FC<OwnProps> = ({ open, handleClose, handleCreateContract }) => {
    const t = useTranslation();
    const textInputRef = React.useRef((null as unknown) as HTMLInputElement);
    const [name, setName] = React.useState('');

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    const handleSave = () => {
        handleCreateContract(name);
        handleClose();
        setName('');
    };

    return (
        <Dialog fullWidth open={open} onClose={handleClose}>
            <DialogTitle>{t('ide.contractManagement.createContract')}</DialogTitle>
            <DialogContent dividers>
                <TextField
                    inputProps={{
                        maxLength: 32,
                        minLength: 1,
                    }}
                    ref={textInputRef}
                    fullWidth
                    label={t('common.contract.name')}
                    value={name}
                    variant="filled"
                    size="small"
                    onChange={handleNameChange}
                />
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleClose}>
                    {t('common.cancel')}
                </Button>
                <Button startIcon={<SaveIcon />} color="primary" onClick={handleSave}>
                    {t('common.save')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default CreateContractDialog;
