import React from 'react';
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import json from 'react-syntax-highlighter/dist/cjs/languages/hljs/json';
import python from 'react-syntax-highlighter/dist/cjs/languages/hljs/python';
import a11yDark from 'react-syntax-highlighter/dist/cjs/styles/hljs/a11y-dark';
import github from 'react-syntax-highlighter/dist/cjs/styles/hljs/github';

// Material UI
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
// Services
import toast from '../../../services/toast';
// Utils
import { copyToClipboard } from '../../../utils/clipboard';
// State Management
import selectors from '../../../store/selectors';
import { appendClasses } from '../../../utils/style/classes';
import CodeEditor from './CodeEditor';

SyntaxHighlighter.registerLanguage('json', json);
SyntaxHighlighter.registerLanguage('python', python);

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            position: 'relative',
            height: 'calc(100% - 13px)',
        },
        buttons: {
            position: 'absolute',
            top: 10,
            right: 20,
            zIndex: 10,
        },
        button: {
            margin: theme.spacing(1),
        },
        syntaxHighlighter: {
            margin: 0,
            minHeight: 50,
            height: '100%',
        },
        round: {
            borderRadius: 10,
        },
    }),
);

interface OwnProps {
    square?: boolean;
    withCopy?: boolean;
    language: string;
    showLineNumbers: boolean;
    text: string;
    wrapLongLines?: boolean;
    editProps?: {
        open: boolean;
        onOpenChange: () => void;
        onCodeChange: (value: string) => void;
    };
}

const CodeBlockWithCopy: React.FC<OwnProps> = ({
    withCopy = true,
    text,
    square = true,
    wrapLongLines = false,
    editProps,
    ...props
}) => {
    const classes = useStyles();
    const isDarkMode = selectors.theme.useThemeMode();
    const t = useTranslation();

    const copy = () => {
        copyToClipboard(text);
        toast.info(t('common.copied'));
    };

    const renderButtons = () => (
        <div className={classes.buttons}>
            {withCopy ? (
                <Button variant="outlined" className={classes.button} onClick={copy}>
                    {t('common.copy')}
                </Button>
            ) : null}
            {!!editProps ? (
                <Button variant="contained" className={classes.button} onClick={editProps.onOpenChange}>
                    {editProps.open ? t('common.viewMode') : t('common.editMode')}
                </Button>
            ) : null}
        </div>
    );

    return (
        <div className={classes.root}>
            {renderButtons()}
            {editProps && editProps.open ? (
                <CodeEditor mode="json" value={text || ''} onChange={editProps.onCodeChange} />
            ) : (
                <SyntaxHighlighter
                    {...{
                        ...props,
                        className: square
                            ? classes.syntaxHighlighter
                            : appendClasses(classes.syntaxHighlighter, classes.round),
                    }}
                    style={isDarkMode ? a11yDark : github}
                    wrapLines
                    wrapLongLines={wrapLongLines}
                >
                    {text}
                </SyntaxHighlighter>
            )}
        </div>
    );
};

export default CodeBlockWithCopy;
