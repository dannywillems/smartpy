import React from 'react';
import { createStyles, makeStyles, Theme, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import operation from '../../../constants/operation';

const BorderLinearProgress = withStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 10,
            borderRadius: 5,
        },
        colorPrimary: {
            backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 700],
        },
        bar: {
            borderRadius: 5,
            backgroundColor: '#1a90ff',
        },
    }),
)(LinearProgress);

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});

type SizeBarProps = {
    label: string;
    size: number;
};
const SizeBar: React.FC<SizeBarProps> = ({ label, size }) => {
    const percentage = (size * 100) / operation.maxOperationDataLength;
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ minWidth: 100 }}>
                <Typography variant="body1" color="textPrimary">
                    {label}
                </Typography>
            </Box>
            <Box sx={{ width: '100%', mr: 1 }}>
                <BorderLinearProgress variant="determinate" value={percentage} />
            </Box>

            <Box sx={{ minWidth: 50 }}>
                <Typography variant="body2" color="textSecondary">{`${percentage.toFixed(1)}%`}</Typography>
            </Box>
            <Box sx={{ minWidth: 150 }}>
                <Typography
                    variant="caption"
                    color="textSecondary"
                >{`(${size} of ${operation.maxOperationDataLength} Bytes) `}</Typography>
            </Box>
        </Box>
    );
};

type OwnProps = {
    codeSize: number;
    storageSize: number;
};

const ContractSizes: React.FC<OwnProps> = ({ codeSize, storageSize }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <SizeBar label="Code" size={codeSize} />
            <SizeBar label="Storage" size={storageSize} />
            <SizeBar label="Combined" size={codeSize + storageSize} />
        </div>
    );
};

export default ContractSizes;
