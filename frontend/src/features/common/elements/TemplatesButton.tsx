import React from 'react';
// Material UI
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import SquareFoot from '@material-ui/icons/SquareFoot';

interface OwnProps {
    label: string;
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, label, ...props }, ref) => (
    <Tooltip title={label}>
        <Button
            startIcon={onlyIcon ? null : <SquareFoot />}
            color="primary"
            variant="contained"
            aria-label="templates"
            {...{ ...props, ref }}
        >
            {onlyIcon ? <SquareFoot /> : label}
        </Button>
    </Tooltip>
));
