import React from 'react';
// Material UI
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import ShareOutlinedIcon from '@material-ui/icons/ShareOutlined';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

interface OwnProps {
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, ...props }, ref) => (
    <Tooltip title="Share">
        <Button
            startIcon={onlyIcon ? null : <ShareOutlinedIcon />}
            color="primary"
            variant="contained"
            aria-label="share"
            endIcon={onlyIcon ? null : <ArrowDropDownIcon />}
            {...{ ...props, ref }}
        >
            {onlyIcon ? <ShareOutlinedIcon /> : 'Share'}
        </Button>
    </Tooltip>
));
