import React from 'react';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
// Material UI Icons
import DeleteIcon from '@material-ui/icons/DeleteSweep';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

// Local Components
import SettingsMenuItem from './SettingsMenuItem';
import ShareMenuItem from '../../common/components/ShareMenuItem';
import ToolsMenuItem from './ToolsMenuItem';
import TemplatesMenuItem from './TemplatesMenuItem';
import { IDEContract, MichelsonIDESettings } from 'SmartPyModels';
import ProtocolSelector from './ProtocolSelector';
import { getBase } from '../../../utils/url';

// Local Hooks
import useCommonStyles from '../../../hooks/useCommonStyles';
import useTranslation from '../../i18n/hooks/useTranslation';
import debounce from '../../../utils/debounce';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            height: 50,
            borderWidth: 1,
            backgroundColor: theme.palette.background.paper,
        },
        gridItem: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        noMargin: {
            margin: 0,
        },
    }),
);

const debouncer = debounce(1000);

interface OwnProps {
    clearOutputs: () => void;
    downloadOutputPanel: () => void;
    compileContract: () => void;
    contract?: IDEContract;
    settings: MichelsonIDESettings;
}
const EditorToolBar: React.FC<OwnProps> = (props) => {
    const classes = { ...useStyles(), ...useCommonStyles() };
    const refs = {
        testsButton: React.useRef<HTMLButtonElement>(null),
        playButton: React.useRef<HTMLButtonElement>(null),
    };
    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));
    const t = useTranslation();

    const { clearOutputs, downloadOutputPanel, compileContract, contract, settings } = props;

    React.useEffect(() => {
        debouncer(compileContract);
    }, [settings.protocol]);

    return (
        <div className={classes.root}>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item xs={12} sm={4} className={classes.gridItem}>
                    <ButtonGroup disableElevation>
                        <Tooltip
                            title={t('michelsonIde.toolBar.runCode') as string}
                            aria-label="run-code"
                            placement="left"
                        >
                            <Button
                                ref={refs.playButton}
                                onClick={compileContract}
                                endIcon={onlyIcon ? null : <PlayArrowIcon />}
                                variant="contained"
                            >
                                {onlyIcon ? <PlayArrowIcon /> : 'Run'}
                            </Button>
                        </Tooltip>
                        <Tooltip
                            title={t('michelsonIde.toolBar.clearOutputs') as string}
                            aria-label="clear-outputs"
                            placement="right"
                        >
                            <Button onClick={clearOutputs} variant="outlined">
                                <DeleteIcon />
                            </Button>
                        </Tooltip>
                    </ButtonGroup>
                </Grid>
                <Grid item xs={12} sm={2} className={classes.gridItem}>
                    <ProtocolSelector />
                </Grid>
                <Grid item xs={12} sm={6} className={classes.gridItem}>
                    <Grid container spacing={2} justifyContent="center" className={classes.noMargin}>
                        <Grid item>
                            <TemplatesMenuItem onlyIcon={onlyIcon} />
                        </Grid>
                        <Grid item>
                            <ToolsMenuItem onlyIcon={onlyIcon} downloadOutputPanel={downloadOutputPanel} />
                        </Grid>
                        <Grid item>
                            <ShareMenuItem
                                onlyIcon={onlyIcon}
                                code={contract?.code}
                                baseUrl={`${window.location.origin}${getBase()}/michelson`}
                            />
                        </Grid>
                        <Grid item>
                            <SettingsMenuItem onlyIcon={onlyIcon} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
};

export default EditorToolBar;
