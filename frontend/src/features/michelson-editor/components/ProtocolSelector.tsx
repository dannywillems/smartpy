import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import selectors from '../../../store/selectors';
import actions from '../../../store/root-action';
import { Protocol } from '../../../constants/protocol';

const useStyles = makeStyles(() =>
    createStyles({
        select: {
            padding: 12,
            minWidth: 90,
        },
    }),
);

const ProtocolSelector: React.FC = () => {
    const classes = useStyles();
    const settings = selectors.michelsonEditor.useSettings();
    const dispatch = useDispatch();

    const handleProtocolChange = (event: React.ChangeEvent<{ value: string }>) => {
        dispatch(actions.michelsonEditor.updateSettings({ protocol: event.target.value }));
    };

    return (
        <Select
            variant="filled"
            value={settings.protocol || Protocol.EDO}
            classes={{ filled: classes.select }}
            onChange={handleProtocolChange}
        >
            {Object.values(Protocol).map((protocol) => (
                <MenuItem key={protocol} value={protocol}>
                    {protocol}
                </MenuItem>
            ))}
        </Select>
    );
};

export default ProtocolSelector;
