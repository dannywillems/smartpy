export default {
    showShortcutsWin: {
        name: 'showShortcutsWin',
        description: 'Show Shortcuts',
        bindKey: {
            win: 'Shift-Ctrl-H',
            mac: '',
        },
    },
    showShortcutsMac: {
        name: 'showShortcutsMac',
        description: 'Show Shortcuts',
        bindKey: {
            win: '',
            mac: 'Shift-Cmd-H',
        },
    },
    runCode: {
        name: 'runCode',
        description: 'Run Code',
        bindKey: {
            win: 'Ctrl-Enter',
            mac: 'Cmd-Enter',
        },
    },
} as { [key: string]: { name: string; description: string; bindKey: { win: string; mac: string } } };
