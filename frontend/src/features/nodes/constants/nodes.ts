const constants = {
    healthCheckInterval: 10000,
    allowedDelay: 5 * 60000,
};

export default constants;
