import React from 'react';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

// Local Components
import NodesMonitor from '../containers/NodesMonitor';

import MeshAnimation from '../../common/components/animated/Mesh';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
        },
        title: {
            color: theme.palette.primary.main,
            ...theme.typography.overline,
            fontWeight: 'bold',
            fontSize: '1.5em',
            marginBottom: 20,
        },
        description: {
            color: theme.palette.primary.light,
            margin: 20,
        },
        mesh: {
            top: 0,
            left: 0,
            position: 'absolute',
            width: '100%',
            height: '100%',
        },
        content: {
            zIndex: 1000,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
    }),
);

const Nodes = () => {
    const classes = useStyles();
    return (
        <div className={classes.root} id="nodes">
            <div className={classes.content}>
                <Typography variant="h4" className={classes.title}>
                    SmartPy Public Nodes
                </Typography>
                <NodesMonitor />
            </div>
            <MeshAnimation className={classes.mesh} />
        </div>
    );
};

export default Nodes;
