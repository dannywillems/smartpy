import React from 'react';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import MeshAnimation from '../../common/components/animated/Mesh';

import NotFoundSVG from '../elements/NotFoundSVG';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
        },
        mesh: {
            top: 0,
            left: 0,
            position: 'absolute',
            width: '100%',
            height: '100%',
        },
    }),
);

const NotFound = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <NotFoundSVG />
            <MeshAnimation className={classes.mesh} />
        </div>
    );
};

export default NotFound;
