import React from 'react';
const NotFoundSVG = () => {
    return (
        <svg viewBox="0 0 250 320" width="600" height="320">
            <path
                d="m 72.737155,133.95826 c -23.360896,-58.805339 -11.680448,-29.40267 0,0 z"
                fill="#66aacc"
                strokeWidth="0.706071"
            />
            <path
                fill="#006dcc"
                d="M 37.69581,154.58755 V 96.967804 l 87.95378,-51.21755 88.6546,51.928905 V 136.448 l -50.45954,30.23258 -51.16036,-29.8769 13.31571,-7.82491 37.14383,22.052 37.14382,-22.052 V 105.85974 L 126,61.400061 51.712348,105.50406 v 41.25858 z"
                strokeWidth="0.706071"
            />
            <path
                d="M 73.437982,118.30845 125.4426,87.669592 175.61529,116.93634 162.79341,124.71064 126,102.65864 73.437982,133.2469 Z"
                fill="#66aacc"
                strokeWidth="0.706071"
            />
            <path
                d="m 214.30419,162.41245 v 57.61975 l -87.95378,51.21755 -88.6546,-51.92891 V 180.552 l 50.459537,-30.23258 51.160363,29.8769 -13.31571,7.82491 -37.143826,-22.052 -37.143826,22.052 v 23.11903 L 126,255.59994 l 74.28765,-44.104 v -41.25858 z"
                fill="#006dcc"
                strokeWidth="0.706071"
            />
            <path
                fill="#66aacc"
                d="m 177.71776,198.69155 -52.00462,30.63887 -50.172677,-29.26676 12.821873,-7.7743 36.793404,22.052 52.56202,-30.58826 z"
                strokeWidth="0.706071"
            />
            <text
                fontStyle="italic"
                fontWeight="bold"
                fontStretch="normal"
                fontSize="192px"
                fontFamily="sans-serif"
                letterSpacing="0px"
                fill="#006dcc"
                fillOpacity="1"
                stroke="none"
                x="-105.17813"
                y="217.85048"
            >
                4
            </text>
            <text
                fontStyle="italic"
                fontWeight="bold"
                fontStretch="normal"
                fontSize="192px"
                fontFamily="sans-serif"
                letterSpacing="0px"
                fill="#006dcc"
                fillOpacity="1"
                stroke="none"
                x="246.84479"
                y="216.696"
            >
                4
            </text>
        </svg>
    );
};

export default NotFoundSVG;
