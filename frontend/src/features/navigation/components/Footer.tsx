import React from 'react';

// Material UI
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ListSubheader from '@material-ui/core/ListSubheader';
import TwitterIcon from '@material-ui/icons/Twitter';
import TelegramIcon from '@material-ui/icons/Telegram';
import Divider from '@material-ui/core/Divider';

// Local UI Components
import MediumIcon from '../../common/elements/icons/Medium';
import GitLabIcon from '../../common/elements/icons/GitLab';
// Constants
import info from '../../../constants/info';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            background: theme.palette.primary.dark,
            borderTopWidth: 2,
            borderTopStyle: 'solid',
        },
        zoomRoot: {
            position: 'fixed',
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
        fabToTop: {
            borderStyle: 'solid',
            borderWidth: 2,
        },
        copyrightDiv: {
            paddingTop: 30,
            paddingBottom: 10,
            textAlign: 'center',
        },
        followUsText: {
            textAlign: 'center',
            color: theme.palette.common.white,
            fontSize: '1.5rem',
            fontWeight: 'bold',
            textTransform: 'capitalize',
            marginBottom: 5,
        },
        followUsGrid: {
            margin: '10px 0 10px 0',
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
        centeredContent: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        fabLabel: {
            paddingRight: 5,
            textTransform: 'none',
        },
        divider: {
            background: theme.palette.common.white,
        },
    }),
);

const ScrollTop = () => {
    const classes = useStyles();

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        const anchor = ((event.target as HTMLDivElement).ownerDocument || document).querySelector(
            '#back-to-top-anchor',
        );

        if (anchor) {
            anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    };

    return (
        <div onClick={handleClick} role="presentation" className={classes.zoomRoot}>
            <Zoom in={trigger}>
                <Fab color="primary" size="small" aria-label="scroll back to top" className={classes.fabToTop}>
                    <KeyboardArrowUpIcon />
                </Fab>
            </Zoom>
        </div>
    );
};

const Footer: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root} id="contact">
            <Grid container alignItems="center" justifyContent="center">
                <Grid item xs={11}>
                    <ListSubheader className={classes.followUsText}>Follow Us</ListSubheader>
                    <Divider className={classes.divider} />
                    <Grid
                        container
                        spacing={1}
                        alignItems="center"
                        justifyContent="center"
                        className={classes.followUsGrid}
                    >
                        <Grid item xs={11} sm={2} className={classes.centeredContent}>
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="Twitter"
                                href="https://twitter.com/SmartPy_io"
                                classes={{ label: classes.fabLabel }}
                            >
                                <TwitterIcon className={classes.extendedIcon} color="primary" />
                                Twitter
                            </Fab>
                        </Grid>
                        <Grid item xs={11} sm={2} className={classes.centeredContent}>
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="Telegram"
                                href="https://t.me/SmartPy_io"
                                classes={{ label: classes.fabLabel }}
                            >
                                <TelegramIcon className={classes.extendedIcon} color="primary" />
                                Telegram
                            </Fab>
                        </Grid>
                        <Grid item xs={11} sm={2} className={classes.centeredContent}>
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="Medium"
                                href="https://smartpy-io.medium.com"
                                classes={{ label: classes.fabLabel }}
                            >
                                <MediumIcon className={classes.extendedIcon} fontSize="small" />
                                Medium
                            </Fab>
                        </Grid>
                        <Grid item xs={11} sm={2} className={classes.centeredContent}>
                            <Fab
                                variant="extended"
                                size="small"
                                color="secondary"
                                aria-label="GitLab"
                                href="https://gitlab.com/SmartPy/smartpy"
                                classes={{ label: classes.fabLabel }}
                            >
                                <GitLabIcon className={classes.extendedIcon} fontSize="small" />
                                GitLab
                            </Fab>
                        </Grid>
                    </Grid>
                    <Divider className={classes.divider} />
                </Grid>
                <Grid item xs={11} className={classes.copyrightDiv}>
                    <Typography variant="caption" align="justify" color="secondary">
                        {info.copyright}
                    </Typography>
                </Grid>
            </Grid>
            <ScrollTop />
        </div>
    );
};

export default Footer;
