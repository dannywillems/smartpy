import React from 'react';
import Fab, { FabProps } from '@material-ui/core/Fab';
import { Link as RouterLink } from 'react-router-dom';

import { LocationDescriptor } from 'history';

interface OwnProps extends FabProps {
    to: LocationDescriptor;
}

const RouterFab: React.FC<OwnProps> = ({ to, ...props }) => (
    <Fab
        {...props}
        component={React.forwardRef((linkProps, ref) => (
            <RouterLink {...linkProps} to={to} />
        ))}
    />
);

export default RouterFab;
