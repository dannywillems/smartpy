import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import './index.css';
import './features/i18n';
import App from './App';
import CircularProgress from './features/loader/components/CircularProgressWithText';
import * as serviceWorker from './serviceWorker';

import { createStore } from './store';

const { store, persistor, history } = createStore();

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate loading={<CircularProgress loading={true} />} persistor={persistor}>
                <App history={history} />
            </PersistGate>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
