import StringMatchers from './StringMatchers';

const matchers = {
    StringMatchers,
};

export { StringMatchers };
export default matchers;
