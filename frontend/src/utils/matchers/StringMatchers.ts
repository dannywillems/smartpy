/**
 * @summary Verifies if a string contains occurrences of other string (Case Insensitive).
 * @param {string} s1 - String
 * @param {string} s2 - Sub-String
 * @return {boolean} Will return true if s1 includes any substring s2, and false otherwise
 */
export const includesInsensitive = (s1 = '', s2 = ''): boolean => {
    return !!s1 && s1.toLowerCase().includes(s2.toLowerCase());
};

const StringMatchers = {
    includesInsensitive,
};

export default StringMatchers;
