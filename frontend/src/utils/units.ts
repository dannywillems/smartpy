import BigNumber from 'bignumber.js';

type UnitInfo = {
    char: string;
    unit: number;
};

export enum TezUnit {
    uTez = 'uTez',
    mTez = 'mTez',
    tez = 'tez',
    KTez = 'KTez',
    MTez = 'MTez',
}

export const AmountUnit = {
    [TezUnit.uTez]: { char: 'μꜩ', unit: 1 } as UnitInfo,
    [TezUnit.mTez]: { char: 'mꜩ', unit: 1000 } as UnitInfo,
    [TezUnit.tez]: { char: 'ꜩ', unit: 1000000 } as UnitInfo,
    [TezUnit.KTez]: { char: 'Kꜩ', unit: 1000000000 } as UnitInfo,
    [TezUnit.MTez]: { char: 'Mꜩ', unit: 1000000000000 } as UnitInfo,
};

/**
 * @description Convert amount number. (e.g. from Tez to uTez).
 *
 * @param value Value to be converted.
 * @param from origin unit.
 * @param to target unit.
 *
 * @returns {number} the converted value.
 */
export const convertUnitNumber = (value: BigNumber | string | number, from: UnitInfo, to = AmountUnit.uTez) =>
    new BigNumber(value).dividedBy(to.unit).multipliedBy(from.unit);

/**
 * @description Convert amount and prettify. (e.g. from Tez to uTez).
 *
 * @param value Value to be converted.
 * @param from origin unit.
 * @param to target unit.
 *
 * @returns {string} the converted value.
 */
export const convertUnit = (value: BigNumber | string | number, from: UnitInfo, to = AmountUnit.uTez) =>
    convertUnitNumber(value, from, to).toFormat(3, BigNumber.ROUND_FLOOR);

/**
 * @description Convert amount and include the unit symbol. (e.g. from Tez ꜩ to uTez μꜩ).
 *
 * @param value Value to be converted.
 * @param from origin unit.
 * @param to target unit.
 *
 * @returns {string} the converted value with the respective symbol.
 */
export const convertUnitWithSymbol = (value: BigNumber | string | number, from: UnitInfo, to = AmountUnit.uTez) =>
    `${convertUnit(value, from, to)} ${to.char}`;
