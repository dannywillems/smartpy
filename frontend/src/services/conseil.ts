import { keywords } from '../constants/michelson';
import { TezosLanguageUtil } from 'conseiljs';

TezosLanguageUtil.overrideKeywordList(keywords);

export * from 'conseiljs';
