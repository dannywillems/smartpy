import IPFSClient from 'ipfs-http-client';

import constants from '../constants/ipfs';

// connect to ipfs daemon API server
const ipfsClient = IPFSClient(constants.API);

export const getStringFromCID = async (cid: string) => {
    const stream = ipfsClient.cat(cid);
    let buffer = Buffer.from([]);

    for await (const chunk of stream) {
        buffer = Buffer.concat([buffer, Buffer.from(chunk)]);
    }

    return buffer.toString();
};

export const uploadString = async (value: string) => {
    const { path } = await ipfsClient.add(value);
    return path;
};

const ipfs = {
    getStringFromCID,
    uploadString,
};

export default ipfs;
