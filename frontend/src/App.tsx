import React from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { ConnectedRouter } from 'connected-react-router';
import { History } from 'history';

import Router from './features/navigation/containers/Router';

interface OwnProps {
    history: History;
}

const App = ({ history }: OwnProps) => (
    <HelmetProvider>
        <ConnectedRouter history={history}>
            <Router />
        </ConnectedRouter>
    </HelmetProvider>
);

export default App;
