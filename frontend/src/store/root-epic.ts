import { combineEpics } from 'redux-observable';

import * as editorEpics from '../features/editor/epics';
import * as michelsonEditorEpics from '../features/michelson-editor/epics';

export default combineEpics(...[...Object.values(editorEpics), ...Object.values(michelsonEditorEpics)]);
