import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';

import editorReducer from '../features/editor/reducer';
import michelsonEditorReducer from '../features/michelson-editor/reducer';
import walletReducer from '../features/wallet/reducer';
import themeReducer from '../features/theme/reducer';
import i18nReducer from '../features/i18n/reducer';
import originationReducer from '../features/origination/reducer';

const rootReducer = (history: History) =>
    combineReducers({
        router: connectRouter(history),
        editor: editorReducer,
        michelsonEditor: michelsonEditorReducer,
        theme: themeReducer,
        wallet: walletReducer,
        origination: originationReducer,
        i18n: i18nReducer,
    });

export default rootReducer;
