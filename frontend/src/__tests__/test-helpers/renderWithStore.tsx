import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';
import { ThemeProvider, createMuiTheme } from '@material-ui/core';
import { createStore } from '../../store';

const renderWithStore = (Component: React.ReactNode) => {
    const history = createMemoryHistory();
    const { store } = createStore(history);

    const Wrapper: React.FC = () => (
        <Provider store={store}>
            <ThemeProvider theme={createMuiTheme()}>
                <ConnectedRouter history={history}>{Component}</ConnectedRouter>
            </ThemeProvider>
        </Provider>
    );

    return {
        ...render(<Wrapper />),
        // Added `history` to the returned utilities to allow us
        // to reference it in our tests.
        history,
    };
};

export default renderWithStore;
