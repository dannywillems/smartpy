import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import { createMemoryHistory, History } from 'history';

import App from '../../App';
import { createStore } from '../../store';

interface Options {
    route: string;
    history?: History;
}

const renderWithRouter = ({ route, history = createMemoryHistory({ initialEntries: [route] }) }: Options) => {
    const { store } = createStore(history);

    const Wrapper: React.FC = () => (
        <Provider store={store}>
            <App history={history} />
        </Provider>
    );

    return {
        ...render(<Wrapper />),
        // Added `history` to the returned utilities to allow us
        // to reference it in our tests.
        history,
    };
};

export default renderWithRouter;
