import React, { Suspense } from 'react';
import { act } from 'react-dom/test-utils';

import Home from '../../features/home/views/Home';
import renderWithStore from '../test-helpers/renderWithStore';

describe('Home Page', () => {
    it('Home renders correctly', () => {
        let container;
        act(() => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <Home />
                </Suspense>,
            ).container;
        });
        expect(container).toMatchSnapshot();
    });
});
