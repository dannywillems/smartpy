import StringMatchers from '../../../utils/matchers/StringMatchers';

describe('StringMatchers', () => {
    describe('Method includesInsensitive', () => {
        it('String1 is equal to String2', () => {
            expect(StringMatchers.includesInsensitive('TEST', 'TEST')).toBeTruthy();
        });
        it('String1 does not include String2', () => {
            expect(StringMatchers.includesInsensitive('TEST', 'TEST1')).toBeFalsy();
        });
        it('String1 includes String2', () => {
            expect(StringMatchers.includesInsensitive('TEST_1', 'TEST')).toBeTruthy();
        });
    });
});
