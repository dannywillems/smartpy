(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Smartml
open Writer
open Utils
open Filename

module Main (C : Cmd.S) (P : Primitives.Primitives) = struct
  module Tezos = Smartml_unix.Run_tezos_scenario.Make (P)

  type config =
    { py : string option
    ; kind : Basics.scenario_kind
    ; init : string option
    ; init_expr : string option
    ; output : string option
    ; config : Config.t
    ; sandbox : int option
    ; mockup : string option
    ; purge : bool
    ; html : bool
    ; install : string }

  let config0 =
    { py = None
    ; kind = Test
    ; init = None
    ; init_expr = None
    ; output = None
    ; config = Config.default
    ; sandbox = None
    ; mockup = None
    ; purge = false
    ; html = false
    ; install = "" }

  let usage () =
    print_endline
      {|
Usage: smartpyc <options> <source file>

The <source file> is a python file containing a class that inherits
from sp.Contract.

OPTIONS

(-i | --init) <expression>
    Initialize the contract of the given name and arguments.

 -o | --output
    Specify a different output file (default: contract.smlse).

 -s | --sandbox <port>
    Run scenarios running on a sandbox on localhost at <port>.

 -h | --help
    Print help.

EXAMPLE

smartpyc calculator.py --init 'Calculator()' --output calculator.smlse
|}

  let rec parse c = function
    | [] -> c
    | p :: rest when check_suffix p ".py" -> parse {c with py = Some p} rest
    | "--comp" :: rest -> parse {c with kind = Compilation} rest
    | ("-i" | "--init") :: i :: rest -> parse {c with init = Some i} rest
    | "--init_expr" :: i :: rest -> parse {c with init_expr = Some i} rest
    | ("-o" | "--output") :: o :: rest -> parse {c with output = Some o} rest
    | ("-s" | "--sandbox") :: p :: rest ->
        parse {c with sandbox = Some (int_of_string p)} rest
    | ("-m" | "--mockup") :: m :: rest -> parse {c with mockup = Some m} rest
    | ("-p" | "--purge") :: rest -> parse {c with purge = true} rest
    | "--html" :: rest -> parse {c with html = true} rest
    | "--install" :: f :: rest -> parse {c with install = f} rest
    | ("-h" | "--help") :: _ ->
        usage ();
        exit 0
    | x :: xs ->
        let err () =
          failwith (Printf.sprintf "Don't know what to do with %s." x)
        in
        if String.sub x 0 2 = "--"
        then
          let x = String.(sub x 2 (length x - 2)) in
          match Config.parse_flag (x :: xs) with
          | None -> err ()
          | Some (flag, rest) ->
              parse {c with config = Config.apply_flag flag c.config} rest
        else err ()

  let read_contract_file ~config smlse =
    let contract = Io.read_file smlse in
    let contract = Parsexp.Single.parse_string_exn contract in
    let primitives = (module P : Primitives.Primitives) in
    let scenario_state = Basics.scenario_state () in
    let typing_env = Typing.init_env () in
    let contract =
      Import.import_contract ~primitives ~scenario_state typing_env contract
    in
    let fixEnv =
      Mangler.init_env
        ~reducer:(Interpreter.reducer ~config ~primitives ~scenario_state)
        ()
    in
    let contract = Fixer.fix_contract config fixEnv typing_env contract in
    Interpreter.interpret_contract ~config ~primitives ~scenario_state contract

  let read_expression_file ~config smlse =
    let expr = Io.read_file smlse in
    let expr = Parsexp.Single.parse_string_exn expr in
    let primitives = (module P : Primitives.Primitives) in
    let scenario_state = Basics.scenario_state () in
    let env = Typing.init_env () in
    let expr = Import.import_expr env primitives scenario_state expr in
    let expr = Checker.check_expr config [] expr in
    Interpreter.interpret_expr_external
      ~config
      ~primitives
      ~scenario_state
      ~no_env:[]
      expr

  let mkdir_p x =
    let r = C.run "mkdir" ["-p"; x] in
    if r <> 0 then failwith (Printf.sprintf "mkdir failed: %d" r)

  let rm_rf x =
    let r = C.run "rm" ["-rf"; x] in
    if r <> 0 then failwith (Printf.sprintf "rm failed: %d" r)

  let run_smartpyc_py args =
    let path = Option.default "" (Sys.getenv_opt "PATH") in
    let dir = Filename.dirname Sys.argv.(0) in
    let pythonpath =
      match Sys.getenv_opt "PYTHONPATH" with
      | None -> dir
      | Some pythonpath -> String.concat ":" [dir; pythonpath]
    in
    let env = [("PATH", path); ("PYTHONPATH", pythonpath)] in
    let r = C.run ~env "python3" (Filename.concat dir "smartpyc.py" :: args) in
    match r with
    | 0 -> ()
    | 1 -> exit 1
    | r -> failwith (Printf.sprintf "unexpected exit status from python: %d" r)

  let write_with_init out_smlse fn_py fn_pure_py init =
    run_smartpyc_py ["write_with_init"; out_smlse; fn_py; fn_pure_py; init]

  let write_with_init_expr out_smlse fn_py fn_pure_py init =
    run_smartpyc_py ["write_with_init_expr"; out_smlse; fn_py; fn_pure_py; init]

  let write_tests out_scenario_sc in_py in_pure_py =
    run_smartpyc_py ["write_tests"; out_scenario_sc; in_py; in_pure_py]

  let write_meta out x =
    Io.write_file out (Format.asprintf "%a" (Misc.pp_json_as_json ()) x)

  let write_compiled_tz ~config out c =
    let c = Compiler.compile_value_tcontract ~config c in
    Io.write_file out (Michelson.display_tcontract c);
    c

  let write_compiled_json out c =
    let c = Michelson.to_micheline_tcontract c in
    Io.write_file out (Format.asprintf "%a" (Micheline.pp_as_json ()) c)

  let write_storage_init out storage =
    Option.cata "missing storage" Michelson.string_of_tliteral storage
    |> Io.write_file out

  let write_expression_tz out v =
    Io.write_file out (Michelson.string_of_literal v)

  let write_expression_json out v =
    let lit = Michelson.To_micheline.literal v in
    Io.write_file out (Format.asprintf "%a" (Micheline.pp_as_json ()) lit)

  let write_pre_smartml_michel out c =
    let st = Michel.Transformer.{var_counter = ref 0} in
    let c = Michel_decompiler.decompile_contract st c in
    let c = Michel.Transformer.smartMLify st c in
    let pp = Michel.Expr.(print_precontract print_expr) in
    Io.write_file out (Format.asprintf "%a" pp c);
    c

  let write_decompiled config fn_decompiled c =
    let decompiled_scenario =
      {|
@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
|}
    in
    (* Write _decompiled.py file: *)
    match Michel.Typing.typecheck_precontract c with
    | Error msg ->
        prerr_endline msg;
        exit 1
    | Ok c ->
        let c = Decompiler.smartML_of_michel config c in
        let c = Printer.tcontract_to_string c in
        Io.write_file fn_decompiled (c ^ "\n" ^ decompiled_scenario)

  let compile_with_init ~config out_smlse fn_py init =
    mkdir_p (Filename.dirname out_smlse);
    let out = remove_extension out_smlse in
    let fn_pure_py = out ^ "_pure.py" in
    let fn_meta i = Printf.sprintf "%s.%s.json" out i in
    write_with_init out_smlse fn_py fn_pure_py init;
    let c = read_contract_file ~config out_smlse in
    let _ = write_contract_types (out ^ "_types") c in
    List.iter
      (fun (i, x) -> write_meta (fn_meta i) x)
      (Metadata.for_contract ~config c);
    let c = write_compiled_tz ~config (out ^ "_compiled.tz") c in
    write_compiled_json (out ^ "_compiled.json") c;
    write_storage_init (out ^ "_storage_init.tz") c.storage;
    if config.decompile
    then
      let c = write_pre_smartml_michel (out ^ "_pre_smartml.michel") c in
      write_decompiled config (out ^ "_decompiled.py") c

  let compile_with_init_expression ~config out_smlse fn_py init =
    mkdir_p (Filename.dirname out_smlse);
    let out = remove_extension out_smlse in
    let fn_pure_py = out ^ "_pure.py" in
    write_with_init_expr out_smlse fn_py fn_pure_py init;
    let value = read_expression_file ~config out_smlse in
    let v = Compiler.compile_value ~config value in
    write_expression_tz (out ^ "_michelson.tz") v;
    write_expression_json (out ^ "_michelson.json") v;
    Io.write_file (out ^ "_smartpy_type.txt") (Printer.type_to_string value.vt);
    Io.write_file
      (out ^ "_michelson_type.txt")
      (Michelson.string_of_mtype ~html:false (Compiler.mtype_of_type value.vt))

  let run_scenarios ~kind ~config ~html ~install fn_scenario out_dir =
    let run_scenario scenario =
      let primitives = (module P : Primitives.Primitives) in
      let scenario = Scenario.load_from_string ~primitives config scenario in
      if kind <> scenario.scenario.kind
      then None
      else
        let out_dir = Filename.concat out_dir scenario.scenario.shortname in
        mkdir_p out_dir;
        Some
          (Smartml_scenario.run
             ~config
             ~primitives
             ~html
             ~install
             ~scenario
             (Some out_dir))
    in
    let scenarios =
      Yojson.Basic.Util.to_list (Yojson.Basic.from_file fn_scenario)
    in
    List.concat (List.map_some run_scenario scenarios)

  let run_tests ~kind ~config ~sandbox ~mockup ~html ~install out_dir fn_py =
    mkdir_p out_dir;
    let fn_scenario = Filename.concat out_dir "scenario.json" in
    let fn_pure_py = Filename.concat out_dir "script_pure.py" in
    Io.write_file
      (Filename.concat out_dir "script_init.py")
      (Io.read_file fn_py);
    let errors =
      try
        write_tests fn_scenario fn_py fn_pure_py;
        run_scenarios ~kind ~config ~html ~install fn_scenario out_dir
      with
      | Smartml.Basics.SmartExcept l -> [(`Error, l)]
    in
    let is_error = function
      | `Error, _ -> true
      | _ -> false
    in
    let open Printer in
    ( match List.partition is_error errors with
    | [], [] -> ()
    | (_ :: _ as errors), _ ->
        let f (_, x) = print_endline ("[error] " ^ pp_smart_except false x) in
        List.iter f errors;
        exit 1
    | [], (_ :: _ as warnings) ->
        let f (_, x) = print_endline ("[warning] " ^ pp_smart_except false x) in
        List.iter f warnings );
    let client_command = "_build/tezos-bin/tezos-client" in
    ( match sandbox with
    | None -> ()
    | Some p ->
        let connection =
          Tezos.This_client.Connection.(
            Node {address = None; port = p; tls = false})
        in
        let client =
          Tezos.This_client.state
            ~client_command
            ~confirmation:`Bake
            ~funder:
              "unencrypted:edsk3RFgDiCt7tWB2oe96w1eRw72iYiiqZPLu9nnEY23MYRp2d8Kkx"
            ~connection
            (out_dir ^ "/sandbox")
        in
        let scenarios =
          let open Tezos.Smartml_scenario in
          let scenarios = of_json_file fn_scenario config in
          List.filter (fun x -> kind = x.sc.scenario.kind) scenarios
        in
        let r = Tezos.run ~config ~scenarios ~client () in
        Fmt.pr
          "\n%a\n%!"
          Fmt.(
            vbox
              ~indent:2
              (pair
                 ~sep:cut
                 string
                 (list ~sep:cut (fun ppf -> pf ppf "* `%s`"))))
          ( Fmt.str
              "Tests %s, results:"
              (if r#success then "SUCCEEDED" else "FAILED")
          , [r#results_dot_txt; r#results_dot_json; r#results_full_dot_txt] ) );
    match mockup with
    | None -> ()
    | Some m ->
        let connection = Tezos.This_client.Connection.(Mockup {path = m}) in
        let client =
          Tezos.This_client.state
            ~client_command
            ~confirmation:`Auto
            ~funder:"bootstrap1"
            ~connection
            (out_dir ^ "/mockup")
        in
        let scenarios =
          let open Tezos.Smartml_scenario in
          let scenarios = of_json_file fn_scenario config in
          List.filter (fun x -> kind = x.sc.scenario.kind) scenarios
        in
        let r = Tezos.run ~config ~scenarios ~client () in
        Fmt.pr
          "\n%a\n%!"
          Fmt.(
            vbox
              ~indent:2
              (pair
                 ~sep:cut
                 string
                 (list ~sep:cut (fun ppf -> pf ppf "* `%s`"))))
          ( Fmt.str
              "Tests %s, results:"
              (if r#success then "SUCCEEDED" else "FAILED")
          , [r#results_dot_txt; r#results_dot_json; r#results_full_dot_txt] )

  let run () =
    let { py
        ; kind
        ; init
        ; init_expr
        ; output
        ; config
        ; mockup
        ; sandbox
        ; purge
        ; html
        ; install } =
      parse config0 (List.tl (Array.to_list Sys.argv))
    in
    let fn_py = Option.of_some_exn ~msg:"source file required" py in
    if not (check_suffix fn_py ".py")
    then failwith ("Don't know what to do with " ^ fn_py);
    let do_purge output_dir = if purge then rm_rf output_dir in
    match (init, init_expr) with
    | None, None ->
        let out_dir = Option.default (chop_suffix fn_py ".py") output in
        do_purge out_dir;
        run_tests ~kind ~config ~sandbox ~mockup ~html ~install out_dir fn_py
    | Some init, None ->
        let out_smlse =
          Option.default (chop_suffix fn_py ".py" ^ ".smlse") output
        in
        do_purge (Filename.dirname out_smlse);
        compile_with_init ~config out_smlse fn_py init
    | None, Some init ->
        let out_smlse =
          Option.default (chop_suffix fn_py ".py" ^ ".smlse") output
        in
        do_purge (Filename.dirname out_smlse);
        compile_with_init_expression ~config out_smlse fn_py init
    | Some _, Some _ -> assert false

  let main =
    ( try run () with
    | exn ->
        print_endline (Printer.exception_to_string false exn);
        exit 1 );
    exit 0
end
