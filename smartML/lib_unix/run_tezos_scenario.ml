(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Base

module Date = struct
  include Ptime

  let opt_exn msg o = Option.value_exn ~message:(Fmt.str "Date: %s" msg) o

  let float_to_string f =
    f |> Ptime.of_float_s |> opt_exn "now failed" |> Ptime.to_rfc3339

  let now () = Unix.gettimeofday () |> Ptime.of_float_s |> opt_exn "now failed"
end

let debug = ref false

let () =
  match Caml.Sys.getenv_opt "smartpy_debug" with
  | Some "true" -> debug := true
  | _ -> ()

let dbg fmt =
  ( if !debug
  then
    Fmt.kstr (fun s ->
        Fmt.epr "[DBG:%a]: %s\n%!" Date.(pp_human ~frac_s:3 ()) Date.(now ()) s)
  else Caml.Format.ikfprintf (fun _ -> ()) Caml.Format.err_formatter )
    fmt

module System = struct
  let pp_to_file path f =
    let open Caml in
    let open Format in
    let o = open_out path in
    let ppf = formatter_of_out_channel o in
    pp_set_margin ppf 80;
    f ppf;
    pp_print_flush ppf ();
    flush o;
    close_out o;
    ()

  let read_file path =
    let open Caml in
    let i = open_in path in
    let b = Buffer.create 1020 in
    dbg "read_file: %S" path;
    let rec loop () =
      match input_char i with
      | c ->
          Buffer.add_char b c;
          loop ()
      | exception e ->
          dbg "read_file: %a (%d bytes)" Fmt.exn e (Buffer.length b)
    in
    loop ();
    close_in i;
    Buffer.contents b
end

module More_fmt = struct
  include Fmt

  let vertical_box ?indent ppf f = vbox ?indent (fun ppf () -> f ppf) ppf ()

  let wrapping_box ?indent ppf f = box ?indent (fun ppf () -> f ppf) ppf ()

  let wf ppf fmt = Fmt.kstr (fun s -> box (fun ppf () -> text ppf s) ppf ()) fmt

  let markdown_verbatim_list ppf l =
    vertical_box ~indent:0 ppf (fun ppf ->
        cut ppf ();
        string ppf (String.make 45 '`');
        List.iter l ~f:(fun l ->
            cut ppf ();
            string ppf l);
        cut ppf ();
        string ppf (String.make 45 '`'))

  let long_string ?(max = 30) ppf s =
    match String.sub s ~pos:0 ~len:(max - 2) with
    | s -> pf ppf "%S" (s ^ "...")
    | exception _ -> pf ppf "%S" s

  let json ppf json =
    markdown_verbatim_list
      ppf
      (Ezjsonm.value_to_string ~minify:false json |> String.split ~on:'\n')
end

module Make (Primitives : Smartml.Primitives.Primitives) = struct
  module Smartml_scenario = struct
    type t =
      { name : string
      ; sc : Smartml.Scenario.loaded_scenario }

    let make name sc = {name; sc}

    let pp_quick ppf t =
      Fmt.pf ppf "{%s %d actions}" t.name (List.length t.sc.scenario.actions)

    let of_json_file filename config =
      let scenarios =
        Yojson.Basic.Util.to_list (Yojson.Basic.from_file filename)
      in
      List.map
        ~f:(fun scenario ->
          let shortname = Yojson.Basic.Util.member "shortname" scenario in
          make
            ( Caml.Filename.(basename filename |> chop_extension)
            ^ "-"
            ^ Yojson.Basic.Util.to_string shortname )
            (Smartml.Scenario.load_from_string
               ~primitives:(module Primitives : Smartml.Primitives.Primitives)
               config
               scenario))
        scenarios
  end

  module This_client = struct
    module Io = struct
      type 'a t = 'a

      let return x = x

      let bind x ~f = f x
    end

    module Connection = struct
      type t =
        | Node   of
            { address : string option
            ; port : int
            ; tls : bool }
        | Mockup of {path : string}

      let node ?address ?(tls = false) port = Node {address; port; tls}

      let default = node 20_000
    end

    module R = Monad.Make (struct
      include Smartml.Tezos_scenario.Decorated_result

      let map = `Define_using_bind
    end)

    open R

    let fail m =
      Smartml.Tezos_scenario.Decorated_result.fail
        (Smartml.Tezos_scenario.Error.make m)

    let attach = Smartml.Tezos_scenario.Decorated_result.attach

    let ( // ) = Caml.Filename.concat

    type state =
      { client_command : string
      ; connection : Connection.t
      ; mutable funder : string
      ; mutable root_dir : string
      ; confirmation : [ `Bake | `Wait of int | `Auto ]
      ; mutable command : int
      ; mutable originator : string option
      ; mutable originator_funded : bool
      ; mutable accounts : string list
      ; translate_paths : string -> string }

    let state
        ?(client_command = "tezos-client")
        ?(funder = "funder")
        ?(confirmation = `Wait 1)
        ~connection
        ?(translate_paths = Fn.id)
        root_dir =
      { client_command
      ; connection
      ; funder
      ; root_dir
      ; command = 0
      ; confirmation
      ; originator = None
      ; originator_funded = false
      ; accounts = []
      ; translate_paths }

    module Command_result = struct
      type t = {id : int}

      let make id = {id}

      let pp ppf i = Fmt.pf ppf "{command:%d}" i.id

      let output_path state {id} = state.root_dir // Fmt.str "command_%04d" id

      let get_stdout state i =
        System.read_file (output_path state i // "out.txt")
    end

    let command ?(succeed = true) state args =
      let id = Command_result.make state.command in
      state.command <- state.command + 1;
      let outpath = Command_result.output_path state id in
      let base_dir = state.root_dir // "client-dir" in
      let command_string =
        let exec =
          let connect_args =
            match state.connection with
            | Node {address; port; tls} ->
                (if tls then ["--tls"] else [])
                @ ["--port"; Int.to_string port]
                @ Option.value_map address ~default:[] ~f:(fun a ->
                      ["--addr"; a])
                @ ["--base-dir"; state.translate_paths base_dir]
            | Mockup {path} -> ["--mode"; "mockup"; "--base-dir"; path]
          in
          connect_args
          @ List.map args ~f:(function
                | f when Caml.Sys.file_exists f -> state.translate_paths f
                | f -> f)
        in
        String.concat
          ~sep:" "
          (state.client_command :: List.map exec ~f:Caml.Filename.quote)
      in
      let decorated =
        let outq = Caml.Filename.quote outpath in
        Fmt.str
          "mkdir -p %s %s && {  %s > %s/out.txt 2> %s/err.txt ; }"
          (Caml.Filename.quote base_dir)
          outq
          command_string
          outq
          outq
      in
      let ret = Caml.Sys.command decorated in
      System.pp_to_file
        (outpath // "cmd.txt")
        Fmt.(fun ppf -> string ppf decorated);

      (* dbg "cmd: %s → %d" decorated ret; *)
      ( if succeed && ret <> 0
      then Fmt.kstr fail "Command %a failed" Command_result.pp id
      else return id )
      |> attach
           ~a:
             [ `O
                 [ ("id", `Int id.id)
                 ; ("command", `Path (outpath // "cmd.txt"))
                 ; ("stdout", `Path (outpath // "out.txt"))
                 ; ("stderr", `Path (outpath // "err.txt"))
                 ; ("ret", `Int ret) ] ]

    let default_originator = "smartml-originator"

    let make_account ?sk state name =
      let secret_key =
        let preclean =
          match sk with
          | Some s -> s
          | None ->
              let account = Primitives.Crypto.account_of_seed name in
              account.sk
        in
        match String.is_prefix preclean ~prefix:"unencrypted:" with
        | true -> preclean
        | false -> "unencrypted:" ^ preclean
      in
      command state ["import"; "secret"; "key"; name; secret_key; "--force"]
      >>= fun _ -> return name

    let potential_bake state =
      match state.confirmation with
      | `Wait _ | `Auto ->
          dbg "not baking here";
          return ()
      | `Bake ->
          command
            state
            ["bake"; "for"; state.funder; "--force"; "--minimal-timestamp"]
          >>= fun _ -> return ()

    let init state =
      begin
        if String.is_prefix state.funder ~prefix:"unencrypted:"
        then (
          dbg "We need to import %s" state.funder;
          let name = "the-funding-authority" in
          make_account ~sk:state.funder state name
          >>= fun _ ->
          state.funder <- name;
          return () )
        else return ()
      end
      >>= fun () ->
      List.init 3 ~f:Fn.id
      |> List.fold ~init:(return ()) ~f:(fun pm _ ->
             pm >>= fun () -> potential_bake state)

    let wait_arg state =
      match state.confirmation with
      | `Wait n -> Int.to_string n
      | `Bake | `Auto -> "none"

    let account_name (acc : Smartml.Primitives.account) =
      "Acc-" ^ String.prefix acc.pkh 10

    let rec ensure_account state (acc : Smartml.Primitives.account) ~balance =
      let name = account_name acc in
      if Caml.List.mem name state.accounts
      then return name
      else begin
        state.accounts <- name :: state.accounts;
        make_account state ~sk:acc.sk name
        >>= fun name ->
        command state ["get"; "balance"; "for"; name]
        >>= fun balance_cmd ->
        begin
          match
            Option.(
              String.lsplit2
                (Command_result.get_stdout state balance_cmd)
                ~on:' '
              >>= fun (o, _) ->
              try_with (fun () -> Float.(of_string o * 1_000_000. |> to_int)))
          with
          | Some o -> return o
          | None -> Fmt.kstr fail "Failed to get the balance for %s" acc.pkh
        end
        >>= fun current_balance ->
        if balance <= current_balance
        then return (account_name acc)
        else
          transfer state ~dst:name ~amount:(balance - current_balance)
          >>= fun () ->
          dbg "Made %s with %d (cur: %d)" name balance current_balance;
          return name
      end

    and transfer ?arg ?entry_point ?(amount = 0) ?sender ?source state ~dst =
      let extras =
        Option.value_map arg ~default:[] ~f:(fun a -> ["--arg"; a])
        @ Option.value_map entry_point ~default:[] ~f:(fun e ->
              ["--entrypoint"; e])
      in
      begin
        match (sender, source) with
        | None, None -> return None
        | Some e, Some o when Poly.equal e o -> return (Some e)
        | Some _, Some _ ->
            Fmt.kstr fail "source and sender but different: Not Implemented"
        | Some e, None -> return (Some e)
        | None, Some o -> return (Some o)
      end
      >>= (function
            | None -> return state.funder
            | Some (aoa : _ Smartml.Basics.account_or_address) ->
                Smartml.Basics.(
                  ( match aoa with
                  | Account acc ->
                      ensure_account
                        state
                        acc
                        ~balance:(10_000_000 + (20 * amount))
                  | Address (Smartml.Literal.Real _)
                   |Address (Smartml.Literal.Local _) ->
                      Fmt.kstr fail "source|sender = address: not implemented"
                  )))
      >>= fun src ->
      attach
        ~a:
          [ `O
              [ ( "Transfer"
                , `O
                    [ ("amount", `Int amount)
                    ; ("from", `Code [src])
                    ; ("to", `Code [dst]) ] ) ] ]
        (command
           state
           ( [ "-wait"
             ; wait_arg state
             ; "transfer"
             ; (let tez = amount / 1_000_000 in
                let decimals = amount % 1_000_000 in
                Fmt.str
                  "%d%s"
                  tez
                  (if decimals = 0 then "" else Fmt.str ".%06d" decimals))
             ; "from"
             ; src
             ; "to"
             ; dst
             ; "--burn-cap"
             ; "1" ]
           @ extras ))
      >>= fun _ -> potential_bake state

    let fund_originator state originator =
      if not state.originator_funded
      then begin
        state.originator_funded <- true;
        transfer state ~dst:originator ~amount:10_000_000_000
      end
      else return ()

    let originator state =
      let originator =
        match state.originator with
        | None ->
            state.originator <- Some default_originator;
            make_account state default_originator
        | Some s -> return s
      in
      originator
      >>= fun originator ->
      fund_originator state originator >>= fun () -> return originator

    let originate state ~id ~contract ~storage =
      dbg
        "Supposed to originate:@ `%s...`@ with `%s...`"
        (String.prefix contract 20)
        (String.prefix storage 20);
      originator state
      (* TODO (Rodrigo Quelhas): Confirm with rumkeller why we need a specific originator *)
      >>= fun _ ->
      let name =
        Fmt.str
          "c%03d-%s"
          id
          Caml.Digest.(
            Fmt.kstr string "%s %s %f" contract storage (Unix.gettimeofday ())
            |> to_hex)
      in
      let balance = "10" in
      let burn_cap = "20" in
      let contract_path = state.root_dir // Fmt.str "contract-%s.tz" name in
      System.pp_to_file contract_path Fmt.(fun ppf -> string ppf contract);
      attach
        ~a:[`O [("origination", `Text name); ("code", `Path contract_path)]]
        (command
           state
           [ "--wait"
           ; wait_arg state
           ; "originate"
           ; "contract"
           ; name
           ; "transferring"
           ; balance
           ; "from"
             (* TODO (Rodrigo Quelhas): Confirm with rumkeller why we need a specific originator *)
           ; "bootstrap1"
           ; "running"
           ; contract_path
           ; "--init"
           ; storage
           ; "--burn-cap"
           ; burn_cap ])
      >>= fun origination ->
      let out = Command_result.get_stdout state origination |> String.strip in
      let lines = String.split_lines out in
      match
        List.find lines ~f:(fun s -> String.is_prefix s ~prefix:"New contract")
      with
      | None -> fail "Cannot find contract"
      | Some line ->
          ( match String.split line ~on:' ' with
          | "New" :: "contract" :: addr :: _ ->
              dbg "Contract: %S" addr;
              return addr
          | _ -> fail "Cannot find contract address" )
          >>= fun addr -> potential_bake state >>= fun () -> return addr

    let get_contract_storage state ~address =
      command state ["get"; "contract"; "storage"; "for"; address]
      >>= fun cmd_id -> return (Command_result.get_stdout state cmd_id)

    let run_script state ~contract ~parameter =
      command
        state
        [ "run"
        ; "script"
        ; contract
        ; "on"
        ; "storage"
        ; "Unit"
        ; "and"
        ; "input"
        ; parameter
        ; "-G"
        ; "1000000000" ]
      >>= fun _ -> return ()
  end

  module Scenario_interpreter =
    Smartml.Tezos_scenario.Make_interpreter (This_client) (Primitives)

  let pp_state ?(full = false) ppf s =
    let open Scenario_interpreter.State in
    let open More_fmt in
    let history_item ppf item =
      let status ppf = function
        | `Success -> pf ppf "✔"
        | `Failure -> pf ppf "✖"
        | `None -> pf ppf "━"
      in
      let action ppf =
        let open Smartml.Basics in
        let pp_line ppf = function
          | None -> pf ppf " (l.-1)"
          | Some x -> pf ppf " (l.%d)" x
        in
        function
        | New_contract {id; line_no; _} ->
            pf
              ppf
              "New contract %s%a"
              (Smartml.Printer.string_of_contract_id id)
              pp_line
              line_no
        | Set_delegate {id; line_no} ->
            pf
              ppf
              "Set delegate %s%a"
              (Smartml.Printer.string_of_contract_id id)
              pp_line
              line_no
        | Message {id; message; line_no; title; valid; _} ->
            wrapping_box ~indent:2 ppf (fun ppf ->
                pf
                  ppf
                  "Message %s#%s"
                  (Smartml.Printer.string_of_contract_id id)
                  message;
                pp_line ppf line_no;
                if String.is_empty title then () else pf ppf "@ “%s”" title;
                pf ppf "(valid: %s)" (Smartml.Printer.texpr_to_string valid))
        | Compute {id; line_no; _} ->
            pf ppf "Compute %d" id;
            pp_line ppf line_no
        | Add_flag {flag = _; line_no} ->
            pf ppf "Add_flag";
            pp_line ppf line_no
        | Simulation {id; line_no; _} ->
            pf ppf "Simulation %s" (Smartml.Printer.string_of_contract_id id);
            pp_line ppf line_no
        | ScenarioError {message} -> pf ppf "Error: %s" message
        | Html _ -> pf ppf "<html/>"
        | Verify {line_no; _} ->
            pf ppf "Verify";
            pp_line ppf line_no
        | Show {line_no; _} ->
            pf ppf "Show";
            pp_line ppf line_no
        | Exception l ->
            pf
              ppf
              "Exception (%a)"
              text
              (Smartml.Printer.pp_smart_except false l)
        | DynamicContract {id = {dynamic_id}; tparameter = _; tstorage = _} ->
            pf ppf "DynamicContract Dyn_%d [...]" dynamic_id
      in
      let actions ppf = function
        | [] -> ()
        | [one] -> action ppf one
        | multi ->
            vertical_box ~indent:2 ppf (fun ppf ->
                pf ppf "Multiple actions:";
                cut ppf ();
                List.iter multi ~f:(fun a -> pf ppf "* %a" action a))
      in
      let rec pp_attachment ppf att =
        let pp_obj header ppf obj =
          vertical_box ~indent:2 ppf (fun ppf ->
              pf ppf "\\_%s" header;
              List.iter obj ~f:(fun (k, v) ->
                  cut ppf ();
                  wrapping_box ~indent:3 ppf (fun ppf ->
                      pf ppf "|- %s:@ %a" k pp_attachment v)))
        in
        match att with
        | `Path s -> pf ppf "`%s`" s
        | `Text t -> wf ppf "%s" t
        | `Code [] -> pf ppf "<EMPTY>"
        | `Code [line] -> pf ppf "`%s`" line
        | `Code (first :: lines) ->
            vertical_box ppf (fun ppf ->
                pf ppf "|| %s" first;
                List.iter lines ~f:(fun l ->
                    cut ppf ();
                    pf ppf "|| %s" l))
        | `Int i -> pf ppf "%d" i
        | `Error err ->
            wrapping_box ~indent:4 ppf (fun ppf ->
                pf
                  ppf
                  "Error:@ “%a”"
                  Smartml.Tezos_scenario.Error.pp_quick
                  err)
        | `O [(onek, `O obj)] -> pp_obj (str "%s:" onek) ppf obj
        | `O obj -> pp_obj "" ppf obj
      in
      let open Smartml.Tezos_scenario.History_event in
      vertical_box ~indent:2 ppf (fun ppf ->
          wrapping_box ppf ~indent:2 (fun ppf ->
              wf ppf "%a %a" status item.status actions item.actions;
              match item.status with
              | (`None | `Success) when not full -> string ppf "."
              | _ -> pf ppf ", details:");
          match item.status with
          | (`None | `Success) when not full -> ()
          | _ when Poly.equal item.attachements [] -> pf ppf " <none>."
          | _ ->
              List.iter item.attachements ~f:(fun att ->
                  cut ppf ();
                  pp_attachment ppf att))
    in
    let filtered_history =
      let open Smartml.Tezos_scenario.History_event in
      let seq = Caml.Queue.to_seq s.history in
      if full
      then seq
      else
        Caml.Seq.filter
          (fun item ->
            List.for_all item.actions ~f:(function
                | Html _ | Show _ -> false
                | _ -> true))
          seq
    in
    vertical_box ~indent:2 ppf (fun ppf ->
        let q_lgth = Caml.Queue.length s.history in
        wf
          ppf
          "Scenario interpretation state: (%a history item%s):"
          int
          q_lgth
          (if q_lgth = 1 then "" else "s");
        Caml.Seq.iter
          (fun item ->
            cut ppf ();
            history_item ppf item)
          filtered_history)

  let pp_all_results ?full ppf results =
    let open Smartml_scenario in
    List.iter results ~f:(fun (scen, state_opt, res, dur) ->
        let open More_fmt in
        vertical_box ppf (fun ppf ->
            wf
              ppf
              "#====== Test Results for %s: %a ======#"
              scen.name
              (fun ppf -> function
                | Ok () -> pf ppf "All OK!"
                | Error _ -> pf ppf "FAILED ☹")
              res;
            cut ppf ();
            pf ppf "⏰ Total Time: %0.2f s." dur;
            cut ppf ();
            Result.iter_error
              res
              ~f:(fun {Smartml.Tezos_scenario.Error.message} ->
                pf ppf "Error: %s@," message);
            Option.iter state_opt ~f:(fun state -> pp_state ?full ppf state);
            cut ppf ()))

  let run ~config ?(advancement = Fmt.epr "%s\n%!") ~scenarios ~client () =
    dbg "To-do:@ %a" Fmt.(list ~sep:sp Smartml_scenario.pp_quick) scenarios;

    let _ =
      Fmt.kstr
        Caml.Sys.command
        "rm -fr %s; mkdir -p %s"
        client.This_client.root_dir
        client.This_client.root_dir
    in
    let global_start = Unix.gettimeofday () in
    let results =
      List.map scenarios ~f:(fun ({name; sc} as scen) ->
          dbg "Running %s" name;
          Fmt.kstr advancement "Running scenario %s ..." name;
          let start_time = Unix.gettimeofday () in
          let stop () = Unix.gettimeofday () -. start_time in
          let client_dir = This_client.(client.root_dir // name) in
          let client = This_client.{client with root_dir = client_dir} in
          let write_result_file ?client r =
            (* Here we make a deterministic result file.
                     - For successes we record the number of steps.
                     - For failures this number is (for now) not fully
                       deterministic because it depends on the value of
                       `NOW` for instance. *)
            let open Scenario_interpreter.State in
            let result_summary =
              let open Smartml.Tezos_scenario.History_event in
              let pp item =
                let action =
                  match item.actions with
                  | [] -> "no_action"
                  | _ ->
                      List.map
                        item.actions
                        ~f:
                          Smartml.Basics.(
                            function
                            | New_contract {line_no} -> ("New_contract", line_no)
                            | Message {line_no} -> ("Message", line_no)
                            | Verify {line_no} -> ("Verify", line_no)
                            | Compute {line_no} -> ("Compute", line_no)
                            | Simulation {line_no} -> ("Simulation", line_no)
                            | ScenarioError _ -> ("ScenarioError", None)
                            | Html {line_no} -> ("Html", line_no)
                            | Show {line_no} -> ("Show", line_no)
                            | Set_delegate {line_no} -> ("Set_delegate", line_no)
                            | DynamicContract {line_no} ->
                                ("DynamicContract", line_no)
                            | Exception _ -> ("Exception", None)
                            | Add_flag {line_no} -> ("Add_flag", line_no))
                      |> List.map ~f:(fun (name, line_no) ->
                             Printf.sprintf
                               "%s,%s"
                               (Option.value_map
                                  ~default:"____"
                                  ~f:(fun s -> Printf.sprintf "%04d" s)
                                  line_no)
                               name)
                      |> String.concat ~sep:""
                in
                let result =
                  match item.status with
                  | `Failure -> "KO"
                  | `Success -> "OK"
                  | `None -> "__"
                in
                (action, result)
              in
              let history =
                Option.value_map
                  ~default:[]
                  ~f:(fun x ->
                    Caml.Queue.fold (fun prev item -> item :: prev) [] x.history)
                  client
              in
              match history with
              | [] -> [("no_history", "")]
              | history -> history |> List.rev_map ~f:pp
            in
            let _ = Fmt.kstr Caml.Sys.command "mkdir -p %s" client_dir in
            System.pp_to_file
              (client_dir ^ "/result.tsv")
              Fmt.(
                fun ppf ->
                  pf
                    ppf
                    "%s %s\n%s\n%!"
                    scen.name
                    ( match r with
                    | Ok () -> "OK"
                    | Error _ -> "ERROR" )
                    (String.concat
                       ~sep:"\n"
                       (List.map
                          ~f:(fun (action, result) ->
                            Printf.sprintf "  %s,%s" result action)
                          result_summary)))
          in
          match This_client.init client with
          | {result = Ok (); _} ->
              Fmt.kstr advancement "Client initialized.";
              let state =
                Scenario_interpreter.State.of_smartml
                  sc.scenario_state
                  ~client
                  ~log_advancement:advancement
              in
              begin
                match
                  Scenario_interpreter.run
                    ~config
                    state
                    sc.typing_env
                    sc.scenario
                with
                | Ok () ->
                    write_result_file ~client:state (Ok ());
                    (scen, Some state, Ok (), stop ())
                | Error err ->
                    Fmt.epr
                      "ERROR: %a\n%!"
                      Smartml.Tezos_scenario.Error.pp_quick
                      err;
                    write_result_file ~client:state (Error err);
                    (scen, Some state, Error err, stop ())
                | exception e ->
                    Fmt.epr "ERROR: %a\n%!" Fmt.exn e;
                    let message =
                      Fmt.str
                        "Exception: %s"
                        (Smartml.Printer.exception_to_string false e)
                    in
                    let e = Error {Smartml.Tezos_scenario.Error.message} in
                    write_result_file ~client:state e;
                    (scen, Some state, e, stop ())
              end
          | {result = Error err; _} ->
              Fmt.kstr
                advancement
                "Client initialization error: %a."
                Smartml.Tezos_scenario.Error.pp_quick
                err;
              write_result_file (Error err);
              (scen, None, Error err, stop ()))
    in
    Fmt.kstr advancement "Writing results.";
    let global_end = Unix.gettimeofday () in
    let global_time = global_end -. global_start in
    let pp_header ppf () =
      let open More_fmt in
      vertical_box ppf (fun ppf ->
          string ppf (String.make 80 '#');
          cut ppf ();
          let nb = List.length results in
          pf ppf "* Ran %d test%s.@," nb (if nb = 1 then "" else "s");
          pf ppf "* Total time: %f s.@," global_time;
          pf ppf "  * Start: %s@," Date.(float_to_string global_start);
          pf ppf "  * End:   %s@," Date.(float_to_string global_end);
          string ppf (String.make 80 '#'));
      cut ppf ()
    in
    let results_dot_txt =
      Caml.Filename.concat client.This_client.root_dir "results.txt"
    in
    System.pp_to_file results_dot_txt (fun ppf ->
        pp_header ppf ();
        pp_all_results ppf results);
    let results_full_dot_txt =
      Caml.Filename.concat client.This_client.root_dir "results-full.txt"
    in
    System.pp_to_file results_full_dot_txt (fun ppf ->
        pp_header ppf ();
        pp_all_results ~full:true ppf results);
    let results_dot_json =
      Caml.Filename.concat client.This_client.root_dir "results.json"
    in
    let results_save =
      let open Ezjsonm in
      let open Smartml_scenario in
      let open Scenario_interpreter.State in
      let one_result (scen, state, res, (_ : float)) =
        dict
          [ ("name", string scen.name)
          ; ( "result"
            , string
                ( match res with
                | Ok () -> "OK"
                | Error _ -> "ERROR" ) )
          ; ( "items"
            , int
                (Option.value_map ~default:0 state ~f:(fun x ->
                     Caml.Queue.length x.history)) ) ]
      in
      dict [("tezos-scenario-result.v0", list one_result results)]
    in
    System.pp_to_file results_dot_json (fun ppf ->
        Fmt.string ppf (Ezjsonm.value_to_string ~minify:false results_save));
    dbg "%s, %s" results_dot_txt results_full_dot_txt;
    object
      method success =
        List.for_all results ~f:(function
            | _, state, Ok (), _ ->
              ( match state with
              | None -> true
              | Some {history; _} ->
                  Caml.Queue.fold
                    (fun prev h ->
                      let open Smartml.Tezos_scenario.History_event in
                      prev
                      &&
                      match h.status with
                      | `Failure -> false
                      | `Success | `None -> true)
                    true
                    history )
            | _ -> false)

      method results_dot_txt = results_dot_txt

      method results_full_dot_txt = results_full_dot_txt

      method results_dot_json = results_dot_json
    end
end
