(* Copyright 2019-2021 Smart Chain Arena LLC. *)

include module type of Stdlib.String

val pp : Format.formatter -> t -> unit

include Data.S with type t = string
