(* Copyright 2019-2021 Smart Chain Arena LLC. *)

include Stdlib.String

let pp ppf = Format.fprintf ppf "%s"

include Data.Make (struct
  type t = string

  let compare = compare
end)
