(* Copyright 2019-2021 Smart Chain Arena LLC. *)
type type0 =
  | T_unit
  | T_bool
  | T_nat
  | T_int
  | T_mutez
  | T_string
  | T_bytes
  | T_chain_id
  | T_timestamp
  | T_address
  | T_key
  | T_key_hash
  | T_baker_hash
  | T_signature
  | T_operation
  | T_sapling_state       of {memo : int}
  | T_sapling_transaction of {memo : int}
  | T_never
  | T_bls12_381_g1
  | T_bls12_381_g2
  | T_bls12_381_fr
[@@deriving eq, ord, show {with_path = false}]

type type1 =
  | T_option
  | T_list
  | T_set
  | T_contract
  | T_ticket
[@@deriving eq, ord, show {with_path = false}]

type type2 =
  | T_lambda
  | T_map
  | T_big_map
[@@deriving eq, ord, show {with_path = false}]

let strings_of_type0 = function
  | T_unit -> ["unit"]
  | T_bool -> ["bool"]
  | T_nat -> ["nat"]
  | T_int -> ["int"]
  | T_mutez -> ["mutez"]
  | T_string -> ["string"]
  | T_bytes -> ["bytes"]
  | T_chain_id -> ["chain_id"]
  | T_timestamp -> ["timestamp"]
  | T_address -> ["address"]
  | T_key -> ["key"]
  | T_key_hash -> ["key_hash"]
  | T_baker_hash -> ["baker_hash"]
  | T_signature -> ["signature"]
  | T_operation -> ["operation"]
  | T_sapling_state {memo} -> ["sapling_state"; string_of_int memo]
  | T_sapling_transaction {memo} -> ["sapling_transaction"; string_of_int memo]
  | T_never -> ["never"]
  | T_bls12_381_g1 -> ["bls12_381_g1"]
  | T_bls12_381_g2 -> ["bls12_381_g2"]
  | T_bls12_381_fr -> ["bls12_381_fr"]

let string_of_type1 = function
  | T_option -> "option"
  | T_list -> "list"
  | T_set -> "set"
  | T_contract -> "contract"
  | T_ticket -> "ticket"

let string_of_type2 = function
  | T_lambda -> "lambda"
  | T_map -> "map"
  | T_big_map -> "big_map"

let string_of_type0 t =
  match strings_of_type0 t with
  | [s] -> s
  | l -> Printf.sprintf "(%s)" (String.concat " " l)

let print_type0 ppf x = Format.fprintf ppf "%s" (string_of_type0 x)

let print_type1 ppf x = Format.fprintf ppf "%s" (string_of_type1 x)

let print_type2 ppf x = Format.fprintf ppf "%s" (string_of_type2 x)
