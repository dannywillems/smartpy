(* Copyright 2019-2021 Smart Chain Arena LLC. *)
type type0 =
  | T_unit
  | T_bool
  | T_nat
  | T_int
  | T_mutez
  | T_string
  | T_bytes
  | T_chain_id
  | T_timestamp
  | T_address
  | T_key
  | T_key_hash
  | T_baker_hash
  | T_signature
  | T_operation
  | T_sapling_state       of {memo : int}
  | T_sapling_transaction of {memo : int}
  | T_never
  | T_bls12_381_g1
  | T_bls12_381_g2
  | T_bls12_381_fr
[@@deriving eq, ord, show]

type type1 =
  | T_option
  | T_list
  | T_set
  | T_contract
  | T_ticket
[@@deriving eq, ord, show]

type type2 =
  | T_lambda
  | T_map
  | T_big_map
[@@deriving eq, ord, show]

val string_of_type0 : type0 -> string

val strings_of_type0 : type0 -> string list

val string_of_type1 : type1 -> string

val string_of_type2 : type2 -> string

val print_type0 : Format.formatter -> type0 -> unit

val print_type1 : Format.formatter -> type1 -> unit

val print_type2 : Format.formatter -> type2 -> unit
