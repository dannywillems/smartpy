(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Michelson_base.Type
open Printf

let full_types_and_tags = false

type 'm mtype_f =
  | MT0       of type0
  | MT1       of type1 * 'm
  | MT2       of type2 * 'm * 'm
  | MTpair    of
      { fst : 'm
      ; snd : 'm
      ; annot1 : string option
      ; annot2 : string option }
  | MTor      of
      { left : 'm
      ; right : 'm
      ; annot1 : string option
      ; annot2 : string option }
  | MTmissing of string
[@@deriving eq, ord, fold, map, show {with_path = false}]

type mtype =
  { mt : mtype mtype_f
  ; annot_type : string option (* :a *)
  ; annot_variable : string option (* @a *) }
[@@deriving eq, ord, show {with_path = false}]

let mk_mtype ?annot_type ?annot_variable mt = {mt; annot_type; annot_variable}

let rec cata_mtype f {mt; annot_type; annot_variable} =
  f ?annot_type ?annot_variable (map_mtype_f (cata_mtype f) mt)

let cata_mtype_stripped f =
  cata_mtype (fun ?annot_type:_ ?annot_variable:_ x -> f x)

let has_missing_type =
  cata_mtype_stripped (function
      | MTmissing t -> [t]
      | x -> fold_mtype_f ( @ ) [] x)

let mt_unit = mk_mtype (MT0 T_unit)

let mt_bool = mk_mtype (MT0 T_bool)

let mt_nat = mk_mtype (MT0 T_nat)

let mt_int = mk_mtype (MT0 T_int)

let mt_mutez = mk_mtype (MT0 T_mutez)

let mt_string = mk_mtype (MT0 T_string)

let mt_chain_id = mk_mtype (MT0 T_chain_id)

let mt_bytes = mk_mtype (MT0 T_bytes)

let mt_bls12_381_g1 = mk_mtype (MT0 T_bls12_381_g1)

let mt_bls12_381_g2 = mk_mtype (MT0 T_bls12_381_g2)

let mt_bls12_381_fr = mk_mtype (MT0 T_bls12_381_fr)

let mt_timestamp = mk_mtype (MT0 T_timestamp)

let mt_address = mk_mtype (MT0 T_address)

let mt_key = mk_mtype (MT0 T_key)

let mt_key_hash = mk_mtype (MT0 T_key_hash)

let mt_baker_hash = mk_mtype (MT0 T_baker_hash)

let mt_signature = mk_mtype (MT0 T_signature)

let mt_operation = mk_mtype (MT0 T_operation)

let mt_sapling_state memo = mk_mtype (MT0 (T_sapling_state {memo}))

let mt_never = mk_mtype (MT0 T_never)

let mt_sapling_transaction memo = mk_mtype (MT0 (T_sapling_transaction {memo}))

let mt_option t = mk_mtype (MT1 (T_option, t))

let mt_list t = mk_mtype (MT1 (T_list, t))

let mt_set t = mk_mtype (MT1 (T_set, t))

let mt_contract t = mk_mtype (MT1 (T_contract, t))

let mt_ticket t = mk_mtype (MT1 (T_ticket, t))

let mt_pair ?annot1 ?annot2 fst snd =
  mk_mtype (MTpair {fst; snd; annot1; annot2})

let mt_or ?annot1 ?annot2 left right =
  mk_mtype (MTor {left; right; annot1; annot2})

let mt_lambda t1 t2 = mk_mtype (MT2 (T_lambda, t1, t2))

let mt_map t1 t2 = mk_mtype (MT2 (T_map, t1, t2))

let mt_big_map t1 t2 = mk_mtype (MT2 (T_big_map, t1, t2))

let mt_missing s = mk_mtype (MTmissing s)

let remove_annots =
  cata_mtype_stripped (function
      | MTpair {fst; snd} -> mt_pair fst snd
      | MTor {left; right} -> mt_or left right
      | x -> mk_mtype x)

type ad_step =
  | A
  | D
[@@deriving eq, ord, show {with_path = false}]

type tezos_int = Bigint.t [@@deriving eq, ord, show {with_path = false}]

type stack =
  | Stack_ok     of mtype list
  | Stack_failed
[@@deriving eq, ord, show {with_path = false}]

type ('i, 'literal) instr_f =
  | MIerror                 of string
  | MIcomment               of string list
  | MImich                  of mtype Basics.inline_michelson
  | MIdip                   of 'i
  | MIdipn                  of int * 'i
  | MIloop                  of 'i
  | MIiter                  of 'i
  | MImap                   of 'i
  | MIdrop
  | MIdropn                 of int
  | MIdup                   of int
  | MIdig                   of int
  | MIdug                   of int
  | MIfailwith
  | MIif                    of 'i * 'i
  | MIif_left               of 'i * 'i
  | MIif_some               of 'i * 'i
  | MIif_cons               of 'i * 'i
  | MInil                   of mtype
  | MIempty_set             of mtype
  | MIempty_bigmap          of mtype * mtype
  | MIempty_map             of mtype * mtype
  | MIcons
  | MInone                  of mtype
  | MIsome
  | MIpair                  of string option * string option
  | MIleft                  of string option * string option * mtype
  | MIright                 of string option * string option * mtype
  | MIpush                  of mtype * 'literal
  | MIseq                   of 'i list
  | MIswap
  | MIunpair                of bool list
  | MIunit
  | MIfield                 of ad_step list
  | MIsetField              of ad_step list
  | MIcontract              of string option * mtype
  | MIcast                  of mtype * mtype
  | MIexec
  | MIapply
  | MIlambda                of mtype * mtype * 'i
  | MIcreate_contract       of
      { tparameter : mtype
      ; tstorage : mtype
      ; code : 'i }
  | MIself                  of string option
  | MIaddress
  | MIself_address
  | MIimplicit_account
  | MItransfer_tokens
  | MIcheck_signature
  | MIset_delegate
  | MIsapling_empty_state   of int
  | MIsapling_verify_update
  | MInever
  | MIticket
  | MIread_ticket
  | MIsplit_ticket
  | MIjoin_tickets
  | MIpairing_check
  | MIeq
  | MIneq
  | MIle
  | MIlt
  | MIge
  | MIgt
  | MIcompare
  | MImul
  | MIadd
  | MIsub
  | MIediv
  | MInot
  | MIand
  | MIor
  | MIlsl
  | MIlsr
  | MIxor
  | MIconcat                of {arity : [ `Unary | `Binary ] option}
  | MIslice
  | MIsize
  | MIget
  | MIgetn                  of int
  | MIupdate
  | MIupdaten               of int
  | MIget_and_update
  | MIsender
  | MIsource
  | MIamount
  | MIbalance
  | MInow
  | MIlevel
  | MIchain_id
  | MImem
  | MIhash_key
  | MIblake2b
  | MIsha256
  | MIsha512
  | MIkeccak
  | MIsha3
  | MIabs
  | MIneg
  | MIint
  | MIisnat
  | MIpack
  | MIunpack                of mtype
  | MItotal_voting_power
  | MIvoting_power
[@@deriving eq, ord, show {with_path = false}, map, fold]

type ('instr, 'literal) literal_f =
  | Int    of tezos_int
  | Bool   of bool
  | String of string
  | Bytes  of string
  | Unit
  | Pair   of 'literal * 'literal
  | None_
  | Left   of 'literal
  | Right  of 'literal
  | Some_  of 'literal
  | Seq    of 'literal list
  | Elt    of ('literal * 'literal)
  | Instr  of 'instr
  | AnyMap of ('literal * 'literal) list
[@@deriving eq, ord, show {with_path = false}, map, fold]

let sequence_literal_f =
  let open Result in
  function
  | (Int _ | Bool _ | String _ | Bytes _ | Unit | None_) as l -> Ok l
  | Pair (x, y) -> map2 (fun x y -> Pair (x, y)) x y
  | Left x -> map (fun x -> Left x) x
  | Right x -> map (fun x -> Right x) x
  | Some_ x -> map (fun x -> Some_ x) x
  | Seq xs -> map (fun x -> Seq x) (sequence_list xs)
  | Elt (x, y) -> map2 (fun x y -> Elt (x, y)) x y
  | AnyMap xs -> map (fun x -> AnyMap x) (map_list (uncurry (map2 pair)) xs)
  | Instr x -> map (fun x -> Instr x) x

let sequence_instr_f =
  let open Result in
  function
  | MIdip x -> map (fun x -> MIdip x) x
  | MIdipn (n, x) -> map (fun x -> MIdipn (n, x)) x
  | MIloop x -> map (fun x -> MIloop x) x
  | MIiter x -> map (fun x -> MIiter x) x
  | MImap x -> map (fun x -> MImap x) x
  | MIif (i1, i2) -> map2 (fun i1 i2 -> MIif (i1, i2)) i1 i2
  | MIif_left (i1, i2) -> map2 (fun i1 i2 -> MIif_left (i1, i2)) i1 i2
  | MIif_some (i1, i2) -> map2 (fun i1 i2 -> MIif_some (i1, i2)) i1 i2
  | MIif_cons (i1, i2) -> map2 (fun i1 i2 -> MIif_cons (i1, i2)) i1 i2
  | MIlambda (t1, t2, i) -> map (fun x -> MIlambda (t1, t2, x)) i
  | MIcreate_contract {tparameter; tstorage; code} ->
      map (fun code -> MIcreate_contract {tparameter; tstorage; code}) code
  | MIseq is -> map (fun is -> MIseq is) (sequence_list is)
  | MIpush (t, l) -> map (fun l -> MIpush (t, l)) l
  | ( MIdrop | MIdropn _ | MIdup _ | MIfailwith | MIcons | MIsome | MIpair _
    | MIswap | MIunpair _ | MIunit | MIself _ | MIself_address | MIexec
    | MIapply | MIaddress | MIimplicit_account | MItransfer_tokens
    | MIcheck_signature | MIset_delegate | MIeq | MIneq | MIle | MIlt | MIge
    | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv | MInot | MIand | MIor
    | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice | MIsize | MIget | MIgetn _
    | MIupdaten _ | MIupdate | MIget_and_update | MIsender | MIsource | MIamount
    | MIbalance | MInow | MIlevel | MImem | MIhash_key | MIblake2b | MIsha256
    | MIsha512 | MIkeccak | MIsha3 | MIabs | MIneg | MIint | MIisnat | MIpack
    | MIerror _ | MIcomment _ | MImich _ | MIdig _ | MIdug _ | MInil _
    | MIempty_set _ | MIempty_bigmap _ | MIempty_map _ | MInone _ | MIleft _
    | MIright _ | MIfield _ | MIsetField _ | MIcontract _ | MIcast _
    | MIunpack _ | MIchain_id | MIsapling_empty_state _ | MInever
    | MIsapling_verify_update | MIticket | MIread_ticket | MIsplit_ticket
    | MIjoin_tickets | MIpairing_check | MItotal_voting_power | MIvoting_power
      ) as instr ->
      return instr

type instr = {instr : (instr, literal) instr_f}

and literal = {literal : (instr, literal) literal_f}
[@@deriving eq, ord, show {with_path = false}, map, fold]

type ('i, 'l) alg =
  { f_instr : ('i, 'l) instr_f -> 'i
  ; f_literal : ('i, 'l) literal_f -> 'l }

let cata {f_instr; f_literal} =
  let rec cata_instr {instr} =
    f_instr (map_instr_f cata_instr cata_literal instr)
  and cata_literal {literal} =
    f_literal (map_literal_f cata_instr cata_literal literal)
  in
  (cata_instr, cata_literal)

let cata_instr alg = fst (cata alg)

let cata_literal alg = snd (cata alg)

module MLiteral = struct
  let compare x y = compare_literal x y

  let int i = {literal = Int i}

  let small_int i = {literal = Int (Bigint.of_int i)}

  let bool x = {literal = Bool x}

  let string x = {literal = String x}

  let bytes x = {literal = Bytes x}

  let unit = {literal = Unit}

  let left x = {literal = Left x}

  let right x = {literal = Right x}

  let some x = {literal = Some_ x}

  let pair x1 x2 = {literal = Pair (x1, x2)}

  let none = {literal = None_}

  let list xs = {literal = Seq xs}

  let set xs = {literal = Seq (Base.List.dedup_and_sort ~compare xs)}

  let seq xs = {literal = Seq xs}

  let elt k v = {literal = Elt (k, v)}

  let mk_map xs =
    { literal =
        AnyMap
          (Base.List.dedup_and_sort
             ~compare:(fun (k1, _) (k2, _) -> compare k1 k2)
             xs) }

  let sapling_empty_state = seq []

  let instr body = {literal = Instr body}

  let rec to_michelson_string instr_to_string ~protect : literal -> string =
    let continue ~protect = to_michelson_string instr_to_string ~protect in
    let open Printf in
    let prot ~protect s = if protect then Printf.sprintf "(%s)" s else s in
    fun {literal} ->
      match literal with
      | Int i -> Big_int.string_of_big_int i
      | Unit -> "Unit"
      | String s -> Printf.sprintf "%S" s
      | Bool true -> "True"
      | Bool false -> "False"
      | Pair (l, r) ->
          prot
            ~protect
            (sprintf
               "Pair %s %s"
               (continue ~protect:true l)
               (continue ~protect:true r))
      | None_ -> "None"
      | Left l -> prot ~protect (sprintf "Left %s" (continue ~protect:true l))
      | Right l -> prot ~protect (sprintf "Right %s" (continue ~protect:true l))
      | Some_ l -> prot ~protect (sprintf "Some %s" (continue ~protect:true l))
      | Bytes string_bytes -> "0x" ^ Misc.Hex.hexcape string_bytes
      | Seq xs ->
          sprintf
            "{%s}"
            (String.concat
               "; "
               (List.filter_map
                  (fun x ->
                    let x = continue ~protect:false x in
                    if x = "" then None else Some x)
                  xs))
      | AnyMap _ -> assert false
      | Elt (k, v) ->
          sprintf
            "Elt %s %s"
            (continue ~protect:true k)
            (continue ~protect:true v)
      | Instr i -> instr_to_string i

  let to_michelson_string = to_michelson_string ~protect:true
end

let string_of_ad_path p =
  String.concat
    ""
    (List.map
       (function
         | A -> "A"
         | D -> "D")
       p)

let s_expression_of_mtype ?full ?human =
  let is_full = full = Some () in
  let is_human = human = Some () && not is_full in
  let open Base.Sexp in
  let f ?annot_type ?annot_variable mt =
    let annots =
      let get pref = function
        | None -> None
        | Some s -> Some (Atom (pref ^ s))
      in
      Base.List.filter_opt [get ":" annot_type; get "@" annot_variable]
    in
    let mk = function
      | [] -> assert false
      | [x] -> x
      | xs -> List xs
    in
    let atom s = mk (Atom s :: annots) in
    let call s l = List ((Atom s :: annots) @ l) in
    let insert_field_annot a e =
      match a with
      | None | Some "" -> e
      | Some a ->
          let a = Atom ("%" ^ a) in
          ( match e with
          | Atom s -> List [Atom s; a]
          | List (Atom s :: xs) -> List (Atom s :: a :: xs)
          | _ -> assert false )
    in
    match annot_variable with
    | Some a when is_human ->
      ( match Base.String.split ~on:'.' a with
      | [] -> assert false
      | hd :: xs ->
          let rec collapse = function
            | ("left" | "right") :: xs -> collapse xs
            | [last] -> Atom ("@" ^ hd ^ "%" ^ last)
            | _ -> Atom ("@" ^ a)
          in
          collapse xs )
    | _ ->
      ( match mt with
      | MT0 t ->
        begin
          match strings_of_type0 t with
          | [s] -> atom s
          | l -> List (List.map (fun x -> Atom x) l)
        end
      | MT1 (t, t1) -> call (string_of_type1 t) [t1]
      | MT2 (t, t1, t2) -> call (string_of_type2 t) [t1; t2]
      | MTpair {annot1; annot2; fst; snd} ->
          call
            "pair"
            [insert_field_annot annot1 fst; insert_field_annot annot2 snd]
      | MTor {annot1; annot2; left; right} ->
          call
            "or"
            [insert_field_annot annot1 left; insert_field_annot annot2 right]
      | MTmissing s -> call "missing_type_conversion" [Atom s] )
  in
  cata_mtype f

let string_of_mtype ?full ?human ?protect ~html t =
  let s_expr = s_expression_of_mtype ?full ?human t in
  let maybe_escape_string =
    (* See how this is used in `_opam/lib/sexplib0/sexp.ml` *)
    Base.Sexp.Private.mach_maybe_esc_str
  in
  let sexp_to_string_flat sexp =
    let open Base.Sexp in
    let buf = Buffer.create 512 in
    let rec go = function
      | Atom s -> Buffer.add_string buf (maybe_escape_string s)
      | List [] -> Buffer.add_string buf "()"
      | List (h :: t) ->
          let is_partial =
            match h with
            | Atom "missing_type_conversion" when html -> true
            | _ -> false
          in
          Buffer.add_char buf '(';
          if is_partial then Buffer.add_string buf "<span class='partialType'>";
          go h;
          Base.List.iter t ~f:(fun elt ->
              Buffer.add_char buf ' ';
              go elt);
          if is_partial then Buffer.add_string buf "</span>";
          Buffer.add_char buf ')'
    in
    go sexp;
    Buffer.contents buf
  in
  match (protect, s_expr) with
  | None, List l -> List.map sexp_to_string_flat l |> String.concat " "
  | None, Atom a -> maybe_escape_string a
  | Some (), any -> sexp_to_string_flat any

let memo_string_of_mtype_human =
  Misc.memoize ~clear_after:1000 (fun _f (full, se) ->
      string_of_mtype
        ?full:(if full then Some () else None)
        ~human:()
        ~html:false
        se)

let memo_string_of_mtype_human ?full t =
  memo_string_of_mtype_human (full = Some (), t)

let string_of_ok_stack ?full stack =
  String.concat " : " (List.map (memo_string_of_mtype_human ?full) stack)

let string_of_stack ?full = function
  | Stack_ok stack -> string_of_ok_stack ?full stack
  | Stack_failed -> "FAILED"

let strip_annots {mt} = mk_mtype mt

let strip_annot_variable {mt; annot_type} = mk_mtype mt ?annot_type

(** {1 Stack helpers} *)

type tliteral =
  { tliteral : (tinstr, tliteral) literal_f
  ; t : mtype Result.t }

and tinstr =
  { tinstr : (tinstr, tliteral) instr_f
  ; stack : stack Result.t }
[@@deriving eq, ord, show {with_path = false}]

type ('i, 'l) talg =
  { f_tinstr : stack:stack Result.t -> ('i, 'l) instr_f -> 'i
  ; f_tliteral : t:mtype Result.t -> ('i, 'l) literal_f -> 'l }

let tcata {f_tinstr; f_tliteral} =
  let rec cata_tinstr {tinstr; stack} =
    f_tinstr ~stack (map_instr_f cata_tinstr cata_tliteral tinstr)
  and cata_tliteral {tliteral; t} =
    f_tliteral ~t (map_literal_f cata_tinstr cata_tliteral tliteral)
  in
  (cata_tinstr, cata_tliteral)

let cata_tinstr alg = fst (tcata alg)

let cata_tliteral alg = snd (tcata alg)

(* Paramorphisms on instructions and literals. *)
let para_alg ~p_instr ~p_literal =
  let f_instr i = ({instr = map_instr_f fst fst i}, p_instr i) in
  let f_literal l = ({literal = map_literal_f fst fst l}, p_literal l) in
  {f_instr; f_literal}

let _size_tinstr, _size_tliteral =
  let f_tinstr ~stack:_ = fold_instr_f ( + ) ( + ) 1 in
  let f_tliteral ~t:_ = fold_literal_f ( + ) ( + ) 1 in
  tcata {f_tinstr; f_tliteral}

let erase_types_instr, erase_types_literal =
  let f_tinstr ~stack:_ instr = {instr} in
  let f_tliteral ~t:_ literal = {literal} in
  tcata {f_tinstr; f_tliteral}

type instr_spec =
  { name : string
  ; rule :
         tparameter:mtype
      -> mtype list
      -> (stack Result.t -> tinstr, mtype -> tliteral) instr_f
      -> (tinstr, tliteral) instr_f * stack Result.t
  ; commutative : bool
  ; arities : (int * int) option }

let mk_spec_raw name ?commutative ?arities rule =
  {name; commutative = commutative = Some (); arities; rule}

let mk_spec name ?commutative ?arities rule =
  let rule ~tparameter stack instr =
    let err msg =
      let tinstr =
        map_instr_f (fun x -> x (Error "error")) (fun _ -> assert false) instr
      in
      (tinstr, Error msg)
    in
    match rule ~tparameter stack instr with
    | Some x -> x
    | None -> err (name ^ ": unexpected stack")
  in
  {name; commutative = commutative = Some (); arities; rule}

let mk_spec_no_sub name ?commutative ?arities rule =
  let rule ~tparameter stack instr =
    let tinstr =
      map_instr_f (fun _ -> assert false) (fun _ -> assert false) instr
    in
    let stack = rule ~tparameter stack in
    Some (tinstr, stack)
  in
  mk_spec name ?commutative ?arities rule

let mk_spec_basic name ?commutative ~arities rule =
  let arities =
    (* We ensure that every spec declares proper arities.
       Small hack for CONCAT which is non-regular in Michelson. *)
    match arities with
    | -1, -1 -> None
    | _ -> Some arities
  in
  let rule ~tparameter:_ stack instr =
    match rule stack with
    | None -> None
    | Some (n, xs) ->
        let tinstr =
          map_instr_f (fun _ -> assert false) (fun _ -> assert false) instr
        in
        let stack = Ok (Stack_ok (xs @ List.drop n stack)) in
        Some (tinstr, stack)
  in
  mk_spec name ?commutative ?arities rule

let mk_spec_const name t =
  mk_spec_basic ~arities:(0, 1) name (fun _ -> Some (0, [t]))

(** {1 Unification}  *)

let unify_annots pref ?tolerant a b =
  match (a, b) with
  | Some a, Some b when a = b -> Ok (Some a)
  | Some a, Some b ->
      if tolerant = Some ()
      then Ok None
      else
        Error
          (Printf.sprintf
             "Cannot unify annotations '%s%s' and '%s%s'"
             pref
             a
             pref
             b)
  | _ -> Ok None

let rec unify_types t u =
  let mk {mt} =
    match
      ( unify_annots ":" t.annot_type u.annot_type
      , unify_annots "@" ~tolerant:() t.annot_variable u.annot_variable )
    with
    | Error e, _ | _, Error e -> Error e
    | Ok annot_type, Ok annot_variable -> Ok {mt; annot_type; annot_variable}
  in
  let open Result in
  let err () =
    Error
      (Printf.sprintf
         "Cannot unify types '%s' and '%s'."
         (show_mtype t)
         (show_mtype u))
  in
  match (t.mt, u.mt) with
  | MT0 t1, MT0 t2 -> if equal_type0 t1 t2 then mk t else err ()
  | MT1 (T_option, t), MT1 (T_option, u) -> mt_option <$> unify_types t u
  | MT1 (T_list, t), MT1 (T_list, u) -> mt_list <$> unify_types t u
  | MT1 (T_ticket, t), MT1 (T_ticket, u) -> mt_ticket <$> unify_types t u
  | MT1 (T_set, t), MT1 (T_set, u) -> mt_set <$> unify_types t u
  | MT1 (T_contract, t), MT1 (T_contract, u) -> mt_contract <$> unify_types t u
  | ( MTpair {fst = t1; snd = t2; annot1 = a1; annot2 = a2}
    , MTpair {fst = u1; snd = u2; annot1 = b1; annot2 = b2} ) ->
      let* annot1 = unify_annots "%" a1 b1 in
      let* v1 = unify_types t1 u1 in
      let* annot2 = unify_annots "%" a2 b2 in
      let* v2 = unify_types t2 u2 in
      mk (mt_pair ?annot1 v1 ?annot2 v2)
  | ( MTor {left = t1; right = t2; annot1 = a1; annot2 = a2}
    , MTor {left = u1; right = u2; annot1 = b1; annot2 = b2} ) ->
      let* annot1 = unify_annots "%" a1 b1 in
      let* v1 = unify_types t1 u1 in
      let* annot2 = unify_annots "%" a2 b2 in
      let* v2 = unify_types t2 u2 in
      mk (mt_or ?annot1 v1 ?annot2 v2)
  | MT2 (T_lambda, t1, t2), MT2 (T_lambda, u1, u2) ->
      mt_lambda <$> unify_types t1 u1 <*> unify_types t2 u2 >>= mk
  | MT2 (T_map, t1, t2), MT2 (T_map, u1, u2) ->
      mt_map <$> unify_types t1 u1 <*> unify_types t2 u2 >>= mk
  | MT2 (T_big_map, t1, t2), MT2 (T_big_map, u1, u2) ->
      mt_big_map <$> unify_types t1 u1 <*> unify_types t2 u2 >>= mk
  (* TODO Distinguish different missing types? *)
  | MTmissing t_name, MTmissing u_name when t_name = u_name ->
      mk (mt_missing t_name)
  | _ -> err ()

let unifiable_types t u = Base.Result.is_ok (unify_types t u)

let unify_stack_elements t1 t2 =
  match unify_types t1 t2 with
  | Ok t -> Some t
  | Error _ -> None

let rec unify_ok_stacks s1 s2 =
  match (s1, s2) with
  | se1 :: s1, se2 :: s2 ->
      Option.map2
        (fun x xs -> x :: xs)
        (unify_stack_elements se1 se2)
        (unify_ok_stacks s1 s2)
  | [], [] -> Some []
  | _ -> None

let unifiable_ok_stacks t u = Option.is_some (unify_ok_stacks t u)

let unify_stacks s1 s2 =
  match (s1, s2) with
  | Stack_ok s1, Stack_ok s2 ->
      Option.map (fun x -> Stack_ok x) (unify_ok_stacks s1 s2)
  | Stack_failed, s2 -> Some s2
  | s1, Stack_failed -> Some s1

let initial_stack ~tparameter ~tstorage =
  Stack_ok
    [ mt_pair
        {tparameter with annot_variable = Some "parameter"}
        {tstorage with annot_variable = Some "storage"} ]

(** {1 Michelson instructions} *)

let mi_seq =
  let rule ~tparameter:_ stack = function
    | MIseq xs ->
        let rec f r stack = function
          | [] -> Some (MIseq (List.rev r), stack)
          | x :: xs ->
              let (x : tinstr) = x stack in
              f (x :: r) x.stack xs
        in
        f [] (Ok (Stack_ok stack)) xs
    | _ -> assert false
  in
  mk_spec "(instruction sequence)" rule

let mi_comment =
  let rule ~tparameter:_ stack = Ok (Stack_ok stack) in
  mk_spec_no_sub "(comment instruction)" rule

let mi_error =
  let rule ~tparameter:_ _stack = Error "error" in
  mk_spec_no_sub "(error instruction)" rule

let mi_failwith =
  let rule ~tparameter:_ = function
    | _ :: _ -> Ok Stack_failed
    | [] -> Error "FAILWITH on empty stack"
  in
  mk_spec_no_sub "FAILWITH" ~arities:(1, 0) rule

let mi_never =
  let rule ~tparameter:_ = function
    | {mt = MT0 T_never} :: _ -> Ok Stack_failed
    | _ -> Error "NEVER on empty stack"
  in
  mk_spec_no_sub "NEVER" ~arities:(1, 0) rule

let mi_ticket =
  mk_spec_basic "TICKET" ~arities:(2, 1) (function
      | t :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_ticket t])
      | _ -> None)

let mi_read_ticket =
  mk_spec_basic "READ_TICKET" ~arities:(1, 2) (function
      | {mt = MT1 (T_ticket, t)} :: _ ->
          Some (1, [mt_pair mt_address (mt_pair t mt_nat); mt_ticket t])
      | _ -> None)

let mi_join_tickets =
  mk_spec_basic "JOIN_TICKETS" ~arities:(1, 1) (function
      | { mt =
            MTpair
              { fst = {mt = MT1 (T_ticket, _)} as t1
              ; snd = {mt = MT1 (T_ticket, _)} as t2 } }
        :: _
        when unifiable_types t1 t2 ->
          Some (1, [mt_option t1])
      | _ -> None)

let mi_split_ticket =
  mk_spec_basic "SPLIT_TICKET" ~arities:(2, 1) (function
      | ({mt = MT1 (T_ticket, _)} as t)
        :: {mt = MTpair {fst = {mt = MT0 T_nat}; snd = {mt = MT0 T_nat}}} :: _
        ->
          Some (2, [mt_option (mt_pair t t)])
      | _ -> None)

let mi_pairing_check =
  mk_spec_basic "PAIRING_CHECK" ~arities:(1, 1) (function
      | { mt =
            MT1
              ( T_list
              , { mt =
                    MTpair
                      { fst = {mt = MT0 T_bls12_381_g1}
                      ; snd = {mt = MT0 T_bls12_381_g2} } } ) }
        :: _ ->
          Some (1, [mt_bool])
      | _ -> None)

let cond_aux x y =
  let open Result in
  let* sx = x.stack in
  let* sy = y.stack in
  Option.cata (error "cannot unify branches") ok (unify_stacks sx sy)

let mi_if =
  let rule ~tparameter:_ stack instr =
    match (stack, instr) with
    | {mt = MT0 T_bool} :: tail, MIif (x, y) ->
        let x = x (Ok (Stack_ok tail)) in
        let y = y (Ok (Stack_ok tail)) in
        Some (MIif (x, y), cond_aux x y)
    | _, MIif _ -> None
    | _ -> assert false
  in
  mk_spec "IF" rule

let mi_if_some =
  let rule ~tparameter:_ stack instr =
    match (stack, instr) with
    | {mt = MT1 (T_option, t)} :: tail, MIif_some (x, y) ->
        let a =
          match t.annot_variable with
          | Some v -> v ^ ".some"
          | None -> "some"
        in
        let t = {t with annot_variable = Some a} in
        let x = x (Ok (Stack_ok (t :: tail))) in
        let y = y (Ok (Stack_ok tail)) in
        Some (MIif_some (x, y), cond_aux x y)
    | _, MIif_some _ -> None
    | _ -> assert false
  in
  mk_spec "IF_SOME" rule

let mi_if_left =
  let rule ~tparameter:_ stack instr =
    match (stack, instr) with
    | ( {mt = MTor {annot1; annot2; left = t; right = u}; annot_variable} :: tail
      , MIif_left (x, y) ) ->
        let open Option in
        let fa v = v ^ "." ^ Option.default "left" annot1 in
        let t = {t with annot_variable = Option.map fa annot_variable} in
        let fa v = v ^ "." ^ Option.default "right" annot2 in
        let u = {u with annot_variable = Option.map fa annot_variable} in
        let x = x (Ok (Stack_ok (t :: tail))) in
        let y = y (Ok (Stack_ok (u :: tail))) in
        return (MIif_left (x, y), cond_aux x y)
    | _, MIif_left _ -> None
    | _ -> assert false
  in
  mk_spec "IF_LEFT" rule

let mi_if_cons =
  let rule ~tparameter:_ stack instr =
    match (stack, instr) with
    | {mt = MT1 (T_list, t)} :: tail, MIif_cons (x, y) ->
        let open Option in
        let x = x (Ok (Stack_ok (t :: mt_list t :: tail))) in
        let y = y (Ok (Stack_ok tail)) in
        return (MIif_cons (x, y), cond_aux x y)
    | _, MIif_cons _ -> None
    | _ -> assert false
  in
  mk_spec "IF_CONS" rule

let mi_dip =
  let rule ~tparameter:_ stack = function
    | MIdip body ->
      ( match stack with
      | [] -> None
      | t :: tail ->
          let body = body (Ok (Stack_ok tail)) in
          let tinstr = MIdip body in
          ( match body.stack with
          | Ok (Stack_ok tail') -> Some (tinstr, Ok (Stack_ok (t :: tail')))
          | _ -> Some (tinstr, Error "DIP: body error") ) )
    | _ -> assert false
  in
  mk_spec "DIP" rule

let mi_dipn =
  let rule ~tparameter:_ stack = function
    | MIdipn (n, body) when n < List.length stack ->
        assert (n >= 0);
        let body = body (Ok (Stack_ok (List.drop n stack))) in
        let tinstr = MIdipn (n, body) in
        ( match body.stack with
        | Ok (Stack_ok stack') ->
            Some (tinstr, Ok (Stack_ok (List.take n stack @ stack')))
        | _ -> Some (tinstr, Error "DIP: body error") )
    | _ -> assert false
  in
  mk_spec "DIPn" rule

let is_hot =
  let open Ternary in
  let is_hot ?annot_type:_ ?annot_variable:_ mt =
    match mt with
    | MT1 (T_ticket, _) -> Yes
    | MT1 (T_contract, _) | MT2 (T_lambda, _, _) -> No
    | MTmissing _ -> Maybe
    | t -> fold_mtype_f or_ No t
  in
  cata_mtype is_hot

let is_duppable t = is_hot t <> Yes

let mi_dup ~strict_dup i =
  assert (i >= 1);
  let rec get acc n = function
    | x :: _ when n = 1 ->
        if (not strict_dup) || is_duppable x
        then Some (i, x :: List.rev (x :: acc))
        else None
    | x :: rest -> get (x :: acc) (n - 1) rest
    | [] -> None
  in
  mk_spec_basic "DUP" ~arities:(i, i + 1) (get [] i)

let mi_dig n =
  let rule ~tparameter:_ stack =
    let hi, lo = List.split_at n stack in
    match lo with
    | [] -> Error (Printf.sprintf "DIG %i: stack too short" n)
    | x :: lo -> Ok (Stack_ok ((x :: hi) @ lo))
  in
  mk_spec_no_sub "DIG" rule

let mi_dug n =
  let rule ~tparameter:_ = function
    | x :: tail ->
        if n > List.length tail
        then Error (Printf.sprintf "DUG %i: stack too short" n)
        else
          let hi, lo = List.split_at n tail in
          Ok (Stack_ok (hi @ (x :: lo)))
    | [] -> Error "DUG: empty stack"
  in
  mk_spec_no_sub "DUG" rule

let mi_swap =
  mk_spec_basic "SWAP" ~arities:(2, 2) (function
      | a :: b :: _ -> Some (2, [b; a])
      | _ -> None)

let mi_drop =
  mk_spec_basic "DROP" ~arities:(1, 0) (function
      | _ :: _ -> Some (1, [])
      | [] -> None)

let mi_dropn n =
  mk_spec_basic "DROP" ~arities:(n, 0) (function
      | _ :: _ -> Some (n, [])
      | [] -> None)

let unpair_size = List.fold_left (fun acc x -> acc + if x then 1 else 0) 0

let mi_unpair fields =
  assert (List.length fields >= 2);
  let rec aux acc fields stack =
    match (stack, fields) with
    | _, [] -> Some (1, List.rev acc)
    | se :: _, [true] -> Some (1, List.rev (se :: acc))
    | {mt = MTpair {fst; snd}} :: rest, select :: fields ->
        let acc = if select then fst :: acc else acc in
        aux acc fields (snd :: rest)
    | _ -> None
  in
  mk_spec_basic "UNPAIR" ~arities:(1, unpair_size fields) (aux [] fields)

let mi_iter =
  let rule ~tparameter:_ stack instr =
    match (stack, instr) with
    | c :: tail, MIiter body ->
        let el =
          match c.mt with
          | MT2 (T_map, k, v) -> Some (mt_pair k v)
          | MT1 ((T_list | T_set), t) -> Some t
          | _ -> None
        in
        ( match el with
        | None -> None
        | Some el ->
            let body = body (Ok (Stack_ok (el :: tail))) in
            let tinstr = MIiter body in
            ( match body.stack with
            | Ok (Stack_ok stack') when unifiable_ok_stacks tail stack' ->
                Some (tinstr, Ok (Stack_ok tail))
            | _ -> Some (tinstr, Error "LOOP: incompatible body") ) )
    | [], MIiter _ -> None
    | _ -> assert false
  in
  mk_spec "ITER" rule

let mi_loop =
  let rule ~tparameter:_ stack = function
    | MIloop body ->
      ( match stack with
      | {mt = MT0 T_bool} :: tail ->
          let body = body (Ok (Stack_ok tail)) in
          let tinstr = MIloop body in
          ( match body.stack with
          | Ok (Stack_ok ({mt = MT0 T_bool} :: tail'))
            when unifiable_ok_stacks tail tail' ->
              Some (tinstr, Ok (Stack_ok tail))
          | _ -> Some (tinstr, Error "LOOP: incompatible body") )
      | _ -> None )
    | _ -> assert false
  in
  mk_spec "LOOP" rule

let mi_lambda =
  let rule ~tparameter:_ stack = function
    | MIlambda (t_in, t_out, body) ->
        let body = body (Ok (Stack_ok [t_in])) in
        let tinstr = MIlambda (t_in, t_out, body) in
        let stack =
          let ok = Ok (Stack_ok (mt_lambda t_in t_out :: stack)) in
          match body.stack with
          | Ok (Stack_ok [t_out']) when unifiable_types t_out t_out' -> ok
          | Ok (Stack_ok _) -> Error "LAMBDA: non-singleton result stack"
          | Ok Stack_failed -> ok
          | Error _ -> Error "error"
        in
        (tinstr, stack)
    | _ -> assert false
  in
  mk_spec_raw "LAMBDA" rule

let mi_map =
  let rule ~tparameter:_ stack instr =
    match (stack, instr) with
    | c :: tail, MImap body ->
        let wrap_v =
          match c.mt with
          | MT2 (T_map, k, v) -> Some (mt_map k, mt_pair k v)
          | MT1 (T_list, v) -> Some (mt_list, v)
          | _ -> None
        in
        ( match wrap_v with
        | None -> None
        | Some (wrap, v) ->
            let body = body (Ok (Stack_ok (v :: tail))) in
            let tinstr = MImap body in
            ( match body.stack with
            | Ok (Stack_ok (v' :: tail')) when unifiable_ok_stacks tail tail' ->
                Some (tinstr, Ok (Stack_ok (wrap v' :: tail)))
            | _ -> Some (tinstr, Error "MAP: incompatible body") ) )
    | [], MImap _ -> None
    | _ -> assert false
  in
  mk_spec "MAP" rule

let mi_pair ?annot1 ?annot2 () =
  mk_spec_basic "PAIR" ~arities:(2, 1) (function
      | a :: b :: _ -> Some (2, [mt_pair ?annot1 ?annot2 a b])
      | _ -> None)

let mi_cons =
  mk_spec_basic "CONS" ~arities:(2, 1) (function
      | a :: {mt = MT1 (T_list, a')} :: _ ->
        ( match unify_types a a' with
        | Ok t -> Some (2, [mt_list (strip_annots t)])
        | Error _ -> None )
      | _ -> None)

let mi_get =
  mk_spec_basic "GET" ~arities:(2, 1) (function
      | key :: {mt = MT2 (T_map, key', value)} :: _
        when unifiable_types key key' ->
          Some (2, [mt_option value])
      | key :: {mt = MT2 (T_big_map, key', value)} :: _
        when unifiable_types key key' ->
          Some (2, [mt_option value])
      | _ -> None)

let rec get_comb_type n = function
  | t when n = 0 -> Some (1, t)
  | {mt = MTpair {fst}} when n = 1 -> Some (1, fst)
  | {mt = MTpair {snd}} -> get_comb_type (n - 2) snd
  | _ -> None

let mi_getn n =
  mk_spec_basic "GET" ~arities:(1, 1) (function
      | t :: _ -> Option.map (fun (i, t) -> (i, [t])) (get_comb_type n t)
      | [] -> None)

let mi_updaten n =
  mk_spec_basic "UPDATE" ~arities:(2, 1) (function
      | t1 :: t :: _ ->
        begin
          match get_comb_type n t with
          | None -> None
          | Some (_, t2) ->
              if unifiable_types t1 t2 then Some (2, [t]) else None
        end
      | _ :: _ | [] -> None)

let mi_eq =
  mk_spec_basic "EQ" ~commutative:() ~arities:(1, 1) (function
      | {mt = MT0 T_int} :: _ -> Some (1, [mt_bool])
      | _ -> None)

let mi_neq = {mi_eq with name = "NEQ"}

let mi_lt = {mi_neq with name = "LT"; commutative = false}

let mi_le = {mi_lt with name = "LE"}

let mi_gt = {mi_lt with name = "GT"}

let mi_ge = {mi_lt with name = "GE"}

let mi_neg =
  mk_spec_basic "NEG" ~arities:(1, 1) (function
      | {mt = MT0 T_nat} :: _ -> Some (1, [mt_int])
      | {mt = MT0 T_int} :: _ -> Some (1, [mt_int])
      | {mt = MT0 T_bls12_381_g1} :: _ -> Some (1, [mt_bls12_381_g1])
      | {mt = MT0 T_bls12_381_g2} :: _ -> Some (1, [mt_bls12_381_g2])
      | {mt = MT0 T_bls12_381_fr} :: _ -> Some (1, [mt_bls12_381_fr])
      | _ -> None)

let mi_abs =
  mk_spec_basic "ABS" ~arities:(1, 1) (function
      | {mt = MT0 T_int} :: _ -> Some (1, [mt_nat])
      | _ -> None)

let mi_isnat =
  mk_spec_basic "ISNAT" ~arities:(1, 1) (function
      | {mt = MT0 T_int} :: _ -> Some (1, [mt_option mt_nat])
      | _ -> None)

let mi_int =
  mk_spec_basic "INT" ~arities:(1, 1) (function
      | {mt = MT0 T_nat} :: _ -> Some (1, [mt_int])
      | {mt = MT0 T_bls12_381_fr} :: _ -> Some (1, [mt_int])
      | _ -> None)

let rec is_comparable mtype =
  match mtype.mt with
  | MT0
      ( T_unit | T_bool | T_nat | T_int | T_mutez | T_string | T_bytes
      | T_chain_id | T_timestamp | T_address | T_key | T_key_hash | T_baker_hash
      | T_signature | T_never ) ->
      true
  | MT1 (T_option, t) -> is_comparable t
  | MTpair {fst = t1; snd = t2} | MTor {left = t1; right = t2} ->
      is_comparable t1 && is_comparable t2
  | MT0
      ( T_operation | T_sapling_state _ | T_sapling_transaction _
      | T_bls12_381_g1 | T_bls12_381_g2 | T_bls12_381_fr )
   |MT1 ((T_list | T_set | T_contract | T_ticket), _)
   |MT2 ((T_lambda | T_map | T_big_map), _, _)
   |MTmissing _ ->
      false

let mi_compare =
  mk_spec_basic "COMPARE" ~arities:(2, 1) (function
      | a :: b :: _
        when is_comparable a && is_comparable b && unifiable_types a b ->
          Some (2, [mt_int])
      | _ -> None)

let mi_add =
  mk_spec_basic "ADD" ~commutative:() ~arities:(2, 1) (function
      | {mt = MT0 T_int} :: {mt = MT0 T_int} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_int} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_nat} :: {mt = MT0 T_int} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_nat])
      | {mt = MT0 T_mutez} :: {mt = MT0 T_mutez} :: _ -> Some (2, [mt_mutez])
      | {mt = MT0 T_timestamp} :: {mt = MT0 T_int} :: _ ->
          Some (2, [mt_timestamp])
      | {mt = MT0 T_int} :: {mt = MT0 T_timestamp} :: _ ->
          Some (2, [mt_timestamp])
      | {mt = MT0 T_bls12_381_g1} :: {mt = MT0 T_bls12_381_g1} :: _ ->
          Some (2, [mt_bls12_381_g1])
      | {mt = MT0 T_bls12_381_g2} :: {mt = MT0 T_bls12_381_g2} :: _ ->
          Some (2, [mt_bls12_381_g2])
      | {mt = MT0 T_bls12_381_fr} :: {mt = MT0 T_bls12_381_fr} :: _ ->
          Some (2, [mt_bls12_381_fr])
      | _ -> None)

let mi_sub =
  mk_spec_basic "SUB" ~arities:(2, 1) (function
      | {mt = MT0 T_int} :: {mt = MT0 T_int} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_int} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_nat} :: {mt = MT0 T_int} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_mutez} :: {mt = MT0 T_mutez} :: _ -> Some (2, [mt_mutez])
      | {mt = MT0 T_timestamp} :: {mt = MT0 T_int} :: _ ->
          Some (2, [mt_timestamp])
      | {mt = MT0 T_timestamp} :: {mt = MT0 T_timestamp} :: _ ->
          Some (2, [mt_int])
      | _ -> None)

let mi_mul =
  mk_spec_basic "MUL" ~commutative:() ~arities:(2, 1) (function
      | {mt = MT0 T_int} :: {mt = MT0 T_int} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_int} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_nat} :: {mt = MT0 T_int} :: _ -> Some (2, [mt_int])
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_nat])
      | {mt = MT0 T_mutez} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_mutez])
      | {mt = MT0 T_nat} :: {mt = MT0 T_mutez} :: _ -> Some (2, [mt_mutez])
      | {mt = MT0 T_bls12_381_g1} :: {mt = MT0 T_bls12_381_fr} :: _ ->
          Some (2, [mt_bls12_381_g1])
      | {mt = MT0 T_bls12_381_g2} :: {mt = MT0 T_bls12_381_fr} :: _ ->
          Some (2, [mt_bls12_381_g2])
      | {mt = MT0 T_bls12_381_fr} :: {mt = MT0 T_bls12_381_fr} :: _ ->
          Some (2, [mt_bls12_381_fr])
      | {mt = MT0 T_nat} :: {mt = MT0 T_bls12_381_fr} :: _ ->
          Some (2, [mt_bls12_381_fr])
      | {mt = MT0 T_int} :: {mt = MT0 T_bls12_381_fr} :: _ ->
          Some (2, [mt_bls12_381_fr])
      | {mt = MT0 T_bls12_381_fr} :: {mt = MT0 T_nat} :: _ ->
          Some (2, [mt_bls12_381_fr])
      | {mt = MT0 T_bls12_381_fr} :: {mt = MT0 T_int} :: _ ->
          Some (2, [mt_bls12_381_fr])
      | _ -> None)

let mi_ediv =
  mk_spec_basic "EDIV" ~arities:(2, 1) (function
      | {mt = MT0 T_int} :: {mt = MT0 T_int} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | {mt = MT0 T_int} :: {mt = MT0 T_nat} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | {mt = MT0 T_nat} :: {mt = MT0 T_int} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ ->
          Some (2, [mt_option (mt_pair mt_nat mt_nat)])
      | {mt = MT0 T_mutez} :: {mt = MT0 T_nat} :: _ ->
          Some (2, [mt_option (mt_pair mt_mutez mt_mutez)])
      | {mt = MT0 T_mutez} :: {mt = MT0 T_mutez} :: _ ->
          Some (2, [mt_option (mt_pair mt_nat mt_mutez)])
      | _ -> None)

let mi_not =
  mk_spec_basic "NOT" ~arities:(1, 1) (function
      | {mt = MT0 T_bool} :: _ -> Some (1, [mt_bool])
      | _ -> None)

let mi_and =
  mk_spec_basic "AND" ~commutative:() ~arities:(2, 1) (function
      | {mt = MT0 T_bool} :: {mt = MT0 T_bool} :: _ -> Some (2, [mt_bool])
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_nat])
      | _ -> None)

let mi_or = {mi_and with name = "OR"}

let mi_xor = {mi_or with name = "XOR"}

let mi_lsl =
  mk_spec_basic "LSL" ~arities:(2, 1) (function
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_nat])
      | _ -> None)

let mi_lsr =
  mk_spec_basic "LSR" ~arities:(2, 1) (function
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: _ -> Some (2, [mt_nat])
      | _ -> None)

let mi_unit = mk_spec_const "UNIT" mt_unit

let mi_nil t = mk_spec_const "NIL" (mt_list t)

let mi_empty_set t = mk_spec_const "EMPTY_SET" (mt_set t)

let mi_empty_map k v = mk_spec_const "EMPTY_MAP" (mt_map k v)

let mi_empty_big_map k v = mk_spec_const "EMPTY_BIG_MAP" (mt_big_map k v)

let mi_sapling_empty_state memo =
  mk_spec_const "SAPLING_EMPTY_STATE" (mt_sapling_state memo)

let mi_none t = mk_spec_const "NONE" (mt_option t)

let mi_push =
  let rule ~tparameter:_ stack = function
    | MIpush (t, l) ->
        let l = l t in
        let tinstr = MIpush (t, l) in
        let stack =
          match l.t with
          | Ok _ -> Ok (Stack_ok (t :: stack))
          | Error e -> Error e
        in
        (tinstr, stack)
    | _ -> assert false
  in
  mk_spec_raw "PUSH" ~arities:(0, 1) rule

let mi_some =
  mk_spec_basic "SOME" ~arities:(1, 1) (function
      | t :: _ -> Some (1, [mt_option (strip_annots t)])
      | [] -> None)

let mi_left ?annot1 ?annot2 b =
  mk_spec_basic "LEFT" ~arities:(1, 1) (function
      | a :: _ -> Some (1, [mt_or ?annot1 ?annot2 (strip_annots a) b])
      | [] -> None)

let mi_right ?annot1 ?annot2 a =
  mk_spec_basic "RIGHT" ~arities:(1, 1) (function
      | b :: _ -> Some (1, [mt_or ?annot1 ?annot2 a (strip_annots b)])
      | [] -> None)

(** Select the part of the type designated by the ad_path. *)
let rec ad_path_in_type ops t =
  match (ops, t) with
  | [], _ -> Some t
  | A :: p, {mt = MTpair {fst}} -> ad_path_in_type p fst
  | D :: p, {mt = MTpair {snd}} -> ad_path_in_type p snd
  | _ :: _, _ -> None

let mi_field steps =
  mk_spec_basic
    ~arities:(1, 1)
    (Printf.sprintf "C%sR" (string_of_ad_path steps))
    (function
      | t :: _ ->
        ( match ad_path_in_type steps t with
        | None -> None
        | Some t -> Some (1, [t]) )
      | [] -> None)

let mi_set_field steps =
  mk_spec_basic
    ~arities:(2, 1)
    (Printf.sprintf "SET_C%sR" (string_of_ad_path steps))
    (function
      | t :: x :: _ ->
        ( match ad_path_in_type steps t with
        | Some x' when unifiable_types x x' -> Some (2, [t])
        | _ -> None )
      | _ -> None)

let mi_update =
  mk_spec_basic "UPDATE" ~arities:(3, 1) (function
      | k :: {mt = MT0 T_bool} :: {mt = MT1 (T_set, k')} :: _ ->
          Base.Option.map
            ~f:(fun k -> (3, [mt_set k]))
            (Base.Result.ok (unify_types k k'))
      | k :: {mt = MT1 (T_option, v)} :: {mt = MT2 (T_map, k', v')} :: _ ->
          Option.map2
            (fun k v -> (3, [mt_map k v]))
            (Base.Result.ok (unify_types k k'))
            (Base.Result.ok (unify_types v v'))
      | k :: {mt = MT1 (T_option, v)} :: {mt = MT2 (T_big_map, k', v')} :: _ ->
          Option.map2
            (fun k v -> (3, [mt_big_map k v]))
            (Base.Result.ok (unify_types k k'))
            (Base.Result.ok (unify_types v v'))
      | _ -> None)

let mi_get_and_update =
  mk_spec_basic "GET_AND_UPDATE" ~arities:(3, 2) (function
      | k :: {mt = MT1 (T_option, v)} :: {mt = MT2 (T_map, k', v')} :: _ ->
          Option.map2
            (fun k v -> (3, [mt_option v; mt_map k v]))
            (Base.Result.ok (unify_types k k'))
            (Base.Result.ok (unify_types v v'))
      | k :: {mt = MT1 (T_option, v)} :: {mt = MT2 (T_big_map, k', v')} :: _ ->
          Option.map2
            (fun k v -> (3, [mt_option v; mt_big_map k v]))
            (Base.Result.ok (unify_types k k'))
            (Base.Result.ok (unify_types v v'))
      | _ -> None)

let mi_mem =
  mk_spec_basic "MEM" ~arities:(2, 1) (function
      | k :: {mt = MT1 (T_set, k')} :: _ when unifiable_types k k' ->
          Some (2, [mt_bool])
      | k :: {mt = MT2 (T_map, k', _)} :: _ when unifiable_types k k' ->
          Some (2, [mt_bool])
      | k :: {mt = MT2 (T_big_map, k', _)} :: _ when unifiable_types k k' ->
          Some (2, [mt_bool])
      | _ -> None)

let mi_exec =
  mk_spec_basic "EXEC" ~arities:(2, 1) (function
      | k :: {mt = MT2 (T_lambda, k', v)} :: _ ->
          if unifiable_types k k' then Some (2, [v]) else None
      | _ -> None)

let mi_apply =
  mk_spec_basic "APPLY" ~arities:(2, 1) (function
      | k :: {mt = MT2 (T_lambda, {mt = MTpair {fst = k'; snd = k''}}, v)} :: _
        when unifiable_types k k' ->
          Some (2, [mt_lambda k'' v])
      | _ -> None)

let mi_contract t =
  mk_spec_basic "CONTRACT" ~arities:(1, 1) (function
      | {mt = MT0 T_address} :: _ -> Some (1, [mt_option (mt_contract t)])
      | _ -> None)

let mi_cast t =
  mk_spec_basic "CAST" ~arities:(1, 1) (function
      | _ :: _ -> Some (1, [t])
      | _ -> None)

let mi_transfer_tokens =
  mk_spec_basic "TRANSFER_TOKENS" ~arities:(3, 1) (function
      | p :: {mt = MT0 T_mutez} :: {mt = MT1 (T_contract, p')} :: _
        when unifiable_types p p' ->
          Some (3, [mt_operation])
      | _ -> None)

let mi_set_delegate =
  mk_spec_basic "SET_DELEGATE" ~arities:(1, 1) (function
      | {mt = MT1 (T_option, {mt = MT0 (T_key_hash | T_baker_hash)})} :: _ ->
          Some (1, [mt_operation])
      | _ -> None)

let mi_sapling_verify_update =
  mk_spec_basic "SAPLING_VERIFY_UPDATE" ~arities:(2, 1) (function
      | {mt = MT0 (T_sapling_transaction {memo = m1})}
        :: {mt = MT0 (T_sapling_state {memo = m2})} :: _
        when m1 = m2 ->
          Some (2, [mt_option (mt_pair mt_int (mt_sapling_state m1))])
      | _ -> None)

let mi_hash_key =
  mk_spec_basic "HASH_KEY" ~arities:(1, 1) (function
      | {mt = MT0 T_key} :: _ -> Some (1, [mt_key_hash])
      | _ -> None)

let mi_blake2b =
  mk_spec_basic "BLAKE2B" ~arities:(1, 1) (function
      | {mt = MT0 T_bytes} :: _ -> Some (1, [mt_bytes])
      | _ -> None)

let mi_sha256 = {mi_blake2b with name = "SHA256"}

let mi_sha512 = {mi_blake2b with name = "SHA512"}

let mi_keccak = {mi_blake2b with name = "KECCAK"}

let mi_sha3 = {mi_blake2b with name = "SHA3"}

let mi_check_signature =
  mk_spec_basic "CHECK_SIGNATURE" ~arities:(3, 1) (function
      | {mt = MT0 T_key} :: {mt = MT0 T_signature} :: {mt = MT0 T_bytes} :: _ ->
          Some (3, [mt_bool])
      | _ -> None)

let mi_self_address = mk_spec_const "SELF_ADDRESS" mt_address

let mi_sender = mk_spec_const "SENDER" mt_address

let mi_source = mk_spec_const "SOURCE" mt_address

let mi_amount = mk_spec_const "AMOUNT" mt_mutez

let mi_balance = mk_spec_const "BALANCE" mt_mutez

let mi_now = mk_spec_const "NOW" mt_timestamp

let mi_level = mk_spec_const "LEVEL" mt_nat

let mi_chain_id = mk_spec_const "CHAIN_ID" mt_chain_id

let mi_total_voting_power = mk_spec_const "TOTAL_VOTING_POWER" mt_nat

let mk_concat arity stack =
  let r =
    match stack with
    | Stack_ok ({mt = MT1 (T_list, {mt = MT0 (T_string | T_bytes) as mt})} :: s)
      ->
        Some (`Unary, mt, s)
    | Stack_ok ({mt = MT0 T_string as mt} :: {mt = MT0 T_string} :: s)
     |Stack_ok ({mt = MT0 T_bytes as mt} :: {mt = MT0 T_bytes} :: s) ->
        Some (`Binary, mt, s)
    | _ -> None
  in
  let r =
    match (arity, r) with
    | None, Some (a, mt, s) -> Some (a, mt, s)
    | Some a, Some (a', mt, s) when a = a' -> Some (a, mt, s)
    | _ -> None
  in
  match r with
  | Some (arity, mt, s) ->
      (MIconcat {arity = Some arity}, Ok (Stack_ok (mk_mtype mt :: s)))
  | _ -> (MIconcat {arity}, Error "illegal arguments to CONCAT")

let mi_concat arity =
  let arities =
    (* CONCAT has a variable arity. *)
    match arity with
    | Some `Unary -> (1, 1)
    | Some `Binary -> (2, 1)
    | None -> (-1, -1)
  in
  let rule ~tparameter:_ stack = function
    | MIconcat {arity} -> Some (mk_concat arity (Stack_ok stack))
    | _ -> assert false
  in
  mk_spec "CONCAT" ~arities rule

let mi_pack =
  mk_spec_basic "PACK" ~arities:(1, 1) (function
      | _ :: _ -> Some (1, [mt_bytes])
      | [] -> None)

let mi_unpack t =
  mk_spec_basic "UNPACK" ~arities:(1, 1) (function
      | {mt = MT0 T_bytes} :: _ -> Some (1, [mt_option t])
      | _ -> None)

let mi_slice =
  mk_spec_basic "SLICE" ~arities:(3, 1) (function
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: {mt = MT0 T_string} :: _ ->
          Some (3, [mt_option mt_string])
      | {mt = MT0 T_nat} :: {mt = MT0 T_nat} :: {mt = MT0 T_bytes} :: _ ->
          Some (3, [mt_option mt_bytes])
      | _ -> None)

let mi_size =
  mk_spec_basic "SIZE" ~arities:(1, 1) (function
      | { mt =
            ( MT0 T_string
            | MT0 T_bytes
            | MT1 ((T_set | T_list), _)
            | MT2 ((T_map | T_big_map), _, _) ) }
        :: _ ->
          Some (1, [mt_nat])
      | _ -> None)

let mi_mich ~name ~types_in ~types_out =
  mk_spec_basic
    name
    ~arities:(List.length types_in, List.length types_out)
    (fun stack ->
      if Base.List.is_prefix ~prefix:types_in stack ~equal:equal_mtype
      then Some (List.length types_in, types_out)
      else None)

let mi_self =
  let rec find_entrypoint mtype name =
    match mtype.mt with
    | MTor {left; annot1} when annot1 = Some name -> Some left
    | MTor {right; annot2} when annot2 = Some name -> Some right
    | MTor {left; right} ->
      ( match find_entrypoint left name with
      | None -> find_entrypoint right name
      | x -> x )
    | _ -> None
  in
  let rule ~tparameter stack = function
    | MIself ep_name ->
        let tinstr = MIself ep_name in
        ( match ep_name with
        | None -> Some (tinstr, Ok (Stack_ok (mt_contract tparameter :: stack)))
        | Some ep_name ->
          ( match find_entrypoint tparameter ep_name with
          | None -> None
          | Some t -> Some (tinstr, Ok (Stack_ok (mt_contract t :: stack))) ) )
    | _ -> assert false
  in
  mk_spec "SELF" ~arities:(0, 1) rule

let mi_address =
  mk_spec_basic "ADDRESS" ~arities:(1, 1) (function
      | {mt = MT1 (T_contract, _)} :: _ -> Some (1, [mt_address])
      | _ -> None)

let mi_implicit_account =
  mk_spec_basic "IMPLICIT_ACCOUNT" ~arities:(1, 1) (function
      | {mt = MT0 T_key_hash} :: _ -> Some (1, [mt_contract mt_unit])
      | _ -> None)

let mi_voting_power =
  mk_spec_basic "VOTING_POWER" ~arities:(1, 1) (function
      | {mt = MT0 (T_key_hash | T_baker_hash)} :: _ -> Some (1, [mt_nat])
      | _ -> None)

let mi_create_contract =
  let rule ~tparameter:_ stack = function
    | MIcreate_contract {tparameter; tstorage; code} ->
      ( match stack with
      | {mt = MT1 (T_option, {mt = MT0 (T_key_hash | T_baker_hash)})}
        :: {mt = MT0 T_mutez} :: storage :: tail ->
          let code = code (Ok (initial_stack ~tparameter ~tstorage)) in
          if unifiable_types storage tstorage
          then
            let tinstr = MIcreate_contract {tparameter; tstorage; code} in
            let stack = Ok (Stack_ok (mt_operation :: mt_address :: tail)) in
            Some (tinstr, stack)
          else None
      | _ -> None )
    | _ -> assert false
  in
  mk_spec "CREATE_CONTRACT" ~arities:(3, 2) rule

let spec_of_instr ~strict_dup = function
  | MIadd -> mi_add
  | MIsub -> mi_sub
  | MImul -> mi_mul
  | MIediv -> mi_ediv
  | MInil t -> mi_nil t
  | MIempty_set t -> mi_empty_set t
  | MIempty_bigmap (k, v) -> mi_empty_big_map k v
  | MIempty_map (k, v) -> mi_empty_map k v
  | MInone t -> mi_none t
  | MIpush _ -> mi_push
  | MIcontract (_, t) -> mi_contract t
  | MIcast (_, t) -> mi_cast t
  | MIconcat {arity} -> mi_concat arity
  | MIslice -> mi_slice
  | MIset_delegate -> mi_set_delegate
  | MIsapling_empty_state memo -> mi_sapling_empty_state memo
  | MIsapling_verify_update -> mi_sapling_verify_update
  | MInever -> mi_never
  | MIticket -> mi_ticket
  | MIread_ticket -> mi_read_ticket
  | MIsplit_ticket -> mi_split_ticket
  | MIjoin_tickets -> mi_join_tickets
  | MIpairing_check -> mi_pairing_check
  | MIcheck_signature -> mi_check_signature
  | MIhash_key -> mi_hash_key
  | MIblake2b -> mi_blake2b
  | MIsha256 -> mi_sha256
  | MIsha512 -> mi_sha512
  | MIkeccak -> mi_keccak
  | MIsha3 -> mi_sha3
  | MIeq -> mi_eq
  | MIneq -> mi_neq
  | MIlt -> mi_lt
  | MIle -> mi_le
  | MIgt -> mi_gt
  | MIge -> mi_ge
  | MIneg -> mi_neg
  | MIabs -> mi_abs
  | MIisnat -> mi_isnat
  | MIint -> mi_int
  | MIcompare -> mi_compare
  | MIsender -> mi_sender
  | MIsource -> mi_source
  | MIamount -> mi_amount
  | MIbalance -> mi_balance
  | MInow -> mi_now
  | MIlevel -> mi_level
  | MIchain_id -> mi_chain_id
  | MItotal_voting_power -> mi_total_voting_power
  | MIvoting_power -> mi_voting_power
  | MIpair (annot1, annot2) -> mi_pair ?annot1 ?annot2 ()
  | MIswap -> mi_swap
  | MIdrop -> mi_drop
  | MIdropn n -> mi_dropn n
  | MIunpair n -> mi_unpair n
  | MIunit -> mi_unit
  | MIfailwith -> mi_failwith
  | MIsome -> mi_some
  | MIleft (annot1, annot2, t) -> mi_left ?annot1 ?annot2 t
  | MIright (annot1, annot2, t) -> mi_right ?annot1 ?annot2 t
  | MItransfer_tokens -> mi_transfer_tokens
  | MIcons -> mi_cons
  | MInot -> mi_not
  | MIand -> mi_and
  | MIor -> mi_or
  | MIlsl -> mi_lsl
  | MIlsr -> mi_lsr
  | MIxor -> mi_xor
  | MIfield steps -> mi_field steps
  | MIsetField steps -> mi_set_field steps
  | MIpack -> mi_pack
  | MIunpack t -> mi_unpack t
  | MIdup i -> mi_dup ~strict_dup i
  | MIget -> mi_get
  | MIgetn n -> mi_getn n
  | MIupdaten n -> mi_updaten n
  | MIsize -> mi_size
  | MIupdate -> mi_update
  | MIget_and_update -> mi_get_and_update
  | MImem -> mi_mem
  | MIself _ -> mi_self
  | MIself_address -> mi_self_address
  | MIexec -> mi_exec
  | MIapply -> mi_apply
  | MIaddress -> mi_address
  | MIimplicit_account -> mi_implicit_account
  | MIcreate_contract _ -> mi_create_contract
  | MImich {name; typesIn; typesOut} ->
      mi_mich ~name ~types_in:typesIn ~types_out:typesOut
  | MIdip _ -> mi_dip
  | MIdipn _ -> mi_dipn
  | MIiter _ -> mi_iter
  | MImap _ -> mi_map
  | MIloop _ -> mi_loop
  | MIif _ -> mi_if
  | MIif_some _ -> mi_if_some
  | MIif_left _ -> mi_if_left
  | MIif_cons _ -> mi_if_cons
  | MIdig n -> mi_dig n
  | MIdug n -> mi_dug n
  | MIseq _ -> mi_seq
  | MIlambda _ -> mi_lambda
  | MIcomment _ -> mi_comment
  | MIerror _ -> mi_error

let is_commutative instr = (spec_of_instr ~strict_dup:true instr).commutative

let name_of_instr instr = (spec_of_instr ~strict_dup:true instr).name

(** {1 Type checking} *)

(* Recognize lists of Elts and lists of Instrs. *)
let sanitize_instr, sanitize_literal =
  let f_literal : _ literal_f -> _ = function
    | Seq ({literal = Elt _} :: _ as xs) ->
        let f = function
          | {literal = Elt (k, v)} -> (k, v)
          | _ -> failwith "sanitize: Elt followed by non-Elt"
        in
        {literal = AnyMap (List.map f xs)}
    | Seq ({literal = Instr _} :: _ as xs) ->
        let f = function
          | {literal = Instr i} -> i
          | _ -> failwith "sanitize: instruction followed by non-instruction"
        in
        {literal = Instr {instr = MIseq (List.map f xs)}}
    | literal -> {literal}
  in
  let f_instr instr = {instr} in
  cata {f_instr; f_literal}

(* Match a comb type against the given tuple. *)
let rec match_comb t xs =
  match (t.mt, xs) with
  | _, [x] -> [(t, x)]
  | MTpair {fst; snd}, x :: xs -> (fst, x) :: match_comb snd xs
  | _ -> failwith "match_comb"

(* Roll a list into a right comb. *)
let rec comb_literal = function
  | [x1; x2] -> {tliteral = Pair (x1, x2); t = Result.map2 mt_pair x1.t x2.t}
  | x :: xs ->
      let xs = comb_literal xs in
      {tliteral = Pair (x, xs); t = Result.map2 mt_pair x.t xs.t}
  | _ -> assert false

let typecheck_literal_f
    ~strict_dup:_ ~tparameter:_ (literal : _ literal_f) (t : mtype Result.t) =
  let tliteral' :
      (stack Result.t -> tinstr, mtype Result.t -> tliteral) literal_f =
    map_literal_f snd snd literal
  in
  let err = Error "type error" in
  let r =
    let open Result in
    let* t = map_error (fun _ -> "type error") t in
    match (t.mt, tliteral') with
    | MT0 T_unit, (Unit as l)
     |MT0 T_bool, (Bool _ as l)
     |MT0 T_nat, (Int _ as l)
     |MT0 T_int, (Int _ as l)
     |MT0 T_mutez, (Int _ as l)
     |MT0 T_string, (String _ as l)
     |MT0 T_bytes, (Bytes _ as l)
     |MT0 T_chain_id, (Bytes _ as l)
     |MT0 T_timestamp, ((Int _ | String _) as l)
     |MT0 T_address, ((Bytes _ | String _) as l)
     |MT0 T_key, ((Bytes _ | String _) as l)
     |MT0 T_key_hash, ((Bytes _ | String _) as l)
     |MT0 T_baker_hash, ((Bytes _ | String _) as l)
     |MT0 T_signature, ((Bytes _ | String _) as l)
     |MT0 (T_sapling_state _), (Seq [] as l)
     |MT0 (T_sapling_transaction _), (String "FAKE_SAPLING_TRANSACTION" as l)
     |MT0 T_bls12_381_g1, (Bytes _ as l)
     |MT0 T_bls12_381_g2, (Bytes _ as l)
     |MT0 T_bls12_381_fr, (Bytes _ as l) ->
        return l
    | MT1 (T_contract, _), (String _ as l) ->
        (* needed for scenarios: "Contract_*" *) return l
    | MTpair {fst; snd}, Pair (x1, x2) ->
        return (Pair (x1 (Ok fst), x2 (Ok snd)))
    | MTpair _, Seq xs ->
        let xs = List.map (fun (t, x) -> x (Ok t)) (match_comb t xs) in
        return (comb_literal xs).tliteral
    | MTor {left}, Left x -> return (Left (x (Ok left)))
    | MTor {right}, Right x -> return (Right (x (Ok right)))
    | MT1 (T_option, t), Some_ x -> return (Some_ (x (Ok t)))
    | MT1 (T_option, _), None_ -> return None_
    | MT1 ((T_set | T_list), t), Seq xs ->
        return (Seq (List.map (fun x -> x (Ok t)) xs))
    | MT2 ((T_map | T_big_map), _tk, _tv), Seq [] -> return (AnyMap [])
    | MT2 ((T_map | T_big_map), _tk, _tv), Seq (_ :: _) ->
        assert false (* eliminated by 'sanitize' *)
    | MT2 ((T_map | T_big_map), tk, tv), AnyMap xs ->
        let f (k, v) = (k (Ok tk), v (Ok tv)) in
        return (AnyMap (List.map f xs))
    | MT2 (T_lambda, t_in, _), Instr i ->
        return (Instr (i (Ok (Stack_ok [t_in]))))
    | MT2 (T_lambda, _t1, _), Seq (_ :: _) ->
        assert false (* eliminated by 'sanitize' *)
    | ( ( MT0
            ( T_unit | T_bool | T_nat | T_int | T_mutez | T_string | T_bytes
            | T_chain_id | T_timestamp | T_address | T_key | T_key_hash
            | T_baker_hash | T_signature | T_operation | T_sapling_state _
            | T_sapling_transaction _ | T_never | T_bls12_381_g1
            | T_bls12_381_g2 | T_bls12_381_fr )
        | MT1 ((T_option | T_list | T_set | T_contract | T_ticket), _)
        | MT2 ((T_lambda | T_map | T_big_map), _, _)
        | MTpair _ | MTor _ | MTmissing _ )
      , ( Int _ | Bool _ | String _ | Bytes _ | Unit | Pair _ | None_ | Left _
        | Right _ | Some_ _ | Seq _ | Elt _ | AnyMap _ | Instr _ ) ) ->
        let msg =
          let literal = map_literal_f fst fst literal in
          let l = MLiteral.to_michelson_string show_instr {literal} in
          let t = string_of_mtype ~html:false t in
          sprintf "Literal %s does not have type %s." l t
        in
        error msg
  in
  match r with
  | Ok tliteral ->
      let r =
        sequence_literal_f
          (map_literal_f (fun {stack} -> stack) (fun {t} -> t) tliteral)
      in
      let t = if Result.is_error r then err else t in
      {tliteral; t}
  | Error msg ->
      let tliteral =
        map_literal_f (fun f -> f err) (fun f -> f err) tliteral'
      in
      {tliteral; t = Error msg}

let typecheck_instr_f ~strict_dup ~tparameter i (stack : stack Result.t) :
    tinstr =
  let i : (stack Result.t -> tinstr, mtype Result.t -> tliteral) instr_f =
    map_instr_f snd snd i
  in
  let on_error_stack msg =
    let err = Error "outer error" in
    let tinstr = map_instr_f (fun x -> x err) (fun x -> x err) i in
    {tinstr; stack = Error msg}
  in
  match (stack, i) with
  | _, (MIcomment _ as tinstr) -> {tinstr; stack}
  | Ok (Stack_ok stack), _ ->
      let {rule} = spec_of_instr ~strict_dup i in
      let okify f x = f (Ok x) in
      let tinstr, stack = rule ~tparameter stack (map_instr_f id okify i) in
      {tinstr; stack}
  | Ok Stack_failed, _ -> on_error_stack "instruction on failed stack"
  | Error _, _ -> on_error_stack "previous error"

let typecheck_alg ~strict_dup ~tparameter =
  let p_instr = typecheck_instr_f ~strict_dup ~tparameter in
  let p_literal = typecheck_literal_f ~strict_dup ~tparameter in
  para_alg ~p_instr ~p_literal

let typecheck_instr ~strict_dup ~tparameter stack i =
  let i = sanitize_instr i in
  snd (cata_instr (typecheck_alg ~strict_dup ~tparameter) i) (Ok stack)

let typecheck_literal ~strict_dup ~tparameter t l =
  let l = sanitize_literal l in
  snd (cata_literal (typecheck_alg ~strict_dup ~tparameter) l) (Ok t)

let has_error ~accept_missings =
  let has_missing_type t = if accept_missings then [] else has_missing_type t in
  let f_tinstr ~stack instr =
    match stack with
    | Error s -> [s]
    | Ok _ ->
      ( match instr with
      | MIerror s -> [s]
      | MInil t
       |MIempty_set t
       |MInone t
       |MIleft (_, _, t)
       |MIright (_, _, t)
       |MIcontract (_, t)
       |MIcast (_, t)
       |MIunpack t ->
          has_missing_type t
      | MIempty_bigmap (k, v) | MIempty_map (k, v) ->
          has_missing_type k @ has_missing_type v
      | MIpush (t, _) -> has_missing_type t
      | MImich {typesIn; typesOut} ->
          Base.List.concat_map ~f:has_missing_type typesIn
          @ Base.List.concat_map ~f:has_missing_type typesOut
      | MIlambda (t1, t2, _i) as x ->
          has_missing_type t1
          @ has_missing_type t2
          @ fold_instr_f ( @ ) (curry fst) [] x
      | x -> fold_instr_f ( @ ) (curry fst) [] x )
  in
  let f_tliteral ~t _ =
    match t with
    | Error msg -> [msg]
    | Ok t -> has_missing_type t
  in
  cata_tinstr {f_tinstr; f_tliteral}

let name_of_instr_exn = function
  | ( MIadd | MIsub | MImul | MIediv | MInil _ | MIempty_set _
    | MIempty_bigmap _ | MIempty_map _ | MInone _ | MIpush _ | MIcontract _
    | MIcast _ | MIconcat _ | MIslice | MIcheck_signature | MIset_delegate
    | MIeq | MIle | MIge | MIlt | MIgt | MIneq | MIcompare | MIsender | MIsource
    | MIamount | MIbalance | MInow | MIlevel | MIhash_key | MIblake2b | MIsha256
    | MIsha512 | MIkeccak | MIsha3 | MIabs | MIneg | MIint | MIisnat | MIpair _
    | MIswap | MIdrop | MIdropn _ | MIunpair _ | MIunit | MIfailwith | MIsome
    | MIleft _ | MIright _ | MIcons | MItransfer_tokens | MInot | MIor | MIlsl
    | MIlsr | MIxor | MIand | MIfield _ | MIsetField _ | MIpack | MIunpack _
    | MIdup _ | MIget | MIgetn _ | MIupdaten _ | MIsize | MIupdate
    | MIget_and_update | MImem | MImich _ | MIself _ | MIself_address | MIexec
    | MIapply | MIaddress | MIimplicit_account | MIchain_id
    | MIsapling_empty_state _ | MIsapling_verify_update | MInever | MIticket
    | MIread_ticket | MIsplit_ticket | MIjoin_tickets | MIpairing_check
    | MItotal_voting_power | MIvoting_power ) as instr ->
      name_of_instr instr
  | MIdip _ -> "DIP"
  | MIdipn _ -> "DIPN"
  | MIloop _ -> "LOOP"
  | MIiter _ -> "ITER"
  | MImap _ -> "MAP"
  | MIif_left _ -> "IF_LEFT"
  | MIif_some _ -> "IF_SOME"
  | MIif_cons _ -> "IF_CONS"
  | MIif _ -> "IF"
  | MIdig _ -> "DIG"
  | MIdug _ -> "DUG"
  | MIlambda _ -> "LAMBDA"
  | MIerror _ -> failwith "name_of_instr_exn: MIerror"
  | MIcomment _ -> failwith "name_of_instr_exn: MIcomment"
  | MIseq _ -> failwith "name_of_instr_exn: MIseq"
  | MIcreate_contract _ -> "CREATE_CONTRACT"

let two_field_annots = function
  | Some a1, Some a2 -> ["%" ^ a1; "%" ^ a2]
  | Some a1, None -> ["%" ^ a1]
  | None, Some a2 -> ["%"; "%" ^ a2]
  | None, None -> []

let rec display_literal instr_to_string ~protect : tliteral -> string =
  let continue ~protect = display_literal instr_to_string ~protect in
  let open Printf in
  let prot ~protect s = if protect then Printf.sprintf "(%s)" s else s in
  fun {tliteral} ->
    match tliteral with
    | Int i -> Big_int.string_of_big_int i
    | Unit -> "Unit"
    | String s -> Printf.sprintf "%S" s
    | Bool true -> "True"
    | Bool false -> "False"
    | Pair (l, r) ->
        prot
          ~protect
          (sprintf
             "Pair %s %s"
             (continue ~protect:true l)
             (continue ~protect:true r))
    | None_ -> "None"
    | Left l -> prot ~protect (sprintf "Left %s" (continue ~protect:true l))
    | Right l -> prot ~protect (sprintf "Right %s" (continue ~protect:true l))
    | Some_ l -> prot ~protect (sprintf "Some %s" (continue ~protect:true l))
    | Bytes string_bytes -> "0x" ^ Misc.Hex.hexcape string_bytes
    | Seq xs ->
        sprintf
          "{%s}"
          (String.concat "; " (List.map (continue ~protect:false) xs))
    | Elt (k, v) ->
        sprintf
          "Elt %s %s"
          (continue ~protect:true k)
          (continue ~protect:true v)
    | AnyMap xs ->
        let elt (k, v) =
          sprintf
            "Elt %s %s"
            (continue ~protect:true k)
            (continue ~protect:true v)
        in
        sprintf "{%s}" (String.concat "; " (List.map elt xs))
    | Instr i -> instr_to_string i

let display_literal = display_literal ~protect:true

let seq_snoc xs x =
  match xs with
  | {tinstr = MIseq xs} -> {tinstr = MIseq (xs @ [x]); stack = x.stack}
  | xs -> {tinstr = MIseq [xs; x]; stack = x.stack}

let wrap_in_seq = function
  | {tinstr = MIseq _} as i -> i
  | i -> {tinstr = MIseq [i]; stack = i.stack}

let insert_subsequences =
  let f_tinstr ~stack = function
    | MIseq _ as tinstr -> {tinstr; stack}
    | tinstr -> {tinstr = map_instr_f wrap_in_seq id tinstr; stack}
  in
  cata_tinstr {f_tinstr; f_tliteral = (fun ~t tliteral -> {tliteral; t})}

let string_of_instr ~html ~show_types ?sub_sequence indent inst =
  let text_spaces =
    let spaces =
      "                                                                "
    in
    let spaces = spaces ^ spaces ^ spaces ^ spaces in
    Array.init 256 (fun i -> String.sub spaces 0 i)
  in
  let html_spaces =
    let spaces =
      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
    in
    let spaces = spaces ^ spaces ^ spaces ^ spaces in
    let spaces = spaces ^ spaces ^ spaces ^ spaces ^ spaces ^ spaces in
    Array.init 256 (fun i -> String.sub spaces 0 (6 * i))
  in
  let rec string_of_instr ?sub_sequence indent inst =
    let spaces = if html then html_spaces else text_spaces in
    let get_indent () = spaces.(indent) in
    let ppAlign ?app s =
      let app =
        match app with
        | None -> ""
        | Some s -> Printf.sprintf " %s" s
      in
      Printf.sprintf
        "%s%s%s;%s"
        (get_indent ())
        s
        app
        ( if show_types
        then spaces.(max 0 (10 - String.length s - String.length app))
        else "" )
    in
    let span className text =
      if html
      then Printf.sprintf "<span class='%s'>%s</span>" className text
      else text
    in
    let s =
      match inst.tinstr with
      | MIseq [] -> Printf.sprintf "%s{}" (get_indent ())
      | MIseq l ->
          Printf.sprintf
            "%s{\n%s\n%s}%s"
            (get_indent ())
            (String.concat "\n" (List.map (string_of_instr (indent + 2)) l))
            (get_indent ())
            (if sub_sequence = Some () then "" else ";")
      | MIdip code ->
          Printf.sprintf
            "%sDIP\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) code)
      | MIdipn (n, code) ->
          Printf.sprintf
            "%sDIP %i\n%s;"
            (get_indent ())
            n
            (string_of_instr ~sub_sequence:() (indent + 2) code)
      | MIloop code ->
          Printf.sprintf
            "%sLOOP\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) code)
      | MIiter code ->
          Printf.sprintf
            "%sITER\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) code)
      | MImap code ->
          Printf.sprintf
            "%sMAP\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) code)
      | MIif_left (l, r) ->
          Printf.sprintf
            "%sIF_LEFT\n%s\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) l)
            (string_of_instr ~sub_sequence:() (indent + 2) r)
      | MIif_some (l, r) ->
          Printf.sprintf
            "%sIF_SOME\n%s\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) l)
            (string_of_instr ~sub_sequence:() (indent + 2) r)
      | MIif_cons (l, r) ->
          Printf.sprintf
            "%sIF_CONS\n%s\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) l)
            (string_of_instr ~sub_sequence:() (indent + 2) r)
      | MIif (l, r) ->
          Printf.sprintf
            "%sIF\n%s\n%s;"
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 2) l)
            (string_of_instr ~sub_sequence:() (indent + 2) r)
      | MIcomment comments ->
          let lines =
            List.concat (List.map (String.split_on_char '\n') comments)
          in
          String.concat
            "\n"
            (List.map
               (fun line ->
                 Printf.sprintf
                   "%s%s"
                   (get_indent ())
                   (span "comment" (Printf.sprintf "# %s" line)))
               lines)
      | MIdig n -> ppAlign (Printf.sprintf "DIG %d" n)
      | MIdug n -> ppAlign (Printf.sprintf "DUG %d" n)
      | MIdropn n -> ppAlign (Printf.sprintf "DROP %d" n)
      | MIdup 1 -> ppAlign "DUP"
      | MIunpair [true; true] -> ppAlign "UNPAIR"
      | MIsapling_empty_state n ->
          ppAlign (Printf.sprintf "SAPLING_EMPTY_STATE %d" n)
      | MIdup n -> ppAlign (Printf.sprintf "DUP %d" n)
      | MIunpair n -> ppAlign (Printf.sprintf "UNPAIR %d" (unpair_size n))
      | MIgetn n -> ppAlign (Printf.sprintf "GET %d" n)
      | MIupdaten n -> ppAlign (Printf.sprintf "UPDATE %d" n)
      | MIerror error ->
          Printf.sprintf
            "%s%s"
            (get_indent ())
            (span "partialType" (Printf.sprintf "MIerror: %s" error))
      | MIempty_set t
       |MInil t
       |MInone t
       |MIcontract (None, t)
       |MIunpack t
       |MIcast (t, _) ->
          ppAlign
            (name_of_instr_exn inst.tinstr)
            ~app:(string_of_mtype ~protect:() ~html t)
      | MIempty_bigmap (k, v) | MIempty_map (k, v) ->
          ppAlign
            (name_of_instr_exn inst.tinstr)
            ~app:
              (Base.String.concat
                 ~sep:" "
                 [ string_of_mtype ~protect:() ~html k
                 ; string_of_mtype ~protect:() ~html v ])
      | MIleft (a1, a2, t) | MIright (a1, a2, t) ->
          ppAlign
            (Base.String.concat
               ~sep:" "
               (name_of_instr_exn inst.tinstr :: two_field_annots (a1, a2)))
            ~app:(string_of_mtype ~protect:() ~html t)
      | MIpair (a1, a2) ->
          ppAlign
            (Base.String.concat
               ~sep:" "
               (name_of_instr_exn inst.tinstr :: two_field_annots (a1, a2)))
      | MIcontract (Some ep, t) ->
          ppAlign
            (Printf.sprintf
               "CONTRACT %%%s %s"
               ep
               (string_of_mtype ~protect:() ~html t))
      | MIpush (t, l) ->
          ppAlign
            (Printf.sprintf
               "PUSH %s %s"
               (string_of_mtype ~protect:() ~html t)
               (display_literal (string_of_instr (indent + 2)) l))
      | MIself (Some entrypoint) ->
          ppAlign (Printf.sprintf "SELF %%%s" entrypoint)
      | MIlambda (t1, t2, l) ->
          Printf.sprintf
            "%sLAMBDA\n%s  %s\n%s  %s\n%s;"
            (get_indent ())
            (get_indent ())
            (string_of_mtype ~protect:() ~html t1)
            (get_indent ())
            (string_of_mtype ~protect:() ~html t2)
            (string_of_instr ~sub_sequence:() (indent + 2) l)
      | MIcreate_contract {tparameter; tstorage; code} ->
          Printf.sprintf
            "%sCREATE_CONTRACT\n\
             %s { parameter %s;\n\
             %s   storage   %s;\n\
             %s   code\n\
             %s};"
            (get_indent ())
            (get_indent ())
            (string_of_mtype ~protect:() ~html tparameter)
            (get_indent ())
            (string_of_mtype ~protect:() ~html tstorage)
            (get_indent ())
            (string_of_instr ~sub_sequence:() (indent + 5) code)
      | ( MIdrop | MIfailwith | MIcons | MIsome | MIswap | MIunit
        | MIself None
        | MIself_address | MIexec | MIapply | MIaddress | MIimplicit_account
        | MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq | MIneq
        | MIle | MIlt | MIge | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv
        | MInot | MIand | MIor | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice
        | MIsize | MIget | MIupdate | MIget_and_update | MIsender | MIsource
        | MIamount | MIbalance | MInow | MIlevel | MImem | MIhash_key
        | MIblake2b | MIsha256 | MIsha512 | MIkeccak | MIsha3 | MIabs | MIneg
        | MIint | MIisnat | MIpack | MImich _ | MIfield _ | MIsetField _
        | MIchain_id | MIsapling_verify_update | MInever | MIticket
        | MIread_ticket | MIsplit_ticket | MIjoin_tickets | MIpairing_check
        | MItotal_voting_power | MIvoting_power ) as simple ->
          ppAlign (name_of_instr_exn simple)
    in
    let full =
      match inst.tinstr with
      | MIerror _ -> Some ()
      | _ when full_types_and_tags -> Some ()
      | _ -> None
    in
    if show_types && sub_sequence <> Some ()
    then
      match inst.stack with
      | Ok inst ->
          Printf.sprintf
            "%s %s %s"
            s
            (span "comment" "#")
            (span "stack" (Printf.sprintf "%s" (string_of_stack ?full inst)))
      | Error msg -> Printf.sprintf "%s # Error: %s" s (span "partialType" msg)
    else s
  in
  string_of_instr ?sub_sequence indent inst

let string_of_instr ~html ~show_types ?sub_sequence indent inst =
  string_of_instr
    ~html
    ~show_types
    ?sub_sequence
    indent
    (insert_subsequences inst)

let pretty_literal_f literal wrap ppf =
  let open Format in
  let wrap x = if wrap then Format.fprintf ppf "(%t)" x else x ppf in
  match (literal : _ literal_f) with
  | Int i -> fprintf ppf "%s" (Big_int.string_of_big_int i)
  | Unit -> fprintf ppf "Unit"
  | String s -> fprintf ppf "%S" s
  | Bool true -> fprintf ppf "True"
  | Bool false -> fprintf ppf "False"
  | Pair (l, r) -> wrap (fun ppf -> fprintf ppf "Pair %t %t" (l true) (r true))
  | None_ -> fprintf ppf "None"
  | Left x -> wrap (fun ppf -> fprintf ppf "Left %t" (x true))
  | Right x -> wrap (fun ppf -> fprintf ppf "Right %t" (x true))
  | Some_ x -> wrap (fun ppf -> fprintf ppf "Some %t" (x true))
  | Bytes s -> fprintf ppf "0x%s" (Misc.Hex.hexcape s)
  | Seq xs ->
      let f i x =
        let x = x false in
        let s = Format.asprintf "%t" x in
        if i = 0 || s = "" then fprintf ppf "%s" s else fprintf ppf "; %s" s
      in
      fprintf ppf "{";
      List.iteri f xs;
      fprintf ppf "}"
  | Elt (k, v) -> wrap (fun ppf -> fprintf ppf "Elt %t %t" (k true) (v true))
  | Instr i -> i true ppf
  | AnyMap xs ->
      let f i (k, v) =
        if i = 0
        then fprintf ppf "Elt %t %t" (k true) (v true)
        else fprintf ppf "; Elt %t %t" (k true) (v true)
      in
      fprintf ppf "{";
      List.iteri f xs;
      fprintf ppf "}"

let pretty_instr_f i wrap ppf =
  let open Format in
  let name ppf = fprintf ppf "%s" (name_of_instr_exn i) in
  let wrap x = if wrap then Format.fprintf ppf "{ %t }" x else x ppf in
  match i with
  | MIseq [] -> fprintf ppf "{}"
  | MIseq [x] -> fprintf ppf "{ %t }" (x false)
  | MIseq (x :: xs) ->
      let s = Format.asprintf "%t" (x false) in
      let no_space = ref (s <> "") in
      fprintf ppf "{ %s" s;
      List.iter
        (fun x ->
          let s = Format.asprintf "%t" (x false) in
          if s <> ""
          then begin
            if !no_space then fprintf ppf "; %s" s else fprintf ppf "%s" s;
            no_space := true
          end)
        xs;
      fprintf ppf " }"
  | MIdipn (n, i) -> wrap (fun ppf -> fprintf ppf "DIP %i %t" n (i true))
  | MIdip i | MIloop i | MIiter i | MImap i -> fprintf ppf "%t %t" name (i true)
  | MIif_left (i1, i2) | MIif_some (i1, i2) | MIif_cons (i1, i2) | MIif (i1, i2)
    ->
      wrap (fun ppf -> fprintf ppf "%t %t %t" name (i1 true) (i2 true))
  | MIdup 1 -> wrap (fun ppf -> fprintf ppf "%t" name)
  | MIunpair [true; true] -> wrap (fun ppf -> fprintf ppf "%t" name)
  | MIunpair n -> wrap (fun ppf -> fprintf ppf "%t %d" name (unpair_size n))
  | MIdig n
   |MIdug n
   |MIdropn n
   |MIgetn n
   |MIupdaten n
   |MIdup n
   |MIsapling_empty_state n ->
      wrap (fun ppf -> fprintf ppf "%t %d" name n)
  | MIcomment _xs -> wrap (fun _ppf -> ())
  | MIerror msg -> wrap (fun ppf -> fprintf ppf "ERROR %s" msg)
  | MInil t
   |MIempty_set t
   |MInone t
   |MIcontract (None, t)
   |MIunpack t
   |MIcast (t, _)
   |MIleft (_, _, t)
   |MIright (_, _, t) ->
      wrap (fun ppf ->
          fprintf ppf "%t %s" name (string_of_mtype ~protect:() ~html:false t))
  | MIempty_bigmap (k, v) | MIempty_map (k, v) ->
      wrap (fun ppf ->
          fprintf
            ppf
            "%t %s %s"
            name
            (string_of_mtype ~protect:() ~html:false k)
            (string_of_mtype ~protect:() ~html:false v))
  | MIcontract (Some ep, t) ->
      wrap (fun ppf ->
          fprintf
            ppf
            "CONTRACT %%%s %s"
            ep
            (string_of_mtype ~protect:() ~html:false t))
  | MIlambda (t1, t2, c) ->
      wrap (fun ppf ->
          let t1 = string_of_mtype ~protect:() ~html:false t1 in
          let t2 = string_of_mtype ~protect:() ~html:false t2 in
          fprintf ppf "LAMBDA %s %s %t" t1 t2 (c true))
  | MIpush (t, l) ->
      wrap (fun ppf ->
          let t = string_of_mtype ~protect:() ~html:false t in
          let l = l true in
          fprintf ppf "PUSH %s %t" t l)
  | MIcreate_contract {tparameter; tstorage; code} ->
      wrap (fun ppf ->
          fprintf
            ppf
            "CREATE_CONTRACT { parameter %s; storage %s; code %t};"
            (string_of_mtype ~protect:() ~html:false tparameter)
            (string_of_mtype ~protect:() ~html:false tstorage)
            (code true))
  | MIdrop | MIfailwith | MIcons | MIsome | MIswap | MIunit | MIself _
   |MIself_address | MIexec | MIapply | MIaddress | MIimplicit_account
   |MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq | MIneq
   |MIle | MIlt | MIge | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv
   |MInot | MIand | MIor | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice
   |MIsize | MIget | MIupdate | MIget_and_update | MIsender | MIsource
   |MIamount | MIbalance | MInow | MIlevel | MImem | MIhash_key | MIblake2b
   |MIsha256 | MIsha512 | MIkeccak | MIsha3 | MIabs | MIneg | MIint | MIisnat
   |MIpack | MImich _ | MIfield _ | MIsetField _ | MIchain_id
   |MIsapling_verify_update | MInever | MIpair _ | MIticket | MIread_ticket
   |MIsplit_ticket | MIjoin_tickets | MIpairing_check | MItotal_voting_power
   |MIvoting_power ->
      wrap (fun ppf -> fprintf ppf "%t" name)

let size_instr, _size_literal =
  let f_instr = fold_instr_f ( + ) ( + ) 1 in
  let f_literal = fold_literal_f ( + ) ( + ) 1 in
  cata {f_instr; f_literal}

let pretty_alg = {f_instr = pretty_instr_f; f_literal = pretty_literal_f}

let _pretty_instr = cata_instr pretty_alg

let pretty_literal = cata_literal pretty_alg

let string_of_literal m = Format.asprintf "%t" (pretty_literal m true)

module Of_micheline = struct
  open Micheline

  let rec mtype x =
    match mtype_annotated x with
    | mt, None -> mt
    | _, Some _ -> assert false

  and mtype_annotated = function
    | Basics.Primitive {name; annotations; arguments} as p ->
        let mt =
          match (name, arguments) with
          | "pair", [t1; t2] ->
              let fst, annot1 = mtype_annotated t1 in
              let snd, annot2 = mtype_annotated t2 in
              mt_pair ?annot1 ?annot2 fst snd
          | "pair", l ->
            begin
              match List.rev_map mtype_annotated l with
              | [] -> assert false
              | (last, annot) :: rest ->
                  fst
                    (List.fold_left
                       (fun (snd, annot2) (fst, annot1) ->
                         (mt_pair ?annot1 ?annot2 fst snd, None))
                       (last, annot)
                       rest)
            end
          | "or", [t1; t2] ->
              let left, annot1 = mtype_annotated t1 in
              let right, annot2 = mtype_annotated t2 in
              mt_or ?annot1 ?annot2 left right
          | "unit", [] -> mt_unit
          | "bool", [] -> mt_bool
          | "mutez", [] -> mt_mutez
          | "timestamp", [] -> mt_timestamp
          | "nat", [] -> mt_nat
          | "int", [] -> mt_int
          | "string", [] -> mt_string
          | "key", [] -> mt_key
          | "signature", [] -> mt_signature
          | "bytes", [] -> mt_bytes
          | "chain_id", [] -> mt_chain_id
          | "key_hash", [] -> mt_key_hash
          | "baker_hash", [] -> mt_baker_hash
          | "contract", [t] -> mt_contract (mtype t)
          | "address", [] -> mt_address
          | "list", [t] -> mt_list (mtype t)
          | "option", [t] -> mt_option (mtype t)
          | "set", [t] -> mt_set (mtype t)
          | "map", [t1; t2] -> mt_map (mtype t1) (mtype t2)
          | "big_map", [t1; t2] -> mt_big_map (mtype t1) (mtype t2)
          | "lambda", [t1; t2] -> mt_lambda (mtype t1) (mtype t2)
          | "operation", [] -> mt_operation
          | "sapling_state", [Int memo] -> mt_sapling_state (int_of_string memo)
          | "sapling_transaction", [Int memo] ->
              mt_sapling_transaction (int_of_string memo)
          | "never", [] -> mt_never
          | "ticket", [t] -> mt_ticket (mtype t)
          | "bls12_381_g1", [] -> mt_bls12_381_g1
          | "bls12_381_g2", [] -> mt_bls12_381_g2
          | "bls12_381_fr", [] -> mt_bls12_381_fr
          | _ ->
              mk_mtype
                (MTmissing (Printf.sprintf "Parse type error %S" (pretty "" p)))
        in
        List.fold_left
          (fun (mt, fa) a ->
            let update = function
              | Some _ -> failwith "duplicate annotation"
              | None -> Some (String.sub a 1 (String.length a - 1))
            in
            match a.[0] with
            | ':' -> ({mt with annot_type = update mt.annot_type}, fa)
            | '@' -> ({mt with annot_variable = update mt.annot_variable}, fa)
            | '%' -> (mt, update fa)
            | _ -> failwith "cannot parse annotation")
          (mt, None)
          annotations
    | p -> failwith ("Parse type error " ^ pretty "" p)

  let rec literal x : literal =
    match (x : Basics.micheline) with
    | Int i -> MLiteral.int (Big_int.big_int_of_string i)
    | Bytes s -> MLiteral.bytes s
    | String s -> MLiteral.string s
    | Primitive {name; annotations = _; arguments} ->
      ( match (name, arguments) with
      | "Unit", [] -> MLiteral.unit
      | "False", [] -> MLiteral.bool false
      | "True", [] -> MLiteral.bool true
      | "Pair", l ->
        begin
          match List.rev l with
          | [] -> assert false
          | a :: l ->
              List.fold_left
                (fun x y -> MLiteral.pair (literal y) x)
                (literal a)
                l
        end
      | "None", [] -> MLiteral.none
      | "Some", [x] -> MLiteral.some (literal x)
      | "Left", [x] -> MLiteral.left (literal x)
      | "Right", [x] -> MLiteral.right (literal x)
      | "Elt", [k; v] -> MLiteral.elt (literal k) (literal v)
      | _ -> MLiteral.instr (instruction x) )
    | Sequence xs -> MLiteral.seq (List.map literal xs)

  and instruction x =
    let err () =
      MIerror
        (Printf.sprintf "Cannot parse instruction %S" (Basics.show_micheline x))
    in
    let cmp instr = MIseq [{instr = MIcompare}; {instr}] in
    let fail =
      MIseq [{instr = MIpush (mt_unit, MLiteral.unit)}; {instr = MIfailwith}]
    in
    let if_op instr x y =
      MIseq [{instr}; {instr = MIif (instruction x, instruction y)}]
    in
    let ifcmp_op instr x y =
      MIseq
        [ {instr = MIcompare}
        ; {instr}
        ; {instr = MIif (instruction x, instruction y)} ]
    in
    let assert_op instr =
      MIseq [{instr}; {instr = MIif ({instr = MIseq []}, {instr = fail})}]
    in
    let assert_cmp_op instr =
      MIseq
        [ {instr = MIcompare}
        ; {instr}
        ; {instr = MIif ({instr = MIseq []}, {instr = fail})} ]
    in
    let parse_simple_macro = function
      | "FAIL" -> fail
      | "ASSERT" -> MIif ({instr = MIseq []}, {instr = fail})
      | "CMPEQ" -> cmp MIeq
      | "CMPNEQ" -> cmp MIeq
      | "CMPLT" -> cmp MIlt
      | "CMPGT" -> cmp MIgt
      | "CMPLE" -> cmp MIle
      | "CMPGE" -> cmp MIge
      | "ASSERTEQ" -> assert_op MIeq
      | "ASSERTNEQ" -> assert_op MIeq
      | "ASSERTLT" -> assert_op MIlt
      | "ASSERTGT" -> assert_op MIgt
      | "ASSERTLE" -> assert_op MIle
      | "ASSERTGE" -> assert_op MIge
      | "ASSERT_CMPEQ" -> assert_cmp_op MIeq
      | "ASSERT_CMPNEQ" -> assert_cmp_op MIeq
      | "ASSERT_CMPLT" -> assert_cmp_op MIlt
      | "ASSERT_CMPGT" -> assert_cmp_op MIgt
      | "ASSERT_CMPLE" -> assert_cmp_op MIle
      | "ASSERT_CMPGE" -> assert_cmp_op MIge
      | "ASSERT_NONE" -> MIif_some ({instr = fail}, {instr = MIseq []})
      | "ASSERT_SOME" -> MIif_some ({instr = MIseq []}, {instr = fail})
      | prim
        when String.index_opt prim 'D' = Some 0
             && String.index_opt prim 'P' = Some (String.length prim - 1)
             && 2 < String.length prim
             && Base.String.count prim ~f:(fun c -> c = 'U')
                = String.length prim - 2 ->
          let n = String.length prim - 3 in
          MIseq [{instr = MIdig n}; {instr = MIdup 1}; {instr = MIdug (n + 1)}]
      | prim
        when String.index_opt prim 'C' = Some 0
             && String.index_opt prim 'R' = Some (String.length prim - 1) ->
          let l =
            Base.String.fold
              (String.sub prim 1 (String.length prim - 2))
              ~init:(Some [])
              ~f:(fun acc c ->
                match (acc, c) with
                | Some acc, 'A' -> Some (A :: acc)
                | Some acc, 'D' -> Some (D :: acc)
                | _ -> None)
          in
          ( match l with
          | Some l -> MIfield (List.rev l)
          | None -> err () )
      | _ -> err ()
    in
    let instr =
      match x with
      | Sequence [x] -> (instruction x).instr
      | Sequence xs -> MIseq (List.map instruction xs)
      | Primitive {name; annotations; arguments} ->
        ( match (name, arguments) with
        | "RENAME", _ -> MIseq [] (* TODO *)
        | "UNIT", [] -> MIunit
        | "EMPTY_MAP", [k; v] ->
            MIpush (mt_map (mtype k) (mtype v), MLiteral.mk_map [])
        | "EMPTY_SET", [t] -> MIpush (mt_set (mtype t), MLiteral.set [])
        | "EMPTY_BIG_MAP", [k; v] -> MIempty_bigmap (mtype k, mtype v)
        | "DIP", [x] -> MIdip (instruction x)
        | "DIP", [Int i; x] -> MIdipn (int_of_string i, instruction x)
        | prim, [x]
          when String.index_opt prim 'D' = Some 0
               && String.index_opt prim 'P' = Some (String.length prim - 1)
               && 2 < String.length prim
               && Base.String.count prim ~f:(fun c -> c = 'I')
                  = String.length prim - 2 ->
            MIdipn (String.length prim - 2, instruction x)
        | "LOOP", [x] -> MIloop (instruction x)
        | "ITER", [x] -> MIiter (instruction x)
        | "MAP", [x] -> MImap (instruction x)
        | "DROP", [] -> MIdrop
        | "DROP", [Int n] -> MIdropn (int_of_string n)
        | "DUP", [] -> MIdup 1
        | "DUP", [Int n] -> MIdup (int_of_string n)
        | "DIG", [Int i] -> MIdig (int_of_string i)
        | "DUG", [Int i] -> MIdug (int_of_string i)
        | "FAILWITH", [] -> MIfailwith
        | "IF", [x; y] -> MIif (instruction x, instruction y)
        | "IF_LEFT", [x; y] -> MIif_left (instruction x, instruction y)
        | "IF_RIGHT", [x; y] -> MIif_left (instruction y, instruction x)
        | "IF_SOME", [x; y] -> MIif_some (instruction x, instruction y)
        | "IF_NONE", [x; y] -> MIif_some (instruction y, instruction x)
        | "IF_CONS", [x; y] -> MIif_cons (instruction x, instruction y)
        | "NIL", [t] -> MInil (mtype t)
        | "CONS", [] -> MIcons
        | "NONE", [t] -> MInone (mtype t)
        | "SOME", [] -> MIsome
        | "PAIR", [] -> MIpair (None, None)
        | "LEFT", [t] -> MIleft (None, None, mtype t)
        | "RIGHT", [t] -> MIright (None, None, mtype t)
        | "PUSH", [t; l] -> MIpush (mtype t, literal l)
        | "SWAP", [] -> MIswap
        | "UNPAIR", [] -> MIunpair [true; true]
        | "UNPAIR", [Int n] -> MIunpair (List.replicate (int_of_string n) true)
        | "CAR", [] -> MIfield [A]
        | "CDR", [] -> MIfield [D]
        | "CONTRACT", [t] ->
            let entry_point =
              match annotations with
              | [] -> None
              | entry_point :: _ ->
                  Base.String.chop_prefix ~prefix:"%" entry_point
            in
            MIcontract (entry_point, mtype t)
        | "CAST", [t] ->
            let t = mtype t in
            MIcast (t, t)
        | "EXEC", [] -> MIexec
        | "APPLY", [] -> MIapply
        | "LAMBDA", [t; u; x] -> MIlambda (mtype t, mtype u, instruction x)
        | "CREATE_CONTRACT", [x] ->
            let tparameter, tstorage, code =
              if false then failwith (Basics.show_micheline x);
              match x with
              | Sequence
                  [ Primitive {name = "parameter"; arguments = [tparameter]}
                  ; Primitive {name = "storage"; arguments = [tstorage]}
                  ; Primitive {name = "code"; arguments = [code]} ] ->
                  (mtype tparameter, mtype tstorage, instruction code)
              | _ -> assert false
            in
            MIcreate_contract {tparameter; tstorage; code}
        | "SELF", [] ->
            let entry_point =
              match annotations with
              | [] -> None
              | entry_point :: _ ->
                  Base.String.chop_prefix ~prefix:"%" entry_point
            in
            MIself entry_point
        | "ADDRESS", [] -> MIaddress
        | "SELF_ADDRESS", [] -> MIself_address
        | "IMPLICIT_ACCOUNT", [] -> MIimplicit_account
        | "TRANSFER_TOKENS", [] -> MItransfer_tokens
        | "CHECK_SIGNATURE", [] -> MIcheck_signature
        | "SET_DELEGATE", [] -> MIset_delegate
        | "SAPLING_EMPTY_STATE", [Int memo] ->
            MIsapling_empty_state (int_of_string memo)
        | "SAPLING_VERIFY_UPDATE", [] -> MIsapling_verify_update
        | "NEVER", [] -> MInever
        | "READ_TICKET", [] -> MIread_ticket
        | "TICKET", [] -> MIticket
        | "SPLIT_TICKET", [] -> MIsplit_ticket
        | "JOIN_TICKETS", [] -> MIjoin_tickets
        | "PAIRING_CHECK", [] -> MIpairing_check
        | "TOTAL_VOTING_POWER", [] -> MItotal_voting_power
        | "VOTING_POWER", [] -> MIvoting_power
        | "EQ", [] -> MIeq
        | "NEQ", [] -> MIneq
        | "LE", [] -> MIle
        | "LT", [] -> MIlt
        | "GE", [] -> MIge
        | "GT", [] -> MIgt
        | "COMPARE", [] -> MIcompare
        | "MUL", [] -> MImul
        | "ADD", [] -> MIadd
        | "SUB", [] -> MIsub
        | "EDIV", [] -> MIediv
        | "NOT", [] -> MInot
        | "AND", [] -> MIand
        | "OR", [] -> MIor
        | "LSL", [] -> MIlsl
        | "LSR", [] -> MIlsr
        | "XOR", [] -> MIxor
        | "CONCAT", [] -> MIconcat {arity = None}
        | "SLICE", [] -> MIslice
        | "SIZE", [] -> MIsize
        | "GET", [] -> MIget
        | "GET", [Int x] -> MIgetn (int_of_string x)
        | "UPDATE", [] -> MIupdate
        | "UPDATE", [Int x] -> MIupdaten (int_of_string x)
        | "GET_AND_UPDATE", [] -> MIget_and_update
        | "SENDER", [] -> MIsender
        | "SOURCE", [] -> MIsource
        | "AMOUNT", [] -> MIamount
        | "BALANCE", [] -> MIbalance
        | "NOW", [] -> MInow
        | "LEVEL", [] -> MIlevel
        | "CHAIN_ID", [] -> MIchain_id
        | "MEM", [] -> MImem
        | "HASH_KEY", [] -> MIhash_key
        | "BLAKE2B", [] -> MIblake2b
        | "SHA256", [] -> MIsha256
        | "SHA512", [] -> MIsha512
        | "KECCAK", [] -> MIkeccak
        | "SHA3", [] -> MIsha3
        | "ABS", [] -> MIabs
        | "NEG", [] -> MIneg
        | "INT", [] -> MIint
        | "ISNAT", [] -> MIisnat
        | "PACK", [] -> MIpack
        | "UNPACK", [t] -> MIunpack (mtype t)
        | prim, [] -> parse_simple_macro prim
        | "IFEQ", [x; y] -> if_op MIeq x y
        | "IFNEQ", [x; y] -> if_op MIeq x y
        | "IFLT", [x; y] -> if_op MIlt x y
        | "IFGT", [x; y] -> if_op MIgt x y
        | "IFLE", [x; y] -> if_op MIle x y
        | "IFGE", [x; y] -> if_op MIge x y
        | "IFCMPEQ", [x; y] -> ifcmp_op MIeq x y
        | "IFCMPNEQ", [x; y] -> ifcmp_op MIeq x y
        | "IFCMPLT", [x; y] -> ifcmp_op MIlt x y
        | "IFCMPGT", [x; y] -> ifcmp_op MIgt x y
        | "IFCMPLE", [x; y] -> ifcmp_op MIle x y
        | "IFCMPGE", [x; y] -> ifcmp_op MIge x y
        (* TODO Macros: ASSERT_SOME, ASSERT_LEFT, ASSERT_RIGHT *)
        | _ -> err () )
      | _ -> err ()
    in
    {instr}
end

module To_micheline = struct
  open Micheline

  let mtype =
    cata_mtype (fun ?annot_type ?annot_variable t ->
        let annotations =
          let get pref = function
            | None -> None
            | Some s -> Some (pref ^ s)
          in
          Base.List.filter_opt [get ":" annot_type; get "@" annot_variable]
        in
        let prim = primitive ~annotations in
        let insert_field_annot a e =
          match a with
          | None | Some "" -> e
          | Some a ->
              let a = "%" ^ a in
              ( match e with
              | Basics.Primitive {name; annotations; arguments} ->
                  Primitive {name; annotations = a :: annotations; arguments}
              | _ -> assert false )
        in
        match t with
        | MT0 (T_sapling_state {memo}) ->
            prim "sapling_state" [int (string_of_int memo)]
        | MT0 (T_sapling_transaction {memo}) ->
            prim "sapling_transaction" [int (string_of_int memo)]
        | MT0 t -> prim (string_of_type0 t) []
        | MT1 (t, t1) -> prim (string_of_type1 t) [t1]
        | MT2 (t, t1, t2) -> prim (string_of_type2 t) [t1; t2]
        | MTpair {annot1; annot2; fst; snd} ->
            prim
              "pair"
              [insert_field_annot annot1 fst; insert_field_annot annot2 snd]
        | MTor {annot1; annot2; left; right} ->
            prim
              "or"
              [insert_field_annot annot1 left; insert_field_annot annot2 right]
        | MTmissing msg ->
            primitive
              "ERROR"
              [Format.kasprintf string "Cannot compile missing type: %s" msg])

  let dip_seq i = primitive "DIP" [sequence i]

  let rec c_ad_r s =
    (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > CA(\rest=[AD]+)R / S  =>  CAR ; C(\rest)R / S
         > CD(\rest=[AD]+)R / S  =>  CDR ; C(\rest)R / S
      *)
    match s.[0] with
    | 'A' -> primitive "CAR" [] :: c_ad_r (Base.String.drop_prefix s 1)
    | 'D' -> primitive "CDR" [] :: c_ad_r (Base.String.drop_prefix s 1)
    | exception _ -> []
    | other ->
        Format.kasprintf
          failwith
          "c_ad_r macro: wrong char: '%c' (of %S)"
          other
          s

  let rec set_c_ad_r s =
    (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > SET_CA(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CAR ; SET_C(\rest)R } ; CDR ; SWAP ; PAIR } / S
         > SET_CD(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CDR ; SET_C(\rest)R } ; CAR ; PAIR } / S
         Then,
         > SET_CAR  =>  CDR ; SWAP ; PAIR
         > SET_CDR  =>  CAR ; PAIR
      *)
    match s.[0] with
    | 'A' when String.length s > 1 ->
        [ primitive "DUP" []
        ; dip_seq
            (primitive "CAR" [] :: set_c_ad_r (Base.String.drop_prefix s 1))
        ; primitive "CDR" []
        ; primitive "SWAP" []
        ; primitive "PAIR" [] ]
    | 'A' -> [primitive "CDR" []; primitive "SWAP" []; primitive "PAIR" []]
    | 'D' when String.length s > 1 ->
        [ primitive "DUP" []
        ; dip_seq
            (primitive "CDR" [] :: set_c_ad_r (Base.String.drop_prefix s 1))
        ; primitive "CAR" []
        ; primitive "PAIR" [] ]
    | 'D' -> [primitive "CAR" []; primitive "PAIR" []]
    | exception _ ->
        Format.kasprintf failwith "set_c_r_macro: called with no chars: S" s
    | other ->
        Format.kasprintf
          failwith
          "set_c_r_macro: wrong char: '%c' (of %S)"
          other
          s

  let rec literal {literal = l} =
    match l with
    | Int bi -> int (Big_int.string_of_big_int bi)
    | Bool false -> primitive "False" []
    | Bool true -> primitive "True" []
    | String s -> string s
    | Unit -> primitive "Unit" []
    | Bytes b -> bytes b
    | Pair (left, right) -> primitive "Pair" [literal left; literal right]
    | None_ -> primitive "None" []
    | Some_ l -> primitive "Some" [literal l]
    | Left e -> primitive "Left" [literal e]
    | Right e -> primitive "Right" [literal e]
    | Seq xs -> xs |> Base.List.map ~f:literal |> sequence
    | Elt (k, v) -> primitive "Elt" [literal k; literal v]
    | Instr x -> sequence (instruction x)
    | AnyMap xs ->
        let xs = List.map (fun (k, v) -> {literal = Elt (k, v)}) xs in
        literal {literal = Seq xs}

  and instruction (the_instruction : instr) =
    let prim0 ?annotations n = [primitive ?annotations n []] in
    let primn ?annotations n l = [primitive ?annotations n l] in
    let rec_instruction instr = Micheline.sequence (instruction instr) in
    match the_instruction.instr with
    | MIerror s -> primn "ERROR" [string s]
    | MIcomment _comment ->
        []
        (*
          [ primitive "PUSH" [primitive "string" []; string comment]
          ; primitive "DROP" [] ] *)
    | MIdip instr -> primn "DIP" [rec_instruction instr]
    | MIdipn (n, instr) ->
        primn "DIP" [int (Base.Int.to_string n); rec_instruction instr]
    | MIdig n -> primn "DIG" [int (Base.Int.to_string n)]
    | MIdug n -> primn "DUG" [int (Base.Int.to_string n)]
    | MIdup 1 -> primn "DUP" []
    | MIdup n -> primn "DUP" [int (Base.Int.to_string n)]
    | MIunpair [true; true] -> primn "UNPAIR" []
    | MIunpair n -> primn "UNPAIR" [int (Base.Int.to_string (unpair_size n))]
    | MIgetn n -> primn "GET" [int (Base.Int.to_string n)]
    | MIupdaten n -> primn "UPDATE" [int (Base.Int.to_string n)]
    | MIdropn n -> primn "DROP" [int (Base.Int.to_string n)]
    | MIloop instr -> primn "LOOP" [rec_instruction instr]
    | MIiter instr -> primn "ITER" [rec_instruction instr]
    | MImap instr -> primn "MAP" [rec_instruction instr]
    | MIseq ils -> Base.List.concat_map ils ~f:instruction
    | MIif (t, e) -> primn "IF" [rec_instruction t; rec_instruction e]
    | MIif_left (t, e) -> primn "IF_LEFT" [rec_instruction t; rec_instruction e]
    | MIif_some (t, e) -> primn "IF_NONE" [rec_instruction e; rec_instruction t]
    | MIif_cons (t, e) -> primn "IF_CONS" [rec_instruction t; rec_instruction e]
    | MIpush (mt, lit) -> primn "PUSH" [mtype mt; literal lit]
    | MIself (Some entry_point) ->
        primn ~annotations:["%" ^ entry_point] "SELF" []
    | MIpair (a1, a2) ->
        primn ~annotations:(two_field_annots (a1, a2)) "PAIR" []
    | MIright (a1, a2, mty) ->
        primn ~annotations:(two_field_annots (a1, a2)) "RIGHT" [mtype mty]
    | MIleft (a1, a2, mty) ->
        primn ~annotations:(two_field_annots (a1, a2)) "LEFT" [mtype mty]
    | MInone mty -> primn "NONE" [mtype mty]
    | MInil mty -> primn "NIL" [mtype mty]
    | MIempty_set mty -> primn "EMPTY_SET" [mtype mty]
    | MIempty_map (k, v) -> primn "EMPTY_MAP" [mtype k; mtype v]
    | MIempty_bigmap (k, v) -> primn "EMPTY_BIG_MAP" [mtype k; mtype v]
    | MIcontract (None, mty) -> primn "CONTRACT" [mtype mty]
    | MIcontract (Some entry_point, mty) ->
        primn ~annotations:["%" ^ entry_point] "CONTRACT" [mtype mty]
    | MIcast (t, _) -> primn "CAST" [mtype t]
    | MIlambda (t1, t2, b) ->
        primn "LAMBDA" [mtype t1; mtype t2; rec_instruction b]
    | MIunpack mty -> primn "UNPACK" [mtype mty]
    | MIfield op -> c_ad_r (string_of_ad_path op)
    | MIsetField op -> set_c_ad_r (string_of_ad_path op)
    | MIcreate_contract {tparameter; tstorage; code} ->
        primn
          "CREATE_CONTRACT"
          [ sequence
              ( primn "parameter" [mtype tparameter]
              @ primn "storage" [mtype tstorage]
              @ primn "code" [rec_instruction code] ) ]
    | MIsapling_empty_state memo ->
        primn "SAPLING_EMPTY_STATE" [int (string_of_int memo)]
    | ( MIself None
      | MIself_address | MIexec | MIapply | MIaddress | MIimplicit_account
      | MIcons | MIsome | MItransfer_tokens | MIcheck_signature | MIset_delegate
      | MIeq | MIfailwith | MIdrop | MIneq | MIle | MIlt | MIge | MIgt | MIswap
      | MIunit | MIcompare | MImul | MIadd | MIsub | MIediv | MInot | MIand
      | MIor | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice | MIsize | MIget
      | MIupdate | MIget_and_update | MIsource | MIsender | MIamount | MIbalance
      | MInow | MIlevel | MImem | MIpack | MIhash_key | MIblake2b | MIsha256
      | MIsha512 | MIkeccak | MIsha3 | MIabs | MIneg | MIint | MIisnat
      | MImich _ | MIchain_id | MIsapling_verify_update | MInever | MIticket
      | MIread_ticket | MIsplit_ticket | MIjoin_tickets | MIpairing_check
      | MItotal_voting_power | MIvoting_power ) as simple ->
      ( try prim0 (name_of_instr_exn simple) with
      | _ -> [sequence [primitive "ERROR-NOT-SIMPLE" []]] )
end

let count_bigmaps =
  let f_tinstr ~stack:_ _instr = 0 in
  let f_tliteral ~t literal =
    let i =
      match t with
      | Ok {mt = MT2 (T_big_map, _, _)} -> 1
      | _ -> 0
    in
    fold_literal_f ( + ) ( + ) i literal
  in
  cata_tliteral {f_tinstr; f_tliteral}

type contract =
  { tparameter : mtype
  ; tstorage : mtype
  ; code : instr
  ; lazy_entry_points : literal option
  ; storage : literal option }
[@@deriving show {with_path = false}]

type tcontract =
  { tparameter : mtype
  ; tstorage : mtype
  ; code : tinstr
  ; lazy_entry_points : tliteral option
  ; storage : tliteral option }
[@@deriving show {with_path = false}]

let unexpected_final_stack_error = "Unexpected final stack"

let erase_types_contract {tparameter; tstorage; code; lazy_entry_points; storage}
    =
  let lazy_entry_points = Option.map erase_types_literal lazy_entry_points in
  let code = erase_types_instr code in
  let storage = Option.map erase_types_literal storage in
  ({tparameter; tstorage; code; lazy_entry_points; storage} : contract)

let render_tinstr = string_of_instr ~html:true

let display_tinstr = string_of_instr ~html:false

let string_of_tliteral x = string_of_literal (erase_types_literal x)

let typecheck_contract
    ~strict_dup
    ({tparameter; tstorage; code; lazy_entry_points; storage} : contract) =
  let code =
    typecheck_instr
      ~strict_dup
      ~tparameter
      (initial_stack ~tparameter ~tstorage)
      code
  in
  let storage =
    Option.map (typecheck_literal ~strict_dup ~tparameter tstorage) storage
  in
  let code =
    match code.stack with
    | Ok (Stack_ok [{mt = MTpair {fst; snd}}])
      when unifiable_types fst (mt_list mt_operation)
           && unifiable_types snd tstorage ->
        code
    | Ok Stack_failed -> code
    | Ok _ ->
        let msg = unexpected_final_stack_error in
        let err = {tinstr = MIerror msg; stack = code.stack} in
        seq_snoc code err
    | Error _ -> code
  in
  let lazy_entry_points =
    match (tstorage.mt, lazy_entry_points) with
    | MTpair {snd}, Some x ->
        Some (typecheck_literal ~strict_dup ~tparameter snd x)
    | _, Some _ -> failwith "lazy entry points, but storage not a pair"
    | _, None -> None
  in
  {tparameter; tstorage; code; lazy_entry_points; storage}

let has_error_tcontract ~accept_missings {tparameter; tstorage; code} =
  let errors =
    let e = has_error ~accept_missings code in
    if accept_missings
    then e
    else e @ has_missing_type tstorage @ has_missing_type tparameter
  in
  let rec clean acc = function
    | e1 :: e2 :: rest when e1 = e2 -> clean acc (e1 :: rest)
    | e :: rest -> clean (e :: acc) rest
    | [] -> List.rev acc
  in
  clean [] errors

let to_micheline_tcontract {tstorage; tparameter; code} =
  let open Micheline in
  let code =
    match To_micheline.instruction (erase_types_instr code) with
    | [Sequence _] as l -> l
    | l -> [sequence l]
  in
  sequence
    [ primitive "storage" [To_micheline.mtype tstorage]
    ; primitive "parameter" [To_micheline.mtype tparameter]
    ; primitive "code" code ]

let display_tcontract {tstorage; tparameter; code} =
  Printf.sprintf
    "parameter %s;\nstorage   %s;\ncode\n%s;"
    (string_of_mtype ~protect:() ~html:false tparameter)
    (string_of_mtype ~protect:() ~html:false tstorage)
    (display_tinstr ~sub_sequence:() ~show_types:true 2 code)

let render_tcontract {tstorage; tparameter; code} =
  Printf.sprintf
    "parameter %s;<br>storage &nbsp;&nbsp;%s;<br><div \
     class='michelsonLine'>code<br>%s;</div>"
    (string_of_mtype ~protect:() ~html:true tparameter)
    (string_of_mtype ~protect:() ~html:true tstorage)
    (render_tinstr ~sub_sequence:() ~show_types:true 2 code)

let render_tcontract_no_types {tstorage; tparameter; code} =
  Printf.sprintf
    "parameter %s;<br>storage &nbsp;&nbsp;%s;<br><div \
     class='michelsonLine'>code<br>%s;</div>"
    (string_of_mtype ~protect:() ~html:true tparameter)
    (string_of_mtype ~protect:() ~html:true tstorage)
    (render_tinstr ~sub_sequence:() ~show_types:false 2 code)

let profile_of_arity (m, n) = (m, Some (n - m))

let arity_of_profile = function
  | m, None -> (m, None)
  | m, Some d -> (m, Some (m + d))

let profile =
  let open Result in
  let ( =?= ) x y =
    match x with
    | Some x when x <> y -> error "profile: unequal p"
    | _ -> return ()
  in
  let if_some d x = Option.map (fun _ -> x) d in
  let same x y =
    match (x, y) with
    | Some x, Some y ->
        if x = y then return (Some x) else error "profile: unequal d"
    | None, Some y -> return (Some y)
    | Some x, None -> return (Some x)
    | None, None -> return None
  in
  let pred = Option.map (fun x -> x - 1) in
  let succ = Option.map (fun x -> x + 1) in
  let f_instr i =
    let* i = sequence_instr_f i in
    match i with
    | MIfailwith -> return (1, None)
    | MInever -> return (1, None)
    | MIdip (p, d) -> return (p + 1, d)
    | MIdipn (i, (p, d)) -> return (p + i, d)
    | MIloop (p, d) ->
        let* () = d =?= 1 in
        return (p + 1, if_some d (-1))
    | MIiter (p, d) ->
        let* () = d =?= -1 in
        return (p, Some (-1))
    | MImap (p, d) ->
        let* () = d =?= 0 in
        return (p, if_some d 0)
    | MIdig n | MIdug n -> return (n + 1, Some 0)
    | MIif ((p1, d1), (p2, d2)) ->
        let* d = same d1 d2 in
        return (max p1 p2 + 1, pred d)
    | MIif_some ((p1, d1), (p2, d2)) ->
        let* d = same d1 (pred d2) in
        return (max p1 (p2 + 1), d)
    | MIif_left ((p1, d1), (p2, d2)) ->
        let* d = same d1 d2 in
        return (max p1 p2, d)
    | MIif_cons ((p1, d1), (p2, d2)) ->
        let* d = same (succ d1) (pred d2) in
        return (max (p1 + 1) (p2 - 1), d)
    | MIseq xs ->
        let f = function
          | _, Error e -> Error e
          | (p1, None), _ -> return (p1, None)
          (* TODO Fix the compiler/rewriter such that they never emit
             instructions after FAILWITH and assert false here. *)
          | (p1, Some d1), Ok (p2, Some d2) ->
              return (max p1 (p2 - d1), Some (d1 + d2))
          | (p1, Some d1), Ok (p2, None) -> return (max p1 (p2 - d1), None)
        in
        List.fold_right (curry f) xs (return (0, Some 0))
    | MIcomment _ -> return (0, Some 0)
    | MIlambda _ -> return (0, Some 1)
    | MIconcat {arity = Some `Unary} -> return (1, Some 0)
    | MIconcat {arity = Some `Binary} -> return (2, Some (-1))
    | MIconcat {arity = None} -> failwith "profile: CONCAT arity undetermined"
    | MIerror _ -> return (0, Some 0)
    | i ->
      ( match spec_of_instr ~strict_dup:false i with
      | {arities = Some a} -> return (profile_of_arity a)
      | _ -> assert false )
  in
  cata_instr {f_instr; f_literal = (fun _ -> return ())}

let has_profile pr instr = Ok pr = profile {instr}

let has_arity a instr = has_profile (profile_of_arity a) instr

let arity instr = Result.map arity_of_profile (profile instr)

let rec mtype_examples t =
  match t.mt with
  | MT0 T_unit -> [MLiteral.unit]
  | MT0 T_bool -> [MLiteral.bool false; MLiteral.bool true]
  | MT0 T_nat ->
      [ MLiteral.small_int 0
      ; MLiteral.small_int 2
      ; MLiteral.small_int 4
      ; MLiteral.small_int 8 ]
  | MT0 T_int ->
      [ MLiteral.small_int (-2)
      ; MLiteral.small_int 10
      ; MLiteral.small_int 5
      ; MLiteral.small_int 3 ]
  | MT0 T_mutez ->
      [ MLiteral.small_int 0
      ; MLiteral.small_int 1000
      ; MLiteral.small_int 2000000
      ; MLiteral.small_int 3000000 ]
  | MT0 T_string ->
      [ MLiteral.string ""
      ; MLiteral.string "foo"
      ; MLiteral.string "bar"
      ; MLiteral.string "SmartPy" ]
  | MT0 T_chain_id
   |MT0 T_bytes
   |MT0 T_bls12_381_g1
   |MT0 T_bls12_381_g2
   |MT0 T_bls12_381_fr ->
      [ MLiteral.bytes ""
      ; MLiteral.bytes (Misc.Hex.unhex "00")
      ; MLiteral.bytes (Misc.Hex.unhex "010203")
      ; MLiteral.bytes (Misc.Hex.unhex "0FFF") ]
  | MT0 T_timestamp ->
      [ MLiteral.small_int 0
      ; MLiteral.small_int 1000
      ; MLiteral.small_int 2000000
      ; MLiteral.small_int 3000000 ]
  | MT0 T_address ->
      [ MLiteral.string "tz1..."
      ; MLiteral.string "tz2..."
      ; MLiteral.string "tz3..."
      ; MLiteral.string "KT1..." ]
  | MT0 T_key_hash ->
      [ MLiteral.string "tz1..."
      ; MLiteral.string "tz2..."
      ; MLiteral.string "tz3..." ]
  | MT0 T_baker_hash ->
      [ MLiteral.string "SG1a..."
      ; MLiteral.string "SG1b..."
      ; MLiteral.string "SG1c..." ]
  | MT0 T_signature -> [MLiteral.string "edsigt..."; MLiteral.string "edsigu..."]
  | MT0 T_key ->
      [ MLiteral.string "edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG"
      ; MLiteral.string "edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2"
      ]
  | MT1 (T_option, t) ->
      List.map MLiteral.some (mtype_examples t) @ [MLiteral.none]
  | MT1 (T_list, t) -> [MLiteral.seq (mtype_examples t); MLiteral.seq []]
  | MT1 (T_set, t) -> [MLiteral.seq (mtype_examples t)]
  | MT1 (T_contract, _t) ->
      [ MLiteral.string "KT1a..."
      ; MLiteral.string "KT1b..."
      ; MLiteral.string "KT1c..."
      ; MLiteral.string "KT1d..." ]
  | MTpair {fst; snd} ->
      let l1 = mtype_examples fst in
      let l2 = mtype_examples snd in
      begin
        match (l1, l2) with
        | a1 :: a2 :: _, b1 :: b2 :: _ ->
            [ MLiteral.pair a1 b1
            ; MLiteral.pair a2 b2
            ; MLiteral.pair a1 b2
            ; MLiteral.pair a2 b1 ]
        | _ ->
            List.fold_left
              (fun acc b ->
                List.fold_left (fun acc a -> MLiteral.pair a b :: acc) acc l1)
              []
              l2
      end
  | MTor {left; right} ->
      let l1 = mtype_examples left in
      let l2 = mtype_examples right in
      begin
        match (l1, l2) with
        | a1 :: a2 :: _, b1 :: b2 :: _ ->
            [ MLiteral.left a1
            ; MLiteral.left a2
            ; MLiteral.right b1
            ; MLiteral.right b2 ]
        | _ ->
            List.fold_left
              (fun acc b ->
                List.fold_left (fun acc a -> MLiteral.pair a b :: acc) acc l1)
              []
              l2
      end
  | MT2 (T_lambda, _, _) -> [MLiteral.seq []]
  | MT2 ((T_map | T_big_map), k, v) ->
      let l1 = mtype_examples k in
      let l2 = mtype_examples v in
      let rec map2 f acc l1 l2 =
        match (l1, l2) with
        | a1 :: l1, a2 :: l2 -> map2 f (f a1 a2 :: acc) l1 l2
        | _ -> List.rev acc
      in
      let l = map2 MLiteral.elt [] l1 l2 in
      [MLiteral.seq l]
  | MTmissing s -> [MLiteral.string (Printf.sprintf "no value for %S" s)]
  | MT0 T_operation -> [MLiteral.string "operation"]
  | MT0 (T_sapling_state _) -> [MLiteral.seq []]
  | MT0 T_never -> [MLiteral.string "no value in type never"]
  | MT0 (T_sapling_transaction _) -> [MLiteral.string "sapling_transaction"]
  | MT1 (T_ticket, _) -> [MLiteral.string "no example for tickets"]

let on_instrs f =
  let f_instr instr = f {instr} in
  let f_literal literal = {literal} in
  cata_literal {f_instr; f_literal}
