(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed

type t = tcommand

let build ~line_no c = {c; ct = Type.full_unknown (); line_no}

let verify ~line_no e message = build ~line_no (CVerify (e, message))

let whileLoop ~line_no e l = build ~line_no (CWhile (e, l))

let delItem ~line_no x y = build ~line_no (CDelItem (x, y))

let rec set ~line_no x y =
  match x.e with
  | EPrim0 (ELocal "__operations__") -> build ~line_no (CSetVar (x, y))
  | EPrim0 (ELocal "__storage__")
   |EItem _
   |EPrim0 (ELocal _)
   |EPrim1 (EAttr _, _) ->
    ( match y.e with
    | EPrim3
        ( EUpdate_map
        , map
        , key
        , {e = EPrim1 (EVariant "None", {e = EPrim0 (ECst Unit)})} )
      when erase_types_expr x = erase_types_expr map ->
        delItem ~line_no x key
    | EPrim3 (EUpdate_map, map, key, {e = EPrim1 (EVariant "Some", v)})
      when erase_types_expr x = erase_types_expr map ->
        set ~line_no (Expr.item ~line_no map key None None) v
    | _ -> build ~line_no (CSetVar (x, y)) )
  | _ ->
      raise
        (Basics.SmartExcept
           [ `Text "Syntax Error"
           ; `Br
           ; `Expr x
           ; `Text "is not a variable"
           ; `Line line_no ])

let defineLocal ~line_no name e = build ~line_no (CDefineLocal (name, e))

let rec bind ~line_no x c1 c2 =
  match c1.c with
  | CBind (y, c1a, c1b) -> bind ~line_no y c1a (bind ~line_no x c1b c2)
  | _ -> build ~line_no (CBind (x, c1, c2))

let result ~line_no x = build ~line_no (CResult x)

let rec seq ~line_no = function
  | [] -> result ~line_no (Expr.cst ~line_no Literal.unit)
  | [x] -> x
  | {c = CResult {e = EPrim0 (ECst Literal.Unit)}} :: xs -> seq ~line_no xs
  | x :: xs -> bind ~line_no None x (seq ~line_no xs)
