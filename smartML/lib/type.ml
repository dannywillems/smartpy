(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Config

let sort_row r =
  Base.List.sort r ~compare:(fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2)

(** Maps over a reference, creating a new one. *)
let map_ref f {contents} = {contents = f contents}

type 't f =
  | TUnit
  | TBool
  | TInt                of {isNat : bool Unknown.t_ref}
  | TTimestamp
  | TString
  | TBytes
  | TRecord             of
      { layout : Layout.t Unknown.t_ref
      ; row : (string * 't) list }
  | TVariant            of
      { layout : Layout.t Unknown.t_ref
      ; row : (string * 't) list }
  | TSet                of {telement : 't}
  | TMap                of
      { big : bool Unknown.t_ref
      ; tkey : 't
      ; tvalue : 't }
  | TAddress
  | TContract           of 't
  | TKeyHash
  | TBakerHash
  | TKey
  | TSignature
  | TToken
  | TUnknown            of 't unknownType_f ref [@equal ( == )]
  | TTuple              of 't list
  | TList               of 't
  | TLambda             of 't * 't
  | TChainId
  | TSecretKey
  | TOperation
  | TSaplingState       of {memo : int Unknown.t_ref}
  | TSaplingTransaction of {memo : int Unknown.t_ref}
  | TNever
  | TTicket             of 't
  | TBls12_381_g1
  | TBls12_381_g2
  | TBls12_381_fr
[@@deriving eq, map, fold, show {with_path = false}, ord]

and 't unknownType_f =
  | UUnknown of string
  | URecord  of (string * 't) list
  | UTuple   of (int * 't) list
  | UVariant of (string * 't) list
  | UExact   of 't
[@@deriving eq, map, fold, show {with_path = false}, ord]

module F = struct
  type 'a t = 'a f [@@deriving eq, ord, show {with_path = false}, map]
end

include Fix (F)
include FixEQ (F)
include FixORD (F)
include FixSHOW (F)
include FixFUNCTOR (F)

type unknownType = t unknownType_f

type tvariable = string * t [@@deriving eq, show {with_path = false}]

let default_layout l =
  let l = ref l in
  let rec layout n =
    if n = 1
    then (
      match !l with
      | [] -> assert false
      | a :: rest ->
          l := rest;
          Layout.leaf a a )
    else
      let n2 = n / 2 in
      let l1 = layout n2 in
      let l2 = layout (n - n2) in
      Binary_tree.Node (l1, l2)
  in
  let size = List.length !l in
  if size = 0 then failwith "default_layout of size 0";
  layout size

let default_layout_of_row r = default_layout (List.map fst r)

let rec comb_layout right_left = function
  | [] -> failwith "comb_layout"
  | [lbl] -> Layout.leaf lbl lbl
  | lbl :: xs ->
    ( match right_left with
    | `Right ->
        Binary_tree.node (Layout.leaf lbl lbl) (comb_layout right_left xs)
    | `Left ->
        Binary_tree.node (comb_layout right_left xs) (Layout.leaf lbl lbl) )

let comb_layout_of_row right_left r = comb_layout right_left (List.map fst r)

let build t = F t

let unknown_raw x = build (TUnknown x)

let full_unknown =
  let id = ref 0 in
  fun () ->
    let v = string_of_int !id in
    incr id;
    unknown_raw (ref (UUnknown v))

let rec getRepr t =
  match unF t with
  | TUnknown {contents = UExact t} -> getRepr t
  | u -> u

let unit = build TUnit

let address = build TAddress

let contract t = build (TContract t)

let bool = build TBool

let bytes = build TBytes

let variant layout row = build (TVariant {layout; row = sort_row row})

let variant_default_layout row =
  build
    (TVariant
       { layout = ref (Unknown.UnValue (default_layout_of_row row))
       ; row = sort_row row })

let key_hash = build TKeyHash

let baker_hash = build TBakerHash

let baker_type_of_protocol = function
  | Delphi | Edo -> key_hash
  | Florence -> baker_hash

let int_raw ~isNat = build (TInt {isNat})

let int () = int_raw ~isNat:(ref (Unknown.UnValue false))

let nat () = int_raw ~isNat:(ref (Unknown.UnValue true))

let intOrNat () = int_raw ~isNat:(ref (Unknown.UnUnknown ""))

let key = build TKey

let chain_id = build TChainId

let secret_key = build TSecretKey

let operation = build TOperation

let sapling_state memo = build (TSaplingState {memo = Unknown.of_option memo})

let sapling_transaction memo =
  build (TSaplingTransaction {memo = Unknown.of_option memo})

let never = build TNever

let map ~big ~tkey ~tvalue = build (TMap {big; tkey; tvalue})

let set ~telement = build (TSet {telement})

let record layout row = build (TRecord {layout; row = sort_row row})

let record_default_layout row =
  let layout = ref (Unknown.UnValue (default_layout_of_row row)) in
  build (TRecord {layout; row = sort_row row})

let record_or_unit layout = function
  | [] -> unit
  | l -> record layout l

let signature = build TSignature

let option t = variant_default_layout [("None", unit); ("Some", t)]

let key_value tkey tvalue =
  record_default_layout [("key", tkey); ("value", tvalue)]

let head_tail thead ttail =
  record_default_layout [("head", thead); ("tail", ttail)]

let tor t u = variant_default_layout [("Left", t); ("Right", u)]

let string = build TString

let timestamp = build TTimestamp

let token = build TToken

let uvariant name t = unknown_raw (ref (UVariant [(name, t)]))

let urecord fields =
  let cmp (x, _) (y, _) = Stdlib.compare x y in
  let fields = List.sort cmp fields in
  unknown_raw (ref (URecord fields))

let utuple i t = unknown_raw (ref (UTuple [(i, t)]))

let account =
  record_default_layout
    [ ("seed", string)
    ; ("address", address)
    ; ("public_key", key)
    ; ("public_key_hash", key_hash)
    ; ("secret_key", secret_key) ]

let pair t1 t2 = build (TTuple [t1; t2])

let tuple ts = build (TTuple ts)

let list t = build (TList t)

let ticket t = build (TTicket t)

let lambda t1 t2 = build (TLambda (t1, t2))

let bls12_381_g1 = build TBls12_381_g1

let bls12_381_g2 = build TBls12_381_g2

let bls12_381_fr = build TBls12_381_fr

let has_unknowns =
  cata (function
      | TUnknown _ -> true
      | x -> fold_f ( || ) false x)

let is_bigmap t =
  match getRepr t with
  | TMap {big} ->
    ( match Unknown.getRefOption big with
    | Some true -> true
    | _ -> false )
  | _ -> false

let is_hot =
  let open Ternary in
  cata (function
      | TTicket _ -> Yes
      | TLambda _ -> No
      | TContract _ -> No
      | TUnknown {contents = UUnknown _} -> Maybe
      | t -> fold_f or_ No t)
