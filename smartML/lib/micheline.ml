(* Copyright 2019-2021 Smart Chain Arena LLC. *)

module Smartml_literal = Literal
open Utils
open Utils.Misc
open Basics

type t = micheline

let rec to_json : _ -> Utils.Misc.json = function
  | Int i -> J_record [("int", J_string i)]
  | String s -> J_record [("string", J_string s)]
  | Bytes s -> J_record [("bytes", J_string (Misc.Hex.hexcape s))]
  | Primitive {name; annotations; arguments} ->
      let entries = [] in
      let entries =
        if annotations = []
        then entries
        else
          ("annots", J_list (List.map (fun x -> J_string x) annotations))
          :: entries
      in
      let entries =
        if arguments = []
        then entries
        else ("args", J_list (List.map to_json arguments)) :: entries
      in
      J_record (("prim", J_string name) :: entries)
  | Sequence xs -> J_list (List.map to_json xs)

let pp_as_json ?margin ?max_indent () ppf x =
  Misc.pp_json_as_json ?margin ?max_indent () ppf (to_json x)

let unAnnot = function
  | [] -> ""
  | annots -> Printf.sprintf "(%s)" (String.concat ", " annots)

let rec pretty indent = function
  | Int i -> Printf.sprintf "%s%s;" indent i
  | String s -> Printf.sprintf "%s'%s';" indent s
  | Bytes s -> Printf.sprintf "%s0x%s;" indent (Misc.Hex.hexcape s)
  | Primitive {name; annotations; arguments = []} ->
      Printf.sprintf "%s%s%s;" indent name (unAnnot annotations)
  | Primitive {name; annotations; arguments} ->
      Printf.sprintf
        "%s%s%s{\n%s\n%s};"
        indent
        name
        (unAnnot annotations)
        (String.concat
           "\n"
           (List.map (fun x -> pretty (indent ^ "  ") x) arguments))
        indent
  | Sequence [] -> Printf.sprintf "%sSeq{}" indent
  | Sequence l ->
      Printf.sprintf
        "%sSeq{\n%s\n%s};"
        indent
        (String.concat "\n" (List.map (pretty (indent ^ "  ")) l))
        indent

let left x = Primitive {name = "Left"; annotations = []; arguments = [x]}

let right x = Primitive {name = "Right"; annotations = []; arguments = [x]}

let annotName t = String.sub t 1 (String.length t - 1)

let extractAnnot default = function
  | n1 :: n2 :: _ ->
      if String.sub n1 0 1 = "%"
      then annotName n1
      else if String.sub n2 0 1 = "%"
      then annotName n2
      else annotName n1 ^ "_" ^ annotName n2
  | [n] -> annotName n
  | [] -> default

let identity x = failwith ("Not handled " ^ pretty "" x)

let error prefix x = Value.string ("Not handled " ^ prefix ^ " " ^ pretty "" x)

let unPAIRn = function
  (* "pair" is necessary (tzstats KT1GgUJwMQoFayRYNwamRAYCvHBLzgorLoGo) *)
  | Sequence (m1 :: rest)
   |Primitive {name = "Pair" | "pair"; arguments = m1 :: (_ :: _ :: _ as rest)}
    ->
      Primitive
        { name = "Pair"
        ; annotations = []
        ; arguments =
            [m1; Primitive {name = "Pair"; annotations = []; arguments = rest}]
        }
  | Primitive {name = "pair"; arguments} ->
      Primitive {name = "Pair"; annotations = []; arguments}
  | x -> x

let to_value =
  let rec parse_record (layout : Layout.t) row m =
    let rec aux layout m =
      match (layout : _ Binary_tree.t) with
      | Leaf Layout.{source = a} ->
          let t = List.assoc a row in
          [(a, to_value t m)]
      | Node (l1, l2) ->
        begin
          match unPAIRn m with
          | Primitive {name = "Pair"; arguments = [m1; m2]} ->
              let r1 = aux l1 m1 in
              let r2 = aux l2 m2 in
              r1 @ r2
          | _ ->
              Printf.ksprintf
                failwith
                "parse_record %s %s"
                (Layout.show layout)
                (pretty "" m)
        end
    in
    aux layout m
  and parse_variant (layout : Layout.t) row m =
    let rec aux layout m =
      match (layout, m) with
      | Binary_tree.Leaf Layout.{source = a}, m ->
          let t = List.assoc a row in
          (a, to_value t m)
      | Binary_tree.Node (l1, _l2), Primitive {name = "Left"; arguments = [m]}
        ->
          aux l1 m
      | Binary_tree.Node (_l1, l2), Primitive {name = "Right"; arguments = [m]}
        ->
          aux l2 m
      | _ ->
          Printf.ksprintf
            failwith
            "parse_variant %s %s"
            (Layout.show layout)
            (pretty "" m)
    in
    aux layout m
  and to_value t m =
    match (Type.getRepr t, m) with
    | TTuple [t1; t2], _ ->
      begin
        match unPAIRn m with
        | Primitive {name = "Pair"; arguments = [x1; x2]} ->
            Value.tuple [to_value t1 x1; to_value t2 x2]
        | _ ->
            Printf.ksprintf
              failwith
              "to_value tuple || %s || %s ||"
              (Printer.type_to_string t)
              (pretty "" m)
      end
    | ( TVariant {row = [("Left", t1); ("Right", _t2)]}
      , Primitive {name = "Left"; arguments = [x]} ) ->
        Value.variant "Left" (to_value t1 x) t
    | ( TVariant {row = [("Left", _t1); ("Right", t2)]}
      , Primitive {name = "Right"; arguments = [x]} ) ->
        Value.variant "Right" (to_value t2 x) t
    | ( TVariant {row = [("None", F TUnit); ("Some", t)]}
      , Primitive {name = "Some"; arguments = [x]} ) ->
        Value.some (to_value t x)
    | ( TVariant {row = [("None", F TUnit); ("Some", t)]}
      , Primitive {name = "None"; arguments = []} ) ->
        Value.none t
    | TVariant {layout = {contents = UnValue l}; row}, m ->
        let name, v = parse_variant l row m in
        Value.variant name v t
    | (TUnit | TRecord {row = []}), Primitive {name = "Unit"; arguments = []} ->
        Value.unit
    | TRecord {row = []}, _ -> Value.unit
    | TRecord {layout = {contents = UnValue l} as layout; row}, m ->
        let entries = parse_record l row m in
        Value.record ~layout entries
    | TBool, Primitive {name = "False"} -> Value.bool false
    | TBool, Primitive {name = "True"} -> Value.bool true
    | TToken, (Int i | String i) -> Value.mutez (Big_int.big_int_of_string i)
    | TTimestamp, (Int i | String i) ->
        let i =
          if Base.String.contains i 'Z' then SmartDom.parseDate i else i
        in
        Value.timestamp (Big_int.big_int_of_string i)
        (* TODO *)
    | TInt _, (Int i | String i) ->
        Value.intOrNat t (Big_int.big_int_of_string i)
    | TString, String i -> Value.string i
    | TKey, String i -> Value.key i
    | TKey, Bytes b -> Value.key (Utils.Bs58.encode_key (Misc.Hex.hexcape b))
    | TBls12_381_g1, String i -> Value.key i
    | TBls12_381_g2, String i -> Value.key i
    | TBls12_381_fr, String i -> Value.key i
    | TBls12_381_g1, Bytes i -> Value.bls12_381_g1 i
    | TBls12_381_g2, Bytes i -> Value.bls12_381_g2 i
    | TBls12_381_fr, Bytes i -> Value.bls12_381_fr i
    | TSignature, String i -> Value.signature i
    | TSignature, Bytes b ->
        Value.signature (Utils.Bs58.encode_signature (Misc.Hex.hexcape b))
    | TBytes, String i -> Value.bytes (Misc.Hex.unhex i)
    | TBytes, Bytes i -> Value.bytes i
    | TChainId, String i -> Value.chain_id (Misc.Hex.unhex i)
    | TChainId, Bytes i -> Value.chain_id i
    | TKeyHash, String i -> Value.key_hash i
    | TKeyHash, Bytes b ->
        Value.key_hash (Utils.Bs58.encode_key_hash (Misc.Hex.hexcape b))
    | TContract _, String i -> Value.address i
    | TContract _, Bytes b ->
        Value.address (Utils.Bs58.encode_address (Misc.Hex.hexcape b))
    | TAddress, String i -> Value.address i
    | TAddress, Bytes address ->
        Value.address (Utils.Bs58.encode_address (Misc.Hex.hexcape address))
    | TList elt, Sequence l -> Value.list (List.map (to_value elt) l) elt
    | TMap {big; tkey; tvalue}, Sequence l ->
        let pairOfStorage = function
          | Primitive {name = "Elt"; arguments = [k; v]} ->
              (to_value tkey k, to_value tvalue v)
          | p -> failwith (Printf.sprintf "Bad map pair %s " (pretty "" p))
        in
        Value.map ~big ~tkey ~tvalue (List.map pairOfStorage l)
    | TMap {big; tkey; tvalue}, Int _ ->
        (* show empty big maps *)
        Value.map ~big ~tkey ~tvalue []
    | TSet {telement}, Sequence l ->
        Value.set ~telement (List.map (to_value telement) l)
    | ( TTicket t
      , Primitive
          {name = "Pair"; arguments = [String ticketer; content; Int amount]} )
      ->
        let ticketer = Literal.Real ticketer in
        let content = to_value t content in
        let amount = Big_int.big_int_of_string amount in
        Value.ticket ticketer content amount
    | ( TTicket t
      , Primitive
          { name = "Pair"
          ; arguments =
              [ Bytes ticketer
              ; Primitive {name = "Pair"; arguments = [content; Int amount]} ]
          } ) ->
        let ticketer =
          Literal.Real (Utils.Bs58.encode_address (Misc.Hex.hexcape ticketer))
        in
        let content = to_value t content in
        let amount = Big_int.big_int_of_string amount in
        Value.ticket ticketer content amount
    | TLambda (t1, t2), x ->
        Value.string
          (Printf.sprintf
             "Lambda (%s, %s) = %s"
             (Printer.type_to_string t1)
             (Printer.type_to_string t2)
             (pretty "" x))
    | TSaplingState {memo}, _ ->
      begin
        match Unknown.getRefOption memo with
        | Some memo -> Value.literal (Literal.sapling_state_real memo) t
        | None ->
            Printf.ksprintf
              failwith
              "to_value (sapling) %s %s"
              (Printer.type_to_string t)
              (pretty "" m)
      end
    | _ ->
        Printf.ksprintf
          failwith
          "to_value %s %s"
          (Printer.type_to_string t)
          (pretty "" m)
  in
  to_value

let unString = function
  | `String s -> s
  | _ -> assert false

let rec parse (json : Yojson.Basic.t) =
  match json with
  | `String s -> String s
  | `Int _ -> assert false
  | `Bool b ->
      Primitive
        { name = (if b then "true" else "false")
        ; annotations = []
        ; arguments = [] }
  | `Float _ -> assert false
  | `Null -> String "NULL"
  | `Assoc l ->
    ( match l with
    | [("int", `String s)] -> Int s
    | [("string", `String s)] -> String s
    | [("bytes", `String s)] ->
        Bytes (Misc.Hex.unhex s) (* used in tzstats parsing *)
    | _ ->
        let name =
          match List.assoc_opt "prim" l with
          | Some (`String name) -> name
          | _ -> assert false
        in
        let annotations =
          match List.assoc_opt "annots" l with
          | Some (`List annots) -> List.map unString annots
          | _ -> []
        in
        let arguments =
          match List.assoc_opt "args" l with
          | Some (`List args) -> List.map parse args
          | _ -> []
        in
        Primitive {name; annotations; arguments} )
  | `List l -> Sequence (List.map parse l)

let int s = Int s

let string s = String s

let bytes s = Bytes s

(* let chain_id s = String (Misc.Hex.hexcape s) *)

let primitive name ?(annotations = []) arguments =
  Primitive {name; annotations; arguments}

let sequence l = Sequence l
