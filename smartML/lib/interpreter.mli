(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Basics
open Typed

type context_ =
  { sender : Literal.address option
  ; source : Literal.address option
  ; chain_id : string
  ; time : Bigint.t
  ; amount : Bigint.t
  ; level : Bigint.t
  ; voting_powers : (string * Bigint.t) list
  ; line_no : line_no
  ; debug : bool
  ; contract_id : Literal.contract_id option }

(** The initial state of the execution environment, a.k.a. this
    provides a “pseudo-blockchain.” *)
type context

val context_sender : context -> Literal.address option

val context_time : context -> Bigint.t

val context_line_no : context -> line_no

val context_debug : context -> bool

val context :
     ?contract_id:Literal.contract_id
  -> ?sender:Literal.address
  -> ?source:Literal.address
  -> ?chain_id:string
  -> time:Bigint.t
  -> amount:Bigint.t
  -> level:Bigint.t
  -> voting_powers:(string * Bigint.t) list
  -> line_no:line_no
  -> debug:bool
  -> unit
  -> context
(** Build a {!context}. *)

val interpret_message :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> env:Typing.env
  -> context
  -> value_tcontract
  -> tmessage
  -> (value_tcontract * tcommand) option
     * tvalue operation list
     * Execution.error option
     * Execution.step list
(** Evaluation of a contract call ({!tmessage}) within a {!context}. *)

val interpret_expr_external :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> no_env:Basics.smart_except list
  -> scenario_state:scenario_state
  -> texpr
  -> tvalue
(** Evaluation of an expression ({!texpr}) within a {!context}. *)

val interpret_contract :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> tcontract
  -> value_tcontract

val reducer :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> line_no:line_no
  -> texpr
  -> texpr
