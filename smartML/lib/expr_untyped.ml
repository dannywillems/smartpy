(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Untyped

type t = expr [@@deriving show {with_path = false}]

type unary_expr = line_no:line_no -> t -> t

type bin_expr = line_no:line_no -> t -> t -> t

let build ~line_no e = {e; line_no}

let add ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BAdd, x, y))

let mul ~line_no x y =
  build ~line_no (EPrim2 (EBinOpInf (BMul {overloaded = false}), x, y))

let mul_overloaded ~line_no x y =
  build ~line_no (EPrim2 (EBinOpInf (BMul {overloaded = true}), x, y))

let lshift ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BLsl, x, y))

let rshift ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BLsr, x, y))

let e_mod ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BMod, x, y))

let ediv ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BEDiv, x, y))

let div ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BDiv, x, y))

let sub ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BSub, x, y))

let b_or ~line_no e1 e2 = build ~line_no (EPrim2 (EBinOpInf BOr, e1, e2))

let b_and ~line_no e1 e2 = build ~line_no (EPrim2 (EBinOpInf BAnd, e1, e2))

let xor ~line_no e1 e2 = build ~line_no (EPrim2 (EBinOpInf BXor, e1, e2))

let le ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BLe, x, y))

let lt ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BLt, x, y))

let ge ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BGe, x, y))

let gt ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BGt, x, y))

let eq ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BEq, x, y))

let neq ~line_no x y = build ~line_no (EPrim2 (EBinOpInf BNeq, x, y))

let e_max ~line_no x y = build ~line_no (EPrim2 (EBinOpPre BMax, x, y))

let e_min ~line_no x y = build ~line_no (EPrim2 (EBinOpPre BMin, x, y))

let storage ~line_no = build ~line_no (EPrim0 (ELocal "__storage__"))

let attr ~line_no x name = build ~line_no (EPrim1 (EAttr name, x))

let variant ~line_no name x = build ~line_no (EPrim1 (EVariant name, x))

let isVariant ~line_no name x = build ~line_no (EPrim1 (EIsVariant name, x))

let variant_arg ~line_no arg_name =
  build ~line_no (EPrim0 (EVariant_arg arg_name))

let openVariant ~line_no name x missing_message =
  build ~line_no (EOpenVariant (name, x, missing_message))

let updateMap map key value = build (EPrim3 (EUpdate_map, map, key, value))

let params ~line_no = build ~line_no (EPrim0 (ELocal "__parameter__"))

let local ~line_no n = build ~line_no (EPrim0 (ELocal n))

let operations ~line_no = local ~line_no "__operations__"

let global n contract_id = build (EPrim0 (EGlobal (n, contract_id)))

let item ~line_no items key default_value missing_message =
  build ~line_no (EItem {items; key; default_value; missing_message})

let contains ~line_no items member =
  build ~line_no (EPrim2 (EContains, items, member))

let sum ~line_no l = build ~line_no (EPrim1 (ESum, l))

let range ~line_no a b step = build ~line_no (EPrim3 (ERange, a, b, step))

let cons ~line_no x l = build ~line_no (EPrim2 (ECons, x, l))

let cst ~line_no x = build ~line_no (EPrim0 (ECst x))

let unit = cst ~line_no:None Literal.unit

let type_annotation ~line_no e t =
  build ~line_no (EPrim1 (EType_annotation t, e))

let rec strip_type_annotations = function
  | {e = EPrim1 (EType_annotation _, e)} -> strip_type_annotations e
  | e -> e

let record ~line_no entries = build ~line_no (ERecord entries)

let build_list ~line_no ~elems = build ~line_no (EList elems)

let build_map ~line_no ~big ~entries = build ~line_no (EMap (big, entries))

let build_set ~line_no ~entries = build ~line_no (ESet entries)

let hash_key ~line_no e = build ~line_no (EPrim1 (EHash_key, e))

let hashCrypto ~line_no algo e = build ~line_no (EPrim1 (EHash algo, e))

let pack e = build (EPrim1 (EPack, e))

let unpack ~line_no e t = build ~line_no (EPrim1 (EUnpack t, e))

let check_signature ~line_no pk signature message =
  build ~line_no (EPrim3 (ECheck_signature, pk, signature, message))

let account_of_seed ~seed ~line_no =
  build ~line_no (EPrim0 (EAccount_of_seed {seed}))

let make_signature ~line_no ~secret_key ~message ~message_format =
  build ~line_no (EMake_signature {secret_key; message; message_format})

let scenario_var ~line_no id t = build ~line_no (EPrim0 (EScenario_var (id, t)))

let reduce ~line_no e = build ~line_no (EPrim1 (EReduce, e))

let split_tokens ~line_no mutez quantity total =
  build ~line_no (EPrim3 (ESplit_tokens, mutez, quantity, total))

let now = build ~line_no:None (EPrim0 ENow)

let chain_id = build ~line_no:None (EPrim0 EChain_id)

let add_seconds ~line_no t s = build ~line_no (EPrim2 (EAdd_seconds, t, s))

let notE ~line_no x = build ~line_no (EPrim1 (ENot, x))

let absE ~line_no x = build ~line_no (EPrim1 (EAbs, x))

let to_int ~line_no x = build ~line_no (EPrim1 (EToInt, x))

let is_nat ~line_no x = build ~line_no (EPrim1 (EIsNat, x))

let negE ~line_no x = build ~line_no (EPrim1 (ENeg, x))

let signE ~line_no x = build ~line_no (EPrim1 (ESign, x))

let slice ~line_no ~offset ~length ~buffer =
  build ~line_no (ESlice {offset; length; buffer})

let concat_list ~line_no l = build ~line_no (EPrim1 (EConcat_list, l))

let size ~line_no s = build ~line_no (EPrim1 (ESize, s))

let iterator ~line_no name = build ~line_no (EPrim0 (EIter name))

let match_cons ~line_no name = build ~line_no (EPrim0 (EMatchCons name))

let balance = build ~line_no:None (EPrim0 EBalance)

let sender = build ~line_no:None (EPrim0 ESender)

let source = build ~line_no:None (EPrim0 ESource)

let amount = build ~line_no:None (EPrim0 EAmount)

let level = build ~line_no:None (EPrim0 ELevel)

let total_voting_power = build ~line_no:None (EPrim0 ETotal_voting_power)

let self = build ~line_no:None (EPrim0 ESelf)

let self_entry_point ~line_no name =
  build ~line_no (EPrim0 (ESelf_entry_point name))

let contract_address ~line_no e =
  match e.e with
  | EPrim0 ESelf -> build ~line_no (EPrim0 ESelf_address)
  | _ -> build ~line_no (EPrim1 (EContract_address, e))

let implicit_account ~line_no e = build ~line_no (EPrim1 (EImplicit_account, e))

let voting_power ~line_no e = build ~line_no (EPrim1 (EVotingPower, e))

let listRev ~line_no e = build ~line_no (EPrim1 (EListRev, e))

let listItems ~line_no e rev = build ~line_no (EPrim1 (EListItems rev, e))

let listKeys ~line_no e rev = build ~line_no (EPrim1 (EListKeys rev, e))

let listValues ~line_no e rev = build ~line_no (EPrim1 (EListValues rev, e))

let listElements ~line_no e rev = build ~line_no (EPrim1 (EListElements rev, e))

let contract ~line_no entry_point arg_type address =
  build ~line_no (EContract {entry_point; arg_type; address})

let tuple ~line_no es = build ~line_no (ETuple es)

let proj ~line_no i e = build ~line_no (EPrim1 (EProject i, e))

let first ~line_no = proj ~line_no 0

let second ~line_no = proj ~line_no 1

let none ~line_no = variant ~line_no "None" unit

let some ~line_no e = variant ~line_no "Some" e

let left ~line_no l = variant ~line_no "Left" l

let right ~line_no r = variant ~line_no "Right" r

let inline_michelson ~line_no michelson exprs =
  build ~line_no (EMichelson (michelson, exprs))

let map_function ~line_no l f = build ~line_no (EMapFunction {l; f})

let call_lambda ~line_no lambda parameter =
  build ~line_no (EPrim2 (ECallLambda, lambda, parameter))

let apply_lambda ~line_no lambda parameter =
  build ~line_no (EPrim2 (EApplyLambda, lambda, parameter))

let lambda ~line_no id name tParams tResult body clean_stack =
  build ~line_no (ELambda {id; name; tParams; body; tResult; clean_stack})

let lambdaParams ~line_no id name = build ~line_no (ELambdaParams {id; name})

let create_contract ~line_no ~baker {contract} =
  build ~line_no (ECreate_contract {baker; contract_template = contract})

let sapling_empty_state memo =
  build ~line_no:None (EPrim0 (ESaplingEmptyState {memo}))

let sapling_verify_update ~line_no state transaction =
  build ~line_no (ESaplingVerifyUpdate {state; transaction})

let set_delegate ~line_no e = build ~line_no (EPrim1 (ESetDelegate, e))

let transfer ~line_no ~arg ~amount ~destination =
  build ~line_no (ETransfer {arg; amount; destination})

let contract_data ~line_no e = build ~line_no (EPrim0 (EContract_data e))

let contract_balance ~line_no e = build ~line_no (EPrim0 (EContract_balance e))

let contract_baker ~line_no e = build ~line_no (EPrim0 (EContract_baker e))

let ematch ~line_no scrutinee clauses =
  build ~line_no (EMatch (scrutinee, clauses))

let eif ~line_no cond a b = build ~line_no (EPrim3 (EIf, cond, a, b))

let allow_lambda_full_stack ({e; line_no} as x) =
  match e with
  | ELambda params -> build ~line_no (ELambda {params with clean_stack = false})
  | _ -> x

let test_ticket ~line_no ticketer content amount =
  build ~line_no (EPrim2 (ETest_ticket ticketer, content, amount))

let build_ticket ~line_no content amount =
  build ~line_no (EPrim2 (ETicket, content, amount))

let read_ticket ~line_no ticket = build ~line_no (EPrim1 (EReadTicket, ticket))

let split_ticket ~line_no ticket decomposition =
  build ~line_no (EPrim2 (ESplitTicket, ticket, decomposition))

let join_tickets ~line_no tickets =
  build ~line_no (EPrim1 (EJoinTickets, tickets))

let pairing_check ~line_no pairs =
  build ~line_no (EPrim1 (EPairingCheck, pairs))

let get_and_update ~line_no map key value =
  build ~line_no (EPrim3 (EGet_and_update, map, key, value))

let getOpt ~line_no m k = build ~line_no (EPrim2 (EGetOpt, m, k))

let rec of_value v : expr =
  match v.v with
  | Literal l -> cst ~line_no:None l
  | Record entries ->
      build
        ~line_no:None
        (ERecord (entries |> List.map (fun (fld, l) -> (fld, of_value l))))
  | Variant (lbl, arg) ->
      build ~line_no:None (EPrim1 (EVariant lbl, of_value arg))
  | List elems -> build ~line_no:None (EList (List.map of_value elems))
  | Set elems -> build_set ~line_no:None ~entries:(List.map of_value elems)
  | Map entries ->
      let big = Type.is_bigmap v.vt in
      build
        ~line_no:None
        (EMap (big, entries |> List.map (fun (k, v) -> (of_value k, of_value v))))
  | Tuple vs -> tuple ~line_no:None (List.map of_value vs)
  | Closure ({id; name; tParams; tResult; body}, args) ->
      List.fold_left
        (fun f arg -> apply_lambda ~line_no:None f (of_value arg))
        (lambda
           ~line_no:None
           id
           name
           tParams
           tResult
           (erase_types_command body)
           true)
        args
  | Operation _ -> failwith "TODO expr.of_value Operation"
  | Ticket (ticketer, content, amount) ->
    ( match ticketer with
    | Local ticketer ->
        let content = of_value content in
        let amount = cst ~line_no:None (Literal.int amount) in
        test_ticket ~line_no:None ticketer content amount
    | _ -> failwith "cannot handle non-local or dynamic local ticketer" )
