(* Copyright 2019-2021 Smart Chain Arena LLC. *)

type protocol =
  | Delphi
  | Edo
  | Florence
[@@deriving eq, ord, show]

type lazy_entry_points =
  | Single
  | Multiple
[@@deriving eq, ord, show]

type exceptions =
  | FullDebug
  | Message
  | VerifyOrLine
  | DefaultLine
  | Line
  | DefaultUnit
  | Unit
[@@deriving eq, ord, show]

type t =
  { simplify_via_michel : bool
  ; decompile : bool
  ; erase_comments : bool
  ; disable_dup_check : bool
  ; protocol : protocol
  ; lazy_entry_points : lazy_entry_points option
  ; exceptions : exceptions }
[@@deriving eq, show]

val default : t

type flag =
  | Simplify_via_michel of bool
  | Decompile           of bool
  | Erase_comments      of bool
  | Disable_dup_check   of bool
  | Protocol            of protocol
  | Lazy_entry_points   of lazy_entry_points option
  | Exceptions          of exceptions
[@@deriving eq, ord, show]

val parse_flag : string list -> (flag * string list) option

val apply_flag : flag -> t -> t

val apply_flags : flag list -> t -> t

val protocol_of_string : string -> protocol
