(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed

type t = tcommand

val verify : line_no:line_no -> texpr -> texpr option -> t

val whileLoop : line_no:line_no -> texpr -> t -> t

val set : line_no:line_no -> texpr -> texpr -> t

val defineLocal : line_no:line_no -> string -> texpr -> t

val seq : line_no:line_no -> t list -> t
