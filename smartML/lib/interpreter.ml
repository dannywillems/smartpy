(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Basics
open Typed
open Utils.Misc
open Primitives

type context_ =
  { sender : Literal.address option
  ; source : Literal.address option
  ; chain_id : string
  ; time : Bigint.t
  ; amount : Bigint.t
  ; level : Bigint.t
  ; voting_powers : (string * Bigint.t) list
  ; line_no : line_no
  ; debug : bool
  ; contract_id : Literal.contract_id option }

type context = context_ ref

let context
    ?contract_id
    ?sender
    ?source
    ?(chain_id = "")
    ~time
    ~amount
    ~level
    ~voting_powers
    ~line_no
    ~debug
    () =
  let source =
    match source with
    | None -> sender
    | _ -> source
  in
  ref
    { sender
    ; source
    ; chain_id
    ; time
    ; amount
    ; level
    ; voting_powers
    ; line_no
    ; debug
    ; contract_id }

let context_sender c = !c.sender

let context_time c = !c.time

let context_line_no c = !c.line_no

let context_debug c = !c.debug

(** Internal exception: raised by {!interpret_command} and caught by {!interpret_contract}. *)
exception WrongCondition of texpr * line_no * tvalue option

type root =
  | R_storage
  | R_local   of string

type step =
  | S_attr      of string
  | S_item_map  of tvalue
  | S_item_list of int

type path =
  { root : root
  ; steps : step list }

let string_of_step = function
  | S_attr attr -> "." ^ attr
  | S_item_list i -> Printf.sprintf "[%d]" i
  | S_item_map key -> Printf.sprintf "[%s]" (Printer.value_to_string key)

let _string_of_steps steps = String.concat "" (List.map string_of_step steps)

let extend_path {root; steps} steps' = {root; steps = steps @ steps'}

let ( @. ) = Lens.( @. )

type variable_value =
  | Iter        of (tvalue * path option)
  | Heap_ref    of tvalue ref
  | Variant_arg of tvalue
  | Match_list  of tvalue

type env =
  { parameters : tvalue
  ; primitives : (module Primitives)
  ; scenario_state : scenario_state
  ; context : context_
  ; variables : (string * variable_value) list ref
  ; storage : tvalue ref
  ; baker : tvalue ref
  ; balance : Bigint.t ref
  ; operations : tvalue operation list ref
  ; debug : bool
  ; steps : Execution.step list ref
  ; lambda_params : (int * tvalue) list
  ; global_variables : (string * texpr) list
  ; current_locals : string list ref
  ; typing_env : Typing.env
  ; tparameter : Type.t
  ; tparameter_ep : Type.t }

let lens_heap_ref =
  Lens.bi
    (function
      | Heap_ref x -> x
      | _ -> failwith "not a heap reference")
    (fun x -> Heap_ref x)

let lens_of_root env = function
  | R_storage -> Lens.ref env.storage
  | R_local name ->
      Lens.ref env.variables
      @. Lens.assoc_exn ~equal:( = ) ~key:name ~err:"local not found"
      @. lens_heap_ref
      @. Lens.unref

(** Convert a step into a lens that points to an optional value,
   accompanied by an error message for when the value is missing. *)
let lens_of_step ~line_no =
  let line_no = string_of_line_no line_no in
  function
  | S_item_list i ->
      ( Value.lens_list_nth i
      , Printf.sprintf "Line %s: Index '%d' out of range." line_no i )
  | S_item_map key ->
      ( Value.lens_map_at ~key
      , Printf.sprintf
          "Line %s: Key '%s' not found."
          line_no
          (Printer.value_to_string key) )
  | S_attr attr ->
      ( Value.lens_record_at ~attr
      , Printf.sprintf "Line %s: Impossible: attribute not found" line_no )

let rec lens_of_steps ~line_no acc = function
  | [s] ->
      let l, err = lens_of_step ~line_no s in
      (acc @. l, err)
  | s :: steps ->
      let l, err = lens_of_step ~line_no s in
      lens_of_steps ~line_no (acc @. l @. Lens.some ~err) steps
  | [] -> (Lens.option ~err:"lens_of_steps", "lens_of_steps")

let lens_of_path ~line_no env {root; steps} =
  let l, err = lens_of_steps ~line_no Lens.id steps in
  (lens_of_root env root @. l, err)

let addStep ?(sub_steps = ref []) upperSteps env c elements =
  let buildStep () =
    { Execution.iters =
        Base.List.filter_map
          ~f:(function
            | n, Iter (v, _) -> Some (n, (v, None))
            | _ -> None)
          !(env.variables)
    ; locals =
        Base.List.filter_map
          ~f:(function
            | n, Heap_ref x -> Some (n, !x)
            | _ -> None)
          !(env.variables)
    ; storage = !(env.storage)
    ; balance = !(env.balance)
    ; operations = [] (*env.operations*)
    ; command = c
    ; substeps = sub_steps
    ; elements }
  in
  if env.debug then upperSteps := buildStep () :: !upperSteps

let add_variable name value vars =
  match List.assoc_opt name vars with
  | Some _ -> failwith ("Variable '" ^ name ^ "' already defined")
  | None -> (name, value) :: vars

let assert_unit c = function
  | {v = Literal Unit} -> ()
  | _ ->
      raise
        (SmartExcept
           [ `Text "Command doesn't return unit"
           ; `Br
           ; `Text (Printer.tcommand_to_string c)
           ; `Br
           ; `Line c.line_no ])

let rec interpret_expr_
    ~config ?optional_primitives upper_steps env scenario_state =
  let env e =
    match env with
    | `Env env -> env
    | `NoEnv no_env ->
        raise
          (SmartExcept
             [`Text "Missing environment"; `Br; `Expr e; `Br; `Rec no_env])
  in
  let rec interp e =
    let with_primitives f =
      match optional_primitives with
      | None ->
          failwith
            (Printf.sprintf
               "Missing crypto primitives for evaluating %s"
               (Printer.texpr_to_string e))
      | Some primitives -> f primitives
    in
    with_primitives (fun primitives ->
        let module P = (val primitives : Primitives) in
        match e.e with
        | EPrim0 prim ->
          begin
            match prim with
            | ECst (Literal.Address (Local local_contract, entry_point) as x) ->
              begin
                match
                  Hashtbl.find_opt scenario_state.addresses local_contract
                with
                | Some addr -> Value.address ?entry_point addr
                | None -> Value.literal x Type.address
              end
            | ECst x -> Value.literal x (Literal.type_of x)
            | ESender ->
              begin
                match (env e).context.sender with
                | Some address -> Value.meta_address address
                | None ->
                    raise (SmartExcept [`Text "Sender is undefined"; `Expr e])
              end
            | ESource ->
              begin
                match (env e).context.source with
                | Some address -> Value.meta_address address
                | None ->
                    raise (SmartExcept [`Text "Source is undefined"; `Expr e])
              end
            | ENow -> Value.timestamp (env e).context.time
            | EChain_id -> Value.chain_id (env e).context.chain_id
            | EAmount -> Value.mutez (env e).context.amount
            | ELevel -> Value.nat (env e).context.level
            | ETotal_voting_power ->
                Value.nat
                  (List.fold_left
                     (fun acc (_, v) -> Big_int.add_big_int acc v)
                     Big_int.zero_big_int
                     (env e).context.voting_powers)
            | EBalance -> Value.mutez !((env e).balance)
            | EGlobal (id, _contract_id) ->
              ( match List.assoc_opt id (env e).global_variables with
              | Some e -> interp e
              | None -> Printf.ksprintf failwith "Missing global variable %s" id
              )
            | ESelf_address ->
                let env = env e in
                ( match env.context.contract_id with
                | Some i -> Value.local_address i
                | None ->
                    failwith
                      ( "Interpreter. sp.self is not available in this context: "
                      ^ Printer.texpr_to_string e ) )
            | ESelf ->
                let env = env e in
                ( match env.context.contract_id with
                | Some i -> Value.local_contract i env.tparameter
                | None ->
                    failwith
                      ( "Interpreter. sp.self is not available in this context: "
                      ^ Printer.texpr_to_string e ) )
            | ESelf_entry_point entry_point ->
                let env = env e in
                let i =
                  match env.context.contract_id with
                  | Some i -> i
                  | None ->
                      failwith
                        ( "Interpreter. sp.self_entry_point is not available \
                           in this context: "
                        ^ Printer.texpr_to_string e )
                in
                let msg = "interpreter: ESelf_entry_point" in
                let {value_tcontract = {tparameter}} =
                  Hashtbl.find_opt env.scenario_state.contracts i
                  |> Option.of_some ~msg
                in
                let row =
                  match tparameter with
                  | F (TVariant {row}) -> row
                  | _ -> failwith "self_entry_point: tparameter not a variant"
                in
                let t = Option.of_some ~msg (List.assoc_opt entry_point row) in
                Value.local_contract ~entry_point i t
            | EVariant_arg n ->
              ( match List.assoc_opt n !((env e).variables) with
              | Some (Variant_arg x) -> x
              | _ -> failwith (Printf.sprintf "Not a variant argument: %s" n) )
            | EMatchCons name ->
              ( match List.assoc_opt name !((env e).variables) with
              | Some (Match_list v) -> v
              | _ ->
                  failwith
                    (Printf.sprintf
                       "Missing var %s in env [%s]"
                       name
                       (String.concat
                          "; "
                          (List.map (fun (x, _) -> x) !((env e).variables)))) )
            | EIter name ->
              ( match List.assoc_opt name !((env e).variables) with
              | Some (Iter (v, _)) -> v
              | _ ->
                  failwith
                    (Printf.sprintf
                       "Missing var %s in env [%s]"
                       name
                       (String.concat
                          "; "
                          (List.map (fun (x, _) -> x) !((env e).variables)))) )
            | ESaplingEmptyState {memo} ->
                Value.literal
                  (Literal.sapling_test_state memo [])
                  (Type.sapling_state (Some memo))
            | ELocal "__parameter__" -> (env e).parameters
            | ELocal "__storage__" -> !((env e).storage)
            | ELocal "__operations__" ->
                Value.list
                  (List.map Value.operation !((env e).operations))
                  Type.operation
            | ELocal name ->
              ( match List.assoc_opt name !((env e).variables) with
              | Some (Heap_ref {contents = x}) -> x
              | _ -> failwith ("Not a local: " ^ name) )
            | EContract_balance id ->
              ( match Hashtbl.find_opt scenario_state.contracts id with
              | Some t -> t.value_tcontract.balance
              | None ->
                  raise
                    (SmartExcept
                       [ `Text "Missing contract balance for id: "
                       ; `Text (Printer.string_of_contract_id id)
                       ; `Br
                       ; `Line e.line_no ]) )
            | EContract_baker id ->
              ( match Hashtbl.find_opt scenario_state.contracts id with
              | Some {value_tcontract = {baker}} -> baker
              | None ->
                  raise
                    (SmartExcept
                       [ `Text "Missing contract for id: "
                       ; `Text (Printer.string_of_contract_id id)
                       ; `Br
                       ; `Line e.line_no ]) )
            | EContract_data id ->
              ( match Hashtbl.find_opt scenario_state.contracts id with
              | Some t ->
                ( match t.value_tcontract.storage with
                | Some storage -> storage
                | None ->
                    raise
                      (SmartExcept
                         [ `Text "Missing contract storage for id: "
                         ; `Text (Printer.string_of_contract_id id)
                         ; `Br
                         ; `Line e.line_no ]) )
              | None ->
                  raise
                    (SmartExcept
                       [ `Text "Missing contract for id: "
                       ; `Text (Printer.string_of_contract_id id)
                       ; `Br
                       ; `Line e.line_no ]) )
            | EContract_typed id ->
              ( match Hashtbl.find_opt scenario_state.contracts id with
              | Some t -> Value.local_contract id t.value_tcontract.tparameter
              | None ->
                  raise
                    (SmartExcept
                       [ `Text "Missing contract for id: "
                       ; `Text (Printer.string_of_contract_id id)
                       ; `Br
                       ; `Line e.line_no ]) )
            | EScenario_var (id, _) ->
              ( match Hashtbl.find_opt scenario_state.variables id with
              | Some x -> x
              | None ->
                  raise
                    (SmartExcept
                       [ `Text "Missing scenario variable for id: "
                       ; `Text (string_of_int id)
                       ; `Br
                       ; `Line e.line_no ]) )
            | EAccount_of_seed {seed} ->
                let account = P.Crypto.account_of_seed seed in
                let seed = Value.string seed in
                let pkh = Value.key_hash account.pkh in
                let address = Value.address account.pkh in
                let pk = Value.key account.pk in
                let sk = Value.secret_key account.sk in
                Value.record
                  [ ("seed", seed)
                  ; ("address", address)
                  ; ("public_key", pk)
                  ; ("public_key_hash", pkh)
                  ; ("secret_key", sk) ]
          end
        | EPrim1 (prim, x) ->
            let pp () = [] in
            ( match prim with
            | ENot -> Value.bool (not (Value.unBool ~pp (interp x)))
            | EAbs ->
                Value.nat (Big_int.abs_big_int (Value.unInt ~pp (interp x)))
            | EToInt ->
              ( match (interp x).v with
              | Literal (Bls12_381_fr hex) ->
                  Value.int
                    (Big_int.big_int_of_string
                       (P.Bls12.convertFrToInt (Misc.Hex.hexcape hex)))
              | Literal (Int {i}) -> Value.int i
              | _ -> failwith ("Cannot cast to Int " ^ Printer.texpr_to_string e)
              )
            | EIsNat ->
                let x = Value.unInt ~pp (interp x) in
                if 0 <= Big_int.compare_big_int x Big_int.zero_big_int
                then Value.some (Value.nat x)
                else Value.none (Type.nat ())
            | ENeg ->
              begin
                match (interp x).v with
                | Literal (Bls12_381_g1 hex) ->
                    Value.bls12_381_g1
                      (Misc.Hex.unhex (P.Bls12.negateG1 (Misc.Hex.hexcape hex)))
                | Literal (Bls12_381_g2 hex) ->
                    Value.bls12_381_g2
                      (Misc.Hex.unhex (P.Bls12.negateG2 (Misc.Hex.hexcape hex)))
                | Literal (Bls12_381_fr hex) ->
                    Value.bls12_381_fr
                      (Misc.Hex.unhex (P.Bls12.negateFr (Misc.Hex.hexcape hex)))
                | Literal (Int {i}) -> Value.int (Big_int.minus_big_int i)
                | _ ->
                    Format.kasprintf
                      failwith
                      "Type-error: Cannot negate %s value. %s"
                      (Printer.texpr_to_string x)
                      (Printer.value_to_string (interp x))
              end
            | ESign ->
                let x = Value.unInt ~pp (interp x) in
                let result = Big_int.compare_big_int x Big_int.zero_big_int in
                Value.int (Bigint.of_int result)
            | ESum ->
              ( match (interp x).v with
              | List l | Set l ->
                  let {v; vt} =
                    List.fold_left
                      (fun x y -> Value.plus_inner ~primitives x y)
                      (Value.zero_of_type e.et)
                      l
                  in
                  Value.build v vt
              | Map l ->
                  let {v; vt} =
                    List.fold_left
                      (fun x (_, y) -> Value.plus_inner ~primitives x y)
                      (Value.zero_of_type e.et)
                      l
                  in
                  Value.build v vt
              | _ -> failwith ("bad sum " ^ Printer.texpr_to_string e) )
            | EListRev ->
                let l = interp x in
                ( match (l.v, Type.getRepr x.et) with
                | Basics.List l, TList t -> Value.list (List.rev l) t
                | _ -> assert false )
            | EListItems rev ->
                let l = interp x in
                ( match (l.v, Type.getRepr x.et) with
                | Basics.Map l, TMap {tkey; tvalue} ->
                    Value.list
                      ((if rev then List.rev_map else List.map)
                         (fun (k, v) -> Value.record [("key", k); ("value", v)])
                         l)
                      (Type.key_value tkey tvalue)
                | _ -> assert false )
            | EListKeys rev ->
                let l = interp x in
                ( match (l.v, Type.getRepr x.et) with
                | Basics.Map l, TMap {tkey} ->
                    Value.list
                      ((if rev then List.rev_map else List.map) fst l)
                      tkey
                | _ -> assert false )
            | EListValues rev ->
                let l = interp x in
                ( match (l.v, Type.getRepr x.et) with
                | Basics.Map l, TMap {tvalue} ->
                    Value.list
                      ((if rev then List.rev_map else List.map) snd l)
                      tvalue
                | _ -> assert false )
            | EListElements rev ->
                let l = interp x in
                ( match (l.v, Type.getRepr x.et) with
                | Basics.Set l, TSet {telement} ->
                    Value.list (if rev then List.rev l else l) telement
                | _ -> assert false )
            | EPack ->
                let r = interp x in
                {r with vt = x.et} |> Compiler.pack_value ~config |> Value.bytes
            | EConcat_list ->
                let vals =
                  match (interp x).v with
                  | List sl ->
                      Base.List.map sl ~f:(fun sb ->
                          match sb.v with
                          | Literal (String s) | Literal (Bytes s) -> s
                          | _ ->
                              Format.kasprintf
                                failwith
                                "Error while concatenating string/bytes:@ not \
                                 a list of strings/bytes:@ %s"
                                (Printer.texpr_to_string e))
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Error while concatenating string/bytes:@ not a list:@ \
                         %s"
                        (Printer.texpr_to_string e)
                in
                ( match Type.getRepr e.et with
                | TString -> Value.string (String.concat "" vals)
                | TBytes -> Value.bytes (String.concat "" vals)
                | _ ->
                    raise
                      (SmartExcept
                         [ `Text "Bad type in concat (should apply to lists of "
                         ; `Type Type.string
                         ; `Text "or"
                         ; `Type Type.bytes
                         ; `Text ")"
                         ; `Expr e
                         ; `Br
                         ; `Line e.line_no ]) )
            | ESize ->
                let result length = Value.nat (Bigint.of_int length) in
                ( match (interp x).v with
                | Literal (String s) | Literal (Bytes s) ->
                    result (String.length s)
                | List l | Set l -> result (List.length l)
                | Map l -> result (List.length l)
                | _ ->
                    raise
                      (SmartExcept
                         [`Text "Bad type for sp.len"; `Expr x; `Line x.line_no])
                )
            | EHash algo ->
                let v = interp x in
                let as_string =
                  match v.v with
                  | Literal (Bytes b) -> b
                  | Literal _ | Record _ | Closure _
                   |Variant (_, _)
                   |List _ | Set _ | Map _ | Tuple _ | Operation _ | Ticket _ ->
                      Format.kasprintf
                        failwith
                        "Type-error: calling hash:%s(%s) on non-bytes value."
                        (String.lowercase_ascii (string_of_hash_algo algo))
                        (Printer.value_to_string v)
                in
                let hash =
                  let hash =
                    match algo with
                    | BLAKE2B -> P.Crypto.blake2b
                    | SHA256 -> P.Crypto.sha256
                    | SHA512 -> P.Crypto.sha512
                    | KECCAK -> P.Crypto.keccak
                    | SHA3 -> P.Crypto.sha3
                  in
                  hash as_string
                in
                Value.bytes hash
            | EHash_key ->
                let public_key =
                  (interp x).v
                  |> function
                  | Literal (Key k) -> k
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Type-error: hash_key expects a key, not: %s"
                        (Printer.texpr_to_string e)
                in
                Value.key_hash (P.Crypto.hash_key public_key)
            | EReduce -> interp x
            | EProject i ->
                let vs = Value.untuple ~pp (interp x) in
                if i < List.length vs
                then List.nth vs i
                else failwith "interpreter: short tuple"
            | EContract_address ->
              ( match (interp x).v with
              | Literal (Contract (a, entry_point, _)) ->
                  Value.meta_address ?entry_point a
              | _ ->
                  failwith
                    ( "Interpreter. sp.to_address is not available in this \
                       context: "
                    ^ Printer.texpr_to_string x ) )
            | EImplicit_account ->
              ( match (interp x).v with
              | Literal (Key_hash key_hash) -> Value.contract key_hash Type.unit
              | _ ->
                  failwith
                    ( "Interpreter. sp.implicit_account is not available in \
                       this context: "
                    ^ Printer.texpr_to_string e ) )
            | EUnpack _ ->
                failwith
                  ( "Interpreter. Instruction not supported in interpreter: "
                  ^ Printer.texpr_to_string e )
            | ESetDelegate ->
                Value.operation (SetDelegate (Value.unOption (interp x)))
            | EType_annotation _ -> interp x
            | EAttr name ->
              ( match (interp x).v with
              | Record l ->
                ( match List.assoc_opt name l with
                | Some v -> v
                | None ->
                    failwith
                      (Printf.sprintf
                         "Missing field in record [%s] [%s] [%s]."
                         name
                         (Printer.texpr_to_string e)
                         (String.concat ", " (List.map fst l))) )
              | _ -> failwith ("Not a record " ^ Printer.texpr_to_string e) )
            | EIsVariant name ->
              ( match (interp x).v with
              | Variant (cons, _) -> Value.bool (cons = name)
              | _ ->
                  failwith
                    (Printf.sprintf
                       "Not a variant %s %s"
                       (Printer.texpr_to_string e)
                       (Printer.value_to_string (interp x))) )
            | EVariant name -> Value.variant name (interp x) e.et
            | EReadTicket ->
                let t = interp x in
                ( match (interp x).v with
                | Ticket (address, content, amount) ->
                    let address = Value.meta_address address in
                    let amount = Value.nat amount in
                    Value.tuple [Value.tuple [address; content; amount]; t]
                | _ -> assert false )
            | EJoinTickets ->
              ( match (interp x).v with
              | Tuple
                  [ {v = Ticket (ticketer1, content1, amount1)}
                  ; {v = Ticket (ticketer2, content2, amount2)} ] ->
                  if Literal.equal_address ticketer1 ticketer2
                     && Value.equal content1 content2
                  then
                    Value.some
                      (Value.ticket
                         ticketer1
                         content1
                         (Bigint.add_big_int amount1 amount2))
                  else assert false
              | _ -> assert false )
            | EPairingCheck ->
                let vals =
                  match (interp x).v with
                  | List sl ->
                      Base.List.map sl ~f:(fun sb ->
                          match sb.v with
                          | Tuple [i1; i2] ->
                              ( Misc.Hex.hexcape (Value.unBls12_381 i1)
                              , Misc.Hex.hexcape (Value.unBls12_381 i2) )
                          | _ ->
                              Format.kasprintf
                                failwith
                                "Expected a list of pairs (g1, g2) @ %s"
                                (Printer.texpr_to_string e))
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Expected a list @ %s"
                        (Printer.texpr_to_string e)
                in
                Value.bool (P.Bls12.pairingCheck vals)
            | EVotingPower ->
                let key_hash =
                  match (interp x).v with
                  | Literal (Key_hash key_hash) -> key_hash
                  | _ ->
                      failwith
                        ( "Interpreter. sp.voting_power is not available in \
                           this context: "
                        ^ Printer.texpr_to_string e )
                in
                let res =
                  List.find_opt
                    (fun (k, _) ->
                      Value.equal (Value.key_hash k) (Value.key_hash key_hash))
                    (env e).context.voting_powers
                in
                Value.nat
                  ( match res with
                  | Some (_, v) -> v
                  | None -> Big_int.zero_big_int ) )
        | EOpenVariant (name, (x as x0), _missing_message) ->
            let x = interp x in
            ( match x.v with
            | Variant (cons, v) ->
                if cons <> name
                then
                  failwith
                    (Printf.sprintf
                       "Not the proper variant constructor [%s] != [%s] in \
                        [%s]."
                       name
                       cons
                       (Printer.texpr_to_string e))
                else v
            | _ ->
                failwith
                  (Printf.sprintf
                     "Not a variant %s (from %s)"
                     (Printer.value_to_string x)
                     (Printer.texpr_to_string x0)) )
        | EItem {items; key; default_value; missing_message} ->
            let def =
              match default_value with
              | None -> None
              | Some def -> Some (lazy (interp def))
            in
            let missing_message =
              match missing_message with
              | None -> None
              | Some x -> Some (lazy (interp x))
            in
            Value.getItem
              (interp items)
              (interp key)
              def
              missing_message
              ~pp:(fun () ->
                Printf.sprintf
                  "%s[%s]"
                  (Printer.texpr_to_string items)
                  (Printer.texpr_to_string key))
        | EPrim2 (prim2, x, y) ->
          begin
            match prim2 with
            | EGetOpt ->
                let m, k = (x, y) in
                let tm = Type.unF m.et in
                let m = interp m in
                let k = interp k in
                ( match (tm, m.v) with
                | Type.TMap {tvalue}, Map m ->
                  ( match List.assoc_opt ~equal:Value.equal k m with
                  | Some v -> Value.some v
                  | None -> Value.none tvalue )
                | _ -> assert false )
            | EBinOpInf BEq -> Value.bool (Value.equal (interp x) (interp y))
            | EBinOpInf BNeq ->
                Value.bool (not (Value.equal (interp x) (interp y)))
            | EBinOpInf BAdd -> Value.plus ~primitives (interp x) (interp y)
            | EBinOpInf (BMul _) -> Value.mul ~primitives (interp x) (interp y)
            | EBinOpInf BMod -> Value.e_mod (interp x) (interp y)
            | EBinOpInf BDiv -> Value.div (interp x) (interp y)
            | EBinOpInf BSub -> Value.minus (interp x) (interp y)
            | EBinOpInf BLt -> Value.bool (Value.lt (interp x) (interp y))
            | EBinOpInf BLe -> Value.bool (Value.le (interp x) (interp y))
            | EBinOpInf BGt -> Value.bool (Value.lt (interp y) (interp x))
            | EBinOpInf BGe -> Value.bool (Value.le (interp y) (interp x))
            | EBinOpInf BLsl -> Value.shift_left (interp x) (interp y)
            | EBinOpInf BLsr -> Value.shift_right (interp x) (interp y)
            | EBinOpInf BAnd ->
                let pp () = [] in
                Value.bool
                  (Value.unBool ~pp (interp x) && Value.unBool ~pp (interp y))
            | EBinOpInf BOr ->
                let pp () = [] in
                Value.bool
                  (Value.unBool ~pp (interp x) || Value.unBool ~pp (interp y))
            | EBinOpInf BXor -> Value.xor (interp x) (interp y)
            | EBinOpInf BEDiv -> Value.ediv (interp x) (interp y)
            | ECons -> Value.cons (interp x) (interp y)
            | EBinOpPre BMax ->
                let pp () = [] in
                Value.intOrNat
                  x.et
                  (Big_int.max_big_int
                     (Value.unInt ~pp (interp x))
                     (Value.unInt ~pp (interp y)))
            | EBinOpPre BMin ->
                let pp () = [] in
                Value.intOrNat
                  x.et
                  (Big_int.min_big_int
                     (Value.unInt ~pp (interp x))
                     (Value.unInt ~pp (interp y)))
            | EAdd_seconds ->
                let t, s = (x, y) in
                ( match ((interp t).v, (interp s).v) with
                | Literal (Timestamp t), Literal (Int {i = s}) ->
                    Value.timestamp (Big_int.add_big_int t s)
                | _ ->
                    failwith
                      ("Cannot add timestamp " ^ Printer.texpr_to_string e) )
            | EContains ->
                let items, member = (x, y) in
                let member = interp member in
                ( match (interp items).v with
                | Map l ->
                    Value.bool
                      (List.exists (fun (x, _) -> Value.equal x member) l)
                | Set l ->
                    Value.bool (List.exists (fun x -> Value.equal x member) l)
                | _ -> failwith ("Cannot compute " ^ Printer.texpr_to_string e)
                )
            | EApplyLambda ->
                let f, x = (x, y) in
                Value.closure_apply (interp f) (interp x)
            | ECallLambda ->
                let lambda, parameter = (x, y) in
                let env = env e in
                call_lambda
                  ~config
                  upper_steps
                  env
                  (interp lambda)
                  (interp parameter)
            | ETicket ->
                let content = interp x in
                let ticketer =
                  match (env e).context.contract_id with
                  | Some i -> Literal.Local i
                  | None -> assert false
                in
                ( match (interp y).v with
                | Literal (Int {i}) -> Value.ticket ticketer content i
                | _ -> assert false )
            | ETest_ticket ticketer ->
                let content = interp x in
                ( match (interp y).v with
                | Literal (Int {i}) ->
                    Value.ticket Literal.(Local ticketer) content i
                | _ -> assert false )
            | ESplitTicket ->
                let ticket = interp x in
                ( match ((interp x).v, (interp y).v) with
                | ( Ticket (ticketer, content, amount)
                  , Tuple
                      [ {v = Literal (Int {i = a1})}
                      ; {v = Literal (Int {i = a2})} ] ) ->
                    if Bigint.equal (Bigint.add_big_int a1 a2) amount
                    then
                      let t1 = Value.ticket ticketer content a1 in
                      let t2 = Value.ticket ticketer content a2 in
                      Value.some (Value.tuple [t1; t2])
                    else Value.none (Type.pair ticket.vt ticket.vt)
                | _ -> assert false )
          end
        | ERecord [] -> Value.unit
        | ERecord l ->
            let layout =
              match Type.getRepr e.et with
              | TRecord {layout} -> layout
              | _ ->
                  raise
                    (SmartExcept
                       [ `Text "Bad type while evaluating"
                       ; `Expr e
                       ; `Line e.line_no ])
            in
            Value.record ~layout (List.map (fun (s, e) -> (s, interp e)) l)
        | EPrim3 (prim3, x, y, z) ->
          begin
            match prim3 with
            | ESplit_tokens ->
                let mutez, quantity, total = (x, y, z) in
                let pp () = [] in
                Value.mutez
                  (Big_int.div_big_int
                     (Big_int.mult_big_int
                        (Value.unMutez ~pp (interp mutez))
                        (Value.unInt ~pp (interp quantity)))
                     (Value.unInt ~pp (interp total)))
            | ERange ->
                let a, b, step = (x, y, z) in
                let item = a.et in
                ( match ((interp a).v, (interp b).v, (interp step).v) with
                | ( Literal (Int {i = a})
                  , Literal (Int {i = b})
                  , Literal (Int {i = step}) ) ->
                    let a = Big_int.int_of_big_int a in
                    let b = Big_int.int_of_big_int b in
                    let step = Big_int.int_of_big_int step in
                    if step = 0
                    then failwith (Printf.sprintf "Range with 0 step")
                    else if step * (b - a) < 0
                    then Value.list [] item
                    else
                      let rec aux a acc =
                        if (b - a) * step <= 0
                        then List.rev acc
                        else
                          aux
                            (a + step)
                            (Value.intOrNat item (Bigint.of_int a) :: acc)
                      in
                      Value.list (aux a []) item
                | _ -> failwith ("bad range" ^ Printer.texpr_to_string e) )
            | ECheck_signature ->
                let pk_expr, sig_expr, msg_expr = (x, y, z) in
                Dbg.on := false;
                let public_key =
                  (interp pk_expr).v
                  |> function
                  | Literal (Key k) -> k
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Type-error: check_signature expects a key, not: %s"
                        (Printer.texpr_to_string e)
                in
                let signature =
                  (interp sig_expr).v
                  |> function
                  | Literal (Signature s) -> s
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Type-error: check_signature expects a signature, not: \
                         %s"
                        (Printer.texpr_to_string e)
                in
                let msg =
                  (interp msg_expr).v
                  |> function
                  | Literal (Bytes b) -> b
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Type-error: check_signature expects bytes (3rd \
                         argument), not: %s"
                        (Printer.texpr_to_string e)
                in
                Value.bool (P.Crypto.check_signature ~public_key ~signature msg)
            | EIf ->
                let c, t, e = (x, y, z) in
                let condition = interp c in
                let pp () = [] in
                if Value.unBool ~pp condition then interp t else interp e
            | EUpdate_map ->
                let open Value in
                let map, key, value = (interp x, interp y, interp z) in
                ( match map.v with
                | Map xs ->
                    let value = unOption value in
                    let m' = Lens.set (Lens.assoc ~equal ~key) value xs in
                    {map with v = Map m'}
                | _ -> assert false )
            | EGet_and_update ->
                let open Value in
                let map, key, value = (interp x, interp y, interp z) in
                ( match map.v with
                | Map xs ->
                    let value' = unOption value in
                    let old, m' =
                      Lens.get_and_set (Lens.assoc ~equal ~key) value' xs
                    in
                    let map = {map with v = Map m'} in
                    Value.tuple [Value.option value.vt old; map]
                | _ -> assert false )
          end
        | EMapFunction {l; f} ->
            let f = interp f in
            let env = env e in
            ( match ((interp l).v, Type.getRepr f.vt) with
            | List l, TLambda (_, t) ->
                l
                |> List.map (call_lambda ~config upper_steps env f)
                |> fun l -> Value.list l t
            | ( Map l
              , TLambda (F (TRecord {row = [("key", tkey); ("value", _)]}), t) )
              ->
                l
                |> List.map (fun (k, v) ->
                       ( k
                       , call_lambda
                           ~config
                           upper_steps
                           env
                           f
                           (Value.record [("key", k); ("value", v)]) ))
                |> fun l ->
                Value.map ~big:(ref (Unknown.UnValue false)) ~tkey ~tvalue:t l
            | _ ->
                Printf.ksprintf
                  failwith
                  "Not a list or a map %s"
                  (Printer.type_to_string f.vt) )
        | ESlice {offset; length; buffer} ->
            Basics.(
              ( match
                  ((interp offset).v, (interp length).v, (interp buffer).v)
                with
              | ( Literal (Int {i = ofs_bi})
                , Literal (Int {i = len_bi})
                , Literal (String s) ) ->
                ( try
                    Value.some
                      (Value.string
                         (String.sub
                            s
                            (Big_int.int_of_big_int ofs_bi)
                            (Big_int.int_of_big_int len_bi)))
                  with
                | _ -> Value.none Type.string )
              | ( Literal (Int {i = ofs_bi})
                , Literal (Int {i = len_bi})
                , Literal (Bytes s) ) ->
                ( try
                    Value.some
                      (Value.bytes
                         (String.sub
                            s
                            (Big_int.int_of_big_int ofs_bi)
                            (Big_int.int_of_big_int len_bi)))
                  with
                | _ -> Value.none Type.bytes )
              | _ ->
                  Format.kasprintf
                    failwith
                    "Error while slicing string/bytes: %s"
                    (Printer.texpr_to_string e) ))
        | EMap (_, entries) ->
          ( match Type.getRepr e.et with
          | TMap {big; tkey; tvalue} ->
              Value.map
                ~big
                ~tkey
                ~tvalue
                (List.fold_left
                   (fun entries (key, value) ->
                     let key = interp key in
                     (key, interp value)
                     :: Base.List.Assoc.remove ~equal:Value.equal entries key)
                   []
                   entries)
          | _ ->
              Format.kasprintf
                failwith
                "%s is not a map: %s"
                (Printer.type_to_string e.et)
                (Printer.texpr_to_string e) )
        | EList items -> Value.build (List (List.map interp items)) e.et
        | ESet l ->
          begin
            match Type.getRepr e.et with
            | TSet {telement} -> Value.set ~telement (List.map interp l)
            | _ -> assert false
          end
        | EContract {arg_type; address; entry_point} ->
          ( match (entry_point, interp address) with
          | entry_point, {v = Literal (Literal.Address (addr, None)); vt = _}
           |None, {v = Literal (Literal.Address (addr, entry_point)); vt = _} ->
              begin
                match addr with
                | Real _ -> ()
                | Local id ->
                  begin
                    match
                      (entry_point, Hashtbl.find_opt scenario_state.contracts id)
                    with
                    | _, None -> ()
                    | Some entry_point, Some contract ->
                      ( match
                          Type.getRepr contract.value_tcontract.tparameter
                        with
                      | TVariant {row = [_]} ->
                          raise
                            (SmartExcept
                               [ `Text "Entry point annotation ("
                               ; `Text entry_point
                               ; `Text ") for contract ("
                               ; `Text (Printer.string_of_contract_id id)
                               ; `Text ") with only one entry point."
                               ; `Br
                               ; `Line e.line_no ])
                      | _ -> () )
                    | None, Some contract ->
                      ( match
                          Type.getRepr contract.value_tcontract.tparameter
                        with
                      | TVariant {row = [(_, t)]} ->
                          let before = Printer.type_to_string t in
                          Typing.assertEqual
                            ~line_no:e.line_no
                            ~env:(env e).typing_env
                            arg_type
                            t
                            ~pp:(fun () ->
                              [ `Text "Bad sp.contract type"
                              ; `Type arg_type
                              ; `Type t ]);
                          Solver.apply (env e).typing_env;
                          let after = Printer.type_to_string t in
                          if before <> after
                          then
                            raise
                              (SmartExcept
                                 [ `Text
                                     "Contract type changed during evaluation \
                                      for contract ("
                                 ; `Text (Printer.string_of_contract_id id)
                                 ; `Text ")."
                                 ; `Br
                                 ; `Expr e
                                 ; `Br
                                 ; `Text before
                                 ; `Br
                                 ; `Text "becomes"
                                 ; `Br
                                 ; `Text after
                                 ; `Br
                                 ; `Text "Please set a stable type."
                                 ; `Br
                                 ; `Line e.line_no ])
                      | _ -> () )
                  end
              end;
              Value.some (Value.meta_contract ?entry_point addr arg_type)
          | _, res ->
              Format.kasprintf
                failwith
                "wrong result for EContract: %a"
                Value.pp
                res )
        | ETuple es -> Value.tuple (List.map interp es)
        | ELambda l -> Value.closure_init l
        | ELambdaParams {id; name = _} -> List.assoc id (env e).lambda_params
        | EMake_signature {secret_key; message; message_format} ->
            let secret_key =
              (interp secret_key).v
              |> function
              | Literal (Secret_key k) -> k
              | _ ->
                  raise
                    (SmartExcept
                       [ `Text "Type error in secret key"
                       ; `Expr e
                       ; `Expr secret_key
                       ; `Line e.line_no ])
            in
            let message =
              (interp message).v
              |> function
              | Literal (Bytes k) ->
                ( match message_format with
                | `Raw -> k
                | `Hex ->
                    Misc.Hex.unhex
                      Base.(
                        String.chop_prefix k ~prefix:"0x"
                        |> Option.value ~default:k) )
              | _ ->
                  Format.kasprintf
                    failwith
                    "Type-error: make_signature expects bytes, not: %s"
                    (Printer.texpr_to_string e)
            in
            P.Crypto.sign ~secret_key message |> Value.signature
        | EMichelson _ ->
            failwith
              ( "Interpreter. Instruction not supported in interpreter: "
              ^ Printer.texpr_to_string e )
        | ETransfer {destination; arg; amount} ->
            let arg = interp arg in
            let destination = interp destination in
            let amount = interp amount in
            Value.operation (Transfer {arg; destination; amount})
        | ECreate_contract {baker; contract_template} ->
            let baker = interp baker in
            let contract =
              map_contract_f interp (fun x -> x) (fun x -> x) contract_template
            in
            let dynamic_id =
              !((env e).scenario_state.next_dynamic_address_id)
            in
            incr (env e).scenario_state.next_dynamic_address_id;
            Value.record
              [ ( "operation"
                , Value.operation
                    (CreateContract
                       {id = C_dynamic {dynamic_id}; baker; contract}) )
              ; ("address", Value.meta_address (Local (C_dynamic {dynamic_id})))
              ]
        | EMatch _ -> failwith "TODO: ematch"
        | ESaplingVerifyUpdate {state; transaction} ->
            let pp () = [] in
            let state = interp state in
            let transaction = interp transaction in
            let source, target, amount =
              Value.unSaplingTransaction ~pp transaction
            in
            let output =
              match (source, target) with
              | None, None -> assert false
              | None, Some _ -> Bigint.minus_big_int amount
              | Some _, None -> amount
              | Some _, Some _ -> Bigint.zero_big_int
            in
            let memo, state = Value.unSaplingState ~pp state in
            let state =
              match source with
              | None -> Some state
              | Some source ->
                ( match List.assoc_opt source state with
                | Some x when Bigint.ge_big_int x amount ->
                    Some
                      ( (source, Bigint.sub_big_int x amount)
                      :: List.remove_assoc source state )
                | _ -> None )
            in
            let state =
              match (state, target) with
              | None, _ -> None
              | Some state, None -> Some state
              | Some state, Some target ->
                ( match List.assoc_opt target state with
                | Some x ->
                    Some
                      ( (target, Bigint.add_big_int x amount)
                      :: List.remove_assoc target state )
                | _ -> Some ((target, amount) :: state) )
            in
            ( match state with
            | None ->
                Value.none
                  (Type.pair (Type.int ()) (Type.sapling_state (Some memo)))
            | Some state ->
                Value.some
                  (Value.tuple
                     [ Value.int output
                     ; Value.literal
                         (Literal.sapling_test_state memo state)
                         (Type.sapling_state (Some memo)) ]) ))
  in
  interp

and interpret_expr upper_steps env =
  interpret_expr_
    ~optional_primitives:env.primitives
    upper_steps
    (`Env env)
    env.scenario_state

and path_of_expr ~config upper_steps env =
  let rec of_expr acc e =
    match e.e with
    | EPrim0 (ELocal "__storage__") -> Some {root = R_storage; steps = acc}
    | EPrim0 (ELocal name) -> Some {root = R_local name; steps = acc}
    | EItem {items = cont; key; default_value = None; missing_message = None} ->
        let key = interpret_expr ~config upper_steps env key in
        ( match Type.getRepr cont.et with
        | TMap _ -> of_expr (S_item_map key :: acc) cont
        | _ ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "GetItem"
                 ; `Expr e
                 ; `Expr cont
                 ; `Text "is not a map"
                 ; `Line e.line_no ]) )
    | EPrim1 (EAttr name, expr) -> of_expr (S_attr name :: acc) expr
    | EPrim0 (EIter name) ->
      ( match List.assoc name !(env.variables) with
      | Iter (_, Some p) -> Some (extend_path p acc)
      | Iter (_, None) -> None (* Iteratee not an l-expression. *)
      | _ -> failwith "not an iter" )
    | _ -> None
  in
  of_expr []

and interpret_command
    ~config upper_steps env ({line_no} as initialCommand : tcommand) : tvalue =
  let interpret_command = interpret_command ~config in
  let interpret_expr = interpret_expr ~config in
  let path_of_expr = path_of_expr ~config in
  let pp () = [] in
  match initialCommand.c with
  | CNever message ->
      ignore (interpret_expr upper_steps env message : Value.t);
      failwith "Evaluation of sp.never. It should not happen."
  | CFailwith message ->
      raise
        (SmartExcept
           ( [ `Text "Failure:"
             ; `Text
                 (Printer.value_to_string
                    (interpret_expr upper_steps env message)) ]
           @ ( if Expr.is_constant message
             then []
             else [`Br; `Text "("; `Expr message; `Text ")"] )
           @ [`Br; `Line line_no] ))
  | CIf (c, t, e) ->
      let sub_steps = ref [] in
      let condition = interpret_expr upper_steps env c in
      let r =
        if Value.unBool ~pp condition
        then interpret_command sub_steps env t
        else interpret_command sub_steps env e
      in
      addStep
        ~sub_steps
        upper_steps
        env
        initialCommand
        [("condition", condition)];
      r
  | CMatchCons {expr; id; ok_match; ko_match} ->
    ( match Value.unList ~pp (interpret_expr upper_steps env expr) with
    | [] ->
        let sub_steps = ref [] in
        interpret_command sub_steps env ko_match
    | head :: tail ->
        let sub_steps = ref [] in
        let env =
          { env with
            variables =
              ref
                (add_variable
                   id
                   (Match_list
                      (Value.record
                         [("head", head); ("tail", Value.list tail head.vt)]))
                   !(env.variables)) }
        in
        interpret_command sub_steps env ok_match )
  | CMatchProduct (s, p, c) ->
      let sub_steps = ref [] in
      let variables = !(env.variables) in
      let vs =
        match p with
        | Pattern_single x ->
            let v = interpret_expr upper_steps env s in
            [(x, Variant_arg v)]
        | Pattern_tuple ns ->
            let vs = Value.untuple ~pp (interpret_expr upper_steps env s) in
            List.map2 ~err:"match" (fun n v -> (n, Variant_arg v)) ns vs
        | Pattern_record bs ->
            let r = Value.un_record (interpret_expr upper_steps env s) in
            let f {var; field} =
              (var, Variant_arg (List.assoc_exn ~msg:"match" field r))
            in
            List.map f bs
      in
      let variables = List.fold_right (uncurry add_variable) vs variables in
      let env = {env with variables = ref variables} in
      interpret_command sub_steps env c
  | CModifyProduct (lhs, p, c) ->
    ( match path_of_expr upper_steps env lhs with
    | None ->
        failwith
          (Printf.sprintf
             "Line %s: Cannot assign to '%s'"
             (string_of_line_no initialCommand.line_no)
             (Printer.texpr_to_string lhs))
    | Some lhs_path ->
        let lhs_lens, _err = lens_of_path ~line_no env lhs_path in
        let sub_steps = ref [] in
        let variables = !(env.variables) in
        let vs =
          match p with
          | Pattern_single x ->
              let v = interpret_expr upper_steps env lhs in
              [(x, Heap_ref (ref v))]
          | Pattern_tuple ns ->
              let vs = Value.untuple ~pp (interpret_expr upper_steps env lhs) in
              List.map2 ~err:"match" (fun n v -> (n, Heap_ref (ref v))) ns vs
          | Pattern_record bs ->
              let r = Value.un_record (interpret_expr upper_steps env lhs) in
              let f {var; field} =
                (var, Heap_ref (ref (List.assoc_exn ~msg:"match" field r)))
              in
              List.map f bs
        in
        let variables = List.fold_right (uncurry add_variable) vs variables in
        let env = {env with variables = ref variables} in
        let rhs = Some (interpret_command sub_steps env c) in
        Lens.set lhs_lens rhs ();
        Value.unit )
  | CMatch (scrutinee, cases) ->
      let sub_steps = ref [] in
      let scrutinee = interpret_expr upper_steps env scrutinee in
      ( match scrutinee.v with
      | Variant (cons, arg) ->
          ( match
              List.find_opt
                (fun (constructor, _, _) -> constructor = cons)
                cases
            with
          | None -> ()
          | Some (_constructor, arg_name, body) ->
              assert_unit
                initialCommand
                (interpret_command
                   sub_steps
                   { env with
                     variables =
                       ref
                         (add_variable
                            arg_name
                            (Variant_arg arg)
                            !(env.variables)) }
                   body) );
          addStep
            ~sub_steps
            upper_steps
            env
            initialCommand
            [("match", scrutinee)];
          Value.unit
      | _ -> assert false )
  | CBind (x, c1, c2) ->
      let outer_locals = !(env.current_locals) in
      env.current_locals := [];
      let y = interpret_command upper_steps env c1 in
      ( match x with
      | None -> ()
      | Some x ->
          env.variables := add_variable x (Heap_ref (ref y)) !(env.variables) );
      let r = interpret_command upper_steps env c2 in
      env.variables :=
        List.filter
          (fun (n, _) -> not (List.mem n !(env.current_locals)))
          !(env.variables);
      env.current_locals := outer_locals;
      r
  | CSetVar ({e = EPrim0 (ELocal "__operations__")}, rhs) ->
      let rhs = Some (interpret_expr upper_steps env rhs) in
      ( match rhs with
      | None -> assert false
      | Some rhs ->
          env.operations :=
            List.map (Value.unoperation ~pp) (Value.unList ~pp rhs);
          (* todo add steps  *)
          Value.unit )
  | CSetVar (lhs, rhs) ->
      ( match path_of_expr upper_steps env lhs with
      | None ->
          failwith
            (Printf.sprintf
               "Line %s: Cannot assign to '%s'"
               (string_of_line_no initialCommand.line_no)
               (Printer.texpr_to_string lhs))
      | Some lhs ->
          let lhs, _err = lens_of_path ~line_no env lhs in
          let rhs = Some (interpret_expr upper_steps env rhs) in
          Lens.set lhs rhs () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CDelItem (map, key) ->
      ( match path_of_expr upper_steps env map with
      | None ->
          failwith
            (Printf.sprintf
               "Line %s: Cannot assign to '%s'"
               (string_of_line_no line_no)
               (Printer.texpr_to_string map))
      | Some path ->
          let key = interpret_expr upper_steps env key in
          let l, _err =
            lens_of_path ~line_no env (extend_path path [S_item_map key])
          in
          Lens.set l None () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CUpdateSet (set, elem, add) ->
      ( match path_of_expr upper_steps env set with
      | None ->
          failwith
            (Printf.sprintf
               "Line %s: Cannot remove from '%s'"
               (string_of_line_no line_no)
               (Printer.texpr_to_string set))
      | Some path ->
          let elem = interpret_expr upper_steps env elem in
          let l, err = lens_of_path ~line_no env path in
          Lens.set (l @. Lens.some ~err @. Value.lens_set_at ~elem) add () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CDefineLocal (name, e) ->
      env.current_locals := name :: !(env.current_locals);
      env.variables :=
        add_variable
          name
          (Heap_ref (ref (interpret_expr upper_steps env e)))
          !(env.variables);
      addStep upper_steps env initialCommand [];
      Value.unit
  | CFor (name, iteratee, body) ->
      let sub_steps = ref [] in
      let iteratee_v = interpret_expr upper_steps env iteratee in
      let iteratee_l = path_of_expr upper_steps env iteratee in
      ( match iteratee_v.v with
      | List elems ->
          let step i v =
            let path =
              Base.Option.map iteratee_l ~f:(fun p ->
                  extend_path p [S_item_list i])
            in
            assert_unit
              initialCommand
              (interpret_command
                 sub_steps
                 { env with
                   variables =
                     ref (add_variable name (Iter (v, path)) !(env.variables))
                 }
                 body)
          in
          List.iteri step elems
      | _ -> assert false );
      addStep ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CWhile (e, cmd) ->
      let sub_steps = ref [] in
      while Value.unBool ~pp (interpret_expr upper_steps env e) do
        assert_unit initialCommand (interpret_command sub_steps env cmd)
      done;
      addStep ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CVerify (x, message) ->
      let v = interpret_expr upper_steps env x in
      addStep upper_steps env initialCommand [];
      ( match v.v with
      | Literal (Bool true) -> Value.unit
      | Literal (Bool false) ->
          raise
            (WrongCondition
               ( x
               , line_no
               , Base.Option.map ~f:(interpret_expr upper_steps env) message ))
      | _ ->
          failwith
            (Printf.sprintf
               "Failed condition [%s] in line %s"
               (Printer.value_to_string v)
               (string_of_line_no line_no)) )
  | CResult e -> interpret_expr upper_steps env e
  | CComment _ -> Value.unit
  | CSetType _ -> Value.unit
  | CSetResultType (c, _) -> interpret_command upper_steps env c
  | CTrace e ->
      let e = interpret_expr upper_steps env e in
      print_endline (Printer.value_to_string e);
      Value.unit

and call_lambda ~config upper_steps env lambda parameter =
  let pp () = [] in
  let l, args = Value.unclosure ~pp lambda in
  let parameter =
    List.fold_left (fun acc v -> Value.tuple [v; acc]) parameter args
  in
  let steps = ref [] in
  let env =
    { env with
      operations = ref []
    ; lambda_params = (l.id, parameter) :: env.lambda_params }
  in
  let r = interpret_command ~config steps env l.body in
  addStep
    ~sub_steps:steps
    upper_steps
    env
    l.body
    [("lambda input", parameter); ("lambda output", r)];
  r

let interpret_message
    ~config
    ~primitives
    ~scenario_state
    ~env
    context
    { value_tcontract =
        { balance
        ; storage
        ; tstorage
        ; baker
        ; entry_points
        ; entry_points_layout
        ; tparameter
        ; unknown_parts
        ; global_variables } }
    {channel; params} =
  let interpret_command = interpret_command ~config in
  let filtered_entry_points =
    match
      List.filter
        (fun ({channel = x} : _ entry_point) -> channel = x)
        entry_points
    with
    | [] ->
      ( match (channel, entry_points) with
      | "default", [ep] -> [ep]
      | _ -> [] )
    | l -> l
  in
  let typing_env = env in
  match filtered_entry_points with
  | [] -> (None, [], Some (Execution.Exec_channel_not_found channel), [])
  | _ :: _ :: _ -> failwith "Too many channels"
  | [{channel = dt; paramsType; body = e}] ->
      Typing.assertEqual
        ~line_no:!context.line_no
        ~env
        params.vt
        paramsType
        ~pp:(fun () ->
          [ `Text "Bad params type"
          ; `Value params
          ; `Text "of type"
          ; `Type params.vt
          ; `Br
          ; `Text "is not of the expected type"
          ; `Type paramsType
          ; `Br
          ; `Line !context.line_no ]);

      (* fix ? *)
      let storage =
        match storage with
        | Some storage -> storage
        | None ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "Missing storage in contract"
                 ; `Line !context.line_no ])
      in
      ( match Value.checkType dt storage with
      | Some _ as error -> (None, [], error, [])
      | None ->
          let pp () = [] in
          let env =
            { parameters = params
            ; storage = ref storage
            ; baker = ref baker
            ; variables = ref []
            ; current_locals = ref []
            ; primitives
            ; scenario_state
            ; context = !context
            ; balance =
                { contents =
                    Big_int.add_big_int
                      !context.amount
                      (Value.unMutez ~pp balance) }
            ; operations = {contents = []}
            ; debug = !context.debug
            ; steps = ref []
            ; lambda_params = []
            ; global_variables
            ; typing_env
            ; tparameter
            ; tparameter_ep = paramsType }
          in
          ( try
              let _ = interpret_command env.steps env e in
              ( Some
                  ( { value_tcontract =
                        { balance = Value.mutez !(env.balance)
                        ; storage = Some !(env.storage)
                        ; baker = !(env.baker)
                        ; tstorage
                        ; tparameter
                        ; entry_points
                        ; entry_points_layout
                        ; unknown_parts
                        ; flags = []
                        ; global_variables
                        ; metadata = []
                        ; views = [] } }
                  , e )
              , List.rev !(env.operations)
              , None
              , !(env.steps) )
            with
          | WrongCondition (c, line_no, message) ->
              ( None
              , []
              , Some (Execution.Exec_wrong_condition (c, line_no, message))
              , !(env.steps) )
          | SmartExcept l ->
              (None, [], Some (Execution.Exec_error l), !(env.steps))
          | Failure f ->
              (None, [], Some (Execution.Exec_error [`Text f]), !(env.steps)) )
      )

let interpret_expr_external ~config ~primitives ~no_env ~scenario_state =
  interpret_expr_
    ~config
    ~optional_primitives:primitives
    (ref [])
    (`NoEnv no_env)
    scenario_state

let interpret_contract ~config ~primitives ~scenario_state {tcontract} =
  let conv name =
    interpret_expr_external
      ~config
      ~primitives
      ~no_env:[`Text ("Compute " ^ name)]
      ~scenario_state
  in
  { value_tcontract =
      { tcontract with
        balance = conv "balance" tcontract.balance
      ; storage = Option.map (conv "storage") tcontract.storage
      ; baker = conv "baker" tcontract.baker
      ; metadata =
          List.map (map_snd (map_meta (conv "storage"))) tcontract.metadata } }

let reducer ~config ~primitives ~scenario_state ~line_no x =
  Expr.of_value
    (interpret_expr_external
       ~config
       ~primitives
       ~no_env:[`Text "sp.reduce"; `Expr x; `Br; `Line line_no]
       ~scenario_state
       x)
