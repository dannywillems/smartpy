(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed
open Utils
open Control

type mangle_env =
  { substContractData : (Literal.contract_id, texpr) Hashtbl.t
  ; reducer : line_no:line_no -> texpr -> texpr }

let init_env ?substContractData ~reducer () =
  let substContractData =
    match substContractData with
    | None -> Hashtbl.create 0
    | Some s -> s
  in
  {substContractData; reducer}

let mangle_expr_f env typing_env line_no et = function
  | EPrim1 (EReduce, inner) -> env.reducer ~line_no inner
  | EPrim0 (EContract_data i) as e ->
      let subst = Hashtbl.find_opt env.substContractData i in
      ( match subst with
      | Some x ->
          Typing.assertEqual
            ~line_no:x.line_no
            ~env:typing_env
            et
            x.et
            ~pp:(fun () -> [`Text "Substitution"; `Exprs [{e; et; line_no}; x]]);
          x
      | None -> {e; et; line_no} )
  | e -> {e; et; line_no}

let mangle_talg env typing_env =
  { f_texpr = mangle_expr_f env typing_env
  ; f_tcommand = (fun line_no ct c -> {c; ct; line_no})
  ; f_ttype = (fun t -> t) }

let mangle_expr env typing_env = cata_texpr (mangle_talg env typing_env)

let mangle_command env typing_env = cata_tcommand (mangle_talg env typing_env)

let mangle_entry_point env typing_env (ep : _ entry_point) =
  {ep with body = mangle_command env typing_env ep.body}

let mangle_poly_contract env typing_env c =
  { c with
    entry_points = List.map (mangle_entry_point env typing_env) c.entry_points
  ; global_variables =
      List.map (map_snd (mangle_expr env typing_env)) c.global_variables }

let mangle_contract env typing_env {tcontract} =
  {tcontract = mangle_poly_contract env typing_env tcontract}

let mangle_value_contract env typing_env {value_tcontract} =
  {value_tcontract = mangle_poly_contract env typing_env value_tcontract}
