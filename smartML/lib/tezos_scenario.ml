(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open! Basics
open! Scenario
open! Caml.Format

module Error = struct
  type t = {message : string}

  let make message = {message}

  let pp_quick ppf e = pp_print_text ppf e.message
end

module Decorated_result = struct
  type content =
    [ `Path of string
    | `Text of string
    | `Code of string list
    | `Int of int
    | `Error of Error.t
    | `O of (string * content) list
    ]

  type 'a t =
    { result : ('a, Error.t) Result.t
    ; attach : content list }

  let return o = {result = Ok o; attach = []}

  let fail e = {result = Error e; attach = []}

  let bind x ~f =
    match x.result with
    | Ok o ->
        let rr = f o in
        {result = rr.result; attach = x.attach @ rr.attach}
    | Error _ as e -> {result = e; attach = x.attach}

  let attach ~a x = {x with attach = x.attach @ a}
end

(**
   A generic client for Tezos, to be implemented by tezos-client,
   ConseilJS, or some JSON output, etc..
 *)
module type Tezos_client = sig
  module Io : sig
    type 'a t

    val return : 'a -> 'a t

    val bind : 'a t -> f:('a -> 'b t) -> 'b t
  end

  type state

  val originate :
       state
    -> id:int
    -> contract:string
    -> storage:string
    -> string Decorated_result.t Io.t

  val transfer :
       ?arg:string
    -> ?entry_point:string
    -> ?amount:int
    -> ?sender:Literal.address Basics.account_or_address
    -> ?source:Literal.address Basics.account_or_address
    -> state
    -> dst:string
    -> unit Decorated_result.t Io.t

  val get_contract_storage :
    state -> address:string -> string Decorated_result.t Io.t

  val run_script :
    state -> contract:string -> parameter:string -> unit Decorated_result.t Io.t
end

module History_event = struct
  type t =
    { actions : Basics.taction list
    ; status : [ `Success | `Failure | `None ]
    ; expecting_success : bool
    ; attachements : Decorated_result.content list }

  let make
      ?(actions = [])
      ?(status = `None)
      ?(expecting_success = true)
      ?(attachements = [])
      () =
    {actions; status; expecting_success; attachements}
end

module Make_interpreter (Client : Tezos_client) (Prims : Primitives.Primitives) =
struct
  module IO = Base.Monad.Make (struct
    type 'a t = ('a, Error.t) Result.t Client.Io.t

    let return x = Client.Io.return (Ok x)

    let bind x ~f =
      Client.Io.bind x ~f:(function
          | Ok x -> f x
          | Error x -> Client.Io.return (Error x))

    let map = `Define_using_bind
  end)

  module Decorated_IO = Base.Monad.Make (struct
    type 'a t = 'a Decorated_result.t Client.Io.t

    let return x = Client.Io.return (Decorated_result.return x)

    let bind x ~f =
      Client.Io.bind x ~f:(function
          | Decorated_result.{result = Ok o; attach = a} ->
              Client.Io.bind (f o) ~f:(fun res ->
                  Client.Io.return (Decorated_result.attach ~a res))
          | {result = Error _; attach = _} as e -> Client.Io.return e)

    let map = `Define_using_bind
  end)

  open IO
  module Hashtbl = Caml.Hashtbl
  module Queue = Caml.Queue

  module State = struct
    type t =
      { client : Client.state
      ; smartml : scenario_state
      ; history : History_event.t Queue.t
      ; log_advancement : string -> unit }

    let of_smartml ?(log_advancement = fun _ -> ()) ~client smartml =
      {client; smartml; history = Caml.Queue.create (); log_advancement}

    let fresh ~client () = of_smartml ~client (scenario_state ())

    let add_contract_address {smartml; _} ~id ~address =
      Hashtbl.add smartml.addresses id address

    let get_contract state ~id =
      Option.(
        Hashtbl.find_opt state.smartml.addresses id
        >>= fun kt1 ->
        Hashtbl.find_opt state.smartml.contracts id >>= fun c -> return (kt1, c))

    let add_history state ev = Queue.add ev state.history

    let log_advancement state fmt =
      Caml.Format.kasprintf state.log_advancement fmt
  end

  let primitives = (module Prims : Primitives.Primitives)

  let update_tcontract ~config state ~id ~contract =
    (* Piece of “duplicated” code from `smartML/lib/smartml_scenario.ml`: *)
    let contract =
      match Hashtbl.find_opt state.State.smartml.contracts id with
      | Some contract -> contract
      | None ->
          Interpreter.interpret_contract
            ~config
            ~primitives
            ~scenario_state:state.smartml
            contract
    in
    Hashtbl.replace state.smartml.contracts id contract;

    (* let t = Hashtbl.find state.smartml.contract_data_types id in *)
    contract

  let deal_with_result
      ?(add_attachments = [])
      ?not_valid
      ?result_content
      ?(actions = [])
      state
      making_result =
    let open Decorated_result in
    Client.Io.bind (making_result ()) ~f:(fun result ->
        let ev ~a status =
          History_event.
            { actions
            ; status
            ; expecting_success = Base.Poly.(not_valid = None)
            ; attachements = add_attachments @ result.attach @ a }
        in
        match (result.result, not_valid) with
        | Ok o, None ->
            let a =
              Base.Option.value_map ~default:[] result_content ~f:(fun conv ->
                  conv o)
            in
            State.add_history state (ev ~a `Success);
            IO.return o
        | Ok o, Some _ ->
            let a =
              Base.Option.value_map ~default:[] result_content ~f:(fun conv ->
                  conv o)
            in
            State.add_history state (ev ~a `Failure);
            Client.Io.return (Error (Error.make "Unexpected success"))
        | Error e, None ->
            State.add_history state (ev ~a:[`Error e] `Failure);
            Client.Io.return (Error e)
        | Error e, Some v ->
            State.add_history state (ev ~a:[`Error e] `Success);
            IO.return v)

  (*
module Hack_contract_entry_points = struct
    let allow_fake_blockchain_state (contract : tcontract) ~now ~chain_id =
      List.map contract.entry_points ~f:(fun e ->
          { e with
            paramsType =
              Type.record
                [ ("chain_id", Type.chain_id)
                ; ("now", Type.timestamp)
                ; ("p", e.paramsType) ]
          ; body =
              Command.map_expressions e.body ~f:(function
                  | ENow -> (* get .now field *) ENot ENow
                  | e -> e) })
  end
 *)

  let contract_of_verification_texpr
      ~config ~primitives ~scenario_state ~typing_env condition =
    let substContractData = Hashtbl.create 10 in
    let mangle_env =
      Mangler.init_env
        ~substContractData
        ~reducer:(Interpreter.reducer ~config ~primitives ~scenario_state)
        ()
    in
    let paramsType =
      let fields =
        Utils.List.sort
          compare
          (Hashtbl.fold
             (fun key {value_tcontract = {tstorage}} l ->
               let key = Printer.string_of_contract_id key in
               (sprintf "k%s" key, tstorage) :: l)
             scenario_state.contracts
             [])
      in
      let layout =
        ref (Unknown.UnValue (Type.comb_layout_of_row `Right fields))
      in
      Type.record_or_unit layout fields
    in
    Solver.apply typing_env;
    let params = Expr.params ~line_no:None in
    Hashtbl.iter
      (fun key {value_tcontract = {tstorage}} ->
        let getData =
          Expr.attr
            ~line_no:params.line_no
            params
            (sprintf "k%s" (Printer.string_of_contract_id key))
        in
        Typing.assertEqual
          ~line_no:getData.line_no
          ~env:typing_env
          getData.et
          tstorage
          ~pp:(fun () -> [`Text "Substitution"; `Expr getData; `Type tstorage]);
        Solver.apply typing_env;
        let getData = Closer.close_expr getData in
        Hashtbl.add substContractData key getData)
      scenario_state.contracts;
    let condition = Mangler.mangle_expr mangle_env typing_env condition in
    Solver.apply typing_env;
    let condition = Closer.close_expr condition in
    let entryPoint =
      { channel = "verify"
      ; paramsType
      ; originate = true
      ; body = Command.verify ~line_no:condition.line_no condition None }
    in
    let tparameter = Type.variant_default_layout [("verify", paramsType)] in
    let value_tcontract =
      { balance = Value.mutez Big_int.zero_big_int
      ; tstorage = Type.unit
      ; storage = Some Value.unit
      ; baker = Value.none Type.key_hash
      ; entry_points = [entryPoint]
      ; entry_points_layout = None
      ; tparameter
      ; flags = [Exceptions VerifyOrLine]
      ; global_variables = []
      ; metadata = []
      ; views = []
      ; unknown_parts = None }
    in
    Fixer.fix_value_tcontract config mangle_env typing_env {value_tcontract}

  let handle_action ~config state typing_env action =
    match action with
    | New_contract {id; contract; line_no; accept_unknown_types} ->
        State.log_advancement
          state
          "New contract %s (l. %d)"
          (Printer.string_of_contract_id id)
          (Utils.Option.default (-1) line_no);
        let substContractData = Hashtbl.create 10 in
        let scenario_state =
          scenario_state ()
          (* TODO Propagate this state (within State.t?). *)
        in
        let mangle_env =
          Mangler.init_env
            ~substContractData
            ~reducer:(Interpreter.reducer ~config ~primitives ~scenario_state)
            ()
        in
        let contract =
          Mangler.mangle_contract mangle_env typing_env {tcontract = contract}
        in
        Solver.apply typing_env;
        let contract_full = update_tcontract ~config state ~id ~contract in
        let compiled_contract =
          Compiler.compile_value_tcontract ~config contract_full
        in
        let storage =
          match contract_full.value_tcontract.storage with
          | None -> "missing storage"
          | Some storage ->
              let storage = Compiler.compile_value ~config storage in
              let storage =
                match compiled_contract.lazy_entry_points with
                | None -> storage
                | Some entry_points ->
                    Michelson.MLiteral.pair
                      storage
                      (Michelson.erase_types_literal entry_points)
              in
              Michelson.string_of_literal storage
        in
        if List.length
             (Michelson.has_error_tcontract
                ~accept_missings:false
                compiled_contract)
           <> 0
        then return (assert accept_unknown_types)
        else
          deal_with_result
            ~result_content:(fun s -> [`Text (asprintf "New contract: %s" s)])
            ~actions:[action]
            state
            (fun () ->
              let id =
                match id with
                | Literal.C_static {static_id} -> static_id
                | C_dynamic _ -> assert false
              in
              Client.originate
                state.client
                ~id
                ~contract:(Michelson.display_tcontract compiled_contract)
                ~storage)
          >>= fun address ->
          State.add_contract_address state ~id ~address;
          return ()
    | Set_delegate _ -> return ()
    | Message
        { id
        ; valid
        ; params
        ; line_no
        ; title = _
        ; messageClass = _
        ; sender
        ; source
        ; chain_id = _
        ; time = _
        ; amount
        ; message } ->
        State.log_advancement
          state
          "Calling message %s#%s (l. %d)"
          (Printer.string_of_contract_id id)
          message
          (Utils.Option.default (-1) line_no);
        ( match State.get_contract state ~id with
        | None -> assert false
        | Some (kt1, smartml_contract) ->
            let scenario_state = state.smartml in
            let amount =
              let pp () =
                [`Text "Computing amount"; `Expr amount; `Line amount.line_no]
              in
              Value.unMutez
                ~pp
                (Interpreter.interpret_expr_external
                   ~config
                   ~primitives
                   ~no_env:(pp ())
                   ~scenario_state
                   amount)
            in
            let valid =
              let pp () =
                [`Text "Computing valid"; `Expr valid; `Line valid.line_no]
              in
              Value.unBool
                ~pp
                (Interpreter.interpret_expr_external
                   ~config
                   ~primitives
                   ~no_env:(pp ())
                   ~scenario_state
                   valid)
            in
            let parse_address = function
              | Account x -> Account x
              | Address (address : Basics.Typed.texpr) ->
                  let pp () =
                    [ `Text "Computing address"
                    ; `Expr address
                    ; `Line address.line_no ]
                  in
                  Address
                    (Value.unAddress
                       ~pp
                       (Interpreter.interpret_expr_external
                          ~config
                          ~primitives
                          ~no_env:(pp ())
                          ~scenario_state
                          address))
            in
            let params_type =
              match
                Caml.(
                  List.find_opt
                    (fun x -> (x : _ entry_point).channel = message)
                    smartml_contract.value_tcontract.entry_points)
              with
              | Some x -> x.paramsType
              | None -> assert false
            in
            Typing.assertEqual
              ~line_no
              ~env:typing_env
              params_type
              params.et
              ~pp:(fun () -> assert false);
            let params =
              let tvalue =
                Interpreter.interpret_expr_external
                  ~config
                  ~primitives
                  ~no_env:
                    [ `Text "Computing params"
                    ; `Expr params
                    ; `Line params.line_no ]
                  ~scenario_state
                  params
              in
              Solver.apply typing_env;
              let tvalue =
                let subst vt = function
                  | Literal (Literal.Contract (Local id, entry_point, t)) ->
                    ( match State.get_contract state ~id with
                    | None -> assert false
                    | Some (kt1, _smartml_contract) ->
                        {v = Literal (Literal.contract ?entry_point kt1 t); vt}
                    )
                  | Literal (Literal.Address (Local id, entry_point)) ->
                    ( match State.get_contract state ~id with
                    | None -> assert false
                    | Some (kt1, _smartml_contract) ->
                        {v = Literal (Literal.address ?entry_point kt1); vt} )
                  | v -> {v; vt}
                in
                Value.cata subst tvalue
              in
              let mich = Compiler.compile_value ~config tvalue in
              Michelson.string_of_literal mich
            in
            let entry_point =
              match
                List.length smartml_contract.value_tcontract.entry_points
              with
              | 1 -> None
              | _ -> Some message
            in
            let not_valid = if valid then None else Some () in
            deal_with_result ?not_valid ~actions:[action] state (fun () ->
                Client.transfer
                  state.client
                  ?sender:(Option.map parse_address sender)
                  ?source:(Option.map parse_address source)
                  ~dst:kt1
                  ?entry_point
                  ~amount:(Big_int.int_of_big_int amount)
                  ~arg:params) )
    | Verify {condition; line_no} ->
        State.log_advancement
          state
          "Verifying condition (l. %d)"
          (Utils.Option.default (-1) line_no);
        let scenario_state = state.smartml in
        let contract =
          contract_of_verification_texpr
            ~config
            ~primitives
            ~scenario_state
            ~typing_env
            condition
        in
        let michelson_contract =
          Michelson.display_tcontract
            (Compiler.compile_value_tcontract ~config contract)
        in
        let storage_layout =
          match Type.unF contract.value_tcontract.tparameter with
          | TVariant
              { row =
                  [ ( "verify"
                    , F (TRecord {layout = {contents = UnValue lay}; _}) ) ] }
            ->
              Some lay
          | _ -> None
        in
        (* let michelson_storage = Compiler.michelson_storage contract in *)
        let add_attachments =
          let code s = `Code (Base.String.split ~on:'\n' s) in
          [ `O
              [ ( "debug"
                , `O
                    [ ("condition", code (Printer.texpr_to_string condition))
                    ; ( "layout"
                      , code
                          (Base.Option.value_map
                             ~default:"NONE"
                             ~f:Layout.show
                             storage_layout) ) ] ) ] ]
        in
        deal_with_result
          ~add_attachments
          ~actions:[action]
          state
          Decorated_IO.(
            fun () ->
              let bug s =
                Client.Io.return
                  (Decorated_result.fail (Error.make ("BUG: " ^ s)))
              in
              let get_storage id kt1 prev_m =
                prev_m
                >>= fun prev ->
                Client.get_contract_storage state.client ~address:kt1
                >>= fun res ->
                return
                  (* This has to be the same format as in
                       smartML/lib/smartml_scenario.ml *)
                  ( ( sprintf "k%s" (Printer.string_of_contract_id id)
                    , Base.String.(
                        tr ~target:'\n' ~replacement:' ' res |> strip) )
                  :: prev )
              in
              Hashtbl.fold get_storage state.smartml.addresses (return [])
              >>= fun storages ->
              begin
                match storage_layout with
                | None -> bug "storage-layout-not-set"
                | Some layout ->
                    let get_field f =
                      match
                        Base.List.Assoc.find storages ~equal:String.equal f
                      with
                      | Some s -> return s
                      | None -> kasprintf bug "missing storage: %S" f
                    in
                    let rec make_pairs = function
                      | Binary_tree.Leaf Layout.{target = field} ->
                          get_field field
                      | Binary_tree.Node (l, r) ->
                          make_pairs l
                          >>= fun left ->
                          make_pairs r
                          >>= fun right ->
                          let paren ppf s =
                            match String.contains s ' ' with
                            | true -> fprintf ppf "(%s)" s
                            | false -> fprintf ppf "%s" s
                          in
                          return (asprintf "Pair %a %a" paren left paren right)
                    in
                    make_pairs layout
              end
              >>= fun storages_laid_out ->
              Client.run_script
                state.client
                ~parameter:storages_laid_out
                ~contract:michelson_contract)
    | Compute _ | Simulation _ | ScenarioError _ | Html _ | Show _
     |Exception _ | DynamicContract _ | Add_flag _ ->
        let () =
          let status =
            match action with
            | Compute _ | ScenarioError _ | Exception _ | DynamicContract _ ->
                `Failure
            | _ -> `None
          in
          State.add_history
            state
            History_event.(make ~status ~actions:[action] ())
        in
        return ()

  let run ~config state typing_env {actions} =
    Base.List.fold actions ~init:(return ()) ~f:(fun prevm action ->
        prevm >>= fun () -> handle_action ~config state typing_env action)
end
