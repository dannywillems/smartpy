(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Untyped

type t = expr [@@deriving show]

type unary_expr = line_no:line_no -> t -> t

type bin_expr = line_no:line_no -> t -> t -> t

val add : bin_expr

val mul : bin_expr

val mul_overloaded : bin_expr

val e_mod : bin_expr

val ediv : bin_expr

val div : bin_expr

val sub : bin_expr

val xor : bin_expr

val le : bin_expr

val lt : bin_expr

val ge : bin_expr

val gt : bin_expr

val eq : bin_expr

val neq : bin_expr

val b_or : bin_expr

val b_and : bin_expr

val e_max : bin_expr

val e_min : bin_expr

val storage : line_no:line_no -> t

val operations : line_no:line_no -> t

val attr : line_no:line_no -> t -> string -> t

val variant : line_no:line_no -> string -> t -> t

val isVariant : line_no:line_no -> string -> t -> t

val variant_arg : line_no:line_no -> string -> t

val openVariant : line_no:line_no -> string -> t -> t option -> t

val updateMap : t -> t -> t -> line_no:line_no -> t

val params : line_no:line_no -> t

val local : line_no:line_no -> string -> t

val global : string -> Literal.static_id -> line_no:line_no -> t

val item : line_no:line_no -> t -> t -> t option -> t option -> t

val contains : bin_expr

val sum : unary_expr

val range : line_no:line_no -> t -> t -> t -> t

val cons : bin_expr

val cst : line_no:line_no -> Literal.t -> t

val unit : t

val type_annotation : line_no:line_no -> t -> Type.t -> t

val strip_type_annotations : t -> t

val record : line_no:line_no -> (string * t) list -> t

val build_list : line_no:line_no -> elems:t list -> t

val build_map : line_no:line_no -> big:bool -> entries:(t * t) list -> t

val build_set : line_no:line_no -> entries:t list -> t

val hash_key : line_no:line_no -> t -> t

val hashCrypto : line_no:line_no -> hash_algo -> t -> t

val pack : t -> line_no:line_no -> t

val unpack : line_no:line_no -> t -> Type.t -> t

val check_signature : line_no:line_no -> t -> t -> t -> t

val account_of_seed : seed:string -> line_no:line_no -> t

val make_signature :
     line_no:line_no
  -> secret_key:t
  -> message:t
  -> message_format:[ `Hex | `Raw ]
  -> t

val scenario_var : line_no:line_no -> int -> Type.t -> t

val reduce : line_no:line_no -> t -> t

val split_tokens : line_no:line_no -> t -> t -> t -> t

val now : t

val chain_id : t

val add_seconds : bin_expr

val notE : unary_expr

val absE : unary_expr

val to_int : unary_expr

val is_nat : unary_expr

val negE : unary_expr

val signE : unary_expr

val slice : line_no:line_no -> offset:t -> length:t -> buffer:t -> t

val concat_list : line_no:line_no -> t -> t

val size : line_no:line_no -> t -> t

val iterator : line_no:line_no -> string -> t

val match_cons : line_no:line_no -> string -> t

val balance : t

val sender : t

val source : t

val amount : t

val self : t

val total_voting_power : t

val self_entry_point : line_no:line_no -> string -> t

val contract_address : line_no:line_no -> t -> t

val implicit_account : unary_expr

val voting_power : unary_expr

val listRev : line_no:line_no -> t -> t

val listItems : line_no:line_no -> t -> bool -> t

val listKeys : line_no:line_no -> t -> bool -> t

val listValues : line_no:line_no -> t -> bool -> t

val listElements : line_no:line_no -> t -> bool -> t

val contract : line_no:line_no -> string option -> Type.t -> t -> t

val tuple : line_no:line_no -> t list -> t

val proj : line_no:line_no -> int -> t -> t

val first : line_no:line_no -> t -> t

val second : line_no:line_no -> t -> t

val none : line_no:line_no -> t

val some : unary_expr

val left : unary_expr

val right : unary_expr

val inline_michelson :
  line_no:line_no -> Type.t inline_michelson list -> t list -> t

val map_function : bin_expr

val call_lambda : bin_expr

val apply_lambda : bin_expr

val lambda :
  line_no:line_no -> int -> string -> Type.t -> Type.t -> command -> bool -> t

val create_contract : line_no:line_no -> baker:t -> contract -> t

val lambdaParams : line_no:line_no -> int -> string -> t

val lshift : bin_expr

val rshift : bin_expr

val transfer :
  line_no:line_no -> arg:expr -> amount:expr -> destination:expr -> t

val set_delegate : line_no:line_no -> expr -> t

val contract_data : line_no:line_no -> Literal.contract_id -> t

val ematch : line_no:line_no -> t -> (string * t) list -> t

val contract_balance : line_no:line_no -> Literal.contract_id -> t

val contract_baker : line_no:line_no -> Literal.contract_id -> t

val eif : line_no:line_no -> t -> t -> t -> t

val allow_lambda_full_stack : t -> t

val sapling_empty_state : int -> t

val sapling_verify_update : line_no:line_no -> t -> t -> t

val level : t

val test_ticket : line_no:line_no -> Literal.contract_id -> t -> t -> t

val build_ticket : line_no:line_no -> t -> t -> t

val read_ticket : line_no:line_no -> t -> t

val split_ticket : line_no:line_no -> t -> t -> t

val join_tickets : line_no:line_no -> t -> t

val pairing_check : line_no:line_no -> t -> t

val get_and_update : line_no:line_no -> t -> t -> t -> t

val getOpt : line_no:line_no -> t -> t -> t

val of_value : tvalue -> t
