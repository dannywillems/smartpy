(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed

type t = texpr [@@deriving show {with_path = false}]

type unary_expr = line_no:line_no -> t -> t

type bin_expr = line_no:line_no -> t -> t -> t

let build ~line_no e = {e; et = Type.full_unknown (); line_no}

let build_with_type ~line_no e et = {e; et; line_no}

let attr ~line_no x name = build ~line_no (EPrim1 (EAttr name, x))

let variant ~line_no name x = build ~line_no (EPrim1 (EVariant name, x))

let isVariant ~line_no name x = build ~line_no (EPrim1 (EIsVariant name, x))

let openVariant ~line_no name x missing_message =
  build ~line_no (EOpenVariant (name, x, missing_message))

let params ~line_no = build ~line_no (EPrim0 (ELocal "__parameter__"))

let local ~line_no n = build ~line_no (EPrim0 (ELocal n))

let operations ~line_no = local ~line_no "__operations__"

let item ~line_no items key default_value missing_message =
  build ~line_no (EItem {items; key; default_value; missing_message})

let cons ~line_no x l = build ~line_no (EPrim2 (ECons, x, l))

let cst ~line_no x = build ~line_no (EPrim0 (ECst x))

let unit = cst ~line_no:None Literal.unit

let type_annotation ~line_no e t =
  build ~line_no (EPrim1 (EType_annotation t, e))

let rec strip_type_annotations = function
  | {e = EPrim1 (EType_annotation _, e)} -> strip_type_annotations e
  | e -> e

let build_list ~line_no ~elems = build ~line_no (EList elems)

let test_ticket ~line_no ticketer content amount =
  build ~line_no (EPrim2 (ETest_ticket ticketer, content, amount))

let build_set ~line_no ~entries = build ~line_no (ESet entries)

let now = build ~line_no:None (EPrim0 ENow)

let slice ~line_no ~offset ~length ~buffer =
  build ~line_no (ESlice {offset; length; buffer})

let concat_list ~line_no l = build ~line_no (EPrim1 (EConcat_list, l))

let size ~line_no s = build ~line_no (EPrim1 (ESize, s))

let contract ~line_no entry_point arg_type address =
  build ~line_no (EContract {entry_point; arg_type; address})

let tuple ~line_no es = build ~line_no (ETuple es)

let none ~line_no = variant ~line_no "None" unit

let apply_lambda ~line_no lambda parameter =
  build ~line_no (EPrim2 (EApplyLambda, lambda, parameter))

let lambda ~line_no id name tParams tResult body clean_stack =
  build ~line_no (ELambda {id; name; tParams; body; tResult; clean_stack})

let rec of_value v : texpr =
  let r =
    match v.v with
    | Literal l -> cst ~line_no:None l
    | Record entries ->
        build_with_type
          ~line_no:None
          (ERecord (entries |> List.map (fun (fld, l) -> (fld, of_value l))))
          v.vt
    | Variant (lbl, arg) ->
        build_with_type ~line_no:None (EPrim1 (EVariant lbl, of_value arg)) v.vt
    | List elems ->
        build_with_type ~line_no:None (EList (List.map of_value elems)) v.vt
    | Set elems -> build_set ~line_no:None ~entries:(List.map of_value elems)
    | Map entries ->
        let big = Type.is_bigmap v.vt in
        build_with_type
          ~line_no:None
          (EMap
             (big, entries |> List.map (fun (k, v) -> (of_value k, of_value v))))
          v.vt
    | Tuple vs -> tuple ~line_no:None (List.map of_value vs)
    | Closure ({id; name; tParams; tResult; body}, args) ->
        List.fold_left
          (fun f arg -> apply_lambda ~line_no:None f (of_value arg))
          (lambda ~line_no:None id name tParams tResult body true)
          args
    | Operation _ -> failwith "TODO expr.of_value Operation"
    | Ticket (ticketer, content, amount) ->
      ( match ticketer with
      | Local ticketer ->
          let content = of_value content in
          let amount = cst ~line_no:None (Literal.int amount) in
          test_ticket ~line_no:None ticketer content amount
      | _ -> failwith "cannot handle non-local or dynamic local ticketer" )
  in
  {r with et = v.vt}

let is_constant e =
  match e.e with
  | EPrim0 (ECst _) -> true
  | _ -> false

let transfer ~line_no ~arg ~amount ~destination =
  build ~line_no (ETransfer {arg; amount; destination})
