(* Copyright 2019-2021 Smart Chain Arena LLC. *)

val decompile_contract :
     Michel.Transformer.state
  -> Michelson.tcontract
  -> Michel.Expr.expr Michel.Expr.precontract
