(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

let fix_value_tcontract config mangle_env typing_env c =
  let c = Mangler.mangle_value_contract mangle_env typing_env c in
  let c, _, _ = Checker.check_value_tcontract config c in
  Closer.close_value_contract c

let fix_contract config mangle_env typing_env c =
  let c, _, _ = Checker.check_contract config c in
  let c = Mangler.mangle_contract mangle_env typing_env c in
  Solver.apply typing_env;
  let c = Closer.close_contract c in
  let c =
    match c.tcontract.storage with
    | None -> c
    | Some s ->
        let s = Closer.close_expr s in
        let c = {tcontract = {c.tcontract with storage = Some s}} in
        Closer.close_contract c
  in
  {tcontract = {c.tcontract with unknown_parts = None}}
