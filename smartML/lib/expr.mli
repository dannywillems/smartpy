(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed

type t = texpr [@@deriving show]

type unary_expr = line_no:line_no -> t -> t

type bin_expr = line_no:line_no -> t -> t -> t

val operations : line_no:line_no -> t

val attr : line_no:line_no -> t -> string -> t

val isVariant : line_no:line_no -> string -> t -> t

val openVariant : line_no:line_no -> string -> t -> t option -> t

val params : line_no:line_no -> t

val local : line_no:line_no -> string -> t

val item : line_no:line_no -> t -> t -> t option -> t option -> t

val cons : bin_expr

val cst : line_no:line_no -> Literal.t -> t

val unit : t

val type_annotation : line_no:line_no -> t -> Type.t -> t

val strip_type_annotations : t -> t

val build_list : line_no:line_no -> elems:t list -> t

val now : t

val slice : line_no:line_no -> offset:t -> length:t -> buffer:t -> t

val concat_list : line_no:line_no -> t -> t

val size : line_no:line_no -> t -> t

val contract : line_no:line_no -> string option -> Type.t -> t -> t

val none : line_no:line_no -> t

val of_value : tvalue -> t

val is_constant : t -> bool

val transfer :
  line_no:line_no -> arg:texpr -> amount:texpr -> destination:texpr -> t
