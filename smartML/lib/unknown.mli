(* Copyright 2019-2021 Smart Chain Arena LLC. *)

type 'a t =
  | UnUnknown of string
  | UnValue   of 'a
  | UnRef     of 'a t ref
[@@deriving eq, show, ord]

type 'a t_ref = 'a t ref [@@deriving eq, show, ord]

val getRef : 'a t ref -> 'a t ref

val getRefOption : 'a t ref -> 'a option

val unknown : unit -> 'a t ref

val of_option : 'a option -> 'a t ref
