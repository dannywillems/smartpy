(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics.Untyped

val embellish : command -> command
