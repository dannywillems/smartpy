(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils

let export_layout layout =
  match Unknown.getRefOption layout with
  | None -> "None"
  | Some layout ->
      let leaf Layout.{source; target} =
        if source = target
        then Printf.sprintf "(***%s***)" source
        else Printf.sprintf "(***%s as %s***)" source target
      in
      let node l1 l2 = Printf.sprintf "(%s %s)" l1 l2 in
      Printf.sprintf "(Some %s)" (Binary_tree.cata leaf node layout)

let rec export_type t =
  match Type.getRepr t with
  | TBool -> "bool"
  | TString -> "string"
  | TInt {isNat} ->
    ( match Unknown.getRefOption isNat with
    | None -> "intOrNat"
    | Some true -> "nat"
    | Some false -> "int" )
  | TTimestamp -> "timestamp"
  | TBytes -> "bytes"
  | TRecord {row; layout} ->
      Printf.sprintf
        "(record (%s) %s)"
        (String.concat
           " "
           (List.map
              (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
              row))
        (export_layout layout)
  | TVariant {row; layout} ->
      Printf.sprintf
        "(variant (%s) %s)"
        (String.concat
           " "
           (List.map
              (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
              row))
        (export_layout layout)
  | TSet {telement} -> Printf.sprintf "(set %s)" (export_type telement)
  | TMap {tkey; tvalue} ->
      Printf.sprintf "(map %s %s)" (export_type tkey) (export_type tvalue)
      (* TODO big ? *)
  | TToken -> "mutez"
  | TUnit -> "unit"
  | TAddress -> "address"
  | TContract t -> Printf.sprintf "(contract %s)" (export_type t)
  | TKeyHash -> "key_hash"
  | TBakerHash -> "baker_hash"
  | TKey -> "key"
  | TSecretKey -> "secret_key"
  | TChainId -> "chain_id"
  | TSignature -> "signature"
  | TUnknown {contents = UExact t} -> export_type t
  | TUnknown {contents = UUnknown _} -> "unknown"
  | TUnknown {contents = URecord row} ->
      export_type (Type.record_default_layout row)
  | TUnknown {contents = UTuple _} -> failwith "export_type: TODO UTuple"
  | TUnknown {contents = UVariant row} ->
      export_type (Type.variant_default_layout row)
  | TTuple ts ->
      let ts = List.map export_type ts in
      Printf.sprintf "(tuple %s)" (String.concat " " ts)
  | TList t -> Printf.sprintf "(list %s)" (export_type t)
  | TLambda (t1, t2) ->
      Printf.sprintf "(lambda %s %s)" (export_type t1) (export_type t2)
  | TOperation -> "operation"
  | TSaplingState {memo} ->
      let memo =
        match Unknown.getRefOption memo with
        | None -> "unknown"
        | Some i -> string_of_int i
      in
      Printf.sprintf "(sapling_state %s)" memo
  | TSaplingTransaction {memo} ->
      let memo =
        match Unknown.getRefOption memo with
        | None -> "unknown"
        | Some i -> string_of_int i
      in
      Printf.sprintf "(sapling_transaction %s)" memo
  | TNever -> "never"
  | TTicket t -> Printf.sprintf "(ticket %s)" (export_type t)
  | TBls12_381_g1 -> "bls12_381_g1"
  | TBls12_381_g2 -> "bls12_381_g2"
  | TBls12_381_fr -> "bls12_381_fr"
