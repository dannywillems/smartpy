(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Utils

type env =
  { primitives : (module Primitives.Primitives)
  ; scenario_state : scenario_state
  ; entry_point : (string * Type.t) option }

let attribute_source name =
  match Base.String.split (Base.String.strip name) ~on:' ' with
  | [name] -> name
  | [name; "as"; _] -> name
  | [name; "as"] -> name
  | _ -> Printf.ksprintf failwith "Bad attribute format %S" name

let attribute_target name =
  match Base.String.split (Base.String.strip name) ~on:' ' with
  | [name] -> name
  | [_; "as"; target] -> target
  | [_; "as"] -> ""
  | _ -> Printf.ksprintf failwith "Bad attribute format %S" name

let rec import_inner_layout = function
  | Base.Sexp.List [Atom leaf] ->
      Layout.leaf (attribute_source leaf) (attribute_target leaf)
  | Base.Sexp.List [n1; n2] ->
      Binary_tree.node (import_inner_layout n1) (import_inner_layout n2)
  | l -> failwith ("Layout format error " ^ Base.Sexp.to_string l)

let import_layout l layout =
  match l with
  | [] -> ref (Unknown.UnUnknown "")
  | _ ->
      let layout =
        match layout with
        | Base.Sexp.Atom "None" -> Type.default_layout_of_row l
        | List [Atom "Some"; Atom "Right"] -> Type.comb_layout_of_row `Right l
        | List [Atom "Some"; l] -> import_inner_layout l
        | l -> failwith ("Layout format error " ^ Base.Sexp.to_string l)
      in
      ref (Unknown.UnValue layout)

let rec assocList (n : string) = function
  | Base.Sexp.Atom (a : string) :: List l :: _ when Stdlib.(a = n) -> l
  | _ :: l -> assocList n l
  | _ -> failwith ("Cannot find " ^ n)

let default_line_no_f =
  let id x = x in
  let f_expr line_no e default_line_no =
    let line_no = if line_no == None then default_line_no else line_no in
    let e = map_expr_f (fun e -> e line_no) id id e in
    {Untyped.line_no; e}
  in
  let f_command line_no c =
    let c = map_command_f (fun e -> e line_no) id id c in
    {Untyped.line_no; c}
  in
  let f_type = id in
  {f_expr; f_command; f_type}

let _default_line_no_expr e line_no = cata_expr default_line_no_f e line_no

let default_line_no_command e = cata_command default_line_no_f e

let int_option_of_string = function
  | "None" -> None
  | l -> Some (int_of_string l)

let import_line_no = int_option_of_string

let import_bool = function
  | "True" -> true
  | "False" -> false
  | x -> failwith ("import_bool: " ^ x)

let unAtom = function
  | Base.Sexp.Atom n -> n
  | _ -> failwith "unAtom"

let rec import_flags =
  let open Base.Sexp in
  function
  | [] -> []
  | List x :: xs ->
      let x = List.map unAtom x in
      ( match Config.parse_flag x with
      | Some (x, []) -> x :: import_flags xs
      | _ -> failwith ("invalid flag usage: " ^ String.concat ", " x) )
  | xs -> failwith ("import_flags: " ^ to_string (List xs))

let import_binding = function
  | Base.Sexp.List [Atom "binding"; Atom var; Atom field] -> {var; field}
  | x -> failwith ("import_binding: " ^ Base.Sexp.to_string x)

let import_type typing_env =
  let open Type in
  let rec import_type = function
    | Base.Sexp.Atom "unit" -> unit
    | Atom "bool" -> bool
    | Atom "int" -> int ()
    | Atom "nat" -> nat ()
    | Atom "intOrNat" -> intOrNat ()
    | Atom "string" -> string
    | Atom "bytes" -> bytes
    | Atom "chain_id" -> chain_id
    | Atom "mutez" -> token
    | Atom "timestamp" -> timestamp
    | Atom "address" -> address
    | Atom "key_hash" -> key_hash
    | Atom "baker_hash" -> baker_hash
    | Atom "key" -> key
    | Atom "signature" -> signature
    | Atom "operation" -> operation
    | Atom "bls12_381_g1" -> bls12_381_g1
    | Atom "bls12_381_g2" -> bls12_381_g2
    | Atom "bls12_381_fr" -> bls12_381_fr
    | List [Atom "sapling_state"; Atom memo_size] ->
        sapling_state (int_option_of_string memo_size)
    | List [Atom "sapling_transaction"; Atom memo_size] ->
        sapling_transaction (int_option_of_string memo_size)
    | Atom "never" -> never
    | List [Atom "unknown"; Atom id] -> Typing.unknown typing_env ("sp:" ^ id)
    | List [Atom "record"; List l; layout] ->
        let l = List.map importField l in
        record_or_unit (import_layout l layout) l
    | List [Atom "variant"; List l; layout] ->
        let l = List.map importField l in
        variant (import_layout l layout) l
    | List [Atom "list"; t] -> list (import_type t)
    | List [Atom "ticket"; t] -> ticket (import_type t)
    | List [Atom "option"; t] -> option (import_type t)
    | List [Atom "contract"; t] -> contract (import_type t)
    | List [Atom "tuple"; t1; t2] -> pair (import_type t1) (import_type t2)
    | List [Atom "set"; t] -> set ~telement:(import_type t)
    | List [Atom "map"; k; v] ->
        map
          ~big:(ref (Unknown.UnValue false))
          ~tkey:(import_type k)
          ~tvalue:(import_type v)
    | List [Atom "bigmap"; k; v] ->
        map
          ~big:(ref (Unknown.UnValue true))
          ~tkey:(import_type k)
          ~tvalue:(import_type v)
    | List [Atom "lambda"; t1; t2] -> lambda (import_type t1) (import_type t2)
    | List l as t ->
        failwith
          ( "Type format error list "
          ^ Base.Sexp.to_string t
          ^ "  "
          ^ string_of_int (List.length l) )
    | Atom _ as t -> failwith ("Type format error atom " ^ Base.Sexp.to_string t)
  and importField = function
    | List [Atom name; v] -> (name, import_type v)
    | l -> failwith ("Type field format error " ^ Base.Sexp.to_string l)
  in
  import_type

let import_contract_id =
  let open Literal in
  function
  | Base.Sexp.List [Atom "static_id"; Atom id; _] ->
      Literal.C_static {static_id = int_of_string id}
  | Base.Sexp.List [Atom "dynamic_id"; Atom id; _] ->
      Literal.C_dynamic {dynamic_id = int_of_string id}
  | l -> Format.kasprintf failwith "Contract_id format error: %a" Base.Sexp.pp l

let rec import_literal (typing_env : Typing.env) =
  let open Literal in
  function
  | [Base.Sexp.Atom "unit"] -> unit
  | [Atom "string"; Atom n] -> string n
  | [Atom "bytes"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      bytes (Misc.Hex.unhex n)
  | [Atom "bls12_381_g1"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      bls12_381_g1 (Misc.Hex.unhex n)
  | [Atom "bls12_381_g2"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      bls12_381_g2 (Misc.Hex.unhex n)
  | [Atom "bls12_381_fr"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      bls12_381_fr (Misc.Hex.unhex n)
  | [Atom "chain_id_cst"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      chain_id (Misc.Hex.unhex n)
  | [Atom "int"; Atom n] -> int (Big_int.big_int_of_string n)
  | [Atom "intOrNat"; Atom n] ->
      intOrNat (Type.intOrNat ()) (Big_int.big_int_of_string n)
  | [Atom "nat"; Atom n] -> nat (Big_int.big_int_of_string n)
  | [Atom "timestamp"; Atom n] -> timestamp (Big_int.big_int_of_string n)
  | [Atom "bool"; Atom "True"] -> bool true
  | [Atom "bool"; Atom "False"] -> bool false
  | [Atom "key"; Atom n] -> key n
  | [Atom "secret_key"; Atom n] -> secret_key n
  | [Atom "signature"; Atom n] -> signature n
  | [Atom "address"; Atom n] -> address n
  | [Atom "local-address"; id] -> local_address (import_contract_id id)
  | [Atom "local-contract-typed"; id] ->
      let contract_id = import_contract_id id in
      local_contract contract_id (Type.full_unknown ())
  | [Atom "key_hash"; Atom n] -> key_hash n
  | [Atom "baker_hash"; Atom n] -> baker_hash n
  | [Atom "mutez"; List l] ->
      mutez (Base.Option.value_exn (unInt (import_literal typing_env l)))
  | [Atom "mutez"; Atom n] -> mutez (Big_int.big_int_of_string n)
  | [ Atom "sapling_test_transaction"
    ; Atom memo
    ; Atom source
    ; Atom target
    ; Atom amount
    ; _ ] ->
      let source = if source = "" then None else Some source in
      let target = if target = "" then None else Some target in
      sapling_test_transaction
        (int_of_string memo)
        source
        target
        (Big_int.big_int_of_string amount)
  | l ->
      Format.kasprintf failwith "Literal format error: %a" Base.Sexp.pp (List l)

and import_expr_inline_michelson typing_env =
  let import_type t = import_type typing_env t in
  let rec import_expr_inline_michelson = function
    | Base.Sexp.List [Atom "call_michelson"; instr; Atom _line_no] ->
        import_expr_inline_michelson instr
    | Base.Sexp.List (Atom "op" :: Atom name :: args) ->
        let parsed = Micheline_encoding.parse_node name in
        let rec extractTypes acc = function
          | [] -> assert false
          | Base.Sexp.Atom "out" :: out ->
              (List.rev acc, List.map import_type out)
          | t :: args -> extractTypes (import_type t :: acc) args
        in
        let typesIn, typesOut = extractTypes [] args in
        {name; parsed; typesIn; typesOut}
    | input ->
        failwith
          (Printf.sprintf
             "Cannot parse inline michelson %s"
             (Base.Sexp.to_string input))
  in
  import_expr_inline_michelson

and import_expr (typing_env : Typing.env) (import_env : env) =
  let import_type t = import_type typing_env t in
  let import_expr_inline_michelson = import_expr_inline_michelson typing_env in
  let open Expr_untyped in
  let rec import_expr = function
    | Base.Sexp.List (Atom f :: args) as input ->
      ( match (f, args) with
      | "sender", [] -> sender
      | "self", [] -> self
      | "self_entry_point", [Atom name; Atom line_no] ->
          let name =
            if name = ""
            then
              match import_env.entry_point with
              | None -> failwith "import: params type yet unknown"
              | Some (name, _) -> name
            else name
          in
          self_entry_point ~line_no:(import_line_no line_no) name
      | "chain_id", [] -> chain_id
      | "source", [] -> source
      | "now", [] -> now
      | "amount", [] -> amount
      | "level", [] -> level
      | "balance", [] -> balance
      | "total_voting_power", [] -> total_voting_power
      | "eq", [e1; e2; Atom line_no] ->
          eq ~line_no:(import_line_no line_no) (import_expr e1) (import_expr e2)
      | "neq", [e1; e2; Atom line_no] ->
          neq
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "le", [e1; e2; Atom line_no] ->
          le ~line_no:(import_line_no line_no) (import_expr e1) (import_expr e2)
      | "lt", [e1; e2; Atom line_no] ->
          lt ~line_no:(import_line_no line_no) (import_expr e1) (import_expr e2)
      | "ge", [e1; e2; Atom line_no] ->
          ge ~line_no:(import_line_no line_no) (import_expr e1) (import_expr e2)
      | "gt", [e1; e2; Atom line_no] ->
          gt ~line_no:(import_line_no line_no) (import_expr e1) (import_expr e2)
      | "add", [e1; e2; Atom line_no] ->
          add
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "sub", [e1; e2; Atom line_no] ->
          sub
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "mul", [e1; e2; Atom line_no] ->
          mul
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "mul_overloaded", [e1; e2; Atom line_no] ->
          mul_overloaded
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "ediv", [e1; e2; Atom line_no] ->
          ediv
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "truediv", [e1; e2; Atom line_no] ->
          div
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "floordiv", [e1; e2; Atom line_no] ->
          div
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "mod", [e1; e2; Atom line_no] ->
          e_mod
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "or", [e1; e2; Atom line_no] ->
          b_or
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "and", [e1; e2; Atom line_no] ->
          b_and
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "max", [e1; e2; Atom line_no] ->
          e_max
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "min", [e1; e2; Atom line_no] ->
          e_min
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "sum", [a; Atom line_no] ->
          sum ~line_no:(import_line_no line_no) (import_expr a)
      | "to_address", [e; Atom line_no] ->
          contract_address ~line_no:(import_line_no line_no) (import_expr e)
      | "implicit_account", [e; Atom line_no] ->
          implicit_account ~line_no:(import_line_no line_no) (import_expr e)
      | "cons", [e1; e2; Atom line_no] ->
          cons
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "range", [e1; e2; e3; Atom line_no] ->
          range
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
            (import_expr e3)
      | "literal", [List l; Atom line_no] ->
          cst ~line_no:(import_line_no line_no) (import_literal typing_env l)
      | "list", Atom line_no :: l ->
          build_list
            ~line_no:(import_line_no line_no)
            ~elems:(List.map import_expr l)
      | "test_ticket", [ticketer; content; amount; Atom line_no] ->
          let ticketer = import_contract_id ticketer in
          test_ticket
            ~line_no:(import_line_no line_no)
            ticketer
            (import_expr content)
            (import_expr amount)
      | "ticket", [content; amount; Atom line_no] ->
          build_ticket
            ~line_no:(import_line_no line_no)
            (import_expr content)
            (import_expr amount)
      | "read_ticket", [ticket; Atom line_no] ->
          read_ticket ~line_no:(import_line_no line_no) (import_expr ticket)
      | "split_ticket", [ticket; decomposition; Atom line_no] ->
          split_ticket
            ~line_no:(import_line_no line_no)
            (import_expr ticket)
            (import_expr decomposition)
      | "join_tickets", [tickets; Atom line_no] ->
          join_tickets ~line_no:(import_line_no line_no) (import_expr tickets)
      | "pairing_check", [pairs; Atom line_no] ->
          pairing_check ~line_no:(import_line_no line_no) (import_expr pairs)
      | "voting_power", [e; Atom line_no] ->
          voting_power ~line_no:(import_line_no line_no) (import_expr e)
      | "first", [e; Atom line_no] ->
          first ~line_no:(import_line_no line_no) (import_expr e)
      | "second", [e; Atom line_no] ->
          second ~line_no:(import_line_no line_no) (import_expr e)
      | "tuple", Atom line_no :: es ->
          tuple ~line_no:(import_line_no line_no) (List.map import_expr es)
      | "tuple", _ ->
          failwith
            (Printf.sprintf
               "Only supported tuples are pairs: %s"
               (Base.Sexp.to_string input))
      | "neg", [e1; Atom line_no] ->
          negE ~line_no:(import_line_no line_no) (import_expr e1)
      | "abs", [e1; Atom line_no] ->
          absE ~line_no:(import_line_no line_no) (import_expr e1)
      | "toInt", [e1; Atom line_no] ->
          to_int ~line_no:(import_line_no line_no) (import_expr e1)
      | "isNat", [e1; Atom line_no] ->
          is_nat ~line_no:(import_line_no line_no) (import_expr e1)
      | "sign", [e1; Atom line_no] ->
          signE ~line_no:(import_line_no line_no) (import_expr e1)
      | "invert", [e1; Atom line_no] ->
          notE ~line_no:(import_line_no line_no) (import_expr e1)
      | "contractBalance", [id; Atom line_no] ->
          let line_no = import_line_no line_no in
          contract_balance ~line_no (import_contract_id id)
      | "contract_baker", [id; Atom line_no] ->
          let line_no = import_line_no line_no in
          contract_baker ~line_no (import_contract_id id)
      | "contractData", [id; Atom line_no] ->
          let line_no = import_line_no line_no in
          contract_data ~line_no (import_contract_id id)
      | "contract", [Atom entry_point; t; addr; Atom line_no] ->
          let entry_point =
            match entry_point with
            | "" -> None
            | _ -> Some entry_point
          in
          let address = import_expr addr in
          contract
            ~line_no:(import_line_no line_no)
            entry_point
            (import_type t)
            address
      | "data", [] -> local ~line_no:None "__storage__"
      | "operations", [Atom line_no] ->
          operations ~line_no:(import_line_no line_no)
      | "attr", [x; Atom name; Atom line_no] ->
          attr ~line_no:(import_line_no line_no) (import_expr x) name
      | "match_cons", [_e; Atom line_no] ->
          let name = Printf.sprintf "match_cons_%s" line_no in
          match_cons ~line_no:(import_line_no line_no) name
      | "isVariant", [x; Atom name; Atom line_no] ->
          isVariant ~line_no:(import_line_no line_no) name (import_expr x)
      | "variant_arg", [Atom arg_name; Atom line_no] ->
          variant_arg ~line_no:(import_line_no line_no) arg_name
      | "openVariant", [x; Atom name; missing_message; Atom line_no] ->
          let missing_message =
            match missing_message with
            | Atom "None" -> None
            | _ -> Some (import_expr missing_message)
          in
          openVariant
            ~line_no:(import_line_no line_no)
            name
            (import_expr x)
            missing_message
      | "variant", [Atom name; x; Atom line_no] ->
          variant ~line_no:(import_line_no line_no) name (import_expr x)
      | "hashCrypto", [Atom algo; e; Atom line_no] ->
          let algo =
            match algo with
            | "BLAKE2B" -> BLAKE2B
            | "SHA256" -> SHA256
            | "SHA512" -> SHA512
            | "SHA3" -> SHA3
            | "KECCAK" -> KECCAK
            | _ -> failwith ("Unknown hash algorithm: " ^ algo)
          in
          hashCrypto ~line_no:(import_line_no line_no) algo (import_expr e)
      | "hash_key", [e; Atom line_no] ->
          hash_key ~line_no:(import_line_no line_no) (import_expr e)
      | "pack", [e; Atom line_no] ->
          let e = import_expr e in
          pack ~line_no:(import_line_no line_no) e
      | "unpack", [e; t; Atom line_no] ->
          unpack
            ~line_no:(import_line_no line_no)
            (import_expr e)
            (import_type t)
      | "getLocal", [Atom name; Atom line_no] ->
          local ~line_no:(import_line_no line_no) name
      | "params", [Atom line_no] ->
          let line_no = import_line_no line_no in
          params ~line_no
      | "update_map", [map; key; v; Atom line_no] ->
          updateMap
            ~line_no:(import_line_no line_no)
            (import_expr map)
            (import_expr key)
            (import_expr v)
      | "get_and_update", [map; key; v; Atom line_no] ->
          get_and_update
            ~line_no:(import_line_no line_no)
            (import_expr map)
            (import_expr key)
            (import_expr v)
      | "getItem", [a; pos; Atom line_no] ->
          item
            ~line_no:(import_line_no line_no)
            (import_expr a)
            (import_expr pos)
            None
            None
      | "getItemDefault", [a; pos; def; Atom line_no] ->
          item
            ~line_no:(import_line_no line_no)
            (import_expr a)
            (import_expr pos)
            (Some (import_expr def))
            None
      | "getItemMessage", [a; pos; message; Atom line_no] ->
          item
            ~line_no:(import_line_no line_no)
            (import_expr a)
            (import_expr pos)
            None
            (Some (import_expr message))
      | "getOpt", [m; Atom line_no; k] ->
          getOpt
            ~line_no:(import_line_no line_no)
            (import_expr m)
            (import_expr k)
      | "add_seconds", [t; s; Atom line_no] ->
          add_seconds
            ~line_no:(import_line_no line_no)
            (import_expr t)
            (import_expr s)
      | "iter", [Atom name; Atom line_no] ->
          iterator ~line_no:(import_line_no line_no) name
      | "rev", [e; Atom line_no] ->
          listRev ~line_no:(import_line_no line_no) (import_expr e)
      | "items", [e; Atom line_no] ->
          listItems ~line_no:(import_line_no line_no) (import_expr e) false
      | "keys", [e; Atom line_no] ->
          listKeys ~line_no:(import_line_no line_no) (import_expr e) false
      | "values", [e; Atom line_no] ->
          listValues ~line_no:(import_line_no line_no) (import_expr e) false
      | "elements", [e; Atom line_no] ->
          listElements ~line_no:(import_line_no line_no) (import_expr e) false
      | "rev_items", [e; Atom line_no] ->
          listItems ~line_no:(import_line_no line_no) (import_expr e) true
      | "rev_keys", [e; Atom line_no] ->
          listKeys ~line_no:(import_line_no line_no) (import_expr e) true
      | "rev_values", [e; Atom line_no] ->
          listValues ~line_no:(import_line_no line_no) (import_expr e) true
      | "rev_elements", [e; Atom line_no] ->
          listElements ~line_no:(import_line_no line_no) (import_expr e) true
      | "contains", [items; x; Atom line_no] ->
          contains
            ~line_no:(import_line_no line_no)
            (import_expr items)
            (import_expr x)
      | "check_signature", [pk; s; msg; Atom line_no] ->
          check_signature
            ~line_no:(import_line_no line_no)
            (import_expr pk)
            (import_expr s)
            (import_expr msg)
      | "reduce", [e; Atom line_no] ->
          reduce ~line_no:(import_line_no line_no) (import_expr e)
      | "scenario_var", [Atom id; Atom line_no] ->
          let t = Type.full_unknown () in
          scenario_var ~line_no:(import_line_no line_no) (int_of_string id) t
      | "make_signature", [sk; msg; Atom fmt; Atom line_no] ->
          let secret_key = import_expr sk in
          let message = import_expr msg in
          let message_format =
            match String.lowercase_ascii fmt with
            | "raw" -> `Raw
            | "hex" -> `Hex
            | other ->
                Format.kasprintf
                  failwith
                  "make_signature: Wrong message format : %S (l. %s)"
                  other
                  line_no
          in
          make_signature
            ~secret_key
            ~message
            ~message_format
            ~line_no:(import_line_no line_no)
      | "account_of_seed", [Atom seed; Atom line_no] ->
          account_of_seed ~seed ~line_no:(import_line_no line_no)
      | "split_tokens", [mutez; quantity; total; Atom line_no] ->
          split_tokens
            ~line_no:(import_line_no line_no)
            (import_expr mutez)
            (import_expr quantity)
            (import_expr total)
      | "slice", [ofs; len; buf; Atom line_no] ->
          let offset = import_expr ofs in
          let length = import_expr len in
          let buffer = import_expr buf in
          slice ~offset ~length ~buffer ~line_no:(import_line_no line_no)
      | "concat", [l; Atom line_no] ->
          concat_list (import_expr l) ~line_no:(import_line_no line_no)
      | "size", [s; Atom line_no] ->
          size (import_expr s) ~line_no:(import_line_no line_no)
      | "type_annotation", [e; t; Atom line_no] ->
          let e = import_expr e in
          let t = import_type t in
          let line_no = import_line_no line_no in
          type_annotation ~line_no e t
      | "map", Atom line_no :: entries ->
          build_map
            ~line_no:(import_line_no line_no)
            ~big:false
            ~entries:(List.map import_map_entry entries)
      | "set", Atom line_no :: entries ->
          build_set
            ~line_no:(import_line_no line_no)
            ~entries:(List.map import_expr entries)
      | "big_map", Atom line_no :: entries ->
          build_map
            ~line_no:(import_line_no line_no)
            ~big:true
            ~entries:(List.map import_map_entry entries)
      | "record", Atom line_no :: l ->
          let import_exprField = function
            | Base.Sexp.List [Atom name; e] -> (name, import_expr e)
            | l ->
                failwith
                  ("Expression field format error " ^ Base.Sexp.to_string l)
          in
          record ~line_no:(import_line_no line_no) (List.map import_exprField l)
      | "ematch", Atom line_no :: scrutinee :: l ->
          let import_clause = function
            | Base.Sexp.List [Atom name; v] ->
                let e = import_expr v in
                (name, e)
            | l -> failwith ("Clause format error: " ^ Base.Sexp.to_string l)
          in
          let scrutinee = import_expr scrutinee in
          ematch
            ~line_no:(import_line_no line_no)
            scrutinee
            (List.map import_clause l)
      | "eif", [cond; a; b; Atom line_no] ->
          let cond = import_expr cond in
          let a = import_expr a in
          let b = import_expr b in
          eif ~line_no:(import_line_no line_no) cond a b
      | "call_michelson", instr :: Atom line_no :: args ->
          inline_michelson
            ~line_no:(import_line_no line_no)
            [import_expr_inline_michelson instr]
            (List.map import_expr args)
      | "seq_michelson", Atom line_no :: Atom length :: args ->
          let rec split n acc args =
            if n = 0
            then (List.rev acc, args)
            else
              match args with
              | [] -> assert false
              | a :: args -> split (n - 1) (a :: acc) args
          in
          let ops, args = split (int_of_string length) [] args in
          inline_michelson
            ~line_no:(import_line_no line_no)
            (List.map import_expr_inline_michelson ops)
            (List.map import_expr args)
      | "global", [Atom name; id; Atom line_no] ->
          let static_id =
            match import_contract_id id with
            | C_static static_id -> static_id
            | C_dynamic _ -> assert false
          in
          global ~line_no:(import_line_no line_no) name static_id
      | "map_function", [l; f; Atom line_no] ->
          map_function
            ~line_no:(import_line_no line_no)
            (import_expr l)
            (allow_lambda_full_stack (import_expr f))
      | "lambda", [Atom id; Atom name; Atom line_no; List commands] ->
          let clean_stack = true in
          import_lambda id name line_no commands clean_stack
      | "lambdaParams", [Atom id; Atom name; Atom line_no; _tParams] ->
          let line_no = import_line_no line_no in
          lambdaParams ~line_no (int_of_string id) name
      | "call_lambda", [lambda; parameter; Atom line_no] ->
          call_lambda
            ~line_no:(import_line_no line_no)
            (import_expr lambda)
            (import_expr parameter)
      | "apply_lambda", [lambda; parameter; Atom line_no] ->
          apply_lambda
            ~line_no:(import_line_no line_no)
            (import_expr lambda)
            (import_expr parameter)
      | "lshift", [expression; shift; Atom line_no] ->
          lshift
            ~line_no:(import_line_no line_no)
            (import_expr expression)
            (import_expr shift)
      | "rshift", [expression; shift; Atom line_no] ->
          rshift
            ~line_no:(import_line_no line_no)
            (import_expr expression)
            (import_expr shift)
      | "xor", [e1; e2; Atom line_no] ->
          xor
            ~line_no:(import_line_no line_no)
            (import_expr e1)
            (import_expr e2)
      | "set_delegate", [x; Atom line_no] ->
          set_delegate ~line_no:(import_line_no line_no) (import_expr x)
      | "sapling_empty_state", [Atom memo; Atom _line_no] ->
          sapling_empty_state (int_of_string memo)
      | "sapling_verify_update", [state; transaction; Atom line_no] ->
          sapling_verify_update
            ~line_no:(import_line_no line_no)
            (import_expr state)
            (import_expr transaction)
      | "transfer", [e1; e2; e3; Atom line_no] ->
          transfer
            ~line_no:(import_line_no line_no)
            ~arg:(import_expr e1)
            ~amount:(import_expr e2)
            ~destination:(import_expr e3)
      | ( "create_contract"
        , [ List [Atom "contract"; contract]
          ; List [Atom "storage"; storage]
          ; List [Atom "baker"; baker]
          ; List [Atom "amount"; amount]
          ; Atom line_no ] ) ->
          let {contract} =
            import_contract
              ~primitives:import_env.primitives
              ~scenario_state:import_env.scenario_state
              typing_env
              contract
          in
          let storage =
            match (storage, contract.storage) with
            | Atom "None", Some storage -> storage
            | Atom "None", None -> assert false
            | e, _ -> import_expr e
          in
          let balance = import_expr amount in
          let baker =
            match baker with
            | Atom "None" -> none ~line_no:None
            | e -> import_expr e
          in
          create_contract
            ~line_no:(import_line_no line_no)
            ~baker
            {contract = {contract with balance; storage = Some storage}}
      | s, l ->
        ( try cst ~line_no:None (import_literal typing_env (Atom s :: l)) with
        | _ ->
            failwith
              (Printf.sprintf
                 "Expression format error (a %i) %s"
                 (List.length l)
                 (Base.Sexp.to_string_hum input)) ) )
    | x -> failwith ("Expression format error (b) " ^ Base.Sexp.to_string_hum x)
  and import_map_entry = function
    | Base.Sexp.List [k; v] -> (import_expr k, import_expr v)
    | e ->
        failwith
          (Printf.sprintf "import_map_entry: '%s'" (Base.Sexp.to_string e))
  and import_lambda id name line_no body clean_stack =
    let line_no = import_line_no line_no in
    let tParams = Type.full_unknown () in
    let tResult = Type.full_unknown () in
    lambda
      ~line_no
      (int_of_string id)
      name
      tParams
      tResult
      (Command_untyped.seq
         ~line_no
         (import_commands typing_env import_env body))
      clean_stack
  in
  import_expr

and import_meta_expr (typing_env : Typing.env) (import_env : env) =
  let import_expr = import_expr typing_env import_env in
  let rec import_meta_expr = function
    | Base.Sexp.List (Atom f :: args) as input ->
      ( match (f, args) with
      | "meta_list", _loc :: l -> Misc.List (List.map import_meta_expr l)
      | "meta_map", _loc :: l ->
          let import_elem = function
            | Base.Sexp.List [Atom "elem"; k; v] ->
                (import_expr k, import_meta_expr v)
            | _ -> assert false
          in
          Misc.Map (List.map import_elem l)
      | "meta_expr", [e] -> Misc.Other (import_expr e)
      | "meta_offchain_view", [Atom name; _line_no] -> Misc.Offchain_view name
      | _ ->
          failwith
            ("Meta expression format error " ^ Base.Sexp.to_string_hum input) )
    | input ->
        failwith
          ("Meta expression format error " ^ Base.Sexp.to_string_hum input)
  in
  import_meta_expr

and import_command typing_env import_env =
  let import_type t = import_type typing_env t in
  let import_expr e = import_expr typing_env import_env e in
  let import_commands cs = import_commands typing_env import_env cs in
  let open Command_untyped in
  function
  | Base.Sexp.List (List _ :: _ as cs) -> seq ~line_no:None (import_commands cs)
  | Base.Sexp.List (Atom f :: args) as input ->
    ( match (f, args) with
    | "result", [x; Atom line_no] ->
        result ~line_no:(import_line_no line_no) (import_expr x)
    | "failwith", [x; Atom line_no] ->
        sp_failwith ~line_no:(import_line_no line_no) (import_expr x)
    | "never", [x; Atom line_no] ->
        never ~line_no:(import_line_no line_no) (import_expr x)
    | "verify", [x; Atom line_no] ->
        verify ~line_no:(import_line_no line_no) (import_expr x) None
    | "verify", [x; message; Atom line_no] ->
        verify
          ~line_no:(import_line_no line_no)
          (import_expr x)
          (Some (import_expr message))
    | "defineLocal", [Atom name; expr; Atom line_no] ->
        defineLocal ~line_no:(import_line_no line_no) name (import_expr expr)
    | "whileBlock", [e; List l; Atom line_no] ->
        let line_no = import_line_no line_no in
        whileLoop ~line_no (import_expr e) (seq ~line_no (import_commands l))
    | "forGroup", [Atom name; e; List l; Atom line_no] ->
        let e = import_expr e in
        let line_no = import_line_no line_no in
        forGroup ~line_no name e (seq ~line_no (import_commands l))
    | ( "match"
      , [scrutinee; Atom constructor; Atom arg_name; List body; Atom line_no] )
      ->
        let line_no = import_line_no line_no in
        mk_match
          ~line_no
          (import_expr scrutinee)
          [(constructor, arg_name, seq ~line_no (import_commands body))]
    | "match_cases", [scrutinee; _arg; List cases; Atom line_no] ->
        let line_no = import_line_no line_no in
        let parse_case = function
          | Base.Sexp.List
              [ Atom "match"
              ; _
              ; Atom constructor
              ; Atom arg_name
              ; List body
              ; Atom line_no ] ->
              let line_no = import_line_no line_no in
              (constructor, arg_name, seq ~line_no (import_commands body))
          | input -> failwith ("Bad case parsing: " ^ Base.Sexp.to_string input)
        in
        let cases = List.map parse_case cases in
        mk_match ~line_no (import_expr scrutinee) cases
    | "set_type", [e; t; Atom line_no] ->
        set_type
          ~line_no:(import_line_no line_no)
          (import_expr e)
          (import_type t)
    | "set_result_type", [List cs; t; Atom line_no] ->
        let line_no = import_line_no line_no in
        set_result_type
          ~line_no
          (seq ~line_no (import_commands cs))
          (import_type t)
    | "set", [v; e; Atom line_no] ->
        set ~line_no:(import_line_no line_no) (import_expr v) (import_expr e)
    | "delItem", [e; k; Atom line_no] ->
        delItem
          ~line_no:(import_line_no line_no)
          (import_expr e)
          (import_expr k)
    | "updateSet", [e; k; Atom b; Atom line_no] ->
        updateSet
          ~line_no:(import_line_no line_no)
          (import_expr e)
          (import_expr k)
          (import_bool b)
    | "trace", [e; Atom line_no] ->
        trace ~line_no:(import_line_no line_no) (import_expr e)
    | "bind", _ ->
        raise
          (SmartExcept
             [ `Text
                 "Command format error. sp.bind_block() can be used as follows:"
             ; `Br
             ; `Text "x = sp.bind_block('x'):"
             ; `Br
             ; `Text "with x:"
             ; `Br
             ; `Text "    ..."
             ; `Br
             ; `Text "... x ..."
             ; `Br
             ; `Text (Base.Sexp.to_string input) ])
    | _ -> failwith ("Command format error (a) " ^ Base.Sexp.to_string input) )
  | input -> failwith ("Command format error (b) " ^ Base.Sexp.to_string input)

and import_match_cons typing_env import_env expr ok_match ko_match line_no_ =
  let import_expr e = import_expr typing_env import_env e in
  let open Command_untyped in
  let expr = import_expr expr in
  let line_no = import_line_no line_no_ in
  mk_match_cons
    ~line_no
    expr
    (Printf.sprintf "match_cons_%s" line_no_)
    (seq ~line_no (import_commands typing_env import_env ok_match))
    (seq ~line_no (import_commands typing_env import_env ko_match))

and import_commands (typing_env : Typing.env) (import_env : env) =
  let import_expr e = import_expr typing_env import_env e in
  let import_command c = import_command typing_env import_env c in
  let open Command_untyped in
  let rec import_commands r = function
    | Base.Sexp.List [Atom "bind"; Atom name; c1] :: c2 ->
        List.rev_append
          r
          [ bind
              ~line_no:None
              (Some name)
              (import_command c1)
              (seq ~line_no:None (import_commands [] c2)) ]
    | Base.Sexp.List
        [Atom "seq"; Atom seq_name; List seq_block; Atom seq_line_no]
      :: List [Atom "bind"; Atom bind_name; List bind_block; Atom bind_line_no]
         :: rest
      when seq_name = bind_name ->
        let c =
          bind
            ~line_no:(import_line_no seq_line_no)
            (Some seq_name)
            (seq
               ~line_no:(import_line_no seq_line_no)
               (import_commands [] seq_block))
            (seq
               ~line_no:(import_line_no bind_line_no)
               (import_commands [] bind_block))
        in
        import_commands (c :: r) rest
    | Base.Sexp.List [Atom "ifBlock"; e; List tBlock; Atom line_no]
      :: List [Atom "elseBlock"; List eBlock] :: rest ->
        let line_no = import_line_no line_no in
        let c =
          ifte
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no (import_commands [] eBlock))
        in
        import_commands (c :: r) rest
    | List [Atom "ifBlock"; e; List tBlock; Atom line_no] :: rest ->
        let c =
          let line_no = import_line_no line_no in
          ifte
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no [])
        in
        import_commands (c :: r) rest
    | List [Atom "ifSomeBlock"; e; Atom _; List tBlock; Atom line_no]
      :: List [Atom "elseBlock"; List eBlock] :: rest ->
        let line_no = import_line_no line_no in
        let c =
          ifteSome
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no (import_commands [] eBlock))
        in
        import_commands (c :: r) rest
    | List [Atom "ifSomeBlock"; e; Atom _; List tBlock; Atom line_no] :: rest ->
        let line_no = import_line_no line_no in
        let c =
          ifteSome
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands [] tBlock))
            (seq ~line_no [])
        in
        import_commands (c :: r) rest
    | List (Atom "match_tuple" :: Atom seq_line_no :: x :: ns) :: rest ->
        let line_no = import_line_no seq_line_no in
        let rest = seq ~line_no (import_commands [] rest) in
        let p = Pattern_tuple (List.map unAtom ns) in
        List.rev_append r [mk_match_product ~line_no (import_expr x) p rest]
    | List (Atom "match_record" :: Atom seq_line_no :: x :: bs) :: rest ->
        let line_no = import_line_no seq_line_no in
        let rest = seq ~line_no (import_commands [] rest) in
        let p = Pattern_record (List.map import_binding bs) in
        List.rev_append r [mk_match_product ~line_no (import_expr x) p rest]
    | List [Atom "modify"; Atom seq_line_no; x; List body; Atom v] :: rest ->
        let line_no = import_line_no seq_line_no in
        let body = seq ~line_no (import_commands [] body) in
        let p = Pattern_single v in
        let c = mk_modify_product ~line_no (import_expr x) p body in
        import_commands (c :: r) rest
    | List (Atom "modify_tuple" :: Atom seq_line_no :: x :: List body :: ns)
      :: rest ->
        let line_no = import_line_no seq_line_no in
        let body = seq ~line_no (import_commands [] body) in
        let p = Pattern_tuple (List.map unAtom ns) in
        let c = mk_modify_product ~line_no (import_expr x) p body in
        import_commands (c :: r) rest
    | List (Atom "modify_record" :: Atom seq_line_no :: x :: List body :: bs)
      :: rest ->
        let line_no = import_line_no seq_line_no in
        let body = seq ~line_no (import_commands [] body) in
        let bs = Pattern_record (List.map import_binding bs) in
        let c = mk_modify_product ~line_no (import_expr x) bs body in
        import_commands (c :: r) rest
    | List [Atom "match_cons"; expr; List ok_match; Atom line_no_]
      :: List [Atom "elseBlock"; List ko_match] :: rest ->
        let m =
          import_match_cons
            typing_env
            import_env
            expr
            ok_match
            ko_match
            line_no_
        in
        import_commands (m :: r) rest
    | List [Atom "match_cons"; expr; List ok_match; Atom line_no_] :: rest ->
        let m =
          import_match_cons typing_env import_env expr ok_match [] line_no_
        in
        import_commands (m :: r) rest
    | x :: l ->
        let x = import_command x in
        (* This sequencing is important for type inferrence. *)
        import_commands (x :: r) l
    | [] -> List.rev r
  in
  import_commands []

and import_contract ~primitives ~scenario_state (typing_env : Typing.env) =
  function
  | Base.Sexp.Atom _ -> failwith "Parse error contract"
  | List l ->
      let storage = assocList "storage" l in
      let storage_type =
        match assocList "storage_type" l with
        | [List []] -> None
        | [t] -> Some t
        | _ -> assert false
      in
      let entry_points_layout = assocList "entry_points_layout" l in
      let messages = assocList "messages" l in
      let flags = assocList "flags" l in
      let global_variables = assocList "globals" l in
      let metadata = assocList "initial_metadata" l in
      let views = assocList "views" l in
      let messages =
        List.map
          (function
            | Base.Sexp.List [Atom name; Atom originate; List command] ->
                let originate = import_bool originate in
                let t = Type.full_unknown () in
                ((name, originate, command), t)
            | x -> failwith ("Message format error " ^ Base.Sexp.to_string x))
          messages
      in
      let global_variables =
        List.map
          (function
            | Base.Sexp.List [Atom name; variable] -> (name, variable)
            | x ->
                failwith
                  ("Global variable format error " ^ Base.Sexp.to_string x))
          global_variables
      in
      let metadata =
        List.map
          (function
            | Base.Sexp.List [Atom name; variable] -> (name, variable)
            | x -> failwith ("Metadata format error " ^ Base.Sexp.to_string x))
          metadata
      in
      let views =
        List.map
          (function
            | Base.Sexp.List
                [ Atom name
                ; Atom has_param
                ; Atom line_no
                ; Atom pure
                ; Atom doc
                ; List commands ] ->
                ( name
                , import_bool has_param
                , import_line_no line_no
                , import_bool pure
                , commands
                , doc )
            | x -> failwith ("View format error " ^ Base.Sexp.to_string x))
          views
      in
      let env0 = {primitives; scenario_state; entry_point = None} in
      let balance =
        match Base.Sexp.List (assocList "balance" l) with
        | List [] ->
            Expr_untyped.cst ~line_no:None (Literal.mutez Big_int.zero_big_int)
        | l -> import_expr typing_env env0 l
      in
      let build_global_variable (name, variable) =
        let import_env = {primitives; scenario_state; entry_point = None} in
        let expression = import_expr typing_env import_env variable in
        (name, expression)
      in
      let global_variables = List.map build_global_variable global_variables in
      let build_metadata (name, variable) =
        let import_env = {primitives; scenario_state; entry_point = None} in
        let expression = import_meta_expr typing_env import_env variable in
        (name, expression)
      in
      let metadata = List.map build_metadata metadata in
      let build_view (name, has_param, line_no, pure, commands, doc) =
        let import_env = {primitives; scenario_state; entry_point = None} in
        let body =
          Command_untyped.seq
            ~line_no
            (import_commands typing_env import_env commands)
        in
        { name
        ; tparameter = (if has_param then Some (Type.full_unknown ()) else None)
        ; pure
        ; body
        ; doc }
      in
      let views = List.map build_view views in
      let entry_points_layout =
        match entry_points_layout with
        | [] -> None
        | _ -> Some (import_inner_layout (Base.Sexp.List entry_points_layout))
      in
      let tparameter =
        match
          ( entry_points_layout
          , List.map
              (fun ((name, _, _), t) -> (name, t))
              (List.filter (fun ((_, originate, _), _) -> originate) messages)
          )
        with
        | _, [] -> Type.unit
        | None, r -> Type.variant_default_layout r
        | Some l, r -> Type.variant (ref (Unknown.UnValue l)) r
      in
      let tstorage, storage, _storage_line_no =
        let get_storage storage =
          import_expr typing_env env0 (Base.Sexp.List storage)
        in
        let get_storage_type t = import_type typing_env t in
        match (storage, storage_type) with
        | [], None ->
            raise
              (SmartExcept
                 [ `Text "Import Error"
                 ; `Text
                     (Printf.sprintf
                        "Missing contract storage and storage type")
                 ; `Line None ])
        | [], Some t -> (get_storage_type t, None, None)
        | storage, t ->
            let storage = get_storage storage in
            ( Base.Option.value_map
                t
                ~f:get_storage_type
                ~default:(Type.full_unknown ())
            , Some storage
            , storage.line_no )
      in
      let build_entry_point ((name, originate, command), t) =
        let import_env =
          {primitives; scenario_state; entry_point = Some (name, t)}
        in
        let body =
          default_line_no_command
            (Command_untyped.seq
               ~line_no:None
               (import_commands typing_env import_env command))
        in
        match import_env.entry_point with
        | Some (_, paramsType) -> {channel = name; paramsType; originate; body}
        | None -> assert false
      in
      let entry_points = List.map build_entry_point messages in
      let flags = import_flags flags in
      { contract =
          { balance
          ; storage
          ; baker = Expr_untyped.none ~line_no:None
          ; tstorage
          ; tparameter
          ; entry_points
          ; entry_points_layout
          ; unknown_parts = None
          ; flags
          ; global_variables
          ; metadata
          ; views } }

let import_expr tenv primitives scenario_state x =
  import_expr tenv {primitives; scenario_state; entry_point = None} x
