(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Type

type env = private
  { unknowns : (string, Type.t) Hashtbl.t
  ; constraints : (line_no * typing_constraint) list ref }

val init_env : unit -> env

val unknown : env -> string -> t

val intType : bool Unknown.t ref -> [> `Int | `Nat | `Unknown ]

val add_constraint : line_no:line_no -> env -> typing_constraint -> unit

val assertEqual :
  line_no:line_no -> pp:(unit -> smart_except list) -> env:env -> t -> t -> unit

val reset_constraints : env -> env
