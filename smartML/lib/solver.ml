(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed
open Printf
open Utils
open Ternary

type constraint_result =
  | Ok
  | Unresolved

let unresolved_error ~line_no = function
  | AssertEqual _ -> assert false
  | HasSub (e, e1, e2) ->
      [ `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be subtracted in "
      ; `Expr e
      ; `Line line_no ]
  | HasDiv (e, e1, e2) ->
      [ `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be divided in "
      ; `Expr e
      ; `Br
      ; `Text "Allowed types are (int|nat, int|nat) and (tez, tez|nat)"
      ; `Line line_no ]
  | HasMap (e, e1, e2) ->
      [ `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be map in "
      ; `Expr e
      ; `Line line_no ]
  | HasBitArithmetic (e, e1, e2) ->
      [ `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be xor-ed in "
      ; `Expr e
      ; `Line line_no ]
  | IsComparable e ->
      [`Text "Type error"; `Expr e; `Text "doesn't have a comparable type"]
  | HasGetItem (l, pos, _t) ->
      [`Text "Type Error"; `Expr l; `Text "cannot get item"; `Expr pos]
  | HasContains (items, member, line_no) ->
      [ `Text "Type Error"
      ; `Expr items
      ; `Text "cannot contains"
      ; `Expr member
      ; `Line line_no ]
  | HasSize e ->
      [ `Text "Type Error"
      ; `Expr e
      ; `Text "has no length or size"
      ; `Line e.line_no ]
  | HasSlice e ->
      [`Text "Type Error"; `Expr e; `Text "cannot be sliced"; `Line e.line_no]
  | HasAdd (e, e1, e2) ->
      [ `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be added in "
      ; `Expr e
      ; `Line e.line_no ]
  | HasMul (e, e1, e2, _overloaded) ->
      [ `Text "Type Error"
      ; `Exprs [e1; e2]
      ; `Text "cannot be multiplied "
      ; `Expr e
      ; `Line e.line_no ]
  | IsInt (_, pp) -> pp ()
  | RowHasEntry {kind; t} ->
      let type_name, _label_name =
        match kind with
        | `Variant -> ("variant type", "constructor")
        | `Record -> ("record type", "field")
      in
      [ `Text "Type Error"
      ; `Type t
      ; `Text "is not a"
      ; `Text type_name
      ; `Line line_no ]
  | SaplingVerify (state, transaction) ->
      [`Text "memo_size error"; `Exprs [state; transaction]; `Line line_no]
  | HasNeg (e, _) ->
      [`Text "Type Error"; `Expr e; `Text "cannot be negated"; `Line e.line_no]
  | HasInt e ->
      [ `Text "Type Error"
      ; `Expr e
      ; `Text "cannot be cast to Int"
      ; `Line e.line_no ]
  | IsNotHot (n, t) ->
      [ `Text "Variable"
      ; `Text n
      ; `Text "of type"
      ; `Type t
      ; `Text "cannot be used twice because it contains a ticket."
      ; `Line line_no ]

let assertEqual_layout ~pp l1 l2 =
  setEqualUnknownOption
    ~pp:(fun () ->
      [ `Text "Type Error"
      ; `Br
      ; `Text "Non matching layouts"
      ; `Br
      ; `Text (Printer.layout_to_string l1)
      ; `Br
      ; `Text "and"
      ; `Br
      ; `Text (Printer.layout_to_string l2)
      ; `Br
      ; `Rec (pp ()) ])
    l1
    l2

let rec assertEqual ~pp t1 t2 =
  match (Type.getRepr t1, Type.getRepr t2) with
  | TBool, TBool
   |TString, TString
   |TBytes, TBytes
   |TToken, TToken
   |TUnit, TUnit
   |TAddress, TAddress
   |TKey, TKey
   |TSecretKey, TSecretKey
   |TKeyHash, TKeyHash
   |TBakerHash, TBakerHash
   |TOperation, TOperation
   |TSignature, TSignature
   |TChainId, TChainId
   |TTimestamp, TTimestamp
   |TNever, TNever
   |TBls12_381_g1, TBls12_381_g1
   |TBls12_381_g2, TBls12_381_g2
   |TBls12_381_fr, TBls12_381_fr ->
      ()
  | TSaplingTransaction {memo = m1}, TSaplingTransaction {memo = m2}
   |TSaplingState {memo = m1}, TSaplingState {memo = m2} ->
      setEqualUnknownOption
        ~pp:(fun () -> [`Text "memo_size mismatch"; `Br; `Rec (pp ())])
        m1
        m2
  | TContract t1, TContract t2 -> assertEqual ~pp t1 t2
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} when isNat1 == isNat2 -> ()
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} ->
      setEqualUnknownOption
        ~pp:(fun () ->
          [`Text "Type sp.TInt / sp.TNat mismatch"; `Br; `Rec (pp ())])
        isNat1
        isNat2
  | TUnknown t1, TUnknown t2 when t1 == t2 -> ()
  | TUnknown ({contents = UTuple l1} as r1), TTuple l2 ->
      let checkInside (i, t) =
        match List.nth_opt l2 i with
        | None ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text "Missing component"
                 ; `Text (sprintf "%d" i)
                 ; `Text "in type"
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | Some t2 -> assertEqual t t2 ~pp
      in
      List.iter checkInside l1;
      r1 := UExact t2
  | TUnknown ({contents = UVariant l1} as r1), TVariant {row = l2}
   |TUnknown ({contents = URecord l1} as r1), TRecord {row = l2} ->
      let checkInside (name, t) =
        match List.assoc_opt name l2 with
        | None ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text "Missing field or variant"
                 ; `Text (sprintf "'%s'" name)
                 ; `Text "in type"
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | Some t2 -> assertEqual t t2 ~pp
      in
      List.iter checkInside l1;
      r1 := UExact t2
  | TMap _, TUnknown _
   |TSet _, TUnknown _
   |TRecord _, TUnknown _
   |TTuple _, TUnknown _
   |TVariant _, TUnknown _ ->
      assertEqual t2 t1 ~pp
  | TUnknown ({contents = UUnknown _} as r), _ -> r := UExact t2
  | _, TUnknown ({contents = UUnknown _} as r) -> r := UExact t1
  | ( TUnknown ({contents = UTuple l1_} as r1)
    , TUnknown ({contents = UTuple l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (_b, t2_) :: l2 ->
            assertEqual ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := UTuple l;
      r2 := UExact t1
  | ( TUnknown ({contents = URecord l1_} as r1)
    , TUnknown ({contents = URecord l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (_b, t2_) :: l2 ->
            assertEqual ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := URecord l;
      r2 := UExact t1
  | ( TUnknown ({contents = UVariant l1_} as r1)
    , TUnknown ({contents = UVariant l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (b, t2_) :: l2 ->
            assert (a = b);
            assertEqual ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := UVariant l;
      r2 := UExact t1
  | TLambda (t11, t12), TLambda (t21, t22) ->
      assertEqual t11 t21 ~pp;
      assertEqual t12 t22 ~pp
  | TTuple ts1, TTuple ts2 ->
      if List.length ts1 = List.length ts2
      then List.iter2 (assertEqual ~pp) ts1 ts2
      else
        raise
          (SmartExcept
             [ `Text "Type Error"
             ; `Br
             ; `Text "Tuples differ in lengths:"
             ; `Type t1
             ; `Type t2
             ; `Br
             ; `Rec (pp ()) ])
  | TTicket t1, TTicket t2 -> assertEqual t1 t2 ~pp
  | TList t1, TList t2 -> assertEqual t1 t2 ~pp
  | TRecord {layout = lo1; row = l1_}, TRecord {layout = lo2; row = l2_} ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> ()
        | [], (a, _) :: _ | (a, _) :: _, [] ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text (sprintf "missing field %s in record" a)
                 ; `Type t1
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | (a, t1_) :: l1, (b, t2_) :: l2 ->
            if a <> b
            then
              raise
                (SmartExcept
                   [ `Text "Type Error"
                   ; `Br
                   ; `Text
                       (sprintf
                          "non matching names [%s] and [%s] in records"
                          a
                          b)
                   ; `Type t1
                   ; `Type t2
                   ; `Br
                   ; `Rec (pp ()) ]);
            assertEqual ~pp t1_ t2_;
            acc l1 l2
      in
      acc (List.sort compare l1_) (List.sort compare l2_);
      let pp_layout () =
        [ `Text "Type Error"
        ; `Br
        ; `Text (sprintf "Different layouts for types")
        ; `Type t1
        ; `Br
        ; `Type t2
        ; `Br
        ; `Rec (pp ()) ]
      in
      assertEqual_layout ~pp:pp_layout lo1 lo2
  | TSet {telement = t1}, TSet {telement = t2} -> assertEqual t1 t2 ~pp
  | TVariant {layout = lo1; row = l1}, TVariant {layout = lo2; row = l2} ->
      let pp_layout () =
        [ `Text "Type Error"
        ; `Br
        ; `Text (sprintf "Different layouts for types")
        ; `Type t1
        ; `Br
        ; `Type t2
        ; `Br
        ; `Rec (pp ()) ]
      in
      assertEqual_layout ~pp:pp_layout lo1 lo2;
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> ()
        | [], _ | _, [] ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text "Incompatible variant types:"
                 ; `Br
                 ; `Type t1
                 ; `Br
                 ; `Text "and"
                 ; `Br
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | (a, t1) :: l1, (b, t2) :: l2 ->
            if a <> b
            then
              raise
                (SmartExcept
                   [ `Text "Type Error"
                   ; `Br
                   ; `Text
                       (sprintf
                          "non matching names [%s] and [%s] in variant \
                           constructors"
                          a
                          b)
                   ; `Type t1
                   ; `Text "and"
                   ; `Type t2
                   ; `Rec (pp ()) ]);
            assertEqual ~pp t1 t2;
            acc l1 l2
      in
      acc (List.sort compare l1) (List.sort compare l2)
  | ( TMap {big = big1; tkey = k1; tvalue = i1}
    , TMap {big = big2; tkey = k2; tvalue = i2} ) ->
      assertEqual ~pp i1 i2;
      assertEqual ~pp k1 k2;
      setEqualUnknownOption
        ~pp:(fun () ->
          [`Text "Type sp.TMap / sp.TBigMap mismatch"; `Br; `Rec (pp ())])
        big1
        big2
  | _ ->
      raise
        (SmartExcept
           [ `Text "Type Error"
           ; `Br
           ; `Type t1
           ; `Text "is not"
           ; `Type t2
           ; `Br
           ; `Rec (pp ()) ])

let checkDiff ~pp e e1 _e2 =
  match Type.getRepr e1.et with
  | TToken ->
      assertEqual ~pp e1.et e.et;
      Ok
  | TInt _ ->
      assertEqual ~pp e.et (Type.int ());
      Ok
  | TTimestamp ->
      assertEqual ~pp e.et (Type.int ());
      Ok
  | TUnknown _ -> Unresolved
  | TString | TBytes | TUnit | TBool | TRecord _ | TVariant _ | TSet _
   |TMap _ | TAddress | TContract _ | TKeyHash | TBakerHash | TKey
   |TSecretKey | TChainId | TSignature | TTuple _ | TList _ | TLambda _
   |TOperation | TSaplingState _ | TSaplingTransaction _ | TNever | TTicket _
   |TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr ->
      raise (SmartExcept (pp ()))

let checkMap ~pp e l f =
  match Type.getRepr f.et with
  | TLambda (s', t') ->
    ( match Type.getRepr l.et with
    | TList t ->
        assertEqual ~pp t s';
        assertEqual ~pp e.et (Type.list t');
        Ok
    | TMap {big; tkey; tvalue} when Unknown.getRefOption big = Some false ->
        assertEqual ~pp (Type.key_value tkey tvalue) s';
        assertEqual ~pp e.et (Type.map ~big ~tkey ~tvalue:t');
        Ok
    | TUnknown _ -> Unresolved
    | TMap _ | TTimestamp | TString | TBytes | TUnit | TBool | TRecord _
     |TVariant _ | TSet _ | TInt _ | TToken | TAddress | TContract _
     |TKeyHash | TBakerHash | TKey | TSecretKey | TChainId | TSignature
     |TTuple _ | TLambda _ | TOperation | TSaplingState _
     |TSaplingTransaction _ | TNever | TTicket _ | TBls12_381_g1
     |TBls12_381_g2 | TBls12_381_fr ->
        raise (SmartExcept (pp ())) )
  | _ -> raise (SmartExcept (pp ()))

let checkDiv ~pp ~line_no e' e1 e2 =
  let fix_cst_intOrNat_as_nat x =
    match (x.e, Type.getRepr x.et) with
    | EPrim0 (ECst _), TInt {isNat} ->
      ( match Unknown.getRefOption isNat with
      | None ->
          assertEqual x.et (Type.nat ()) ~pp;
          Some true
      | x -> x )
    | _, TInt {isNat} -> Unknown.getRefOption isNat
    | _ -> None
  in
  let a_b =
    match (Type.getRepr e1.et, Type.getRepr e2.et) with
    | _, TToken ->
        assertEqual ~pp e1.et Type.token;
        `OK (Type.nat (), Type.token)
    | TToken, TInt _ ->
        assertEqual ~pp e2.et (Type.nat ());
        `OK (Type.token, Type.token)
    | TInt _, TInt _ ->
      begin
        match (fix_cst_intOrNat_as_nat e1, fix_cst_intOrNat_as_nat e2) with
        | Some true, Some true -> `OK (Type.nat (), Type.nat ())
        | Some false, Some true | Some true, Some false | Some false, Some false
          ->
            `OK (Type.int (), Type.nat ())
        | None, _ | _, None -> `Unknown
      end
    | ( ( TInt _ | TToken | TTimestamp | TString | TBytes | TUnit | TBool
        | TRecord _ | TVariant _ | TSet _ | TMap _ | TAddress | TContract _
        | TKeyHash | TBakerHash | TKey | TSecretKey | TChainId | TSignature
        | TUnknown _ | TTuple _ | TList _ | TLambda _ | TOperation
        | TSaplingState _ | TSaplingTransaction _ | TNever | TTicket _
        | TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr )
      , _ ) ->
        raise (SmartExcept (pp ()))
  in
  match a_b with
  | `Unknown -> Unresolved
  | `OK (a, b) ->
      let target_type = Type.option (Type.pair a b) in
      let pp () =
        [ `Text "Type Error"
        ; `Expr e'
        ; `Text "is not compatible with type"
        ; `Type target_type
        ; `Br
        ; `Line line_no ]
      in
      assertEqual ~pp e'.et target_type;
      Ok

let checkBitArithmetic ~pp e e1 _e2 =
  match Type.getRepr e1.et with
  | TBool ->
      ();
      Ok
  | TInt _ ->
      assertEqual ~pp e.et (Type.nat ());
      Ok
  | TUnknown _ -> Unresolved
  | TString | TTimestamp | TBytes | TUnit | TRecord _ | TVariant _ | TSet _
   |TToken | TMap _ | TAddress | TContract _ | TKeyHash | TBakerHash | TKey
   |TSecretKey | TChainId | TSignature | TTuple _ | TList _ | TLambda _
   |TOperation | TSaplingState _ | TSaplingTransaction _ | TNever | TTicket _
   |TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr ->
      raise (SmartExcept (pp ()))

let is_comparable =
  Type.cata (function
      | TUnit | TNever | TBool | TInt _ | TString | TChainId | TBytes | TToken
       |TKeyHash | TBakerHash | TKey | TSignature | TTimestamp | TAddress ->
          Yes
      | TOperation | TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr | TSet _
       |TMap _ | TContract _ | TList _ | TLambda _ | TSaplingState _
       |TSaplingTransaction _ | TTicket _ | TSecretKey ->
          No
      | TUnknown {contents = UExact c} -> c
      | TUnknown {contents = UUnknown _} -> Maybe
      | ( TTuple _ | TRecord _ | TVariant _
        | TUnknown {contents = URecord _ | UTuple _ | UVariant _} ) as t ->
          Type.fold_f and_ Yes t)

let checkComparable ~pp e =
  match is_comparable e.et with
  | Yes -> Ok
  | No -> raise (SmartExcept (pp ()))
  | Maybe -> Unresolved

let checkGetItem ~pp l pos t =
  match Type.getRepr l.et with
  | TMap {tkey; tvalue} ->
      assertEqual ~pp tkey pos.et;
      assertEqual ~pp tvalue t;
      Ok
  | TList _ ->
      let pp () =
        [ `Rec (pp ())
        ; `Br
        ; `Text "A list is not a map."
        ; `Br
        ; `Text
            (sprintf
               " You can use:\n\
               \ - sp.vector(..) to create a map from a list,\n\
               \ - sp.matrix(..) to create a map of maps from a list of lists,\n\
               \ - sp.cube(..) for a list of lists of lists.") ]
      in
      raise (SmartExcept (pp ()))
  | TUnknown _ -> Unresolved
  | _ -> raise (SmartExcept (pp ()))

let apply_constraint line_no c =
  let pp () = unresolved_error ~line_no c in
  match c with
  | AssertEqual (t1, t2, pp) ->
      assertEqual ~pp t1 t2;
      Ok
  | HasSub (e, e1, e2) -> checkDiff ~pp e e1 e2
  | HasDiv (e, e1, e2) -> checkDiv ~pp ~line_no e e1 e2
  | HasMap (e, e1, e2) -> checkMap ~pp e e1 e2
  | HasBitArithmetic (e, e1, e2) -> checkBitArithmetic ~pp e e1 e2
  | IsComparable e -> checkComparable ~pp e
  | HasGetItem (l, pos, t) -> checkGetItem ~pp l pos t
  | HasContains (items, member, _line_no) ->
    ( match Type.getRepr items.et with
    | TSet {telement} ->
        assertEqual ~pp telement member.et;
        Ok
    | TMap {tkey} ->
        assertEqual ~pp tkey member.et;
        Ok
    | TUnknown _ -> Unresolved
    | _ -> raise (SmartExcept (pp ())) )
  | HasSize e ->
    ( match Type.getRepr e.et with
    | TList _ | TString | TBytes | TSet _ -> Ok
    | TMap {big} when Unknown.getRefOption big = Some false -> Ok
    | TUnknown _ | TMap _ -> Unresolved
    | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TAddress
     |TContract _ | TKeyHash | TBakerHash | TKey | TSecretKey | TChainId
     |TSignature | TTuple _ | TLambda _ | TOperation | TToken | TInt _
     |TSaplingState _ | TSaplingTransaction _ | TNever | TTicket _
     |TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr ->
        raise (SmartExcept (pp ())) )
  | HasSlice e ->
    ( match Type.getRepr e.et with
    | TString | TBytes -> Ok
    | TUnknown _ -> Unresolved
    | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TMap _ | TAddress
     |TContract _ | TKeyHash | TBakerHash | TKey | TSecretKey | TChainId
     |TSignature | TTuple _ | TLambda _ | TOperation | TToken | TInt _
     |TSet _ | TList _ | TSaplingState _ | TSaplingTransaction _ | TNever
     |TTicket _ | TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr ->
        raise (SmartExcept (pp ())) )
  | HasAdd (_e, e1, _e2) ->
    ( match Type.getRepr e1.et with
    | TToken | TString | TBytes | TInt _ | TBls12_381_g1 | TBls12_381_g2
     |TBls12_381_fr ->
        Ok
    | TUnknown _ -> Unresolved
    | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TSet _ | TMap _
     |TAddress | TContract _ | TKeyHash | TBakerHash | TKey | TSecretKey
     |TChainId | TSignature | TTuple _ | TList _ | TLambda _ | TOperation
     |TSaplingState _ | TSaplingTransaction _ | TNever | TTicket _ ->
        raise (SmartExcept (pp ())) )
  | HasMul (e, e1, e2, overloaded) ->
      let integer_mul e e1 e2 =
        assertEqual e.et (Type.intOrNat ()) ~pp;
        assertEqual e1.et (Type.intOrNat ()) ~pp;
        assertEqual e2.et (Type.intOrNat ()) ~pp;
        match (Type.getRepr e.et, Type.getRepr e1.et, Type.getRepr e2.et) with
        | TInt {isNat}, TInt {isNat = isNat1}, TInt {isNat = isNat2} ->
          begin
            match
              ( Unknown.getRefOption isNat
              , Unknown.getRefOption isNat1
              , Unknown.getRefOption isNat2 )
            with
            | _, Some true, _ ->
                assertEqual e.et e2.et ~pp;
                Ok
            | _, _, Some true ->
                assertEqual e.et e1.et ~pp;
                Ok
            | _, Some false, _ | _, _, Some false ->
                assertEqual e.et (Type.int ()) ~pp;
                Ok
            | Some true, None, None ->
                assertEqual e1.et (Type.nat ()) ~pp;
                assertEqual e2.et (Type.nat ()) ~pp;
                Ok
            | (Some false | None), None, None -> Unresolved
          end
        | _ -> assert false
      in
      ( match (Type.getRepr e.et, Type.getRepr e1.et, Type.getRepr e2.et) with
      | TInt _, _, _ ->
          if not overloaded
          then Ok (* we know that all types are equal *)
          else integer_mul e e1 e2
      | _, TInt _, TInt _ -> integer_mul e e1 e2
      | _, TInt _, TBls12_381_fr | _, TBls12_381_fr, TInt _ ->
          assertEqual e.et Type.bls12_381_fr ~pp;
          Ok
      | _, (TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr), TBls12_381_fr
       |(TBls12_381_g1 | TBls12_381_g2), _, TBls12_381_fr ->
          assertEqual e.et e1.et ~pp;
          Ok
      | _, (TBls12_381_g1 | TBls12_381_g2), _ ->
          assertEqual e.et e1.et ~pp;
          assertEqual e2.et Type.bls12_381_fr ~pp;
          Ok
      | TUnknown _, _, _ -> Unresolved
      | ( ( TToken | TString | TBytes | TUnit | TBool | TTimestamp | TRecord _
          | TVariant _ | TSet _ | TMap _ | TAddress | TContract _ | TKeyHash
          | TBakerHash | TKey | TSecretKey | TChainId | TSignature | TTuple _
          | TList _ | TLambda _ | TOperation | TSaplingState _
          | TSaplingTransaction _ | TNever | TTicket _ | TBls12_381_g1
          | TBls12_381_g2 | TBls12_381_fr )
        , _
        , _ ) ->
          raise (SmartExcept (pp ())) )
  | IsInt (t, pp) ->
    ( match Type.getRepr t with
    | TInt _ -> Ok
    | TUnknown _ -> Unresolved
    | TToken | TString | TBytes | TUnit | TBool | TTimestamp | TRecord _
     |TVariant _ | TSet _ | TMap _ | TAddress | TContract _ | TKeyHash
     |TBakerHash | TKey | TSecretKey | TChainId | TSignature | TTuple _
     |TList _ | TLambda _ | TOperation | TSaplingState _
     |TSaplingTransaction _ | TNever | TTicket _ | TBls12_381_g1
     |TBls12_381_g2 | TBls12_381_fr ->
        raise (SmartExcept (pp ())) )
  | RowHasEntry {kind; t; label; entry} ->
      let type_name, label_name =
        match kind with
        | `Variant -> ("variant type", "constructor")
        | `Record -> ("record type", "field")
      in
      let f row =
        match List.assoc_opt label row with
        | None ->
            let pp () =
              [ `Text "Type Error"
              ; `Text type_name
              ; `Type t
              ; `Text "does not have"
              ; `Text label_name
              ; `Text label
              ; `Br
              ; `Line line_no ]
            in
            raise (SmartExcept (pp ()))
        | Some t ->
            let pp () =
              [ `Text "Type Error"
              ; `Text type_name
              ; `Type t
              ; `Text "has"
              ; `Text label_name
              ; `Text "of type"
              ; `Type t
              ; `Text "instead of"
              ; `Type entry
              ; `Br
              ; `Line line_no ]
            in
            assertEqual entry t ~pp;
            Ok
      in
      ( match (kind, Type.getRepr t) with
      | `Variant, TVariant {row} -> f row
      | `Record, TRecord {row} -> f row
      | _, TUnknown _ -> Unresolved
      | ( _
        , ( TToken | TString | TBytes | TUnit | TInt _ | TBool | TTimestamp
          | TRecord _ | TVariant _ | TSet _ | TMap _ | TAddress | TContract _
          | TKeyHash | TBakerHash | TKey | TSecretKey | TChainId | TSignature
          | TTuple _ | TList _ | TLambda _ | TOperation | TSaplingState _
          | TSaplingTransaction _ | TNever | TTicket _ | TBls12_381_g1
          | TBls12_381_g2 | TBls12_381_fr ) ) ->
          raise (SmartExcept (pp ())) )
  | SaplingVerify (state, transaction) ->
    begin
      match (Type.getRepr state.et, Type.getRepr transaction.et) with
      | TSaplingState {memo = m1}, TSaplingTransaction {memo = m2} ->
          setEqualUnknownOption ~pp m1 m2;
          ( match (Unknown.getRefOption m1, Unknown.getRefOption m2) with
          | Some m1, Some m2 when m1 = m2 -> Ok
          | Some _, Some _ -> raise (SmartExcept (pp ()))
          | _ -> Unresolved )
      | _ -> raise (SmartExcept (pp ()))
    end
  | HasNeg (e, t) ->
    ( match (Type.getRepr t, Type.getRepr e.et) with
    | (TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr), _
     |_, (TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr) ->
        assertEqual t e.et ~pp;
        Ok
    | TInt _, _ | _, TInt _ ->
        assertEqual e.et (Type.intOrNat ()) ~pp;
        assertEqual t (Type.int ()) ~pp;
        Ok
    | _, TUnknown _ -> Unresolved
    | ( _
      , ( TToken | TString | TBytes | TUnit | TBool | TTimestamp | TRecord _
        | TVariant _ | TSet _ | TMap _ | TAddress | TContract _ | TKeyHash
        | TBakerHash | TKey | TSecretKey | TChainId | TSignature | TTuple _
        | TList _ | TLambda _ | TOperation | TSaplingState _
        | TSaplingTransaction _ | TNever | TTicket _ ) ) ->
        raise (SmartExcept (pp ())) )
  | HasInt e ->
      let pp () =
        [ `Text "Type Error"
        ; `Expr e
        ; `Text "cannot be cast to Int"
        ; `Line e.line_no ]
      in
      ( match Type.getRepr e.et with
      | TInt _ ->
          assertEqual e.et (Type.nat ()) ~pp;
          Ok
      | TBls12_381_fr -> Ok
      | TUnknown _ -> Unresolved
      | TToken | TString | TBytes | TUnit | TBool | TTimestamp | TRecord _
       |TVariant _ | TSet _ | TMap _ | TAddress | TContract _ | TKeyHash
       |TBakerHash | TKey | TSecretKey | TChainId | TSignature | TTuple _
       |TList _ | TLambda _ | TOperation | TSaplingState _
       |TSaplingTransaction _ | TNever | TTicket _ | TBls12_381_g1
       |TBls12_381_g2 ->
          raise (SmartExcept (pp ())) )
  | IsNotHot (_, t) ->
    ( match Type.is_hot t with
    | No -> Ok
    | Yes -> raise (SmartExcept (pp ()))
    | Maybe -> Unresolved )

let show_constraint =
  let open Type in
  function
  | HasAdd (_e, e1, e2) -> Format.asprintf "HasAdd %a, %a" pp e1.et pp e2.et
  | HasMul (_e, e1, e2, _) -> Format.asprintf "HasMul %a, %a" pp e1.et pp e2.et
  | HasSub (_e, e1, e2) -> Format.asprintf "HasSub %a, %a" pp e1.et pp e2.et
  | HasDiv (_e, e1, e2) -> Format.asprintf "HasDiv %a, %a" pp e1.et pp e2.et
  | HasBitArithmetic (_e, e1, e2) ->
      Format.asprintf "HasBitArithmetic %a, %a" pp e1.et pp e2.et
  | HasMap (_e, e1, e2) -> Format.asprintf "HasMap %a, %a" pp e1.et pp e2.et
  | IsComparable e -> Format.asprintf "IsComparable %a" pp e.et
  | HasGetItem (e1, e2, _) ->
      Format.asprintf "HasGetItem %a, %a" pp e1.et pp e2.et
  | HasContains (e1, e2, _line_no) ->
      Format.asprintf "HasContains %a, %a" pp e1.et pp e2.et
  | HasSize e -> Format.asprintf "HasSize %a" pp e.et
  | HasSlice e -> Format.asprintf "HasSlice %a" pp e.et
  | AssertEqual (t1, t2, _pp) ->
      Format.asprintf "AssertEqual %a, %a" pp t1 pp t2
  | IsInt (t, _pp) -> Format.asprintf "IsInt %a" pp t
  | RowHasEntry {kind = _; t; label; entry} ->
      Format.asprintf "RowHasEntry %a %s %a" pp t label pp entry
  | SaplingVerify (state, transaction) ->
      Format.asprintf "SaplingVerify %a %a" pp state.et pp transaction.et
  | HasNeg (e, t) -> Format.asprintf "HasNeg %a %a" pp e.et pp t
  | HasInt e -> Format.asprintf "HasInt %a" pp e.et
  | IsNotHot (n, t) -> Format.asprintf "HasInt %s %a" n pp t

let run constraints =
  let verbosity = 0 in
  let debug v f = if verbosity >= v then f () in
  let step (line_no, c) =
    match apply_constraint line_no c with
    | Ok -> None
    | Unresolved -> Some (line_no, c)
  in
  let rec pass i = function
    | [] -> []
    | cs when i > 100 -> cs
    | cs ->
        debug 2 (fun () ->
            List.iter (fun (_, c) -> printf "    %s\n" (show_constraint c)) cs);
        debug 1 (fun () -> printf "  Pass #%d...\n" i);
        pass (i + 1) (List.filter_map step cs)
  in
  debug 1 (fun () -> print_endline "Constraint resolution...");
  let cs = pass 1 (List.rev constraints) in
  let mandatory = function
    | _, IsComparable _ -> false
    | _, IsNotHot _ -> false
    | _ -> true
  in
  ( match List.find_opt mandatory cs with
  | None -> ()
  | Some (line_no, c) ->
      raise
        (SmartExcept
           ( `Text "Missing typing information"
           :: `Br
           :: unresolved_error ~line_no c )) );
  debug 1 (fun () -> print_endline "All constraints resolved.")

let apply env =
  run !(env.Typing.constraints);
  env.constraints := []
