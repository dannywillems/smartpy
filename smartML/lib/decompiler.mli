(* Copyright 2019-2021 Smart Chain Arena LLC. *)

val smartML_of_michel :
  Config.t -> Michel.Typing.checked_precontract -> Basics.value_tcontract
