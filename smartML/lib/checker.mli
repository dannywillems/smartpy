(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed
open Untyped

val check_command : Config.t -> (string * Type.t) list -> command -> tcommand
(** Typechecks the given command in the given environment. *)

val check_expr : Config.t -> (string * Type.t) list -> expr -> texpr

val check_value_tcontract :
  Config.t -> value_tcontract -> value_tcontract * Type.t * Type.t

val check_contract : Config.t -> contract -> tcontract * Type.t * Type.t

val check_scenario : Config.t -> scenario -> tscenario
