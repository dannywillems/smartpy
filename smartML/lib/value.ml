(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Basics

type t = tvalue [@@deriving eq, show {with_path = false}]

let rec cata f {v; vt} = f vt (map_value_f (cata f) v)

let build v vt = {v; vt}

let literal l t = build (Literal l) t

let int x = literal (Literal.int x) (Type.int ())

let nat x = literal (Literal.nat x) (Type.nat ())

let intOrNat t x = literal (Literal.intOrNat t x) t

let mutez i = literal (Literal.mutez i) Type.token

let timestamp i = literal (Literal.timestamp i) Type.timestamp

let int_of_value v =
  match v.v with
  | Literal (Int {i}) -> Big_int.int_of_big_int i
  | _ ->
      failwith
        (Printf.sprintf
           "Cannot convert value %s into int."
           (Printer.value_to_string v))

let bool_of_value v =
  match v.v with
  | Literal (Bool x) -> x
  | _ ->
      failwith
        (Printf.sprintf
           "Cannot convert value %s into bool."
           (Printer.value_to_string v))

(** Comparison (igoring [tvalue.t]). *)
let rec compare {v = v1} {v = v2} = compare_value_f compare v1 v2

let lt v1 v2 = compare v1 v2 < 0

let le v1 v2 = compare v1 v2 <= 0

let getType {vt} = vt

let openV x = x.v

let string s = literal (Literal.string s) Type.string

let bytes s = literal (Literal.bytes s) Type.bytes

let bls12_381_g1 s = literal (Literal.bls12_381_g1 s) Type.bls12_381_g1

let bls12_381_g2 s = literal (Literal.bls12_381_g2 s) Type.bls12_381_g2

let bls12_381_fr s = literal (Literal.bls12_381_fr s) Type.bls12_381_fr

let chain_id s = literal (Literal.chain_id s) Type.chain_id

let unString ~pp = function
  | {v = Literal (String s)} -> s
  | x ->
      raise (SmartExcept [`Value x; `Text "is not a string"; `Br; `Rec (pp ())])

let operation op = build (Operation op) Type.operation

let unSaplingTransaction ~pp = function
  | {v = Literal (Sapling_test_transaction {source; target; amount})} ->
      (source, target, amount)
  | x ->
      raise
        (SmartExcept
           [`Value x; `Text "is not a sapling transaction"; `Br; `Rec (pp ())])

let unSaplingState ~pp = function
  | {v = Literal (Sapling_test_state {memo; elements})} -> (memo, elements)
  | x ->
      raise
        (SmartExcept
           [`Value x; `Text "is not a sapling state"; `Br; `Rec (pp ())])

let list (l : t list) (t : Type.t) = build (List l) (Type.list t)

let ticket ticketer content amount =
  build (Ticket (ticketer, content, amount)) (Type.ticket content.vt)

let set ~telement l =
  let l = List.sort compare l in
  let rec aux acc = function
    | a :: (b :: _ as rest) ->
        if equal a b then aux acc rest else aux (a :: acc) rest
    | [a] -> List.rev (a :: acc)
    | [] -> List.rev acc
  in
  build (Set (aux [] l)) (Type.set ~telement)

let map ~big ~tkey ~tvalue l =
  let cmp (k1, v1) (k2, v2) =
    match compare k1 k2 with
    | 0 -> compare v1 v2
    | c -> c
  in
  build (Map (List.sort cmp l)) (Type.map ~big ~tkey ~tvalue)

let unit = literal Literal.unit Type.unit

let bool x = literal (Literal.bool x) Type.bool

let unBool ~pp = function
  | {v = Literal (Bool b)} -> b
  | x ->
      raise (SmartExcept [`Value x; `Text "is not a bool"; `Br; `Rec (pp ())])

let unList ~pp = function
  | {v = List l} -> l
  | x ->
      raise (SmartExcept [`Value x; `Text "is not a list"; `Br; `Rec (pp ())])

let unMap ~pp = function
  | {v = Map l} -> l
  | x -> raise (SmartExcept [`Value x; `Text "is not a map"; `Br; `Rec (pp ())])

let unSet ~pp = function
  | {v = Set l} -> l
  | x -> raise (SmartExcept [`Value x; `Text "is not a set"; `Br; `Rec (pp ())])

let unOption v =
  match v.v with
  | Variant ("Some", arg) -> Some arg
  | Variant ("None", _) -> None
  | _ -> assert false

let getItem ~pp items key default_value missing_message =
  match items.v with
  | Map map ->
    ( match (List.assoc_opt ~equal key map, default_value) with
    | Some v, _ -> v
    | None, Some v -> Lazy.force v
    | _ ->
      ( match missing_message with
      | None ->
          failwith
            (Printf.sprintf
               "Missing item in map: (%s) is not in (%s), while evaluating %s"
               (Printer.value_to_string key)
               (Printer.value_to_string items)
               (pp ()))
      | Some missing_message ->
          failwith (Printer.value_to_string (Lazy.force missing_message)) ) )
  | _ ->
      failwith
        (Printf.sprintf
           "Bad getItem %s[%s]"
           (Printer.value_to_string items)
           (Printer.value_to_string key))

let unInt ~pp = function
  | {v = Literal (Int {i})} -> i
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not an integer"; `Br; `Rec (pp ())])

let unMutez ~pp = function
  | {v = Literal (Mutez b)} -> b
  | x ->
      raise (SmartExcept [`Value x; `Text "is not a mutez"; `Br; `Rec (pp ())])

let unBls12_381 = function
  | {v = Literal (Bls12_381_g1 g1)} -> g1
  | {v = Literal (Bls12_381_g2 g2)} -> g2
  | {v = Literal (Bls12_381_fr fr)} -> fr
  | _ -> failwith "Not a BLS12 type"

let unTimestamp ~pp = function
  | {v = Literal (Timestamp t)} -> t
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not a timestamp"; `Br; `Rec (pp ())])

let unChain_id ~pp = function
  | {v = Literal (Chain_id b)} -> b
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not a chain_id"; `Br; `Rec (pp ())])

let unAddress ~pp = function
  | {v = Literal (Address (b, _))} -> b
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not an address"; `Br; `Rec (pp ())])

let unKey_hash ~pp = function
  | {v = Literal (Key_hash b)} -> b
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not an key_hash"; `Br; `Rec (pp ())])

let plus_inner ~primitives x y =
  let module P = (val primitives : Primitives.Primitives) in
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      intOrNat x.vt (Big_int.add_big_int i j) (* TODO type *)
  | Literal (Mutez x), Literal (Mutez y) -> mutez (Big_int.add_big_int x y)
  | Literal (String x), Literal (String y) -> string (x ^ y)
  | Literal (Bytes x), Literal (Bytes y) -> bytes (x ^ y)
  | Literal (Bls12_381_g1 x), Literal (Bls12_381_g1 y) ->
      bls12_381_g1
        (Misc.Hex.unhex
           (P.Bls12.addG1 (Misc.Hex.hexcape x) (Misc.Hex.hexcape y)))
  | Literal (Bls12_381_g2 x), Literal (Bls12_381_g2 y) ->
      bls12_381_g2
        (Misc.Hex.unhex
           (P.Bls12.addG2 (Misc.Hex.hexcape x) (Misc.Hex.hexcape y)))
  | Literal (Bls12_381_fr x), Literal (Bls12_381_fr y) ->
      bls12_381_fr
        (Misc.Hex.unhex
           (P.Bls12.addFr (Misc.Hex.hexcape x) (Misc.Hex.hexcape y)))
  | _ ->
      Printf.ksprintf
        failwith
        "Invalid + operation with different types %s %s "
        (Printer.value_to_string x)
        (Printer.value_to_string y)

let plus ~primitives x y = plus_inner ~primitives x y

let sub x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) -> int (Big_int.sub_big_int i j)
  | Literal (Mutez x), Literal (Mutez y) -> mutez (Big_int.sub_big_int x y)
  | _ -> failwith "Invalid - operation with different types"

let mul ~primitives x y =
  let module P = (val primitives : Primitives.Primitives) in
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      intOrNat x.vt (Big_int.mult_big_int i j) (* TODO type *)
  | Literal (Int {i}), Literal (Bls12_381_fr fr)
   |Literal (Bls12_381_fr fr), Literal (Int {i}) ->
      bls12_381_fr
        (Misc.Hex.unhex
           (P.Bls12.multiplyFrByInt
              (Misc.Hex.hexcape fr)
              (Big_int.string_of_big_int i)))
  | Literal (Bls12_381_g1 x), Literal (Bls12_381_fr fr) ->
      bls12_381_g1
        (Misc.Hex.unhex
           (P.Bls12.multiplyG1ByFr (Misc.Hex.hexcape x) (Misc.Hex.hexcape fr)))
  | Literal (Bls12_381_g2 x), Literal (Bls12_381_fr fr) ->
      bls12_381_g2
        (Misc.Hex.unhex
           (P.Bls12.multiplyG2ByFr (Misc.Hex.hexcape x) (Misc.Hex.hexcape fr)))
  | Literal (Bls12_381_fr x), Literal (Bls12_381_fr fr) ->
      bls12_381_fr
        (Misc.Hex.unhex
           (P.Bls12.multiplyFrByFr (Misc.Hex.hexcape x) (Misc.Hex.hexcape fr)))
  | _ -> failwith "Invalid * operation with different types"

let shift_left x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      if Bigint.compare j (Bigint.of_int 1000000) > 1
      then
        Printf.ksprintf
          failwith
          "shift_left with too big shift value %s << %s"
          (Printer.value_to_string x)
          (Printer.value_to_string y);

      nat (Big_int.shift_left_big_int i (Big_int.int_of_big_int j))
  | _ -> failwith "Invalid << operation with different types"

let shift_right x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      if Bigint.compare j (Bigint.of_int 1000000) > 1
      then
        Printf.ksprintf
          failwith
          "shift_right with too big shift value %s >> %s"
          (Printer.value_to_string x)
          (Printer.value_to_string y);

      nat (Big_int.shift_right_big_int i (Big_int.int_of_big_int j))
  | _ -> failwith "Invalid >> operation with different types"

let xor x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) -> nat (Big_int.xor_big_int i j)
  | Literal (Bool i), Literal (Bool j) -> bool (i <> j)
  | _ -> failwith "Invalid xor operation with different types"

let e_mod x y =
  match (x.v, y.v) with
  | Literal (Int {i}), Literal (Int {i = j}) ->
      intOrNat x.vt (Big_int.mod_big_int i j) (* TODO type *)
  | _ -> failwith "Invalid * operation with different types"

let div_inner x y =
  match (x.v, y.v) with
  | Literal (Int {i = x}), Literal (Int {i = y}) ->
      nat (Big_int.div_big_int x y) (* TODO type *)
  | _ -> failwith "Invalid / operation with different types"

let div x y = div_inner x y

let minus x y =
  match (openV x, openV y) with
  | Literal (Int {i = x}), Literal (Int {i = y}) ->
      int (Big_int.sub_big_int x y) (* TODO type *)
  | Literal (Mutez x), Literal (Mutez y) -> mutez (Big_int.sub_big_int x y)
  | Literal (Timestamp x), Literal (Timestamp y) ->
      int (Big_int.sub_big_int x y)
  | _ -> failwith "Invalid - operation"

let key_hash s = literal (Literal.key_hash s) Type.key_hash

let baker_hash h = literal (Literal.baker_hash h) Type.baker_hash

let key s = literal (Literal.key s) Type.key

let secret_key s = literal (Literal.secret_key s) Type.secret_key

let signature s = literal (Literal.signature s) Type.signature

let record ?layout = function
  | [] -> unit
  | l ->
      let layout =
        match layout with
        | None -> ref (Unknown.UnUnknown "")
        | Some layout -> layout
      in
      build
        (Record l)
        (Type.record_or_unit layout (List.map (fun (s, v) -> (s, v.vt)) l))

let tuple vs = build (Tuple vs) (Type.tuple (List.map (fun {vt} -> vt) vs))

let untuple ~pp = function
  | {v = Tuple vs} -> vs
  | x ->
      raise (SmartExcept [`Value x; `Text "is not a tuple"; `Br; `Rec (pp ())])

let un_record = function
  | {v = Record bs} -> bs
  | _ -> failwith "unRecord"

let variant name x t = build (Variant (name, x)) t

let none t = build (Variant ("None", unit)) (Type.option t)

let some x = build (Variant ("Some", x)) (Type.option x.vt)

let option t = Option.cata (none t) some

let ediv x y =
  let ediv x y ~a_t ~b_t ~a_f ~b_f =
    if Big_int.eq_big_int Big_int.zero_big_int y
    then none (Type.pair a_t b_t)
    else
      some
        (tuple [a_f (Big_int.div_big_int x y); b_f (Big_int.mod_big_int x y)])
  in
  match (x.v, y.v) with
  | Literal (Int {i = x_}), Literal (Int {i = y_}) ->
      let both_nat =
        match (Type.getRepr x.vt, Type.getRepr y.vt) with
        | TInt {isNat = isNat1}, TInt {isNat = isNat2} ->
          begin
            match
              (Unknown.getRefOption isNat1, Unknown.getRefOption isNat2)
            with
            | Some true, Some true -> true
            | (Some _ | None), (Some _ | None) -> false
          end
        | _, _ -> assert false
      in
      if both_nat
      then ediv x_ y_ ~a_t:(Type.nat ()) ~b_t:(Type.nat ()) ~a_f:nat ~b_f:nat
      else ediv x_ y_ ~a_t:(Type.int ()) ~b_t:(Type.nat ()) ~a_f:int ~b_f:nat
  | Literal (Mutez x_), Literal (Mutez y_) ->
      (* tez -> tez -> (nat * tez) *)
      ediv x_ y_ ~a_t:(Type.nat ()) ~b_t:Type.token ~a_f:nat ~b_f:mutez
  | Literal (Mutez x_), Literal (Int {i = y_}) ->
      (* tez -> nat -> (tez * tez) *)
      ediv x_ y_ ~a_t:Type.token ~b_t:Type.token ~a_f:mutez ~b_f:mutez
  | _ -> raise (SmartExcept [`Text "Invalid / operation with different types"])

let intXor a b =
  match (openV a, openV b) with
  | Literal (Int _a), Literal (Int _b) -> assert false (*int (a lxor b)*)
  | _ -> failwith "Invalid intXor operation"

let meta_address ?entry_point s =
  literal (Literal.meta_address ?entry_point s) Type.address

let address ?entry_point s =
  literal (Literal.address ?entry_point s) Type.address

let local_address ?entry_point s =
  literal (Literal.local_address ?entry_point s) Type.address

let meta_contract ?entry_point s t =
  literal (Literal.meta_contract ?entry_point s t) (Type.contract t)

let contract ?entry_point s t =
  literal (Literal.contract ?entry_point s t) (Type.contract t)

let local_contract ?entry_point s t =
  literal (Literal.local_contract ?entry_point s t) (Type.contract t)

let cons x l =
  match l with
  | {v = List l; vt} -> build (List (x :: l)) vt
  | _ -> failwith "Type error list"

(** Access the elements of a list. *)
let lens_list =
  Lens.make (fun x ->
      match (x.v, Type.unF x.vt) with
      | List focus, TList item -> {focus; zip = (fun focus -> list focus item)}
      | _ -> failwith "lens_list")

let lens_list_nth n = Lens.(lens_list @. Lens.nth n)

(** Access the entries of a map. *)
let lens_map =
  Lens.make (function
      | {v = Map focus; vt = F (TMap {big; tkey; tvalue})} ->
          {focus; zip = (fun focus -> map ~big ~tkey ~tvalue focus)}
      | _ -> failwith "lens_map")

let lens_map_at ~key = Lens.(lens_map @. Lens.assoc ~equal ~key)

(** Access the elements of a set. *)
let lens_set =
  Lens.make (function
      | {v = Set focus; vt = F (TSet {telement})} ->
          {focus; zip = (fun focus -> set focus ~telement)}
      | _ -> failwith "lens_set")

let lens_set_at ~elem = Lens.(lens_set @. sorted_list ~equal ~elem)

(** Access the entries of a record. *)
let lens_record =
  Lens.make (fun x ->
      match x.v with
      | Record focus -> {focus; zip = record}
      | _ -> failwith "lens_map")

let lens_record_at ~attr =
  Lens.(lens_record @. Lens.assoc ~equal:( = ) ~key:attr)

let checkType _t _v =
  (* TODO *)
  None

let rec zero_of_type t =
  match Type.getRepr t with
  | TUnit -> unit
  | TBool -> bool false
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Nat -> nat Big_int.zero_big_int
    | `Int -> int Big_int.zero_big_int
    | `Unknown -> int Big_int.zero_big_int )
  | TTimestamp -> timestamp Big_int.zero_big_int
  | TString -> string ""
  | TBytes -> bytes ""
  | TRecord {row} ->
      record ((List.map (fun (lbl, t) -> (lbl, zero_of_type t))) row)
  | TVariant {row = []} -> failwith "zero_of_type: empty variant type"
  | TVariant {row = (cons, t0) :: _} -> variant cons (zero_of_type t0) t
  | TSet {telement} -> set ~telement []
  | TMap {big; tkey; tvalue} -> map ~big ~tkey ~tvalue []
  | TAddress -> address ""
  | TKeyHash -> key_hash ""
  | TBakerHash -> key_hash ""
  | TKey -> key ""
  | TSignature -> signature ""
  | TToken -> mutez Big_int.zero_big_int
  | TUnknown _ -> failwith "zero_of_type: unknown"
  | TTuple ts -> tuple (List.map zero_of_type ts)
  | TList t -> list [] t
  | TChainId -> chain_id ""
  | TSecretKey -> secret_key ""
  | TContract _ | TLambda _ ->
      failwith
        (Printf.sprintf
           "zero_of_type not implemented on type [%s]"
           (Printer.type_to_string t))
  | TOperation -> failwith "zero_of_type: operation"
  | TSaplingState _ -> failwith "zero_of_type: sapling_state"
  | TSaplingTransaction _ -> failwith "zero_of_type: sapling_transaction"
  | TNever -> failwith "zero_of_type: never"
  | TTicket t -> ticket (Literal.Real "") (zero_of_type t) (Bigint.of_int 0)
  | TBls12_381_g1 -> bls12_381_g1 ""
  | TBls12_381_g2 -> bls12_381_g2 ""
  | TBls12_381_fr -> bls12_381_fr ""

let nextId prefix =
  let ids = ref 0 in
  fun () ->
    incr ids;
    Printf.sprintf "%s%i" prefix !ids

let closure_init (l : lambda) =
  build (Closure (l, [])) (Type.lambda l.tParams l.tResult)

let closure_apply v x =
  match v with
  | {v = Closure (l, args); vt = F (Type.TLambda (F (TTuple [_t1; t2]), t))} ->
      build (Closure (l, x :: args)) (Type.lambda t2 t)
  | _ -> failwith "closure_apply: not a closure"

let unclosure ~pp = function
  | {v = Closure (l, args)} -> (l, args)
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not a closure"; `Br; `Rec (pp ())])

let project_literals proj v =
  let rec aux acc path v =
    match v.v with
    | Literal l ->
      ( match proj l with
      | Some a -> (a, path) :: acc
      | None -> acc )
    | Record l -> List.fold_left (fun acc (f, v) -> aux acc (f :: path) v) acc l
    | Variant (f, v) -> aux acc (f :: path) v
    | List l | Set l -> List.fold_left (fun acc v -> aux acc path v) acc l
    | Map l ->
        List.fold_left
          (fun acc (f, v) ->
            aux
              (aux acc ("key" :: path) f)
              (Printer.value_to_string f :: path)
              v)
          acc
          l
    | Tuple [f; s] ->
        let acc = aux acc ("fst" :: path) f in
        aux acc ("snd" :: path) s
    | Tuple _ -> failwith "project_literals: TODO n-tuples"
    | Closure _ -> acc
    | Operation _ -> acc
    | Ticket (_, v, _) -> aux acc path v
  in
  List.rev (aux [] [] v)

let rec get_field_opt field v =
  match v.v with
  | Record l -> List.assoc_opt field l
  | Tuple vs ->
      let rec find = function
        | [] -> None
        | x :: xs ->
          ( match get_field_opt field x with
          | None -> find xs
          | Some x -> Some x )
      in
      find vs
  | _ -> None

let unoperation ~pp = function
  | {v = Operation op} -> op
  | x ->
      raise
        (SmartExcept [`Value x; `Text "is not an operation"; `Br; `Rec (pp ())])
