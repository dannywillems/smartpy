(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control

type 't f = private
  | TUnit
  | TBool
  | TInt                of {isNat : bool Unknown.t ref [@equal ( == )]}
  | TTimestamp
  | TString
  | TBytes
  | TRecord             of
      { layout : Layout.t Unknown.t ref [@equal ( == )]
      ; row : (string * 't) list }
  | TVariant            of
      { layout : Layout.t Unknown.t ref [@equal ( == )]
      ; row : (string * 't) list }
  | TSet                of {telement : 't}
  | TMap                of
      { big : bool Unknown.t ref [@equal ( == )]
      ; tkey : 't
      ; tvalue : 't }
  | TAddress
  | TContract           of 't
  | TKeyHash
  | TBakerHash
  | TKey
  | TSignature
  | TToken
  | TUnknown            of 't unknownType_f ref [@equal ( == )]
  | TTuple              of 't list
  | TList               of 't
  | TLambda             of 't * 't
  | TChainId
  | TSecretKey
  | TOperation
  | TSaplingState       of {memo : int Unknown.t_ref}
  | TSaplingTransaction of {memo : int Unknown.t_ref}
  | TNever
  | TTicket             of 't
  | TBls12_381_g1
  | TBls12_381_g2
  | TBls12_381_fr

and 't unknownType_f =
  | UUnknown of string
  | URecord  of (string * 't) list
  | UTuple   of (int * 't) list
  | UVariant of (string * 't) list
  | UExact   of 't
[@@deriving eq, show, ord, map, fold]

include UNTIED with type 'a f := 'a f

include EQ with type t := t

include ORD with type t := t

include SHOW with type t := t

type unknownType = t unknownType_f

type tvariable = string * t [@@deriving eq, show]

val default_layout : string list -> Layout.t

val default_layout_of_row : (string * t) list -> Layout.t

val comb_layout : [ `Left | `Right ] -> string list -> Layout.t

val comb_layout_of_row : [ `Left | `Right ] -> (string * t) list -> Layout.t

val full_unknown : unit -> t

val getRepr : t -> t f

val unit : t

val address : t

val contract : t -> t

val bool : t

val bytes : t

val key_hash : t

val baker_hash : t

val baker_type_of_protocol : Config.protocol -> t

val int_raw : isNat:bool Unknown.t ref -> t

val int : unit -> t

val nat : unit -> t

val intOrNat : unit -> t

val key : t

val key_value : t -> t -> t

val head_tail : t -> t -> t

val chain_id : t

val secret_key : t

val operation : t

val sapling_state : int option -> t

val sapling_transaction : int option -> t

val never : t

val map : big:bool Unknown.t ref -> tkey:t -> tvalue:t -> t

val set : telement:t -> t

val record : Layout.t Unknown.t ref -> (string * t) list -> t

val variant : Layout.t Unknown.t ref -> (string * t) list -> t

val record_default_layout : (string * t) list -> t

val variant_default_layout : (string * t) list -> t

val record_or_unit : Layout.t Unknown.t ref -> (string * t) list -> t

val signature : t

val option : t -> t

val tor : t -> t -> t

val string : t

val timestamp : t

val token : t

val uvariant : string -> t -> t

val urecord : (string * t) list -> t

val utuple : int -> t -> t

val unknown_raw : unknownType ref -> t

val account : t

val pair : t -> t -> t

val tuple : t list -> t

val list : t -> t

val ticket : t -> t

val lambda : t -> t -> t

val has_unknowns : t -> bool

val is_bigmap : t -> bool

val bls12_381_g1 : t

val bls12_381_g2 : t

val bls12_381_fr : t

val is_hot : t -> Ternary.t
