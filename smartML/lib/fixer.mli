(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

val fix_value_tcontract :
     Config.t
  -> Mangler.mangle_env
  -> Typing.env
  -> value_tcontract
  -> value_tcontract

val fix_contract :
  Config.t -> Mangler.mangle_env -> Typing.env -> contract -> tcontract
