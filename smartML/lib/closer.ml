(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Typed
open Utils
open Control

let has_unknowns_talg =
  {(monoid_talg ( || ) false) with f_ttype = Type.has_unknowns}

let has_unknowns_expr = cata_texpr has_unknowns_talg

let has_unknowns_command = cata_tcommand has_unknowns_talg

let find_unknown_parts ~tstorage ~tparameter ~global_variables ~entry_points =
  let xs =
    [ ("storage", Type.has_unknowns tstorage)
    ; ("tparameter", Type.has_unknowns tparameter) ]
    @ List.concat
        (List.map
           (fun {channel; paramsType; body} ->
             [ (channel ^ "(type)", Type.has_unknowns paramsType)
             ; (channel ^ "(command)", has_unknowns_command body) ])
           entry_points)
    @ List.map (fun (name, e) -> (name, has_unknowns_expr e)) global_variables
  in
  let xs = List.filter_map (fun (s, b) -> if b then Some s else None) xs in
  if xs = [] then None else Some (String.concat ", " xs)

let close_layout row layout =
  match Unknown.getRefOption layout with
  | Some _ -> layout
  | None ->
      setEqualUnknownOption
        ~pp:(fun () -> assert false)
        layout
        (ref (Unknown.UnValue (Type.default_layout_of_row row)));
      layout

let rec close_type t =
  let open Type in
  match unF t with
  | TRecord {layout; row} ->
      record_or_unit
        (close_layout row layout)
        (List.map (fun (name, t) -> (name, close_type t)) row)
  | TVariant {row = [("None", F TUnit)]} -> Type.option (Type.full_unknown ())
  | TVariant {row = [("Some", t)]} -> Type.option (close_type t)
  | TVariant {layout; row} ->
      variant
        (close_layout row layout)
        (List.map (fun (name, t) -> (name, close_type t)) row)
  | TSet {telement} -> set ~telement:(close_type telement)
  | TMap {big; tkey; tvalue} ->
      let pp () = [`Text "Type sp.TMap / sp.TBigMap mismatch"; `Br; `Type t] in
      ( match Unknown.getRefOption big with
      | None -> setEqualUnknownOption ~pp big (ref (Unknown.UnValue false))
      | _ -> () );
      map ~big ~tkey:(close_type tkey) ~tvalue:(close_type tvalue)
  | TContract t -> contract (close_type t)
  | TUnknown ({contents = UExact t} as r) ->
      let t = close_type t in
      r := UExact t;
      t
  | TUnknown {contents = UUnknown _i} -> t
  | TUnknown ({contents = URecord l} as r) ->
      let t =
        record_default_layout
          (List.map (fun (name, t) -> (name, close_type t)) l)
      in
      r := UExact t;
      t
  | TUnknown ({contents = UTuple l} as r) ->
      let ixs = List.map fst l in
      assert (ixs = List.sort Stdlib.compare ixs);
      let ixs' = List.mapi (fun i _ -> i) l in
      if ixs = ixs'
      then (
        let t = Type.tuple (List.map snd l) in
        r := UExact t;
        t )
      else raise (SmartExcept [`Text "Tuple has gaps"; `Type t])
  | TUnknown ({contents = UVariant l} as r) ->
      let t =
        variant_default_layout
          (List.map (fun (name, t) -> (name, close_type t)) l)
      in
      r := UExact t;
      t
  | TTuple ts -> tuple (List.map close_type ts)
  | TList t -> list (close_type t)
  | TLambda (t1, t2) -> lambda (close_type t1) (close_type t2)
  | TUnit | TBool | TInt _ | TString | TBytes | TTimestamp | TOperation
   |TToken | TAddress | TKeyHash | TBakerHash | TKey | TSecretKey | TChainId
   |TSignature | TSaplingState _ | TSaplingTransaction _ | TNever
   |TBls12_381_g1 | TBls12_381_g2 | TBls12_381_fr ->
      t
  | TTicket t -> ticket (close_type t)

let close_talg =
  let f_texpr line_no et e = {e; et = close_type et; line_no} in
  let f_tcommand line_no ct c = {c; ct = close_type ct; line_no} in
  {f_texpr; f_tcommand; f_ttype = close_type}

let close_expr = cata_texpr close_talg

let close_command = cata_tcommand close_talg

let close_entry_point (ep : _ entry_point) =
  {ep with paramsType = close_type ep.paramsType; body = close_command ep.body}

let close_poly_contract c =
  let tstorage = close_type c.tstorage in
  let tparameter = close_type c.tparameter in
  let entry_points = List.map close_entry_point c.entry_points in
  let global_variables = List.map (map_snd close_expr) c.global_variables in
  let unknown_parts =
    find_unknown_parts ~tstorage ~tparameter ~global_variables ~entry_points
  in
  {c with tstorage; tparameter; entry_points; unknown_parts; global_variables}

let close_contract {tcontract} = {tcontract = close_poly_contract tcontract}

let close_value_contract {value_tcontract} =
  {value_tcontract = close_poly_contract value_tcontract}

let close_scenario s =
  {s with actions = List.map (map_action_contract close_contract) s.actions}
