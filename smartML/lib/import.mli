(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics
open Untyped

val import_type : Typing.env -> Sexplib0.Sexp.t -> Type.t

val import_contract_id : Sexplib0.Sexp.t -> Literal.contract_id

val import_literal : Typing.env -> Sexplib0.Sexp.t list -> Literal.t

val import_expr :
     Typing.env
  -> (module Primitives.Primitives)
  -> scenario_state
  -> Sexplib0.Sexp.t
  -> expr

val import_contract :
     primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> Typing.env
  -> Sexplib0.Sexp.t
  -> contract
(** Parse an s-expression into a contract definition. *)
