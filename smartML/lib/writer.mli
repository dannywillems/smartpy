(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

val write_pretty : string -> value_tcontract -> string * int

val write_pretty_html :
  install:string -> string -> value_tcontract -> string * int

val write_contract_michelson : string -> Michelson.tcontract -> string * int

val write_micheline : string -> Micheline.t -> string * int

val write_contract_py :
  string -> Basics.value_tcontract * string -> string * int

val write_contract_michel :
  string -> Michel.Expr.expr Michel.Expr.precontract -> string * int

val write_tvalue : string -> Value.t -> string * int

val write_mliteral : string -> Michelson.literal -> string * int

val pp_contract_types : value_tcontract -> string

val write_contract_types : string -> value_tcontract -> string * int

val write_metadata : string -> Utils.Misc.json -> string * int

val write_html : string -> string -> string * int

val write_csv : string -> string list list -> string * int

val wrap_html_document : install:string -> string -> string
