#!/usr/bin/env nix-shell
#!nix-shell -i bash --pure --keep NIX_PATH --keep NIX_SSL_CERT_FILE --show-trace shell.nix
set -euo pipefail

ENV=nix
COMPILER=ocaml-base-compiler.4.09.1
SWITCH=env/current/switches/$COMPILER

rm -rf env/nix/bin
mkdir -p env/nix/bin
cd env/nix/bin
case $(uname) in
    Darwin)
        ln -sf /usr/bin/sandbox-exec
        ln -sf /usr/sbin/netstat
        ln -sf /usr/bin/open
        ln -sf /usr/bin/xcodebuild
        ;;
    Linux)
        ln -sf /usr/bin/setsid
        ln -sf /usr/bin/netstat
        ln -sf /usr/bin/xdg-open
        ;;
    *)
        echo Unknown OS.
        ;;
esac
cd -

export PATH=$PWD/env/nix/bin:$PATH

echo Pointing to $ENV...
rm -f env/current
ln -s $ENV/ env/current

echo Ensure opam is initialized...
[ -a ~/.opam ] || OPAMNO=1 opam init --bare

echo Ensure opam switch is initialized...
[ -a $SWITCH/_opam ] || opam switch create $SWITCH $COMPILER
rm -rf _opam
ln -s $SWITCH/_opam

echo Ensure opam switch has necessary packages...
opam switch import --yes env/switch.export
opam uninstall --yes --auto-remove
opam switch export env/switch.export
git --no-pager diff env/switch.export

echo Ensure node modules are installed...
cp -f env/current/{package,package-lock}.json .
npm install

echo Done!
