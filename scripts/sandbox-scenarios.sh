#!/usr/bin/env bash

set -e -o pipefail

export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER="Y"

all_commands=""
usage () {
    cat >&2 <<EOF
# Usage: [method=<m>] $0 <cmd>

Where 'method' is naked, local, or docker.
The following commands are available:
EOF
    for c in $all_commands ; do
        echo ""
        ( $c --help ) || :
    done
    cat >&2 <<EOF

# Examples:

Run everything with binaries from ext/tezos/tezos-master/:

     method=local bash smartML/test/sandbox-scenarios.sh buildlocal
     method=local bash smartML/test/sandbox-scenarios.sh fullrun

(this is what 'make test-sandbox-scenarios' does).

Use a docker sandbox to run 3 scenarios:

     # Start it:
     method=docker bash smartML/test/sandbox-scenarios.sh silentbox
     # Check on it:
     method=docker bash smartML/test/sandbox-scenarios.sh status
     method=docker bash smartML/test/sandbox-scenarios.sh runacouple \
        test_baselines/scenario/minikitties/minikitties.py.sc \
        test_baselines/scenario/testCheckSignature/testCheckSignature.py.sc  \
        test_baselines/scenario/multisig/multisig.py.sc
     # ... when done:
     method=docker bash smartML/test/sandbox-scenarios.sh killbox

Start a local interactive sandbox (takes over terminal until you quit it):

     method=local rlwrap bash smartML/test/sandbox-scenarios.sh startbox

('rlwrap' is optional but highly recommended).
EOF
}

say () {
    echo "sase: $*" >&2
}
fail () {
    say "ERROR: $*"
    exit 2
}

minus_help () {
    if [ "$1" = "--help" ] || [ "$1" = "-h" ] ; then
        cat >&2
        exit 0
    else
        cat >/dev/null
    fi
}

smartml='_build/default/smartML/app/smartml.exe'
flextesa_image=registry.gitlab.com/tezos/flextesa:cf6c1290-run

sandbox_container_name=smpy-test-sandbox

flextesa_entrypoint () {
    echo "docker run --rm --name $sandbox_container_name --detach -v $PWD:/work -w /work --network host --entrypoint $1 $flextesa_image"
}

setup_sandbox () {
    export how="$1"
    case "$how" in
        "naked" )
            export flextesa=flextesa
            export smartml_client=tezos-client
            ;;
        "docker" )
            export flextesa="$(flextesa_entrypoint flextesa)"
            export smartml_client="docker exec $sandbox_container_name tezos-client"
            ;;
        "local" )
            export PATH=$tezos_local_path/:$PATH
            export flextesa=tezos-sandbox
            export smartml_client=$tezos_local_path/tezos-client
            ;;
        * )
            say "Don't know how to start sandbox: '$how'" ;;
    esac
    say "Setup-sandbox '$how'
   | flextesa = $flextesa
   | smartml_client = $smartml_client
"
}

setup_sandbox "${method:-local}"

pid_file=_build/test-sandbox/sbox.pid
log_file=_build/test-sandbox/sbox.log
mkdir -p _build/test-sandbox/
all_commands="$all_commands startbox"
startbox () {
    minus_help "$1" <<EOF
## Command startbox:
Start an interactive manual-sandbox on port 20000.
* This uses the 'method' environment-variable to choose the tezos-binaries.
* It starts an interactive sandbox, just type 'help' at the prompt.
* For now it doesn't work with method=docker use silentbox.
EOF
    $flextesa mini \
              --size 1 --base-port 20000 --number-of-boot 2 \
              --protocol-hash PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb \
              --protocol-kind Carthage \
              --no-baking
}
all_commands="$all_commands silentbox"
silentbox () {
    minus_help "$1" <<EOF
## Command silentbox:
Start a manual-sandbox but in the background (also according to 'method').
* log-file: $log_file
* PID-file: $pid_file (usefull only for non-docker)
See also 'killbox' and 'status'.
EOF
    $flextesa mini \
              --size 1 --base-port 20000 --number-of-boot 2 \
              --protocol-hash PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb \
              --protocol-kind Carthage \
              --no-baking --until-level 2_000_000 2> $log_file &
    echo $! > $pid_file
    if [ "$how" = "docker" ] ; then
        sleep 2
    fi
}
all_commands="$all_commands killbox"
killbox () {
    minus_help "$1" <<EOF
## Command killbox:
kill the sandbox according to 'method' (docker kill or using $pid_file)
EOF
    case "$how" in
        "docker" )
            docker kill $sandbox_container_name ;;
        "local" | "naked" )
            ps $(cat $pid_file)
            kill $(cat $pid_file)
            ;;
        * ) say "Don't know how to kill '$how'-ly" ;;
    esac
}
all_commands="$all_commands status"
status () {
    minus_help "$1" <<EOF
## Command status:
Display the current known status of the sandbox setup according to 'method'.
EOF
    say "Sandbox status:"
    {
        case "$how" in
            "docker" )
                docker ps | grep "$sandbox_container_name" \
                    || say "Does not appear to be there..."
                ;;
            "local" | "naked" )
                ps $(cat $pid_file)
                ;;
            * ) say "Don't know how to the status '$how'-ly" ;;
        esac
        $smartml_client -P 20000 bootstrapped
        # In very slow environments, like the CI, it takes a while to
        # reach level 1, so we test for it here:
        $smartml_client -P 20000 rpc get /chains/main/blocks/1/protocols
    } 2>&1 | sed 's/^/  | /'
}
all_commands="$all_commands waitbox"
waitbox () {
    minus_help "$1" <<EOF
## Command waitbox:
Try a few times to get a successful 'status' (= active wait after 'silentbox').
EOF
    count=${1:-10}
    for i in $(seq 1 $count) ; do
        say "waitbox -> ${i}th attempt"
        status && break || say "not yet…"
        if [ "$i" = "$count" ] ; then
            say "Tried too many times"
            exit 3
        fi
        sleep 3
    done
}


run_scenarios () {
    scenarios="$*"
    options=$(for s in $scenarios ; do echo "--sce $s" ; done)
    $smartml run --bake --conn :20000 --root _build/test-scenarios/ \
             --client-command "$smartml_client" \
             --funder unencrypted:edsk3RFgDiCt7tWB2oe96w1eRw72iYiiqZPLu9nnEY23MYRp2d8Kkx \
             $options
    for s in $scenarios ; do
        name=$(basename "$s" | sed 's/.sc$//')
        cat _build/test-scenarios/$name*/result.tsv \
          > test_baselines/sandbox-scenarios/$name.tsv
    done
}

all_scenarios () {
    which=$1
    find test_baselines/scenario_{native,js}/ -iname "$which*.sc" | grep -v __init__ | sort -u
}

all_commands="$all_commands runall"
runall () {
    minus_help "$1" <<EOF
## Command runall:
Run "all" known scenarios.
* Also obeys 'method' to find 'tezos-client'.
* The sandbox has to be already ready (port 20_000).
EOF
    run_scenarios "$(all_scenarios)"
}
all_commands="$all_commands runacouple"
runacouple () {
    minus_help "$1" <<EOF
## Command runacouple:
Like 'runall' but only try a few scenarios, passed as argument.
EOF
    run_scenarios "$*"
}

# What the CI should run.
all_commands="$all_commands fullrun"
fullrun () {
    minus_help "$1" <<EOF
## Command fullrun [<re>]:
Start a sandbox, wait for it, run <n> scenarios, kill the sandbox.
* if '<re> is empty, they run all the known scenarios.
* if not it is a 'find' regular expression prefixed to '*.py.sc'.
EOF
    initials="$1"
    silentbox
    waitbox 15
    run_scenarios "$(all_scenarios $initials)"
    status
    killbox
}

run_scenario_dir () {
    minus_help "$1" <<EOF
## Command run_dir [<re>]:
Start a sandbox, wait for it, run <n> scenarios, kill the sandbox.
* if '<re> is empty, they run all the known scenarios.
* if not it is a 'find' regular expression prefixed to '*.py.sc'.
EOF
    silentbox
    waitbox 15

    $smartml run --bake --conn :20000 --root $2/sandbox/ \
             --client-command "$smartml_client" \
             --funder unencrypted:edsk3RFgDiCt7tWB2oe96w1eRw72iYiiqZPLu9nnEY23MYRp2d8Kkx \
             --sce $1

    status
    killbox
}

ci_check () {
    set -e
    if [ "$(grep 'Ran 0 tests' _build/test-scenarios/results.txt | wc -l)" = "0" ]
    then
        echo "Ok more than one test"
    else
        echo "Problem with finding tests to run"
        exit 2
    fi
    git add test_baselines/sandbox-scenarios/*.tsv
    git diff HEAD --exit-code
}



if [ "$1" = "" ] ; then
    usage
else
    "$@"
fi
