# image:/static/img/logo-only.svg[Logo,50] Releases of SmartPy and SmartPy.io
:nofooter:
:source-highlighter: coderay

:linkattrs:


https://SmartPy.io[SmartPy] is a Python library for constructing Tezos smart contracts.

We maintain several releases of both SmartPy.io and of the corresponding command line interface.

## SmartPy.io

### The Current Release

The current release is: https://SmartPy.io.

Alternative server: https://alt.SmartPy.io.

This is the regular development version offering a good compromise between stability and new features.

The so-called demo version is deprecated.

### Some History

Many changes are long forgotten. Here is some history.

2021-03-04::
* New release is https://smartpy.io/releases/20210304-8004e57f2b88b4212c65609abc1f9acff0f791d5. +
  ** Add (Florence, FlorenceNoBA) networks;
  ** Add protocol flag `florence` to enable Florence features;
  ** Add `baker_hash` Michelson type;
  ** Support `baker_hash` type in *SET_DELEGATE*, *VOTING_POWER*, *CREATE_CONTRACT* instructions;

2021-02::
* New release is
  ** Some small breaking changes in tickets operations. +
  ** Some changes in the flag system. +
  ** Complete removal of the `run-obsolete` and `compile-obsolete`
  commands in the CLI. +
  ** Better interaction with dynamically created contracts in test scenarios.

2021-02-18::
* New release is https://SmartPy.io/releases/20210218-663becf127363ec728080bcf3d29108f97572d49.
  ** Add Falphanet RPC.

2021-02-17::
* New release is https://SmartPy.io/releases/20210217-215a92bfa20c23e833c46dd6f75f8af5ea65e57b. +
  ** Add support for bls12-381 in the test interpreter. +
  ** Fix metadata generation. +
  ** Add support for offchain-view testing.

2021-02-12::
* New major release is https://SmartPy.io/releases/20210212-2affd5164a82b2238c9ee9593cf2a03a58bec979. +
  ** See announcement here: https://medium.com/@smartpy-io/21e2adb72ec3. +
  ** One breaking change in the CLI.

2021-01-21::
* New release is https://SmartPy.io/releases/20210121-73c41a4202aa44f8bd58098b31bceecc7edcf69b.

2021-01-18::
* New release is https://SmartPy.io/releases/20210118-6f466eee56038df8fda8b2f77819b83398346296.

2021-01-05::
* New release is https://SmartPy.io/releases/20210105-2d5cc6b5a46f5de84b3d303e67028d47587dbc68.

2020-12-24::
* New release is https://SmartPy.io/releases/20201224-436b0a5888070d6cf184489f3d7f589e4fd71e3c.

2020-12-15::
* New release is https://SmartPy.io/releases/20201215-e8463215e853d0a2aee6337c48a3f6b6a469c3ab.

2020-12-13::
* New release is https://SmartPy.io/releases/20201213-a502312f7c2cdd9deca8104ef79ef9d8a9787ff9.

2020-12-03::
* New release is https://SmartPy.io/releases/20201203-960e495db3c8cb1cfdbfd4b49a1790ba2654842e.

2020-11-30::
* New release is https://SmartPy.io/releases/20201130-57260ba1fab1eac0833935f2383beef65a909af5.

2020-11-28::
* New release is https://SmartPy.io/releases/20201128-b860cdfdedfe18b3c3538d581fb7d175284a8136.

2020-11-26::
* New release is https://SmartPy.io/releases/20201126-184c26c49ed49bf4167d8c9dbbc3b56374e51aa0.

2020-11-14::
* New release is https://SmartPy.io/releases/20201114-4b1a780963c9ffb723f285ac50f1b105b88beb46.
* New release is https://SmartPy.io/releases/20201114-5f74388ccb0f259a73262142c2f61c11d4071d2d.

2020-11-09::
* New release is https://SmartPy.io/releases/20201109-0e81e41f75db0f5676bb08f6f868477dc53755a8.

2020-11-08::
* Change of release version and mechanism. We now use our new react version.
* New release is tested on https://alt.smartpy.io/releases/20201108-33c4c60012ecf026ee0c0e2384a2c168c2a7cba8.

2020-10-28::
* New dev is https://SmartPy.io/releases/dev-20201028-3d1cc718c466afcc042b75988916c58f7cbee2d4.

2020-10-25::
* New dev is https://SmartPy.io/releases/dev-20201025-47d8831b87b9b38626939be9e23cbff966f4499a.

2020-10-24::
* New dev is https://SmartPy.io/releases/dev-20201024-715699df04b2c8eefba4be4f87d0dc148968cc8e.
* New dev is https://SmartPy.io/releases/dev-20201024-36f157966a1934fb8b24f65b2ac3cf2c2b7491d4.
  (new CLI installer)

2020-10-23::
* New dev is https://SmartPy.io/releases/dev-20201023-06858903030ad0ab23a1585820fc3fca918b1b3a.

2020-10-17::
* New dev is https://SmartPy.io/releases/dev-20201017-b53f502aaa227c9883433cc97e9ff6b783e847e7.

2020-10-14::
* New dev is https://SmartPy.io/releases/dev-20201014-b57366531b7ad14f19a863653d2f47a4a96a29d6.

2020-10-13::
* New dev is https://SmartPy.io/releases/dev-20201013-3f73b27286d189f3ef917c86aa556264b0d5dd8a.

2020-10-12::
* New dev is https://SmartPy.io/releases/dev-20201012-23263858a6143423a5dc45bab717afb4aec996a1.

2020-10-10::
* New dev is https://SmartPy.io/releases/dev-20201010-2d77c07f72777d44dcff933f4a77929904dadb7b.

2020-10-06::
* New dev is https://smartpy.io/releases/dev-20201006-d34afb8381bac41fd57f65f6f19ed50e42dcac4a.

2020-10-03::
* New dev is https://SmartPy.io/releases/dev-20201003-9cf98ec2da902382be63cec34b83904e8a378ad5.

2020-09-26::
* New dev is https://SmartPy.io/releases/dev-20200926-9b06248e88a7cf34e8d8109287f3210ec65b785f.

2020-09-24::
* New dev is https://SmartPy.io/releases/dev-20200924-23b26494361d96abf034bdbb1ad1af396f95fd61.

2020-09-22::
* New dev is https://SmartPy.io/releases/dev-20200922-b42e1a73658248e94bb1b2c86a7a1ea1898e521e.

2020-09-20::
* New dev is https://smartpy.io/releases/dev-20200920-c2657aec577b0b9c3cd42948a6d401f371a172f3.

2020-09-12::
* New dev is https://SmartPy.io/releases/dev-20200912-bbb4b34cb579f3d52320c3d2aed8ebcef04429b0.

2020-09-07::
* New dev is https://smartpy.io/releases/dev-20200907-b2b492cd2b9490e67fe0c21cf8e33a1d37e4b013.

2020-09-05::
* New dev is https://smartpy.io/releases/dev-20200905-df19dc528020cf84872871fdd38195da6602afb0.

2020-08-30::
* New dev is https://SmartPy.io/releases/dev-20200830-61244befe2c4c321c0ae6f807873e3a77811f20a.
* New react version of https://SmartPy.io
* New dev is https://SmartPy.io/releases/dev-20200830-c5ff990e1a430240fad527324bb33eeae811f71a.

2020-08-27::
* New dev is https://SmartPy.io/releases/dev-20200827-c11a23b00e589f9b3769a22933e0247f2b7bb5d7.

2020-08-24::
* New dev is https://SmartPy.io/releases/dev-20200824-49d2e75aa10f51a9194ba744ee19d27731d85e2d.

2020-08-22::
* New dev is https://SmartPy.io/releases/dev-20200822-dd0d095addb54a5227f7fb4f55150de4b065798e.
* New open source version https://gitlab.com/SmartPy/smartpy.

2020-07-22::
* New dev is https://SmartPy.io/releases/dev-20200722-cdf84730ca17634b3212be837f9604aea6abb4a3.
* Demo replaced by former dev version.
* Former demo version is: https://SmartPy.io/demo.20200317.

2020-04-10::
* Demo updated with former Development version.
* https://smartpy-io.medium.com/new-smartpy-io-version-with-support-for-the-ledger-hardware-wallet-and-a-few-other-improvements-5e6f296928d2[New Development Version] -- Ledger Hardware Wallet support, storage less contracts and inter-contract test scenarios.

2020-03-05::
* https://smartpy-io.medium.com/one-year-anniversary-release-b4e52813552c[New Development Version] -- One Year Anniversary Release with custom layouts, private entry points, lambdas, etc.

### Former and Forgotten Versions

These are unsupported former versions that may be of interest for archeological perspectives.

https://SmartPy.io/prev.20190902 +
https://SmartPy.io/prev.dev.20191115 +
https://SmartPy.io/prev.demo.20191121 +
https://SmartPy.io/prev.dev.20200113 +
https://SmartPy.io/prev.demo.20200113 +
https://SmartPy.io/prev.dev.20200305

## SmartPy Command Line Interface

Please see https://SmartPy.io/reference.html#_command_line_interface for install instructions.

### The Current Stable Release

https://SmartPy.io/cli

### Former and Forgotten Versions

These are unsupported former versions that may be of interest for archeological perspectives.

https://SmartPy.io/SmartPyBasicPrev20200113 +
https://SmartPy.io/SmartPyBasicPrevDev20191115 +
https://SmartPy.io/SmartPyBasicPrevDev20200113 +
https://SmartPy.io/SmartPyBasicPrevDev20200214 +
https://SmartPy.io/SmartPyBasicPrevDev20200305 +

## SmartPy Open Source Release

An open source release is accessible here: https://gitlab.com/SmartPy/smartpy.
